package com.mfs.api_middleware.gateway.model;

import lombok.Data;

@Data
public class BillPreviewRestInput {
    private String billerId;
    private String accountNumber;
    private String billNumber;
    private String billAccountNumber;
    private String code;
    private String key1;
    private String key2;
    private String key3;
    private String key4;
    private String key5;
    private String key6;
    private String key7;
}