package com.mfs.api_middleware.gateway.topup;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.mfs.api_middleware.Exception.ApiManagerRequestException;
import com.mfs.api_middleware.Exception.ApiRequestException;
import com.mfs.api_middleware.dto.CommonResponse;
import com.mfs.api_middleware.dto.TopUpBundleRequest;
import com.mfs.api_middleware.dto.TopUpRequest;
import com.mfs.api_middleware.util.CommonConstant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;

import java.time.Duration;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeoutException;

@Slf4j
@Service
public class TopUpGateway {

    @Value("${topup_generic_service.url}")
    private String topUpGenericServiceUrl;

    @Value("${api.auth_header}")
    private String authHeader;

    private final WebClient webClient;

    public TopUpGateway(WebClient.Builder webClientBuilder) {
        this.webClient = webClientBuilder.build();
    }

    public CommonResponse topUpGenericService(TopUpRequest topUpRequest, String accountNo, Locale locale){
        topUpRequest.setAccountNumber(accountNo);
        topUpRequest.setRequestTime(locale);

        String txt = webClient.post()
                .uri(topUpGenericServiceUrl+"/payment")
                .header(CommonConstant.TBL_API_AUTH_HEADER, authHeader)
                .body(BodyInserters.fromValue(topUpRequest))
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, clientResponse -> {
                    log.error("error response: {}", clientResponse);
                    throw new ApiManagerRequestException(CommonConstant.HTTP_4XX_ERROR);
                })
                .onStatus(HttpStatus::is5xxServerError, clientResponse -> {
                    log.error("error response: {}", clientResponse);
                    throw new ApiManagerRequestException(CommonConstant.HTTP_5XX_ERROR);
                })
                .bodyToMono(String.class)
                .timeout(Duration.ofSeconds(120))
                .onTerminateDetach()
                .onErrorMap(throwable -> {
                    if(throwable instanceof ApiManagerRequestException) {
                        throw new ApiRequestException(throwable.getMessage());
                    } else if (throwable instanceof TimeoutException) {
                        throw new ApiRequestException(CommonConstant.TIMEOUT_ERROR);
                    } else {
                        throw new ApiRequestException(CommonConstant.COMMON_ERROR);
                    }
                })
                .block();

        log.debug("vending payment response: {}", txt);
        CommonResponse response = new CommonResponse();
        try {
            BillPaymentTopUpResponse billPaymentTopUpResponse = new Gson().fromJson(txt, BillPaymentTopUpResponse.class);
            if(billPaymentTopUpResponse.getStatusCode().equalsIgnoreCase("200")) {
                response.setMessage(billPaymentTopUpResponse.getMessage());
            } else {
                //throw new ApiRequestException(CommonConstant.HTTP_4XX_ERROR);
                throw new ApiRequestException(billPaymentTopUpResponse.getMessage());
            }
        }catch (JsonParseException e){
            log.debug("JSON parse error: {}", e.getMessage());
            throw new ApiRequestException(CommonConstant.HTTP_5XX_ERROR);
        }
        log.info("Recharge core response: {}",response.getMessage());
        return response;
    }

    public List<BundlePlanMobileDTO> bundleSearch(TopUpBundleRequest input){
        log.info("####### bundleSearch: "+input.toString());
        String txt = webClient.post()
                .uri(topUpGenericServiceUrl+"/search")
                .header(CommonConstant.TBL_API_AUTH_HEADER, authHeader)
                .body(BodyInserters.fromValue(input))
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, clientResponse -> {
                    log.error("error response: {}", clientResponse);
                    throw new ApiManagerRequestException(CommonConstant.HTTP_4XX_ERROR);
                })
                .onStatus(HttpStatus::is5xxServerError, clientResponse -> {
                    log.error("error response: {}", clientResponse);
                    throw new ApiManagerRequestException(CommonConstant.HTTP_5XX_ERROR);
                })
                .bodyToMono(String.class)
                .timeout(Duration.ofSeconds(30))
                .onTerminateDetach()
                .block();

        ObjectMapper mapper = new ObjectMapper();
        List<BundlePlanMobileDTO> response;
        try {
            response = mapper.readValue(txt, new TypeReference<List<BundlePlanMobileDTO>>() {});
        } catch (JsonProcessingException e) {
            log.error("response parsing exception: " + e.getMessage());
            throw new ApiManagerRequestException(e.getMessage());
        }
        return response;
    }


}