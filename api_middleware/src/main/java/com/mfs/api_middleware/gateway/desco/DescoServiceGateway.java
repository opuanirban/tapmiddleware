package com.mfs.api_middleware.gateway.desco;

import org.springframework.stereotype.Service;
import com.mfs.api_middleware.Exception.ApiRequestException;
import com.mfs.api_middleware.gateway.model.BillFetchResponse;
import com.mfs.api_middleware.gateway.nesco.model.BillPaymentInput;
import com.mfs.api_middleware.gateway.nesco.model.BillPaymentResponse;
import com.mfs.api_middleware.gateway.nesco.model.BillPreviewOutput;
import com.mfs.api_middleware.gateway.model.BillPreviewRestInput;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.BodyInserters;
import lombok.extern.slf4j.Slf4j;
import java.time.Duration;

@Service
@Slf4j
public class DescoServiceGateway {
    @Value("${api_desco_service.url}")
    private String descoServiceUrl;

    private final WebClient webClient;

    public DescoServiceGateway(WebClient.Builder webClientBuilder) {
        this.webClient = webClientBuilder.build();
    }

    public BillPreviewOutput summaryDescoPrepaidUnifiedBillPreview(BillPreviewRestInput input){
        log.debug("request: {}", input);
        return webClient.post()
                .uri(descoServiceUrl + "/billing/desco/prepaid/unified/preview")
                .body(BodyInserters.fromValue(input))
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, clientResponse -> {
                    log.error("error in response: {}", clientResponse);
                    throw new ApiRequestException("Bill details request failed");
                })
                .onStatus(HttpStatus::is5xxServerError, clientResponse -> {
                    log.error("error in response: {}", clientResponse);
                    throw new ApiRequestException("Bill details request failed");
                })
                .bodyToMono(BillPreviewOutput.class)
                .timeout(Duration.ofMillis(60000))
                .onTerminateDetach()
                .block();       
    }

    public BillPaymentResponse paymentDescoPrepaidUnifiedBill(BillPaymentInput input){
        log.debug("request: {}", input);
        return webClient.post()        
                .uri(descoServiceUrl + "/billing/desco/prepaid/unified/payment")
                .body(BodyInserters.fromValue(input))
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, clientResponse -> {
                    log.error("error in response: {}", clientResponse);
                    throw new ApiRequestException("Bill details request failed");
                })
                .onStatus(HttpStatus::is5xxServerError, clientResponse -> {
                    log.error("error in response: {}", clientResponse);
                    throw new ApiRequestException("Bill details request failed");
                })
                .bodyToMono(BillPaymentResponse.class)
                .timeout(Duration.ofMillis(20000))
                .onTerminateDetach()
                .block();       
    }
}
