package com.mfs.api_middleware.gateway;

import com.mfs.api_middleware.Exception.ApiRequestException;
import com.mfs.api_middleware.dto.CampaignMain;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import java.time.Duration;

@Slf4j
@Service
public class CampaignGateway {

    @Value("${campaign_manage_service.url}")
    private String campaignUrl;

    private final WebClient webClient;

    public CampaignGateway(WebClient.Builder webClientBuilder) {
        this.webClient = webClientBuilder.build();
    }

    public CampaignMain getBannersAndOffersV2(){
        log.info(campaignUrl+"/campaign/get-all-active");
        return webClient.get()
                .uri(campaignUrl + "/campaign/get-all-active?baseurl=")
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, clientResponse -> {
                    log.error("error in response: {}", clientResponse);
                    throw new ApiRequestException("Bill details request failed");
                })
                .onStatus(HttpStatus::is5xxServerError, clientResponse -> {
                    log.error("error in response: {}", clientResponse);
                    throw new ApiRequestException("Bill details request failed");
                })
                .bodyToMono(CampaignMain.class)
                .timeout(Duration.ofSeconds(20))
                .onTerminateDetach()
                .block();
    }



}