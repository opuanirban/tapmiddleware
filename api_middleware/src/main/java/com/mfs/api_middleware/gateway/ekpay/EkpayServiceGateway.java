package com.mfs.api_middleware.gateway.ekpay;

import com.mfs.api_middleware.Exception.ApiRequestException;
import com.mfs.api_middleware.gateway.model.BillFetchResponse;
import com.mfs.api_middleware.gateway.model.BillPaymentInput;
import com.mfs.api_middleware.gateway.model.BillPaymentResponse;
import com.mfs.api_middleware.gateway.nesco.model.BillPreviewOutput;
import com.mfs.api_middleware.gateway.model.BillPreviewRestInput;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.BodyInserters;
import lombok.extern.slf4j.Slf4j;
import java.time.Duration;

@Slf4j
@Service
public class EkpayServiceGateway {

//    private static final String Ekpay_URL = "http://10.0.1.12:8092";
    private final WebClient webClient;

    public EkpayServiceGateway(WebClient.Builder webClientBuilder, @Value("${api_ekpay_service.url}") String ekpay_URL) {
        this.webClient = webClientBuilder.baseUrl(ekpay_URL).build();
    }

    public BillFetchResponse fetchBill(BillPreviewRestInput input) {
        log.debug("request: {}", input);
        return webClient.post()
                .uri("/billing/preview")
                .body(BodyInserters.fromValue(input))
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, clientResponse -> {
                    log.error("error in response: {}", clientResponse);
                    throw new ApiRequestException("Bill details request failed");
                })
                .onStatus(HttpStatus::is5xxServerError, clientResponse -> {
                    log.error("error in response: {}", clientResponse);
                    throw new ApiRequestException("Bill details request failed");
                })
                .bodyToMono(BillFetchResponse.class)
                .timeout(Duration.ofMillis(20000))
                .onTerminateDetach()
                .block();
    }

    public BillPaymentResponse payBill(BillPaymentInput input) {
        log.debug("request: {}", input);
        return webClient.post()
                .uri("/billing/payment")
                .body(BodyInserters.fromValue(input))
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, clientResponse -> {
                    log.error("error in response: {}", clientResponse);
                    throw new ApiRequestException("Bill details request failed");
                })
                .onStatus(HttpStatus::is5xxServerError, clientResponse -> {
                    log.error("error in response: {}", clientResponse);
                    throw new ApiRequestException("Bill details request failed");
                })
                .bodyToMono(BillPaymentResponse.class)
                .timeout(Duration.ofMillis(20000))
                .onTerminateDetach()
                .block();
    }
}