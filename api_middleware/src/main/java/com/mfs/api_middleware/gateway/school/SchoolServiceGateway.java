package com.mfs.api_middleware.gateway.school;

import com.mfs.api_middleware.Exception.ApiRequestException;
import com.mfs.api_middleware.gateway.model.BillFetchResponse;
import com.mfs.api_middleware.gateway.model.BillPaymentInput;
import com.mfs.api_middleware.gateway.model.BillPaymentResponse;
import com.mfs.api_middleware.gateway.model.BillPreviewRestInput;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;

import java.time.Duration;

@Slf4j
@Service
public class SchoolServiceGateway {

    private final WebClient webClient;

    public SchoolServiceGateway(WebClient.Builder webClientBuilder, @Value("${api_school_service.url}") String school_URL) {
        this.webClient = webClientBuilder.baseUrl(school_URL).build();
    }

    public BillFetchResponse fetchBill(BillPreviewRestInput input) {
        log.debug("request: {}", input);
        return webClient.post()
                .uri("/billing/preview")
                .body(BodyInserters.fromValue(input))
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, clientResponse -> {
                    log.error("error in response: {}", clientResponse);
                    throw new ApiRequestException("Bill details request failed");
                })
                .onStatus(HttpStatus::is5xxServerError, clientResponse -> {
                    log.error("error in response: {}", clientResponse);
                    throw new ApiRequestException("Bill details request failed");
                })
                .bodyToMono(BillFetchResponse.class)
                .timeout(Duration.ofMillis(50000))
                .onTerminateDetach()
                .block();
    }

    public BillPaymentResponse payBill(BillPaymentInput input) {
        log.debug("request: {}", input);
        return webClient.post()
                .uri("/billing/payment")
                .body(BodyInserters.fromValue(input))
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, clientResponse -> {
                    log.error("error in response: {}", clientResponse);
                    throw new ApiRequestException("Bill details request failed");
                })
                .onStatus(HttpStatus::is5xxServerError, clientResponse -> {
                    log.error("error in response: {}", clientResponse);
                    throw new ApiRequestException("Bill details request failed");
                })
                .bodyToMono(BillPaymentResponse.class)
                .timeout(Duration.ofMillis(50000))
                .onTerminateDetach()
                .block();
    }
}
