package com.mfs.api_middleware.gateway.nesco;

import com.mfs.api_middleware.Exception.ApiRequestException;
import com.mfs.api_middleware.gateway.model.BillFetchResponse;
import com.mfs.api_middleware.gateway.nesco.model.BillPaymentInput;
import com.mfs.api_middleware.gateway.nesco.model.BillPaymentResponse;
import com.mfs.api_middleware.gateway.nesco.model.BillPreviewOutput;
import com.mfs.api_middleware.gateway.model.BillPreviewRestInput;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.BodyInserters;
import lombok.extern.slf4j.Slf4j;
import java.time.Duration;

@Slf4j
@Service
public class NescoServiceGateway {
    @Value("${api_nesco_service.url}")
    private String nescoServiceUrl;

    private final WebClient webClient;

    public NescoServiceGateway(WebClient.Builder webClientBuilder) {
        this.webClient = webClientBuilder.build();
    }

    public BillPreviewOutput summaryPrepaidBillPreview(BillPreviewRestInput input){
        log.debug("request: {}", input);
        return webClient.post()
                .uri(nescoServiceUrl + "/billing/nesco/prepaid/preview")
                .body(BodyInserters.fromValue(input))
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, clientResponse -> {
                    log.error("error in response: {}", clientResponse);
                    throw new ApiRequestException("Bill details request failed");
                })
                .onStatus(HttpStatus::is5xxServerError, clientResponse -> {
                    log.error("error in response: {}", clientResponse);
                    throw new ApiRequestException("Bill details request failed");
                })
                .bodyToMono(BillPreviewOutput.class)
                .timeout(Duration.ofMillis(20000))
                .onTerminateDetach()
                .block();       
    }

    public BillFetchResponse fetchPostpaidBillDetails(BillPreviewRestInput input){
        log.debug("request: {}", input);
        return webClient.post()
                .uri(nescoServiceUrl + "/billing/nesco/postpaid/fetch")
                .body(BodyInserters.fromValue(input))
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, clientResponse -> {
                    log.error("error in response: {}", clientResponse);
                    throw new ApiRequestException("Bill details request failed");
                })
                .onStatus(HttpStatus::is5xxServerError, clientResponse -> {
                    log.error("error in response: {}", clientResponse);
                    throw new ApiRequestException("Bill details request failed");
                })
                .bodyToMono(BillFetchResponse.class)
                .timeout(Duration.ofMillis(20000))
                .onTerminateDetach()
                .block();       
    }

    public BillPaymentResponse paymentPostpaidBill(BillPaymentInput input){
        log.debug("request: {}", input);
        return webClient.post()
                .uri(nescoServiceUrl + "/billing/nesco/postpaid/payment")
                .body(BodyInserters.fromValue(input))
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, clientResponse -> {
                    log.error("error in response: {}", clientResponse);
                    throw new ApiRequestException("Bill details request failed");
                })
                .onStatus(HttpStatus::is5xxServerError, clientResponse -> {
                    log.error("error in response: {}", clientResponse);
                    throw new ApiRequestException("Bill details request failed");
                })
                .bodyToMono(BillPaymentResponse.class)
                .timeout(Duration.ofMillis(20000))
                .onTerminateDetach()
                .block();       
    }

    public BillPaymentResponse paymentPrepaidBill(BillPaymentInput input){
        log.debug("request: {}", input);
        return webClient.post()
                .uri(nescoServiceUrl + "/billing/nesco/prepaid/payment")
                .body(BodyInserters.fromValue(input))
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, clientResponse -> {
                    log.error("error in response: {}", clientResponse);
                    throw new ApiRequestException("Bill details request failed");
                })
                .onStatus(HttpStatus::is5xxServerError, clientResponse -> {
                    log.error("error in response: {}", clientResponse);
                    throw new ApiRequestException("Bill details request failed");
                })
                .bodyToMono(BillPaymentResponse.class)
                .timeout(Duration.ofMillis(20000))
                .onTerminateDetach()
                .block();
    }

    public String getPreapidType(String transactionid){
        return webClient.get()
                .uri(nescoServiceUrl + "/billing/nesco/prepaid/search?transactionId=" + transactionid)
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, clientResponse -> {
                    log.error("error in response: {}", clientResponse);
                    throw new ApiRequestException("Bill details request failed");
                })
                .onStatus(HttpStatus::is5xxServerError, clientResponse -> {
                    log.error("error in response: {}", clientResponse);
                    throw new ApiRequestException("Bill details request failed");
                })
                .bodyToMono(String.class)
                .timeout(Duration.ofMillis(20000))
                .onTerminateDetach()
                .block();
    }
}
