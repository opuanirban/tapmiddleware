package com.mfs.api_middleware.gateway.nesco.model;

import com.mfs.api_middleware.gateway.model.BaseResponse;

import lombok.Data;

@Data
public class BillPaymentResponse {
    private BaseResponse response;
    private String dateTime;
    private String billAmount;
    private String billFee;
}
