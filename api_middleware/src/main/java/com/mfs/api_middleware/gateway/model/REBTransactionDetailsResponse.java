package com.mfs.api_middleware.gateway.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class REBTransactionDetailsResponse {
    private Integer Code;
    private Object Message;
}
