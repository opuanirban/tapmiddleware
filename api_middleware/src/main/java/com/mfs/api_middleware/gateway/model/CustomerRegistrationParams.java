package com.mfs.api_middleware.gateway.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class CustomerRegistrationParams {
    @JsonProperty("IsLimitedKYC")
    private Boolean isLimitedKyc;

    @JsonProperty("AppUserNumber")
    private String appUserNumber;

    @JsonProperty("UserNumber")
    private String userNumber;

    @JsonProperty("UserPin")
    private String userPin;

    @JsonProperty("FirstName")
    private String firstName;

    @JsonProperty("LastName")
    private String lastName;

    @JsonProperty("NationalIdNumber")
    private String nationalIdNumber;

    @JsonProperty("Sex")
    private String sex;

    @JsonProperty("UserGroupKey")
    private String userGroupKey;

    @JsonProperty("DoB")
    private String dob;

    @JsonProperty("Occupation")
    private String occupation;

    @JsonProperty("OperatorName")
    private String operatorName;

    @JsonProperty("AccountPin")
    private String accountPin;

    @JsonProperty("BanglaName")
    private String banglaName;

    @JsonProperty("Nominee")
    private String nominee;

    @JsonProperty("NomineeRelation")
    private String nomineeRelation;

    @JsonProperty("FatherName")
    private String fatherName;

    @JsonProperty("MotherName")
    private String motherName;

    @JsonProperty("SpouseName")
    private String spouseName;

    @JsonProperty("PresentAddress")
    private String presentAddress;

    @JsonProperty("PermanentAddress")
    private String permanentAddress;

    @JsonProperty("PhotoString")
    private String photoString;

    @JsonProperty("PhotoName")
    private String photoName;

}
