package com.mfs.api_middleware.gateway.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class TransactionDetailsRequest {
    @JsonProperty("UserNumber")
    private String userNumber;

    @JsonProperty("TransactionNumber")
    private String transactionNumber;
}
