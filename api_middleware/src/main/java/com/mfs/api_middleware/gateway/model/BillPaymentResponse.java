package com.mfs.api_middleware.gateway.model;

import lombok.Data;

import java.util.HashMap;
import java.util.List;

@Data
public class BillPaymentResponse {
    private BaseResponse response;
    private String dateTime;
    private String billAmount;
    private String billFee;
    private HashMap<String, Object> additionalInfo;
}
