package com.mfs.api_middleware.gateway.reb;

import java.time.Duration;

import com.mfs.api_middleware.Exception.ApiRequestException;
import com.mfs.api_middleware.dto.REBPendingTransactionsResponse;
import com.mfs.api_middleware.dto.REBTransactionDetails;
import com.mfs.api_middleware.gateway.model.BillFetchResponse;
import com.mfs.api_middleware.gateway.model.BillPreviewRestInput;
import com.mfs.api_middleware.gateway.nesco.model.BillPaymentInput;
import com.mfs.api_middleware.gateway.nesco.model.BillPaymentResponse;
import com.mfs.api_middleware.util.CommonConstant;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

@Slf4j
@Service
public class REBServiceGateway {
    @Value("${api_reb_service.url}")
    private String rebServiceUrl;

    private final WebClient webClient;

    public REBServiceGateway(WebClient.Builder webClientBuilder) {
        this.webClient = webClientBuilder.build();
    }

    public BillPaymentResponse paymentRebPrepaidBill(BillPaymentInput input){
        log.debug("request: {}", input);
        return webClient.post()        
                .uri(rebServiceUrl + "/billing/reb/prepaid/payment")
                .body(BodyInserters.fromValue(input))
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, clientResponse -> clientResponse.bodyToMono(String.class) // error body as String or other class
                    .flatMap(error -> Mono.error(new ApiRequestException(error))))
                .onStatus(HttpStatus::is5xxServerError, clientResponse -> clientResponse.bodyToMono(String.class) // error body as String or other class
                    .flatMap(error -> Mono.error(new ApiRequestException(error))))
                .bodyToMono(BillPaymentResponse.class)
                .timeout(Duration.ofMillis(60000))
                .onErrorMap(throwable -> {
                    log.error("error: {}", throwable);
                    if(throwable instanceof ApiRequestException) {
                        throw new ApiRequestException(throwable.getMessage());
                    } else {
                        throw new ApiRequestException(CommonConstant.HTTP_5XX_ERROR);
                    }
                })
                .onTerminateDetach()
                .block();       
    }

    public REBTransactionDetails getPaymentDetails(String transactionId){
        log.debug("request: {}", transactionId);
        MultiValueMap<String, String> formData = new LinkedMultiValueMap<>();
        formData.add("transactionId", transactionId);

        return webClient.post()        
                .uri(rebServiceUrl + "/billing/reb/prepaid/details")
                .body(BodyInserters.fromFormData(formData))
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, clientResponse -> clientResponse.bodyToMono(String.class) // error body as String or other class
                    .flatMap(error -> Mono.error(new ApiRequestException(error))))
                .onStatus(HttpStatus::is5xxServerError, clientResponse -> clientResponse.bodyToMono(String.class) // error body as String or other class
                    .flatMap(error -> Mono.error(new ApiRequestException(error))))
                .bodyToMono(REBTransactionDetails.class)
                .timeout(Duration.ofMillis(30000))
                .onErrorMap(throwable -> {
                    log.error("error: {}", throwable);
                    if(throwable instanceof ApiRequestException) {
                        throw new ApiRequestException(throwable.getMessage());
                    } else {
                        throw new ApiRequestException(CommonConstant.HTTP_5XX_ERROR);
                    }
                })
                .onTerminateDetach()
                .block();       
    }

    public REBPendingTransactionsResponse getPendingPaymentList(String msisdn){
        log.debug("request: {}", msisdn);
        MultiValueMap<String, String> formData = new LinkedMultiValueMap<>();
        formData.add("msisdn", msisdn);

        return webClient.post()        
                .uri(rebServiceUrl + "/billing/reb/prepaid/pending-list")
                .body(BodyInserters.fromFormData(formData))
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, clientResponse -> clientResponse.bodyToMono(String.class) // error body as String or other class
                    .flatMap(error -> Mono.error(new ApiRequestException(error))))
                .onStatus(HttpStatus::is5xxServerError, clientResponse -> clientResponse.bodyToMono(String.class) // error body as String or other class
                    .flatMap(error -> Mono.error(new ApiRequestException(error))))
                .bodyToMono(REBPendingTransactionsResponse.class)
                .timeout(Duration.ofMillis(30000))
                .onErrorMap(throwable -> {
                    log.error("error: {}", throwable);
                    if(throwable instanceof ApiRequestException) {
                        throw new ApiRequestException(throwable.getMessage());
                    } else {
                        throw new ApiRequestException(CommonConstant.HTTP_5XX_ERROR);
                    }
                })
                .onTerminateDetach()
                .block();       
    }

    public BillPaymentResponse retryPendingPayment(String transactionId){
        log.debug("request: {}", transactionId);
        MultiValueMap<String, String> formData = new LinkedMultiValueMap<>();
        formData.add("transactionId", transactionId);

        return webClient.post()        
                .uri(rebServiceUrl + "/billing/reb/prepaid/retry")
                .body(BodyInserters.fromFormData(formData))
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, clientResponse -> clientResponse.bodyToMono(String.class) // error body as String or other class
                    .flatMap(error -> Mono.error(new ApiRequestException(error))))
                .onStatus(HttpStatus::is5xxServerError, clientResponse -> clientResponse.bodyToMono(String.class) // error body as String or other class
                    .flatMap(error -> Mono.error(new ApiRequestException(error))))
                .bodyToMono(BillPaymentResponse.class)
                .timeout(Duration.ofMillis(60000))
                .onErrorMap(throwable -> {
                    log.error("error: {}", throwable);
                    if(throwable instanceof ApiRequestException) {
                        throw new ApiRequestException(throwable.getMessage());
                    } else {
                        throw new ApiRequestException(CommonConstant.HTTP_5XX_ERROR);
                    }
                })
                .onTerminateDetach()
                .block();       
    }

    public BillFetchResponse fetchPostpaidBillDetails(BillPreviewRestInput input){
        log.debug("request: {}", input);
        return webClient.post()
                .uri(rebServiceUrl + "/billing/reb/postpaid/fetch")
                .body(BodyInserters.fromValue(input))
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, clientResponse -> {
                    log.error("error in response: {}", clientResponse);
                    throw new ApiRequestException("Bill details request failed");
                })
                .onStatus(HttpStatus::is5xxServerError, clientResponse -> {
                    log.error("error in response: {}", clientResponse);
                    throw new ApiRequestException("Bill details request failed");
                })
                .bodyToMono(BillFetchResponse.class)
                .timeout(Duration.ofMillis(20000))
                .onTerminateDetach()
                .block();
    }

    public BillPaymentResponse paymentPostpaidBill(BillPaymentInput input){
        log.debug("request: {}", input);
        return webClient.post()
                .uri(rebServiceUrl + "/billing/reb/postpaid/payment")
                .body(BodyInserters.fromValue(input))
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, clientResponse -> {
                    log.error("error in response: {}", clientResponse);
                    throw new ApiRequestException("Bill details request failed");
                })
                .onStatus(HttpStatus::is5xxServerError, clientResponse -> {
                    log.error("error in response: {}", clientResponse);
                    throw new ApiRequestException("Bill details request failed");
                })
                .bodyToMono(BillPaymentResponse.class)
                .timeout(Duration.ofMillis(20000))
                .onTerminateDetach()
                .block();
    }
}
