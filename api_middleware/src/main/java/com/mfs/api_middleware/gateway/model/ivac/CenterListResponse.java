package com.mfs.api_middleware.gateway.model.ivac;

import java.util.List;

import com.mfs.api_middleware.dto.IVACCenterItem;

import lombok.Data;

@Data
public class CenterListResponse {
    private List<IVACCenterItem> ivacCenterItems;
}
