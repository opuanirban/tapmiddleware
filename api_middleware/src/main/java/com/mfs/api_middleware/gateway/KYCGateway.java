package com.mfs.api_middleware.gateway;

import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Duration;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeoutException;

import javax.net.ssl.SSLException;

import com.mfs.api_middleware.Exception.ApiRequestException;
import com.mfs.api_middleware.Exception.GigatechException;
import com.mfs.api_middleware.Exception.UserRegistrationRequestException;
import com.mfs.api_middleware.domain.EKYCVerificationRequestInfo;
import com.mfs.api_middleware.domain.EKYCVerificationRequestInfoService;
import com.mfs.api_middleware.dto.KYCRegistrationResponse;
import com.mfs.api_middleware.dto.NIDUploadApiResponse;
import com.mfs.api_middleware.dto.NIDValidationRequest;
import com.mfs.api_middleware.dto.NIDVerificationResponse;
import com.mfs.api_middleware.service.MessageRetrieverService;
import com.mfs.api_middleware.util.CommonConstant;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.MultipartBodyBuilder;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;

import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslContextBuilder;
import io.netty.handler.ssl.util.InsecureTrustManagerFactory;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;
import reactor.netty.http.client.HttpClient;

@Slf4j
@Service
public class KYCGateway {
    @Value("${ekyc.url}")
    private String ekycUrl;
    @Value("${ekyc.token}")
    private String ekycToken;
    
    @Autowired
    MessageRetrieverService messageRetrieverService;
    @Autowired
    ApiManagerGateway apiManagerGateway;
    @Autowired
    EKYCVerificationRequestInfoService requestInfoService;

    private final WebClient webClient;
    private static Map<Integer, String> gigatechResponseMessage;
    static
    {
        gigatechResponseMessage = new HashMap<>();
        gigatechResponseMessage.put(4002, "Customer is successfully re-enrolled, waiting verification");
        gigatechResponseMessage.put(4003, "Customer is successfully enrolled, waiting verification");
        gigatechResponseMessage.put(5001, "Please check your image fields");
        gigatechResponseMessage.put(5002, "Mandatory requested data not present");
        gigatechResponseMessage.put(5003, "id_front_name or id_back_name field can not be more than 100 characters");
        gigatechResponseMessage.put(5004, "Please provide a valid Bangladeshi mobile number without country code");
        gigatechResponseMessage.put(5005, "Please upload a valid photo");
        gigatechResponseMessage.put(6001, "Nid or Date of Birth not found");
        gigatechResponseMessage.put(6002, "Nid or Date of Birth not found");
        gigatechResponseMessage.put(6003, "Applicant with this NID or mobile number already exists");
        gigatechResponseMessage.put(6005, "No applicants record found");
    }

    public KYCGateway(WebClient.Builder webClientBuilder) throws SSLException {
        SslContext sslContext = SslContextBuilder
            .forClient()
            .trustManager(InsecureTrustManagerFactory.INSTANCE)
            .build();
        HttpClient httpClient = HttpClient.create().secure(t -> t.sslContext(sslContext));
        this.webClient =  webClientBuilder.clientConnector(new ReactorClientHttpConnector(httpClient))
            .build();

    }

    public NIDUploadApiResponse nidUpload(MultipartFile id_front, MultipartFile id_back, String msisdn, Boolean isNewRegistration, Locale locale) {
        MultipartBodyBuilder bodyBuilder = new MultipartBodyBuilder();
        bodyBuilder.part("id_front", createTempFileResource(id_front));
        bodyBuilder.part("id_back", createTempFileResource(id_back));

        NIDUploadApiResponse nidUploadApiResponse = webClient.post()
                .uri(ekycUrl + "/agent/customer-registration/?step=1&crop=1")
                .header("Authorization","Token " + ekycToken)
                .body(BodyInserters.fromMultipartData(bodyBuilder.build()))
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, clientResponse -> clientResponse.bodyToMono(String.class) // error body as String or other class
                    .flatMap(error -> Mono.error(new ApiRequestException(error))))
                .onStatus(HttpStatus::is5xxServerError, clientResponse -> clientResponse.bodyToMono(String.class) // error body as String or other class
                    .flatMap(error -> Mono.error(new ApiRequestException(error))))
                .bodyToMono(NIDUploadApiResponse.class)
                .timeout(Duration.ofMillis(30000))
                .onTerminateDetach()
                .onErrorMap(throwable -> {
                    log.error("error: {}", throwable);
                    if(throwable instanceof GigatechException) {
                        throw new ApiRequestException(throwable.getMessage());
                    } else if (throwable instanceof TimeoutException) {
                        throw new ApiRequestException(CommonConstant.TIMEOUT_ERROR);
                    } else {
                        throw new ApiRequestException(CommonConstant.COMMON_ERROR);
                    }
                })
                .block();

        log.info("NID OCR response: {}", nidUploadApiResponse);
        if(nidUploadApiResponse.getStatus().equalsIgnoreCase(CommonConstant.SUCCESS)) {
            NIDValidationRequest nidValidationRequest= new NIDValidationRequest();
            String nid_no= nidUploadApiResponse.getData().getNid_no();
            nidValidationRequest.setNationalIdNumber(nid_no);

            if(apiManagerGateway.validateNID(nidValidationRequest,msisdn,isNewRegistration,locale).booleanValue()) {
                return nidUploadApiResponse;
            } else {
                throw new UserRegistrationRequestException("Applicant with this NID already exists");
            }
        }else {
            throw new GigatechException(gigatechResponseMessage.get(nidUploadApiResponse.getStatus_code()));
        }
    }

    public KYCRegistrationResponse customerRegistration(String nid_no, String dob, String applicant_name_ben, String applicant_name_eng, String father_name, String mother_name, String spouse_name, String pres_address, String perm_address, String id_front_name, String id_back_name, String gender, String profession, String nominee, String nominee_relation, String mobile_number, MultipartFile applicant_photo, Locale locale) {
        if(applicant_photo == null || applicant_photo.isEmpty()) {
            throw new UserRegistrationRequestException("User photo image data not available");
        }

        EKYCVerificationRequestInfo ekycVerificationRequestInfo = new EKYCVerificationRequestInfo();
        ekycVerificationRequestInfo.setApplicantNameBn(applicant_name_ben);
        ekycVerificationRequestInfo.setApplicantNameEn(applicant_name_eng);
        ekycVerificationRequestInfo.setDob(dob);
        ekycVerificationRequestInfo.setFatherName(father_name);
        ekycVerificationRequestInfo.setGender(gender);
        ekycVerificationRequestInfo.setIdBackName(id_back_name);
        ekycVerificationRequestInfo.setIdFrontName(id_front_name);
        ekycVerificationRequestInfo.setMobileNumber(mobile_number);
        ekycVerificationRequestInfo.setMotherName(mother_name);
        ekycVerificationRequestInfo.setNidNo(nid_no);
        ekycVerificationRequestInfo.setNominee(nominee);
        ekycVerificationRequestInfo.setNomineeRelation(nominee_relation);
        ekycVerificationRequestInfo.setPermAddress(perm_address);
        ekycVerificationRequestInfo.setPresAddress(pres_address);
        ekycVerificationRequestInfo.setProfession(profession);
        ekycVerificationRequestInfo.setSpouseName(spouse_name);
        ekycVerificationRequestInfo.setRequestDate(new Date());

        MultipartBodyBuilder bodyBuilder = new MultipartBodyBuilder();
        bodyBuilder.part("nid_no", nid_no);
        bodyBuilder.part("dob", dob);
        bodyBuilder.part("applicant_name_ben", applicant_name_ben);
        bodyBuilder.part("applicant_name_eng", applicant_name_eng);
        bodyBuilder.part("father_name", father_name);
        bodyBuilder.part("mother_name", mother_name);
        bodyBuilder.part("spouse_name", spouse_name);
        bodyBuilder.part("pres_address", pres_address);
        bodyBuilder.part("perm_address", perm_address);
        bodyBuilder.part("id_front_name", id_front_name);
        bodyBuilder.part("id_back_name", id_back_name);
        bodyBuilder.part("gender", gender);
        bodyBuilder.part("profession", profession);
        bodyBuilder.part("nominee", nominee);
        bodyBuilder.part("nominee_relation", nominee_relation);
        bodyBuilder.part("mobile_number", mobile_number);
        bodyBuilder.part("applicant_photo", createTempFileResource(applicant_photo));

        KYCRegistrationResponse kycRegistrationResponse = webClient.post()
                .uri(ekycUrl + "/agent/customer-registration/?step=2")
                .header("Authorization","Token " + ekycToken)
                .body(BodyInserters.fromMultipartData(bodyBuilder.build()))
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, clientResponse -> clientResponse.bodyToMono(String.class) // error body as String or other class
                    .flatMap(error -> Mono.error(new ApiRequestException(error))))
                .onStatus(HttpStatus::is5xxServerError, clientResponse -> clientResponse.bodyToMono(String.class) // error body as String or other class
                    .flatMap(error -> Mono.error(new ApiRequestException(error))))
                .bodyToMono(KYCRegistrationResponse.class)
                .timeout(Duration.ofMillis(30000))
                .onTerminateDetach()
                .onErrorMap(throwable -> {
                    log.error("error: {}", throwable);
                    if(throwable instanceof GigatechException) {
                        throw new ApiRequestException(throwable.getMessage());
                    } else if (throwable instanceof TimeoutException) {
                        throw new ApiRequestException(CommonConstant.TIMEOUT_ERROR);
                    } else {
                        throw new ApiRequestException(CommonConstant.COMMON_ERROR);
                    }
                })
                .block();

        if (kycRegistrationResponse.getStatus().equalsIgnoreCase(CommonConstant.SUCCESS)) {
            requestInfoService.saveEkycRequestInfo(ekycVerificationRequestInfo);
            kycRegistrationResponse.setMessage(gigatechResponseMessage.get(kycRegistrationResponse.getStatus_code()));
            return kycRegistrationResponse;
        } else {
            throw new GigatechException(gigatechResponseMessage.get(kycRegistrationResponse.getStatus_code()));
        }
    }


    public NIDVerificationResponse nidVerifyStatus(String mobile_number, Locale locale) {
        return webClient.post()
                .uri(ekycUrl + "/agent/get-verification-status/")
                .header("Authorization","Token " + ekycToken)
                .body(BodyInserters.fromFormData("mobile_number", mobile_number))
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, clientResponse -> clientResponse.bodyToMono(String.class) // error body as String or other class
                    .flatMap(error -> Mono.error(new ApiRequestException(error))))
                .onStatus(HttpStatus::is5xxServerError, clientResponse -> clientResponse.bodyToMono(String.class) // error body as String or other class
                    .flatMap(error -> Mono.error(new ApiRequestException(error))))
                .bodyToMono(NIDVerificationResponse.class)
                .timeout(Duration.ofMillis(20000))
                .onTerminateDetach()
                .onErrorMap(throwable -> {
                    log.error("error: {}", throwable);
                    if(throwable instanceof GigatechException) {
                        throw new ApiRequestException(throwable.getMessage());
                    } else if (throwable instanceof TimeoutException) {
                        throw new ApiRequestException(CommonConstant.TIMEOUT_ERROR);
                    } else {
                        throw new ApiRequestException(CommonConstant.COMMON_ERROR);
                    }
                })
                .block();
    }


    private Resource createTempFileResource(MultipartFile content) {
        try {
            Path tempFile = Files.createTempFile(content.getName(), "." + FilenameUtils.getExtension(content.getOriginalFilename()));
            Files.write(tempFile, content.getBytes());
            return new FileSystemResource(tempFile.toFile());
        } catch (Exception e) {
            log.error("error: {}", e);
            throw new ApiRequestException(CommonConstant.COMMON_ERROR);
        }
    }
}