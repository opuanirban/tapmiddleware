package com.mfs.api_middleware.gateway.model.reb.apimanager;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class PendingTransactionRequest {
    @JsonProperty("UserNumber")
    private String userNumber;
}
