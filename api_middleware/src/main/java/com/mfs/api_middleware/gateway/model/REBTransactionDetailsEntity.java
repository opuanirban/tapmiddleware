package com.mfs.api_middleware.gateway.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class REBTransactionDetailsEntity {
    private String TxCode;
    private String MeterNumber;
    private Double Amount;
    private String TransactionDate;
    private Double FeeAmount;
    private String Token;
}
