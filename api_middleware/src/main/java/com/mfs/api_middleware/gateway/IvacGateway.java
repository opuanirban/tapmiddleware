package com.mfs.api_middleware.gateway;

import com.mfs.api_middleware.Exception.ApiRequestException;
import com.mfs.api_middleware.dto.IVACCenterItem;
import com.mfs.api_middleware.dto.IvacBillPreviewOutput;
import com.mfs.api_middleware.gateway.model.BillPaymentInput;
import com.mfs.api_middleware.gateway.model.BillPaymentResponse;
import com.mfs.api_middleware.gateway.model.BillPreviewRestInput;
import com.mfs.api_middleware.gateway.model.ivac.CenterListResponse;
import com.mfs.api_middleware.gateway.nesco.model.BillPreviewOutput;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;

import java.time.Duration;

@Slf4j
@Service
public class IvacGateway {

    @Value("${api_ivac_service.url}")
    private String ivacServiceUrl;

    private final WebClient webClient;

    public IvacGateway(WebClient.Builder webClientBuilder) {
        this.webClient = webClientBuilder.build();
    }


    public IvacBillPreviewOutput summaryIvacBillPreview(BillPreviewRestInput input){
        log.debug("request: {}", input);
        return webClient.post()
                .uri(ivacServiceUrl + "/billing/ssl/ivac/preview")
                .body(BodyInserters.fromValue(input))
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, clientResponse -> {
                    log.error("error in response: {}", clientResponse);
                    throw new ApiRequestException("Bill details request failed");
                })
                .onStatus(HttpStatus::is5xxServerError, clientResponse -> {
                    log.error("error in response: {}", clientResponse);
                    throw new ApiRequestException("Bill details request failed");
                })
                .bodyToMono(IvacBillPreviewOutput.class)
                .timeout(Duration.ofMillis(60000))
                .onTerminateDetach()
                .block();
    }

    public BillPaymentResponse paymentIvacBill(BillPaymentInput input){
        log.debug("request: {}", input);
        return webClient.post()
                .uri(ivacServiceUrl + "/billing/ssl/ivac/payment")
                .body(BodyInserters.fromValue(input))
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, clientResponse -> {
                    log.error("error in response: {}", clientResponse);
                    throw new ApiRequestException("Bill details request failed");
                })
                .onStatus(HttpStatus::is5xxServerError, clientResponse -> {
                    log.error("error in response: {}", clientResponse);
                    throw new ApiRequestException("Bill details request failed");
                })
                .bodyToMono(BillPaymentResponse.class)
                .timeout(Duration.ofMillis(20000))
                .onTerminateDetach()
                .block();
    }

    public CenterListResponse ivacCenterList(){
        return webClient.get()
                .uri(ivacServiceUrl + "/billing/ssl/ivac/center-list")
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, clientResponse -> {
                    log.error("error in response: {}", clientResponse);
                    throw new ApiRequestException("Bill details request failed");
                })
                .onStatus(HttpStatus::is5xxServerError, clientResponse -> {
                    log.error("error in response: {}", clientResponse);
                    throw new ApiRequestException("Bill details request failed");
                })
                .bodyToMono(CenterListResponse.class)
                .timeout(Duration.ofMillis(20000))
                .onTerminateDetach()
                .block();
    }

}
