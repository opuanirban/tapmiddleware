package com.mfs.api_middleware.gateway.model.reb.apimanager;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class RetryTransactionRequest {
    @JsonProperty("TransactionId")
    private String transactionId;

    @JsonProperty("UserNumber")
    private String userNumber;

    @JsonProperty("Channel")
    private String channel;

}
