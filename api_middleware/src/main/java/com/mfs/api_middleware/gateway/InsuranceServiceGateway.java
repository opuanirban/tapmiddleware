package com.mfs.api_middleware.gateway;

import com.mfs.api_middleware.Exception.ApiRequestException;
import com.mfs.api_middleware.gateway.model.BillFetchResponse;
import com.mfs.api_middleware.gateway.model.BillPaymentResponse;
import com.mfs.api_middleware.gateway.model.BillPreviewRestInput;
import com.mfs.api_middleware.gateway.nesco.model.BillPaymentInput;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;

import java.time.Duration;

@Slf4j
@Service
public class InsuranceServiceGateway {
    @Autowired
    RestTemplate restTemplate;

    @Value("${api_pil_service.url}")
    private String pilManagerUrl;

    @Value("${api_internet_service.url}")
    private String internetServiceUrl;

    private final WebClient webClient;

    public InsuranceServiceGateway(WebClient.Builder webClientBuilder) {
        this.webClient = webClientBuilder.build();
    }

    public BillFetchResponse getPrimeLifeInsuracneFetch(BillPreviewRestInput input){
        log.debug("request: {}", input);
        return webClient.post()
                .uri(pilManagerUrl + "/billing/insurance/pil/fetch")
                .body(BodyInserters.fromValue(input))
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, clientResponse -> {
                    log.error("error in response: {}", clientResponse);
                    throw new ApiRequestException("Bill details request failed");
                })
                .onStatus(HttpStatus::is5xxServerError, clientResponse -> {
                    log.error("error in response: {}", clientResponse);
                    throw new ApiRequestException("Bill details request failed");
                })
                .bodyToMono(BillFetchResponse.class)
                .timeout(Duration.ofMillis(30000))
                .onTerminateDetach()
                .block();
    }

    public BillPaymentResponse payPrimeLifeInsuranceBill(BillPaymentInput input){
        log.debug("request: {}", input);
        return webClient.post()
                .uri(pilManagerUrl + "/billing/insurance/pil/payment")
                .body(BodyInserters.fromValue(input))
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, clientResponse -> {
                    log.error("error in response: {}", clientResponse);
                    throw new ApiRequestException("Bill details request failed");
                })
                .onStatus(HttpStatus::is5xxServerError, clientResponse -> {
                    log.error("error in response: {}", clientResponse);
                    throw new ApiRequestException("Bill details request failed");
                })
                .bodyToMono(BillPaymentResponse.class)
                .timeout(Duration.ofMillis(30000))
                .onTerminateDetach()
                .block();
    }



}
