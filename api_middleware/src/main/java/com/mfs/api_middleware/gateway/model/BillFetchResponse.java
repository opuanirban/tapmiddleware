package com.mfs.api_middleware.gateway.model;

import lombok.Data;

import java.util.HashMap;
import java.util.List;

@Data
public class BillFetchResponse {

    private BaseResponse response;
    private String billAmount;
    private List<HashMap<String, Object>> tableData;
    private HashMap<String, Object> payload;

}