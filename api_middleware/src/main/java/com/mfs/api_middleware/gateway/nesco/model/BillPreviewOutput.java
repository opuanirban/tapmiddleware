package com.mfs.api_middleware.gateway.nesco.model;

import java.util.HashMap;
import java.util.List;

import com.mfs.api_middleware.gateway.model.BaseResponse;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class BillPreviewOutput {
    private BaseResponse response;
    private List<HashMap<String, Object>> tableData;
    private HashMap<String, Object> payload;
}
