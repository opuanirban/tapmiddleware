package com.mfs.api_middleware.gateway;

import java.time.Duration;

import com.mfs.api_middleware.Exception.ApiRequestException;
import com.mfs.api_middleware.gateway.model.BillFetchResponse;
import com.mfs.api_middleware.gateway.model.BillPaymentInput;
import com.mfs.api_middleware.gateway.model.BillPaymentResponse;
import com.mfs.api_middleware.gateway.model.BillPreviewRestInput;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class JGTDSLServiceGateway {
    @Value("${api_jgtdsl_service.url}")
    private String jgtdslServiceUrl;

    private final WebClient webClient;

    public JGTDSLServiceGateway(WebClient.Builder webClientBuilder) {
        this.webClient = webClientBuilder.build();
    }

    public BillFetchResponse fetchJGTDSLBill(BillPreviewRestInput input){
        log.debug("request: {}", input);
        return webClient.post()
                .uri(jgtdslServiceUrl + "/billing/jgtdsl/fetch")
                .body(BodyInserters.fromValue(input))
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, clientResponse -> {
                    log.error("error in response: {}", clientResponse);
                    throw new ApiRequestException("Bill details request failed");
                })
                .onStatus(HttpStatus::is5xxServerError, clientResponse -> {
                    log.error("error in response: {}", clientResponse);
                    throw new ApiRequestException("Bill details request failed");
                })
                .bodyToMono(BillFetchResponse.class)
                .timeout(Duration.ofMillis(30000))
                .onTerminateDetach()
                .block();       
    }

    public BillPaymentResponse payJGTDSLBill(BillPaymentInput input){
        log.debug("request: {}", input);
        return webClient.post()        
                .uri(jgtdslServiceUrl + "/billing/jgtdsl/payment")
                .body(BodyInserters.fromValue(input))
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, clientResponse -> {
                    log.error("error in response: {}", clientResponse);
                    throw new ApiRequestException("Bill details request failed");
                })
                .onStatus(HttpStatus::is5xxServerError, clientResponse -> {
                    log.error("error in response: {}", clientResponse);
                    throw new ApiRequestException("Bill details request failed");
                })
                .bodyToMono(BillPaymentResponse.class)
                .timeout(Duration.ofMillis(30000))
                .onTerminateDetach()
                .block();       
    }
}
