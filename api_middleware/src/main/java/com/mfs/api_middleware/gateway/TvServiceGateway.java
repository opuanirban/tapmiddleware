package com.mfs.api_middleware.gateway;

import java.time.Duration;

import com.mfs.api_middleware.Exception.ApiRequestException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;

import com.mfs.api_middleware.gateway.model.BillPaymentInput;
import com.mfs.api_middleware.gateway.model.BillPaymentResponse;
import com.mfs.api_middleware.gateway.model.BillPreviewRestInput;
import com.mfs.api_middleware.gateway.nesco.model.BillPreviewOutput;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class TvServiceGateway {
    @Value("${api_tv_service.url}")
    private String tvServiceUrl;

    private final WebClient webClient;

    public TvServiceGateway(WebClient.Builder webClientBuilder) {
        this.webClient = webClientBuilder.build();
    }

    public BillPreviewOutput summaryAkashBillPreview(BillPreviewRestInput input){
        log.debug("request: {}", input);
        return webClient.post()
                .uri(tvServiceUrl + "/billing/tv/akash/preview")
                .body(BodyInserters.fromValue(input))
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, clientResponse -> {
                    log.error("error in response: {}", clientResponse);
                    throw new ApiRequestException("Bill details request failed");
                })
                .onStatus(HttpStatus::is5xxServerError, clientResponse -> {
                    log.error("error in response: {}", clientResponse);
                    throw new ApiRequestException("Bill details request failed");
                })
                .bodyToMono(BillPreviewOutput.class)
                .timeout(Duration.ofMillis(60000))
                .onTerminateDetach()
                .block();       
    }

    public BillPaymentResponse paymentAkashBill(BillPaymentInput input){
        log.debug("request: {}", input);
        return webClient.post()        
                .uri(tvServiceUrl + "/billing/tv/akash/payment")
                .body(BodyInserters.fromValue(input))
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, clientResponse -> {
                    log.error("error in response: {}", clientResponse);
                    throw new ApiRequestException("Bill details request failed");
                })
                .onStatus(HttpStatus::is5xxServerError, clientResponse -> {
                    log.error("error in response: {}", clientResponse);
                    throw new ApiRequestException("Bill details request failed");
                })
                .bodyToMono(BillPaymentResponse.class)
                .timeout(Duration.ofMillis(20000))
                .onTerminateDetach()
                .block();       
    }
}
