package com.mfs.api_middleware.gateway.topup;

import lombok.Data;

@Data
public class BillPaymentTopUpResponse {

    private String statusCode;
    private String message;

}