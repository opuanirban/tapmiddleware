package com.mfs.api_middleware.gateway.reb.apimanager;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class PendingTransactionItem {
    private String TransactionId;
    private String TxCode;
    private String MeterNumber;
    private String Status;
    private String RequestXML;
    private String CreatedDate;
    private String UpdatedDate;
    private String CreatedBy;
    private String UpdatedBy;
}
