package com.mfs.api_middleware.gateway.topup;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class AppPlanDTO {

    private String itemHeadTitle;
    private String itemHeadTitleBn;
    private Integer amount;

    private List<Map<String, String>> dataArray;

}