package com.mfs.api_middleware.service;

import com.mfs.api_middleware.dto.BalanceInquiry;

import java.util.Locale;

public interface BalanceService {
    public BalanceInquiry checkBalance(String userAccount, Locale locale);
}
