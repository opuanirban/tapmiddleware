package com.mfs.api_middleware.service;

import com.mfs.api_middleware.dto.LimitInfoResponse;
import com.mfs.api_middleware.gateway.ApiManagerGateway;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Locale;

@Service
@Slf4j
public class LimitInfoServiceImpl implements LimitInfoService {

    @Autowired
    ApiManagerGateway apiManagerGateway;

    @Override
    public List<LimitInfoResponse> fetchLimit(String userAccount, Locale locale) {
        return apiManagerGateway.limitInfo(userAccount, locale);
    }
}
