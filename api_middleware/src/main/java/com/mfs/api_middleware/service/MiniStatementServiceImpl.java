package com.mfs.api_middleware.service;

import com.mfs.api_middleware.config.ApplicationProperties;
import com.mfs.api_middleware.dto.MiniStatementResponse;
import com.mfs.api_middleware.dto.MiniStatementResponseEntity;
import com.mfs.api_middleware.gateway.ApiManagerGateway;
import com.mfs.api_middleware.service.security.ISecurityService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Service
@Slf4j
public class MiniStatementServiceImpl extends ProcessRequestService implements MinistatementService {
    @Autowired
    ApiManagerGateway apiManagerGateway;

    @Autowired
    ISecurityService securityService;

    @Autowired
    ApplicationProperties applicationProperties;

    @Autowired
    MessageRetrieverService messageRetrieverService;


    @Override
    public MiniStatementResponse fetchMiniStatement(String userAccount,Locale locale) {
        MiniStatementResponse miniStatementResponse = new MiniStatementResponse();
        List<MiniStatementResponseEntity> miniStatementResponseEntityList = new ArrayList<>();

        miniStatementResponseEntityList = apiManagerGateway.miniStatement(userAccount, locale);
        miniStatementResponse.setStatement(miniStatementResponseEntityList);
        return miniStatementResponse;
    }
}
