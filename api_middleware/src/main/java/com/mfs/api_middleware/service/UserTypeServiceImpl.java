package com.mfs.api_middleware.service;

import com.mfs.api_middleware.dto.UserTypeResponse;
import com.mfs.api_middleware.dto.UserTypeResponseWallet;
import com.mfs.api_middleware.gateway.ApiManagerGateway;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Locale;

@Service
@Slf4j
public class UserTypeServiceImpl implements UserTypeService {

    @Autowired
    ApiManagerGateway apiManagerGateway;

    @Override
    public UserTypeResponseWallet fetchUserType(String userAccount, Locale locale) {
        UserTypeResponse response = apiManagerGateway.userType(userAccount, locale);
        UserTypeResponseWallet userTypeResponseWallet = new UserTypeResponseWallet();
        userTypeResponseWallet.setUserType(response.getKey());
        userTypeResponseWallet.setUserName(response.getValue());
        return userTypeResponseWallet;
    }
}
