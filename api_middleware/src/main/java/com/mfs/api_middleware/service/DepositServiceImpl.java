package com.mfs.api_middleware.service;

import com.mfs.api_middleware.Exception.DecodePinException;
import com.mfs.api_middleware.config.ApplicationProperties;
import com.mfs.api_middleware.dto.ApiManagerRequest;
import com.mfs.api_middleware.dto.CommonResponse;
import com.mfs.api_middleware.dto.DepositParams;
import com.mfs.api_middleware.enumeration.ContextUrl;
import com.mfs.api_middleware.gateway.ApiManagerGateway;
import com.mfs.api_middleware.service.security.ISecurityService;
import com.mfs.api_middleware.util.CommonConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Base64;
import java.util.Locale;
import java.util.regex.Pattern;

@Service
public class DepositServiceImpl extends ProcessRequestService implements DepositService {
    @Autowired
    ApiManagerGateway apiManagerGateway;

    @Autowired
    ISecurityService iSecurityService;

    @Autowired
    ApplicationProperties applicationProperties;

    @Override
    public CommonResponse depositMoney(DepositParams depositParams, String accountNo, Locale locale) {
        if (Pattern.matches("......==", depositParams.getPin())) {
            String formattedText = "TRUSTMM FCCO " + depositParams.getAccountNumber() + " " + depositParams.getAmount() + " " + new String(Base64.getDecoder().decode(depositParams.getPin()));
            ApiManagerRequest apiManagerRequest = makeProcessedRequest(applicationProperties.getEncryptionEnabled() ?
                    iSecurityService.encrypt(formattedText, "", "") : formattedText, ContextUrl.API_MANAGER_CONTEXT.getUrl(), accountNo);

            CommonResponse response = getTransactionFormattedResponse(apiManagerGateway, apiManagerRequest, locale, true);
            return response;
        } else {
            throw new DecodePinException(CommonConstant.DECODE_EXCEPTION_MESSAGE);
        }

    }
}
