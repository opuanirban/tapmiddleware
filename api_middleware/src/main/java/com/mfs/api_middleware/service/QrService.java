package com.mfs.api_middleware.service;

import com.mfs.api_middleware.dto.MerchantInfoResponse;

public interface QrService {
    public MerchantInfoResponse getMerchantInfo(String id,String secret);
}
