package com.mfs.api_middleware.service;

import com.mfs.api_middleware.Exception.DecodePinException;
import com.mfs.api_middleware.Exception.TransactionProcessException;
import com.mfs.api_middleware.config.ApplicationProperties;
import com.mfs.api_middleware.dto.ApiManagerRequest;
import com.mfs.api_middleware.dto.CashOutRequest;
import com.mfs.api_middleware.dto.CommonResponse;
import com.mfs.api_middleware.enumeration.ContextUrl;
import com.mfs.api_middleware.enumeration.TransactionEvents;
import com.mfs.api_middleware.gateway.ApiManagerGateway;
import com.mfs.api_middleware.service.security.ISecurityService;
import com.mfs.api_middleware.util.CommonConstant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Base64;
import java.util.Locale;
import java.util.regex.Pattern;

@Service
@Slf4j
public class CashOutServiceImpl extends ProcessRequestService implements CashOutService {

    @Autowired
    ApiManagerGateway apiManagerGateway;

    @Autowired
    ISecurityService iSecurityService;

    @Autowired
    ApplicationProperties applicationProperties;

    @Autowired
    TransactionProcessService transactionProcessService;

    @Override
    public CommonResponse sendCashOut(CashOutRequest cashOutRequest, String accountNo, Locale locale) {

        if (applicationProperties.getCheckEnabled()) {

            return proceedCashout(cashOutRequest, accountNo, locale);

        } else {
            return proceedCashout(cashOutRequest, accountNo, locale);
        }

    }

    private CommonResponse proceedCashout(CashOutRequest cashOutRequest, String accountNo, Locale locale) {
        if (Pattern.matches("......==", cashOutRequest.getPin())) {
            String formattedText = "TRUSTMM RC " + CommonConstant.checkNumber(cashOutRequest.getAgentAccountNo()) + " " + cashOutRequest.getAmount() + " " + new String(Base64.getDecoder().decode(cashOutRequest.getPin()));
            ApiManagerRequest apiManagerRequest = makeProcessedRequest(applicationProperties.getEncryptionEnabled() ?
                    iSecurityService.encrypt(formattedText, "", "") : formattedText, ContextUrl.API_MANAGER_CONTEXT.getUrl(), accountNo);

            CommonResponse response = getTransactionFormattedResponse(apiManagerGateway, apiManagerRequest, locale, true);

            return response;

        } else {
            throw new DecodePinException(CommonConstant.DECODE_EXCEPTION_MESSAGE);
        }

    }
}
