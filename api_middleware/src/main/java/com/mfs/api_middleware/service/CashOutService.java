package com.mfs.api_middleware.service;

import com.mfs.api_middleware.dto.CashOutRequest;
import com.mfs.api_middleware.dto.CommonResponse;

import java.util.Locale;

public interface CashOutService {

    public CommonResponse sendCashOut(CashOutRequest cashOutRequest, String accountNo, Locale locale);
}
