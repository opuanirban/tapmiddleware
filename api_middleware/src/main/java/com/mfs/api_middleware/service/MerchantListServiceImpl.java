package com.mfs.api_middleware.service;


import com.mfs.api_middleware.dto.DataJson;
import com.mfs.api_middleware.dto.MerchantData;
import com.mfs.api_middleware.dto.MerchantDataListResponse;
import com.mfs.api_middleware.dto.MerchantInfoFetchData;
import com.mfs.api_middleware.dto.MerchantListRespone;
import com.mfs.api_middleware.gateway.ApiManagerGateway;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@Slf4j
@Service
public class MerchantListServiceImpl implements MerchantListService {
    @Autowired
    ApiManagerGateway apiManagerGateway;
    @Autowired
    DataFetchService dataFetchService;

    @Override
    public MerchantDataListResponse responseMerchantList() {
        MerchantDataListResponse merchantDataListResponse = new MerchantDataListResponse();
        List<MerchantData> merchantDataList = new ArrayList<>();
        MerchantListRespone merchantListRespone = apiManagerGateway.getMerchantListResponse();
        int id = 0;

        Map<String,Integer> donationMerchant = new HashMap<>();
        DataJson[] donationList = dataFetchService.getDonations(Locale.getDefault());
        for (int index = 0; index < donationList.length; index++) {
            donationMerchant.put(donationList[index].getCode(), 1);    
        }

        for(MerchantInfoFetchData data: merchantListRespone.getUsers()){
            String merchantName = data.getFullName().toLowerCase();
            String merchantNumber = String.valueOf(data.getUserNumber());
            String merchantNumberActual = merchantNumber.startsWith("88")?merchantNumber.substring(2):merchantNumber;
            if(merchantName.contains("test") || !merchantNumberActual.startsWith("01") || donationMerchant.get(merchantNumberActual) != null) {
                continue;
            }
            MerchantData merchantData = new MerchantData();
            merchantData.setId(Long.valueOf(++id));
            merchantData.setMerchantNumber(String.valueOf(data.getUserNumber()));
            merchantData.setMerchantName(data.getFullName());

            merchantDataList.add(merchantData);
        }
        merchantDataListResponse.setMerchantDataList(merchantDataList);
        return merchantDataListResponse;
    }
}
