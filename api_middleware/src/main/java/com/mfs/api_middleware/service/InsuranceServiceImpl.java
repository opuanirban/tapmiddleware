package com.mfs.api_middleware.service;

import com.mfs.api_middleware.Exception.ApiException;
import com.mfs.api_middleware.Exception.ApiRequestException;
import com.mfs.api_middleware.Exception.DecodePinException;
import com.mfs.api_middleware.Exception.TransactionProcessException;
import com.mfs.api_middleware.config.ApplicationProperties;
import com.mfs.api_middleware.dto.ApiManagerRequest;
import com.mfs.api_middleware.dto.CommonResponse;
import com.mfs.api_middleware.dto.InsurancePaymentRequest;
import com.mfs.api_middleware.dto.PrimeLifeInsuranceBillPaymentRequest;
import com.mfs.api_middleware.enumeration.ContextUrl;
import com.mfs.api_middleware.enumeration.TransactionEvents;
import com.mfs.api_middleware.gateway.ApiManagerGateway;
import com.mfs.api_middleware.gateway.InsuranceServiceGateway;
import com.mfs.api_middleware.gateway.model.BaseResponse;
import com.mfs.api_middleware.gateway.model.BillPaymentResponse;
import com.mfs.api_middleware.gateway.nesco.model.BillPaymentInput;
import com.mfs.api_middleware.service.security.ISecurityService;
import com.mfs.api_middleware.util.CommonConstant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Base64;
import java.util.Locale;
import java.util.regex.Pattern;

@Service
@Slf4j
public class InsuranceServiceImpl extends ProcessRequestService implements InsuranceService {

    @Autowired
    ApiManagerGateway apiManagerGateway;

    @Autowired
    ISecurityService iSecurityService;

    @Autowired
    ApplicationProperties applicationProperties;

    @Autowired
    TransactionProcessService transactionProcessService;

    @Autowired
    InsuranceServiceGateway insuranceServiceGateway;



    @Override
    public CommonResponse insurancePayment(InsurancePaymentRequest insurancePaymentRequest, String accountNo, Locale locale) {

        if (applicationProperties.getCheckEnabled()) {
            if (transactionProcessService.findProcess(accountNo, insurancePaymentRequest.getAmount(), insurancePaymentRequest.getPolicyNo(), TransactionEvents.INSURANCE+insurancePaymentRequest.getCode()
                    +insurancePaymentRequest.getPolicyNo())) {
                log.error("Same transaction within 10 minutes");
                throw new TransactionProcessException("User not allowed to do the same transaction within " +
                        applicationProperties.getDuplicateReqTimeOutInMills() / 60000 + " minutes of last transaction");
            } else {
                return doInsurancePayment(insurancePaymentRequest, accountNo, locale);
            }
        } else {
            return doInsurancePayment(insurancePaymentRequest, accountNo, locale);
        }

    }

    @Override
    public CommonResponse primeLifeInsurancePayment(PrimeLifeInsuranceBillPaymentRequest request
            ,String msisdn,
            Locale locale) {
        CommonResponse response = new CommonResponse();
        BillPaymentInput input = new BillPaymentInput();
        input.setBillerCode(request.getBillerCode());
        input.setPaymentChannel("APP");
        input.setAccountNumber(CommonConstant.checkNumber(msisdn));
        input.setPin(request.getPin());
        String notificationNumber  = request.getNotificationNumber() == null || request.getNotificationNumber() == ""
                ? CommonConstant.checkNumber(msisdn) : request.getNotificationNumber();
        input.setNotificationNumber(notificationNumber);
        input.setKey1(request.getPolicyNumber());
        input.setKey2(request.getBillAmount());
        input.setKey3(request.getTransactionId());
        try {
            BillPaymentResponse billPaymentResponse = insuranceServiceGateway.payPrimeLifeInsuranceBill(input);
            BaseResponse baseResponse = billPaymentResponse.getResponse();
            if(baseResponse != null) {
                if (baseResponse.getResponseCode().equals(CommonConstant.SUCCESS_CODE)) {
                    response.setMessage(baseResponse.getResponseMessage());
                    return response;
                } else {
                    throw new ApiRequestException(baseResponse.getResponseMessage());
                }
            }else {
                throw new ApiRequestException(CommonConstant.COMMON_ERROR);
            }

        }catch (Exception ex){
            throw new ApiRequestException(ex.getMessage());
        }
//        BillPaymentResponse response = insuranceServiceGateway.

    }


    public CommonResponse doInsurancePayment(InsurancePaymentRequest insurancePaymentRequest, String accountNo, Locale locale) {

        if (Pattern.matches("......==", insurancePaymentRequest.getPin())) {
            String formattedText = "TRUSTMM " + insurancePaymentRequest.getCode() + " " + insurancePaymentRequest.getPolicyNo() + " " + insurancePaymentRequest.getAmount() + " " + new String(Base64.getDecoder().decode(insurancePaymentRequest.getPin())) + " " + CommonConstant.checkNumber(insurancePaymentRequest.getNotificationNumber());
            if(insurancePaymentRequest.getCode().equals("MA")) {
                String policyNo = insurancePaymentRequest.getPolicyNo();
                policyNo = policyNo.toUpperCase();
                policyNo = policyNo.replaceAll("\\s", "");
                insurancePaymentRequest.setPolicyNo(policyNo);

                if(insurancePaymentRequest.getPolicyType() != null && !insurancePaymentRequest.getPolicyType().isEmpty()){
                    formattedText = formattedText + " " + insurancePaymentRequest.getPolicyType().replace(" ", "_");
                } else {
                    formattedText = formattedText + " n/a";
                }
            }
            
            ApiManagerRequest apiManagerRequest = makeProcessedRequest(applicationProperties.getEncryptionEnabled() ?
                    iSecurityService.encrypt(formattedText, "", "") : formattedText, ContextUrl.API_MANAGER_CONTEXT.getUrl(), accountNo);

            CommonResponse response = getTransactionFormattedResponse(apiManagerGateway, apiManagerRequest, locale, true);
            try {
                transactionProcessService.storeProcess(accountNo, insurancePaymentRequest.getAmount(),insurancePaymentRequest.getPolicyNo(), TransactionEvents.INSURANCE+insurancePaymentRequest.getCode()
                        +insurancePaymentRequest.getPolicyNo());
            } catch (Exception e) {
                log.error(e.getMessage());
            }
            return response;

        } else {
            throw new DecodePinException(CommonConstant.DECODE_EXCEPTION_MESSAGE);
        }


    }
}
