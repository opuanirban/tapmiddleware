package com.mfs.api_middleware.service;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import javax.validation.Valid;

import com.mfs.api_middleware.Exception.ApiRequestException;
import com.mfs.api_middleware.dto.*;
import com.mfs.api_middleware.gateway.InternetServiceGateway;
import com.mfs.api_middleware.gateway.model.BaseResponse;
import com.mfs.api_middleware.gateway.model.BillFetchResponse;
import com.mfs.api_middleware.gateway.model.BillPaymentResponse;
import com.mfs.api_middleware.gateway.model.BillPreviewRestInput;
import com.mfs.api_middleware.gateway.nesco.model.BillPaymentInput;
import com.mfs.api_middleware.util.CommonConstant;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
//import sun.jvm.hotspot.debugger.cdbg.LoadObjectComparator;

@Service
@Slf4j
public class InternetServiceImpl implements InternetService{
    @Autowired
    InternetServiceGateway internetServiceGateway;

    @Override
    public CarnivalBillInfoResponse fetchCarnivalBillDetails(String carnivalCustomerId, String mfsAccountNumber) {
        CarnivalBillInfoResponse billInfoResponse = new CarnivalBillInfoResponse();

        BillPreviewRestInput input = new BillPreviewRestInput();
        input.setAccountNumber(mfsAccountNumber);
        input.setBillerId("CARNIVAL");
        input.setCode("CARNIVAL");
        input.setKey1(carnivalCustomerId);
        try {
            BillFetchResponse billFetchResponse = internetServiceGateway.fetchCarnivalBill(input);
            BaseResponse baseResponse = billFetchResponse.getResponse();
            if (baseResponse != null) {
                if (baseResponse.getResponseCode().equals(CommonConstant.SUCCESS_CODE)) {
                    billInfoResponse.setBillAmount(billFetchResponse.getBillAmount());
                    billInfoResponse.setTransactionId(String.valueOf(billFetchResponse.getPayload().get("key3"))); // key3 for transaction id

                    List<HashMap<String, Object>> tableDatas = billFetchResponse.getTableData();
                    for (int i = 0; i < tableDatas.size(); i++) {
                        HashMap<String, Object> item = tableDatas.get(i);
                        if (item.get("key").equals("customerName")) {
                            billInfoResponse.setAccountName(String.valueOf(item.get("value")));
                        } else if (item.get("key").equals("customerNumber")) {
                            billInfoResponse.setAccountId(String.valueOf(item.get("value")));
                        }
                    }

                    return billInfoResponse;
                } else {
                    throw new ApiRequestException(baseResponse.getResponseMessage());
                }
            } else {
                throw new ApiRequestException(CommonConstant.COMMON_ERROR);
            }
        } catch (Exception e) {
            throw new ApiRequestException(e.getMessage());
        }        
    }

    @Override
    public CommonResponse payCarnivalBill(@Valid CarnivalBillPaymentRequest billPaymentRequest, String mfsAccountNumber) {
        if(billPaymentRequest.getBillAmount().intValue() < 10) {
            throw new ApiRequestException("Invalid amount. Minimum amount is BDT 10.0");
        } else if(billPaymentRequest.getBillAmount().intValue() > 10000) {
            throw new ApiRequestException("Invalid amount. Maximum amount is BDT 10000.0");
        }

        CommonResponse commonResponse = new CommonResponse();
        // populate bill payment request
        BillPaymentInput billPaymentInput = new BillPaymentInput();
        billPaymentInput.setAccountNumber(mfsAccountNumber);
        billPaymentInput.setBillerCode("CARNIVAL");
        billPaymentInput.setKey1(billPaymentRequest.getCarnivalCustomerId());
        billPaymentInput.setKey2(String.valueOf(billPaymentRequest.getBillAmount()));
        billPaymentInput.setKey3(billPaymentRequest.getTransactionId());
        billPaymentInput.setNotificationNumber(billPaymentRequest.getNotificationNumber());
        billPaymentInput.setPin(billPaymentRequest.getPin());
        billPaymentInput.setPaymentChannel(CommonConstant.REQUEST_CHANNEL_APP);
        
        BillPaymentResponse billPaymentResponse = internetServiceGateway.payCarnivalBill(billPaymentInput);
        log.debug("JGTDSL response: {}", billPaymentResponse);
        if(!billPaymentResponse.getResponse().getResponseCode().equals(CommonConstant.SUCCESS_CODE)) {
            throw new ApiRequestException(billPaymentResponse.getResponse().getResponseMessage());
        }

        commonResponse.setMessage(billPaymentResponse.getResponse().getResponseMessage());
        return commonResponse;

    }

    @Override
    public TILBillInfoResponse fetchTilBillDetails(String tilCustomerId, String mfsAccountNumber) {
        TILBillInfoResponse billInfoResponse = new TILBillInfoResponse();

        BillPreviewRestInput input = new BillPreviewRestInput();
        input.setAccountNumber(mfsAccountNumber);
        input.setBillerId("TIL");
        input.setCode("TIL");
        input.setKey1(tilCustomerId);
        try {
            BillFetchResponse billFetchResponse = internetServiceGateway.fetchTilBill(input);
            BaseResponse baseResponse = billFetchResponse.getResponse();
            if(baseResponse == null) throw new ApiRequestException(CommonConstant.COMMON_ERROR);
            if(!baseResponse.getResponseCode().equals(CommonConstant.SUCCESS_CODE))
                throw new ApiRequestException(baseResponse.getResponseMessage());

            billInfoResponse.setBillAmount(billFetchResponse.getBillAmount());
            billInfoResponse.setTransactionId(String.valueOf(billFetchResponse.getResponse().getTransactionId()));
            List<HashMap<String, Object>> tableDatas = billFetchResponse.getTableData();
            for(int i = 0; i < tableDatas.size();i++){
                HashMap<String,Object> item = tableDatas.get(i);
                if(item.get("key").equals("customerName")){
                    billInfoResponse.setCustomerName(String.valueOf(item.get("value")));
                }else if (item.get("key").equals("customerNumber")) {
                    billInfoResponse.setCustomerNumber(String.valueOf(item.get("value")));
                }
            }

        } catch (Exception e) {
            throw new ApiRequestException(e.getMessage());
        }
        return billInfoResponse;
    }

    @Override
    public CommonResponse payTilBill(TilBillPaymentRequest billPaymentRequest, String mfsAccountNumber) {
        if(billPaymentRequest.getBillAmount().intValue() < 10) {
            throw new ApiRequestException("Invalid amount. Minimum amount is BDT 10.0");
        } else if(billPaymentRequest.getBillAmount().intValue() > 10000) {
            throw new ApiRequestException("Invalid amount. Maximum amount is BDT 10000.0");
        }

        CommonResponse commonResponse = new CommonResponse();
        // populate bill payment request
        BillPaymentInput billPaymentInput = new BillPaymentInput();
        billPaymentInput.setBillerCode("TIL");
        billPaymentInput.setPaymentChannel(CommonConstant.REQUEST_CHANNEL_APP);
        billPaymentInput.setAccountNumber(mfsAccountNumber);
        billPaymentInput.setPin(billPaymentRequest.getPin());
        billPaymentInput.setNotificationNumber(billPaymentRequest.getNotificationNumber());
        billPaymentInput.setKey2(billPaymentRequest.getTransactionId());

        BillPaymentResponse billPaymentResponse = internetServiceGateway.payTilBill(billPaymentInput);
        log.debug("TIL response: {}", billPaymentResponse);
        if(!billPaymentResponse.getResponse().getResponseCode().equals(CommonConstant.SUCCESS_CODE)) {
            throw new ApiRequestException(billPaymentResponse.getResponse().getResponseMessage());
        }

        commonResponse.setMessage(billPaymentResponse.getResponse().getResponseMessage());
        return commonResponse;
    }


}
