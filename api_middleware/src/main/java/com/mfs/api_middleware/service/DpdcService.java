package com.mfs.api_middleware.service;

import com.mfs.api_middleware.dto.CommonResponse;
import com.mfs.api_middleware.dto.DPDCBillPayRequest;

import java.util.Locale;

public interface DpdcService {
    public CommonResponse payDpdcBill(DPDCBillPayRequest dpdcBillPayRequest,String accountNo, Locale locale);
}
