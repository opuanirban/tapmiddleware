package com.mfs.api_middleware.service;

import com.mfs.api_middleware.dto.MerchantDataListResponse;
import org.springframework.stereotype.Service;

@Service
public interface MerchantListService {
    MerchantDataListResponse responseMerchantList();
}
