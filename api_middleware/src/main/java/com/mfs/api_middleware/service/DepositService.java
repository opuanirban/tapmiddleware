package com.mfs.api_middleware.service;

import com.mfs.api_middleware.dto.CommonResponse;
import com.mfs.api_middleware.dto.DepositParams;

import java.util.Locale;

public interface DepositService {
    public CommonResponse depositMoney(DepositParams depositParams, String accountNo, Locale locale);
}
