package com.mfs.api_middleware.service;

import java.util.Locale;

import com.mfs.api_middleware.Exception.ApiRequestException;
import com.mfs.api_middleware.dto.AkashPaymentRequest;
import com.mfs.api_middleware.dto.CommonResponse;
import com.mfs.api_middleware.gateway.TvServiceGateway;
import com.mfs.api_middleware.gateway.model.BillPaymentInput;
import com.mfs.api_middleware.gateway.model.BillPaymentResponse;
import com.mfs.api_middleware.util.CommonConstant;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class TvServiceImpl implements TvService {
    @Autowired
    TvServiceGateway akashServiceGateway;
    
    @Override
    public CommonResponse payAkashService(AkashPaymentRequest request, String accountNumber, Locale locale) {
        CommonResponse commonResponse = new CommonResponse();
        // populate bill payment request
        BillPaymentInput billPaymentInput = new BillPaymentInput();
        billPaymentInput.setAccountNumber(accountNumber);
        billPaymentInput.setBillerCode("AKASH");
        billPaymentInput.setKey1(request.getAkashAccountNumber());
        billPaymentInput.setKey2(String.valueOf(request.getBillAmount()));
        billPaymentInput.setNotificationNumber(request.getNotificationNumber());
        billPaymentInput.setPin(request.getPin());
        billPaymentInput.setPaymentChannel(CommonConstant.REQUEST_CHANNEL_APP);
        
        BillPaymentResponse billPaymentResponse = akashServiceGateway.paymentAkashBill(billPaymentInput);
        if(!billPaymentResponse.getResponse().getResponseCode().equals(CommonConstant.SUCCESS_CODE)) {
            throw new ApiRequestException(billPaymentResponse.getResponse().getResponseMessage());
        }

        commonResponse.setMessage(billPaymentResponse.getResponse().getResponseMessage());
        return commonResponse;
    }
    
}
