package com.mfs.api_middleware.service;

import com.mfs.api_middleware.dto.CommonResponse;
import com.mfs.api_middleware.dto.PinInfo;

import java.util.Locale;

public interface PinUpdateService {
    public CommonResponse update_pin(String msisdn, PinInfo pinInfo, Locale locale);
}
