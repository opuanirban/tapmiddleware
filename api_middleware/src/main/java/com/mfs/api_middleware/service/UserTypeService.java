package com.mfs.api_middleware.service;

import com.mfs.api_middleware.dto.UserTypeResponseWallet;

import java.util.Locale;

public interface UserTypeService {
   UserTypeResponseWallet fetchUserType(String userAccount, Locale locale);
}
