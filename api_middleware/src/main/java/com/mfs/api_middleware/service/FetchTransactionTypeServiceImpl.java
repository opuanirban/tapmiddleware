package com.mfs.api_middleware.service;

import com.mfs.api_middleware.dto.FeatureConfigItem;
import com.mfs.api_middleware.dto.FeatureConfigList;
import com.mfs.api_middleware.enumeration.TransactionTypes;
import com.mfs.api_middleware.gateway.ApiManagerGateway;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Service
@Slf4j
public class FetchTransactionTypeServiceImpl implements FetchTransactionTypeService {

    @Autowired
    ApiManagerGateway apiManagerGateway;

    @Override
    public FeatureConfigList fetchTypes(Locale locale) {
//        List<TransactionTypeParams> typeList = apiManagerGateway.getTransactionType(locale);
//
//        List<TransactionTypeParams> filtered =
//                typeList.stream()
//                        .filter(e -> e.getTransactionType().contains(transactionType.getTransactionType()))
//                        .collect(Collectors.toList());

//        FeatureConfigItem code = new FeatureConfigItem();
//        code.setCode(transactionType.getTransactionType());
//        return code;

        FeatureConfigList featureConfigList = new FeatureConfigList();
        List<FeatureConfigItem> featureConfigItems = new ArrayList<>();

        for (TransactionTypes transactionTypes:TransactionTypes.values()) {
            FeatureConfigItem featureConfigItem = new FeatureConfigItem();
            featureConfigItem.setFeatureName(transactionTypes.name());
            featureConfigItem.setCode(transactionTypes.getTransactionType().split("#")[0]);
            featureConfigItem.setMinAmount(new BigDecimal(transactionTypes.getTransactionType().split("#")[1]));
            featureConfigItem.setMaxAmount(new BigDecimal(transactionTypes.getTransactionType().split("#")[2]));

            featureConfigItems.add(featureConfigItem);
        }

        featureConfigList.setFeaturesConfigList(featureConfigItems);
        return featureConfigList;

    }
}
