package com.mfs.api_middleware.service;

import java.util.Locale;

import com.mfs.api_middleware.Exception.ApiRequestException;
import com.mfs.api_middleware.dto.CommonResponse;
import com.mfs.api_middleware.dto.IvacPaymentRequest;
import com.mfs.api_middleware.dto.JGTDSLPaymentRequest;
import com.mfs.api_middleware.gateway.IvacGateway;
import com.mfs.api_middleware.gateway.JGTDSLServiceGateway;
import com.mfs.api_middleware.gateway.model.BillPaymentInput;
import com.mfs.api_middleware.gateway.model.BillPaymentResponse;
import com.mfs.api_middleware.util.CommonConstant;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class UtilityBillServiceImpl implements UtilityBillService {
    @Autowired
    JGTDSLServiceGateway jgtdslServiceGateway;

    @Autowired
    IvacGateway ivacGateway;

    @Override
    public CommonResponse payJGTDSLBill(JGTDSLPaymentRequest request, String tapAccount, Locale locale) {
        CommonResponse commonResponse = new CommonResponse();
        // populate bill payment request
        BillPaymentInput billPaymentInput = new BillPaymentInput();
        billPaymentInput.setAccountNumber(tapAccount);
        billPaymentInput.setBillerCode("JGTDSL");
        billPaymentInput.setKey1(request.getCustomerId());
        billPaymentInput.setKey2(String.valueOf(request.getBillAmount()));
        billPaymentInput.setKey3(request.getTransactionId());
        billPaymentInput.setNotificationNumber(request.getNotificationNumber());
        billPaymentInput.setPin(request.getPin());
        billPaymentInput.setPaymentChannel(CommonConstant.REQUEST_CHANNEL_APP);
        
        BillPaymentResponse billPaymentResponse = jgtdslServiceGateway.payJGTDSLBill(billPaymentInput);
        log.debug("JGTDSL response: {}", billPaymentResponse);
        if(!billPaymentResponse.getResponse().getResponseCode().equals(CommonConstant.SUCCESS_CODE)) {
            throw new ApiRequestException(billPaymentResponse.getResponse().getResponseMessage());
        }

        commonResponse.setMessage(billPaymentResponse.getResponse().getResponseMessage());
        return commonResponse;
    }

    @Override
    public CommonResponse payIvacBill(IvacPaymentRequest request, String tapAccount, Locale locale) {
        CommonResponse commonResponse = new CommonResponse();
        BillPaymentInput input = new BillPaymentInput();
        input.setAccountNumber(tapAccount);
        input.setBillerCode("IVAC");
        input.setPaymentChannel("APP");
        input.setPin(request.getPin());
        input.setNotificationNumber(request.getNotificationNumber());
        input.setKey6(request.getTransactionId());

        BillPaymentResponse billPaymentResponse = ivacGateway.paymentIvacBill(input);
        log.debug("IVAC response: {}", billPaymentResponse);

        if(!billPaymentResponse.getResponse().getResponseCode().equals(CommonConstant.SUCCESS_CODE)){
            throw new ApiRequestException(billPaymentResponse.getResponse().getResponseMessage());
        }

        commonResponse.setMessage(billPaymentResponse.getResponse().getResponseMessage());
        return commonResponse;
    }

}
