package com.mfs.api_middleware.service.security;

public interface ISecurityService {
    String encrypt(String param1, String param2, String param3);
    String decrypt(String param1, String param2, String param3);
}
