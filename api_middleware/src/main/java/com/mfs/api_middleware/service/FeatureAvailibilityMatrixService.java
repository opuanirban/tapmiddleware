package com.mfs.api_middleware.service;

import com.mfs.api_middleware.dto.FeatureAccesibilityMatrix;

public interface FeatureAvailibilityMatrixService {
    public FeatureAccesibilityMatrix getFeatureMatrixList(Boolean isEkycCompleted, Boolean isActive, Integer userGroupId);
}
