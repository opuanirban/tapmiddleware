package com.mfs.api_middleware.service;

import java.util.Locale;

import com.mfs.api_middleware.dto.AkashPaymentRequest;
import com.mfs.api_middleware.dto.CommonResponse;

public interface TvService {
    CommonResponse payAkashService(AkashPaymentRequest request, String accountNumber, Locale locale);
}
