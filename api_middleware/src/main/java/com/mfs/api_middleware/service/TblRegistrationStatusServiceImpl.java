package com.mfs.api_middleware.service;

import com.mfs.api_middleware.config.ApplicationProperties;
import com.mfs.api_middleware.dto.*;
import com.mfs.api_middleware.enumeration.GenderEnum;
import com.mfs.api_middleware.gateway.ApiManagerGateway;
import com.mfs.api_middleware.util.CommonConstant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

@Service
@Slf4j
public class TblRegistrationStatusServiceImpl implements TblRegistrationStatusService {
    @Autowired
    ApiManagerGateway apiManagerGateway;

    @Autowired
    ApplicationProperties applicationProperties;

    @Override
    public TblEkycUpdateResponse passDataToTbl(EkycNotificationRequest data, Locale locale) {
        RegistrationParams details = new RegistrationParams();
        TblEkycUpdateResponse tblEkycUpdateResponse = new TblEkycUpdateResponse();
        if (data.getStatus().equalsIgnoreCase(CommonConstant.PASSED)) {
            details.setBanglaName(data.getDetail().getApplicant_name_ben());
            details.setEnglishName(data.getDetail().getApplicant_name_eng());
            if (data.getDetail().getFather_name()==null) {
                details.setFatherName("");
            } else {
                details.setFatherName(data.getDetail().getFather_name());
            }
            if (data.getDetail().getMother_name()==null) {
                details.setMotherName("");
            } else {
                details.setMotherName(data.getDetail().getMother_name());
            }
            if (data.getDetail().getSpouse_name()==null) {
                details.setSpouseName("");
            } else {
                details.setSpouseName(data.getDetail().getSpouse_name());
            }
            if (data.getDetail().getDob() != null || !data.getDetail().getDob().isEmpty()) {
                try {
                    Date date = new SimpleDateFormat("yyyy/MM/dd").parse(data.getDetail().getDob());
                    details.setDoB(new SimpleDateFormat("yyyy-MM-dd").format(date));
                } catch (ParseException e) {
                    log.error(e.getMessage());
                }
            }
            details.setUserKey(Long.valueOf(0));
            if(data.getDetail().getGender() != null) {
                if (data.getDetail().getGender().equalsIgnoreCase(GenderEnum.F.getGenderGroup())) {
                    details.setSex(GenderEnum.F.name());
                } else if (data.getDetail().getGender().equalsIgnoreCase(GenderEnum.M.getGenderGroup())) {
                    details.setSex(GenderEnum.M.name());
                } else if (data.getDetail().getGender().equalsIgnoreCase(GenderEnum.O.getGenderGroup())) {
                    details.setSex(GenderEnum.O.name());
                }
            }
            details.setOccupation(data.getDetail().getProfession());
            details.setUserNumber(CommonConstant.checkNumber(data.getDetail().getMobile_number()));
            details.setNationalIdNumber(data.getDetail().getNid_no());
            details.setNominee(data.getDetail().getNominee());
            details.setNomineeRelation(data.getDetail().getNominee_relation());
            details.setPresentAddress(data.getDetail().getPres_address());
            details.setPermanentAddress(data.getDetail().getPrem_address());
//            details.setPhotoString(data.getDetail().getApplicant_photo());
            details.setIsLimitedKYC(false);
            String response = apiManagerGateway.registrationStatusUpdate(details, locale);
            log.debug(response);
            if (response.contains(applicationProperties.getSuccessPlaceholderList().get(2)) || response.contains(applicationProperties.getSuccessPlaceholderList().get(4))) {
                tblEkycUpdateResponse.setKycUpdateStatus(CommonConstant.SUCCESS);
            } else {
                tblEkycUpdateResponse.setKycUpdateStatus(CommonConstant.FAILED);
                tblEkycUpdateResponse.setKycUpdateMessage(response);
            }
        }

        return tblEkycUpdateResponse;
    }

    @Override
    public TblRegistrationStatus getKycFromTbl(String msisdn, Locale locale) {
        return apiManagerGateway.getKyc(msisdn, locale);
    }
}
