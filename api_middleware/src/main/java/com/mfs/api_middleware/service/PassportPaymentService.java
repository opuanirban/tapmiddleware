package com.mfs.api_middleware.service;

import com.mfs.api_middleware.dto.CommonResponse;
import com.mfs.api_middleware.dto.NIDFeePayment;
import com.mfs.api_middleware.dto.NidServiceListParams;
import com.mfs.api_middleware.dto.PassportFeePayment;

import java.util.Locale;

public interface PassportPaymentService {
    public CommonResponse payPassportFee(PassportFeePayment passportNidFeePayment, String account, Locale locale);

    public CommonResponse payNIDFee(NIDFeePayment nidFeePayment, String account, Locale locale);

    public NidServiceListParams[] getNidServices(Locale locale);
}
