package com.mfs.api_middleware.service;

import com.mfs.api_middleware.dto.FeatureAccesibilityMatrix;
import com.mfs.api_middleware.dto.FeatureAccesibilityMatrixItem;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class FeatureAvailibilityMatrixServiceImpl implements FeatureAvailibilityMatrixService {

    @Value("${feature-matrix.ekyc-active}")
    private String featureMatrixEKYCActive;

    @Value("${feature-matrix.none-active}")
    private String featureMatrixNoEKYCActive;

    @Value("${feature-matrix.none-unauthorized}")
    private String featureMatrixNoEKYCUnAuthorized;


    @Override
    public FeatureAccesibilityMatrix getFeatureMatrixList(Boolean isEkycCompleted, Boolean isActive, Integer userGroupId) {
        FeatureAccesibilityMatrix featureAccesibilityMatrix = new FeatureAccesibilityMatrix();
        List<FeatureAccesibilityMatrixItem> featureItemList = new ArrayList<>();

        String[] featureList = null;
        if(isEkycCompleted && isActive) {
            featureList = featureMatrixEKYCActive.split("#");
        }else if(!isEkycCompleted && isActive){
            featureList = featureMatrixNoEKYCActive.split("#");
        }else{
            featureList = featureMatrixNoEKYCUnAuthorized.split("#");
        }

        for (String featureItem:featureList) {
            FeatureAccesibilityMatrixItem featureAccesibilityMatrixItem = new FeatureAccesibilityMatrixItem();
            featureAccesibilityMatrixItem.setFeatureName(featureItem.split(":")[0]);
            if(userGroupId != null && userGroupId == 2 && featureAccesibilityMatrixItem.getFeatureName().contains("TOP_UP")) {
                featureAccesibilityMatrixItem.setFeatureAccess(true);   // TODO false
            } else {
                featureAccesibilityMatrixItem.setFeatureAccess(Integer.parseInt(featureItem.split(":")[1]) == 1);
            }
            featureItemList.add(featureAccesibilityMatrixItem);
        }

        featureAccesibilityMatrix.setFeatureList(featureItemList);

        return featureAccesibilityMatrix;
    }
}
