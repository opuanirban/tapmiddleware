package com.mfs.api_middleware.service;

import com.mfs.api_middleware.dto.CommonResponse;
import com.mfs.api_middleware.dto.TopUpBundleRequest;
import com.mfs.api_middleware.dto.TopUpRequest;
import com.mfs.api_middleware.gateway.topup.BundlePlanMobileDTO;

import java.util.List;
import java.util.Locale;

public interface TopUpService {

    public CommonResponse sendTopUp(TopUpRequest topUpRequest, String accountNo, Locale locale);
    public List<BundlePlanMobileDTO> bundleSearch(TopUpBundleRequest topUpRequest);
}
