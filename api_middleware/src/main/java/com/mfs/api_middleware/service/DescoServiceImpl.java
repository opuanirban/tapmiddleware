package com.mfs.api_middleware.service;

import com.mfs.api_middleware.Exception.ApiRequestException;
import com.mfs.api_middleware.Exception.DecodePinException;
import com.mfs.api_middleware.Exception.TransactionProcessException;
import com.mfs.api_middleware.config.ApplicationProperties;
import com.mfs.api_middleware.dto.ApiManagerRequest;
import com.mfs.api_middleware.dto.CommonResponse;
import com.mfs.api_middleware.dto.DescoPostPaidRequest;
import com.mfs.api_middleware.dto.DescoPrepaidRequest;
import com.mfs.api_middleware.enumeration.ContextUrl;
import com.mfs.api_middleware.enumeration.TransactionEvents;
import com.mfs.api_middleware.gateway.ApiManagerGateway;
import com.mfs.api_middleware.gateway.desco.DescoServiceGateway;
import com.mfs.api_middleware.gateway.nesco.model.BillPaymentInput;
import com.mfs.api_middleware.gateway.nesco.model.BillPaymentResponse;
import com.mfs.api_middleware.service.security.ISecurityService;
import com.mfs.api_middleware.util.CommonConstant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Base64;
import java.util.Locale;
import java.util.regex.Pattern;

//import com.sun.org.apache.bcel.internal.generic.LLOAD;

@Service
@Slf4j
public class DescoServiceImpl extends ProcessRequestService implements DescoService {

    @Autowired
    ApiManagerGateway apiManagerGateway;

    @Autowired
    ISecurityService iSecurityService;

    @Autowired
    ApplicationProperties applicationProperties;

    @Autowired
    TransactionProcessService transactionProcessService;

    @Autowired
    DescoServiceGateway descoServiceGateway;

    @Override
    public CommonResponse payDescoBill(DescoPrepaidRequest prepaidRequest, String accountNo, Locale locale) {
        if (applicationProperties.getCheckEnabled()) {
            if (transactionProcessService.findProcess(accountNo, prepaidRequest.getBillAmount(),prepaidRequest.getMeterNumber(), TransactionEvents.DESCO_PREPAID.toString())) {
                log.error("Same transaction within 10 minutes");
                throw new TransactionProcessException("User not allowed to do the same transaction within " +
                        applicationProperties.getDuplicateReqTimeOutInMills() / 60000 + " minutes of last transaction");
            } else {
                if(prepaidRequest.getBillType().equals("UNIFIED")) {
                    return doPayDescoBillUnified(prepaidRequest, accountNo, locale);
                } else {
                    return doPayDescoBill(prepaidRequest, accountNo, locale);
                }
            }
        } else {
            if(prepaidRequest.getBillType().equals("UNIFIED")) {
                return doPayDescoBillUnified(prepaidRequest, accountNo, locale);
            } else {
                return doPayDescoBill(prepaidRequest, accountNo, locale);
            }
        }
    }


    public CommonResponse doPayDescoBill(DescoPrepaidRequest prepaidRequest, String accountNo, Locale locale) {
        if (Pattern.matches("......==", prepaidRequest.getPin())) {
            String formattedText = "TRUSTMM DESPR " + prepaidRequest.getMeterNumber() + " " + prepaidRequest.getBillAmount() + " " + new String(Base64.getDecoder().decode(prepaidRequest.getPin())) + " " + CommonConstant.checkNumber(prepaidRequest.getNotificationNumber());
            ApiManagerRequest apiManagerRequest = makeProcessedRequest(applicationProperties.getEncryptionEnabled() ?
                    iSecurityService.encrypt(formattedText, "", "") : formattedText, ContextUrl.API_MANAGER_CONTEXT.getUrl(), accountNo);

            CommonResponse response = getTransactionFormattedResponse(apiManagerGateway, apiManagerRequest, locale, true);

            try {
                transactionProcessService.storeProcess(accountNo, prepaidRequest.getBillAmount(),prepaidRequest.getMeterNumber(), TransactionEvents.DESCO_PREPAID.toString());
            } catch (Exception e) {
                log.error(e.getMessage());
            }
            return response;

        } else {
            throw new DecodePinException(CommonConstant.DECODE_EXCEPTION_MESSAGE);
        }
    }

    public CommonResponse doPayDescoBillUnified(DescoPrepaidRequest prepaidRequest, String accountNo, Locale locale) {
        CommonResponse commonResponse = new CommonResponse();
        BillPaymentInput request = new BillPaymentInput();
        request.setAccountNumber(accountNo);
        request.setBillerCode("DESPR");
        request.setFee("0");
        request.setKey1(prepaidRequest.getMeterNumber());
        request.setKey3(String.valueOf(prepaidRequest.getBillAmount()));
        request.setKey4(prepaidRequest.getTransactionId());
        request.setNotificationNumber(prepaidRequest.getNotificationNumber());
        request.setPaymentChannel("APP");
        request.setPin(prepaidRequest.getPin());
        BillPaymentResponse response = descoServiceGateway.paymentDescoPrepaidUnifiedBill(request);
        if(response.getResponse().getResponseCode().equals(CommonConstant.SUCCESS_CODE)) {
            commonResponse.setMessage(response.getResponse().getResponseMessage());
        } else {
            throw new ApiRequestException(response.getResponse().getResponseMessage());
        }
        return commonResponse;
    }

    @Override
    public CommonResponse payDescoBill(DescoPostPaidRequest postPaidRequest, String accountNo, Locale locale) {
        if (applicationProperties.getCheckEnabled()) {
            if (transactionProcessService.findProcess(accountNo, new BigDecimal(0),postPaidRequest.getBillNumber(), TransactionEvents.DESCO_POSTPAID.toString())) {
                log.error("Same transaction within 10 minutes");
                throw new TransactionProcessException("User not allowed to do the same transaction within " +
                        applicationProperties.getDuplicateReqTimeOutInMills() / 60000 + " minutes of last transaction");
            } else {
                return doPayDescoBill(postPaidRequest, accountNo, locale);
            }
        } else {
            return doPayDescoBill(postPaidRequest, accountNo, locale);
        }

    }

    public CommonResponse doPayDescoBill(DescoPostPaidRequest postPaidRequest, String accountNo, Locale locale) {
        if (Pattern.matches("......==", postPaidRequest.getPin())) {
            String formattedText = "TRUSTMM DESCO " + postPaidRequest.getBillNumber() + " " + new String(Base64.getDecoder().decode(postPaidRequest.getPin())) + " " + CommonConstant.checkNumber(postPaidRequest.getNotificationNumber());
            ApiManagerRequest apiManagerRequest = makeProcessedRequest(applicationProperties.getEncryptionEnabled() ?
                    iSecurityService.encrypt(formattedText, "", "") : formattedText, ContextUrl.API_MANAGER_CONTEXT.getUrl(), accountNo);

            CommonResponse response = getTransactionFormattedResponse(apiManagerGateway, apiManagerRequest, locale, true);

            try {
                transactionProcessService.storeProcess(accountNo, new BigDecimal(0),postPaidRequest.getBillNumber(), TransactionEvents.DESCO_POSTPAID.toString());
            } catch (Exception e) {
                log.error(e.getMessage());
            }
            return response;

        } else {
            throw new DecodePinException(CommonConstant.DECODE_EXCEPTION_MESSAGE);
        }


    }
}
