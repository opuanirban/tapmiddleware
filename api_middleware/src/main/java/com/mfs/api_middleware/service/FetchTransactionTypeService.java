package com.mfs.api_middleware.service;

import com.mfs.api_middleware.dto.FeatureConfigList;

import java.util.Locale;

public interface FetchTransactionTypeService {
    public FeatureConfigList fetchTypes(Locale locale);
}
