package com.mfs.api_middleware.service;

import com.mfs.api_middleware.dto.CommonResponse;
import com.mfs.api_middleware.dto.DescoPostPaidRequest;
import com.mfs.api_middleware.dto.DescoPrepaidRequest;

import java.util.Locale;

public interface DescoService {
    public CommonResponse payDescoBill(DescoPrepaidRequest prepaidRequest, String accountNo, Locale locale);

    public CommonResponse payDescoBill(DescoPostPaidRequest postPaidRequest,String accountNo,Locale locale);
}
