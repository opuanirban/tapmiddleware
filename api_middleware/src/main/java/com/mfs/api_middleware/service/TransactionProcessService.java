package com.mfs.api_middleware.service;

import java.math.BigDecimal;

public interface TransactionProcessService {
    String storeProcess(String userNo, BigDecimal amount, String event);
    String storeProcess(String userNo, BigDecimal amount,String referenceNo, String event);
    Boolean findProcess(String userNo,BigDecimal amount,String event);
    Boolean findProcess(String userNo,BigDecimal amount,String referenceNo,String event);
    Integer findOtpCount(String userNo);
    void addToBlackList(String userNo);
    Boolean findInBlackList(String userNo);
}
