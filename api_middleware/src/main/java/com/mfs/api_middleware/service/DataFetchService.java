package com.mfs.api_middleware.service;

import com.mfs.api_middleware.dto.*;

import java.util.List;
import java.util.Locale;

public interface DataFetchService {

    public DataJson[] getInsurance(Locale locale);

    public DataJson[] getInstitution(Locale locale);

    public SchoolDataJson[] getInstitutionV2(Locale locale);

    public DataJson[] getOthers(Locale locale);

    public DataJson[] getDonations(Locale locale);

    public BannerItem[] getBanners(Locale locale);

    public BannerAndOffers getBannersAndOffers(Locale locale);
    public CampaignMain getBannersAndOffersV2();

    public List<DataListResponse> getUtility(Locale locale);

    //New development
    public List<DataListResponse> getTickets(Locale locale);

    public List<DataListResponse> getUtilityForNewApp(Locale locale);

    public void modifyDonationMerchantList(String donationMerchantList);

    public DataJson[] getFees(Locale locale);
    public DataJson[] getFeesForNewApp(Locale locale);
}
