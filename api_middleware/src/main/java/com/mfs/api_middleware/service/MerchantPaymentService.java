package com.mfs.api_middleware.service;

import com.mfs.api_middleware.dto.CommonResponse;
import com.mfs.api_middleware.dto.DonationPayment;
import com.mfs.api_middleware.dto.MerchantPayment;

import java.util.Locale;

public interface MerchantPaymentService {
    public CommonResponse payMerchant(MerchantPayment merchantPayment, String account, Locale locale);
    public CommonResponse payDonation(DonationPayment donationPaymentPayment, String account, Locale locale);
}
