package com.mfs.api_middleware.service;

import com.mfs.api_middleware.dto.CommonResponse;
import com.mfs.api_middleware.dto.P2PRequest;

import java.util.Locale;

public interface P2PService {

    public CommonResponse p2p(P2PRequest p2PRequest, String accountNo, Locale locale);
}
