package com.mfs.api_middleware.service;

import com.mfs.api_middleware.dto.CommonResponse;
import com.mfs.api_middleware.dto.FeatureHidePermissionRequest;
import com.mfs.api_middleware.dto.ServiceAvailibilityInfo;
import com.mfs.api_middleware.dto.ServiceConfigResponse;

import java.util.Locale;

public interface ServiceConfigService {
    public ServiceConfigResponse getConfiguration();
    public CommonResponse setHiddenFeatureVersion(FeatureHidePermissionRequest featureHidePermissionRequest, Locale locale);
    public void setServiceAvailibility(ServiceAvailibilityInfo serviceAvailibilityInfo);
}
