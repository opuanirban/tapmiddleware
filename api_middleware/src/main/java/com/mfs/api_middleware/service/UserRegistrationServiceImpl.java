package com.mfs.api_middleware.service;

import com.mfs.api_middleware.Exception.ApiManagerRequestException;
import com.mfs.api_middleware.Exception.DecodePinException;
import com.mfs.api_middleware.Exception.UserRegistrationRequestException;
import com.mfs.api_middleware.config.ApplicationProperties;
import com.mfs.api_middleware.dto.*;
import com.mfs.api_middleware.enumeration.ContextUrl;
import com.mfs.api_middleware.enumeration.GenderEnum;
import com.mfs.api_middleware.enumeration.MobileOperatorEnum;
import com.mfs.api_middleware.enumeration.UserGroupEnum;
import com.mfs.api_middleware.gateway.ApiManagerGateway;
import com.mfs.api_middleware.service.security.ISecurityService;
import com.mfs.api_middleware.util.CommonConstant;
import com.mfs.api_middleware.util.ErrorKeyConstants;
import com.mfs.api_middleware.util.ServiceMessage;
import com.mfs.api_middleware.util.TBLResponsePlaceHolder;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Pattern;

@Slf4j
@Service
public class UserRegistrationServiceImpl extends ProcessRequestService implements UserRegistrationService {

    @Autowired
    ApiManagerGateway apiManagerGateway;

    @Autowired
    ISecurityService iSecurityService;

    @Autowired
    KYCService kycService;

    @Value("${registration.msisdn}")
    private String userNumber;

    @Value("${registration.pin}")
    private String userPin;

    @Autowired
    ApplicationProperties applicationProperties;

    @Autowired
    MessageRetrieverService messageRetrieverService;


    @Override
    public RegistrationResponse userRegistration(UserRegistrationRequest userRegistrationRequest, Locale locale) {

        if (Pattern.matches("......==", userRegistrationRequest.getPin())) {
            String formattedText = "TRUSTMM PPREG " + CommonConstant.checkNumber(userRegistrationRequest.getMobileNumber()) + " " + new String(Base64.getDecoder().decode(userPin)) + " "
                    + userRegistrationRequest.getNid() + " " + userRegistrationRequest.getFirstName().replace(" ", "_") + " " + userRegistrationRequest.getLastName() + " " + userRegistrationRequest.getGender().name() + " "
                    + userRegistrationRequest.getAccountType().getUserGroup() + " " + userRegistrationRequest.getDob() + " " + userRegistrationRequest.getOccupation() + " "
                    + userRegistrationRequest.getMno() + " " + new String(Base64.getDecoder().decode(userRegistrationRequest.getPin()));

            ApiManagerRequest apiManagerRequest = makeProcessedRequest(applicationProperties.getEncryptionEnabled() ?
                    iSecurityService.encrypt(formattedText, "", "") : formattedText, ContextUrl.USER_REGISTRATION.getUrl(), userNumber);

            String responseFromApiManger = apiManagerGateway.apiManagerRequestMethod(apiManagerRequest, locale, false);
            RegistrationResponse response = new RegistrationResponse();
            if (responseFromApiManger.contains(TBLResponsePlaceHolder.REGISTER_USER_SUCCESS)) {
                //  response.setMessage(messageRetrieverService.getMessage(locale, ErrorKeyConstants.API_MANAGER_REGISTRATION_SUCCESS_KEY));
                response.setMessage(ServiceMessage.REGISTRATION_SUCCESS);
                return response;
            } else {
//            throw new ApiManagerRequestException(messageRetrieverService.getMessage(locale, ErrorKeyConstants.API_MANAGER_REGISTRATION_FAIL_KEY));
                throw new ApiManagerRequestException(responseFromApiManger.isEmpty() ? ServiceMessage.REGISTRATION_FAILED : responseFromApiManger);
            }
        } else {
            throw new DecodePinException(CommonConstant.DECODE_EXCEPTION_MESSAGE);
        }
    }

    @Override
    public UserStatusInfo checkForUser(String msisdn, Locale locale) {
        UserStatusInfo userStatusInfo = apiManagerGateway.checkUSer(msisdn, locale);
        return userStatusInfo;
    }

    @Override
    public RegistrationResponse userRegistrationKyc(String pin, String firstName, String lastName, UserGroupEnum accountType, MobileOperatorEnum mno, String nid_no, String dob, String applicant_name_ben, String applicant_name_eng, String father_name, String mother_name, String spouse_name, String pres_address, String perm_address, String id_front_name, String id_back_name, GenderEnum gender, String profession, String nominee, String nominee_relation, String mobile_number, MultipartFile applicant_photo, Locale locale) {

        Date date = null;
        String encodedString = null;
        String encodedStringKyc = null;
        RegistrationResponse response = new RegistrationResponse();
        try {
            date = new SimpleDateFormat("yyyy-MM-dd").parse(dob);
            String extension = FilenameUtils.getExtension(applicant_photo.getOriginalFilename());
            encodedStringKyc = CommonConstant.IMAGE_EXT_LEFT + extension + CommonConstant.IMAGE_EXT_RIGHT + Base64.getMimeEncoder().encodeToString(applicant_photo.getBytes());
            encodedString = Base64.getMimeEncoder().encodeToString(applicant_photo.getBytes());
            log.debug("file name: " + applicant_photo.getOriginalFilename() + " extension: " + extension +
                    " length of encoded string: " + encodedString.length());
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new UserRegistrationRequestException("Date format needs to be yyyy-MM-dd");
        }

        String dobKyc = new SimpleDateFormat("yyyy/MM/dd").format(date);

        KYCRegistrationResponse kycRegistrationResponse = null;
        if (applicationProperties.getLimitedKyc()) {
            throw new UserRegistrationRequestException("Try calling the Asynchronous Registration API");
//            response = prepareLimitedRegistrationData(pin, firstName, lastName, accountType, mno, gender, profession, mobile_number, locale, response);
//            if (response != null) {
//                kycRegistrationResponse = kycService.kycRegister(nid_no, dobKyc, applicant_name_ben, applicant_name_eng, father_name, mother_name, spouse_name, pres_address, perm_address, id_front_name, id_back_name, gender.getGenderGroup(), profession, nominee, nominee_relation, mobile_number, encodedStringKyc, locale);
//                response.setEkyc_status(kycRegistrationResponse.getStatus());
//                response.setEkyc_status_message(kycRegistrationResponse.getMessage());
//                return response;
//            }

        } else {
            kycRegistrationResponse = kycService.kycRegister(nid_no, dobKyc, applicant_name_ben, applicant_name_eng, father_name, mother_name, spouse_name, pres_address, perm_address, id_front_name, id_back_name, gender.getGenderGroup(), profession, nominee, nominee_relation, mobile_number, applicant_photo, locale);
            if (kycRegistrationResponse.getStatus().equalsIgnoreCase(CommonConstant.SUCCESS)) {
                log.info("Thread going to sleep before making next call to gigatech");
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    log.error(e.getMessage());
                }
                log.info("Thread active again");
                NIDVerificationResponse nidVerificationResponse = kycService.verifyNidStatus(mobile_number, locale);
                UserRegistrationRequest userRegistrationRequest = new UserRegistrationRequest();
                if (nidVerificationResponse.getStatus().equalsIgnoreCase(CommonConstant.SUCCESS)) {
                    if (nidVerificationResponse.getData().getStatus().equalsIgnoreCase(CommonConstant.PASSED)) {
                        return prepareRegistrationData(pin, firstName, lastName, accountType, mno, nid_no, dob, applicant_name_ben, father_name, mother_name, spouse_name, pres_address, perm_address, gender, profession, nominee, nominee_relation, mobile_number, locale, response, encodedString);

                    } else if (nidVerificationResponse.getData().getStatus().equalsIgnoreCase(CommonConstant.PENDING) ||
                            nidVerificationResponse.getData().getStatus().equalsIgnoreCase(CommonConstant.EC_REQUESTED)) {

                        log.info("Verification still pending, going to sleep for 5 secs");
                        try {
                            Thread.sleep(5000);
                        } catch (InterruptedException e) {
                            log.error(e.getMessage());
                        }
                        log.info("Thread active again");
                        nidVerificationResponse = kycService.verifyNidStatus(mobile_number, locale);
                        if (nidVerificationResponse.getData().getStatus().equalsIgnoreCase(CommonConstant.PASSED)) {
                            return prepareRegistrationData(pin, firstName, lastName, accountType, mno, nid_no, dob, applicant_name_ben, father_name, mother_name, spouse_name, pres_address, perm_address, gender, profession, nominee, nominee_relation, mobile_number, locale, response, encodedString);
                        } else if (nidVerificationResponse.getData().getStatus().equalsIgnoreCase(CommonConstant.FAILED)) {
                            errorCheck(nidVerificationResponse, locale);
                        } else if (nidVerificationResponse.getData().getStatus().equalsIgnoreCase(CommonConstant.EC_REQUESTED)) {

                            throw new UserRegistrationRequestException(messageRetrieverService.getMessage(locale, ErrorKeyConstants.REGISTRATION_KYC_PENDING_KEY));
                            //throw new UserRegistrationRequestException("User registration with KYC pending for EC Verification");
                        } else if (nidVerificationResponse.getData().getStatus().equalsIgnoreCase(CommonConstant.INVALID)) {
                            throw new UserRegistrationRequestException(messageRetrieverService.getMessage(locale, ErrorKeyConstants.REGISTRATION_NO_NID_KEY));
                            //throw new UserRegistrationRequestException("User registration with KYC failed. No user found with NID and DOB");
                        } else {
                            throw new UserRegistrationRequestException(messageRetrieverService.getMessage(locale, ErrorKeyConstants.REGISTRATION_KYC_FAIL_KEY));
//                    throw new UserRegistrationRequestException("User registration with KYC failed");
                        }

                    } else errorCheck(nidVerificationResponse, locale);
                } else if (nidVerificationResponse.getStatus().equalsIgnoreCase(CommonConstant.FAILED)) {
                    throw new UserRegistrationRequestException(nidVerificationResponse.getMessage());
                } else if (nidVerificationResponse.getStatus().equalsIgnoreCase(CommonConstant.ERROR)) {
                    throw new UserRegistrationRequestException(nidVerificationResponse.getMessage());
                }

            } else if (kycRegistrationResponse.getStatus().equalsIgnoreCase(CommonConstant.FAILED)) {
                if (kycRegistrationResponse.getMessage().equalsIgnoreCase(CommonConstant.KYC_MESSAGE)) {
                    NIDVerificationResponse nidVerificationResponse = kycService.verifyNidStatus(mobile_number, locale);

                    if (nidVerificationResponse.getData() == null) {
                        throw new UserRegistrationRequestException("kyc registration failed: " + nidVerificationResponse.getMessage());
                    }

                    if (nidVerificationResponse.getData().getStatus().equalsIgnoreCase(CommonConstant.PASSED)) {
                        return prepareRegistrationData(pin, firstName, lastName, accountType, mno, nid_no, dob, applicant_name_ben, father_name, mother_name, spouse_name, pres_address, perm_address, gender, profession, nominee, nominee_relation, mobile_number, locale, response, encodedString);
                    } else if (nidVerificationResponse.getData().getStatus().equalsIgnoreCase(CommonConstant.FAILED)) {
                        errorCheck(nidVerificationResponse, locale);
                    } else {
                        throw new UserRegistrationRequestException(messageRetrieverService.getMessage(locale, ErrorKeyConstants.REGISTRATION_KYC_FAIL_KEY));

                        // throw new UserRegistrationRequestException("User registration with KYC failed");
                    }

                } else {
                    throw new UserRegistrationRequestException(kycRegistrationResponse.getMessage());
                }
            } else {
                throw new UserRegistrationRequestException(kycRegistrationResponse.getMessage());
                // throw new UserRegistrationRequestException(messageRetrieverService.getMessage(locale, ErrorKeyConstants.REGISTRATION_KYC_FAIL_KEY));
                //throw new UserRegistrationRequestException("User registration with KYC failed");
            }
        }

        throw new UserRegistrationRequestException(messageRetrieverService.getMessage(locale, ErrorKeyConstants.REGISTRATION_KYC_FAIL_KEY));
//        throw new UserRegistrationRequestException("User registration with KYC failed");
    }

    @Override
    public UserLoginInfo fetchUserLoginInfo(String msisdn, String pin) {
        String loginResponse = apiManagerGateway.userLoginInfo(msisdn, pin);

        if (loginResponse.contains("|")) { // success response has | as data separator
            String[] infos = loginResponse.replace("|", "###")
                    .split("###");
            UserLoginInfo userLoginInfo = new UserLoginInfo();
            userLoginInfo.setUserName(infos[0]);
            userLoginInfo.setBalance(infos[1]);
            userLoginInfo.setEkycComplete(infos[2].equalsIgnoreCase("True"));
            userLoginInfo.setUserStatus(infos[3]);
            if(infos.length > 4) {
                userLoginInfo.setUserGroupId(Integer.parseInt(infos[4]));
            }
            
            return userLoginInfo;
        } else if (loginResponse.isEmpty()) {
            throw new ApiManagerRequestException(CommonConstant.COMMON_ERROR);
        } else {
            throw new ApiManagerRequestException(loginResponse);
        }
    }

    private RegistrationResponse prepareRegistrationData(String pin, String firstName, String lastName, UserGroupEnum accountType, MobileOperatorEnum mno, String nid_no, String dob, String applicant_name_ben, String father_name, String mother_name, String spouse_name, String pres_address, String perm_address, GenderEnum gender, String profession, String nominee, String nominee_relation, String mobile_number, Locale locale, RegistrationResponse response, String encodedString) {
        if (Pattern.matches("......==", pin)) {
            RegistrationParams registrationParams = new RegistrationParams();
            registrationParams.setUserNumber(CommonConstant.checkNumber(mobile_number));
            registrationParams.setAccountPin(new String(Base64.getDecoder().decode(pin)));
            registrationParams.setFirstName(firstName);
            registrationParams.setLastName(lastName);
            registrationParams.setDoB(dob);
            registrationParams.setNationalIdNumber(nid_no);
            registrationParams.setSex(gender.name());
            registrationParams.setUserGroupKey(accountType.getUserGroup());
            registrationParams.setPermanentAddress(perm_address);
            registrationParams.setPresentAddress(pres_address);
            registrationParams.setOccupation(profession);
            registrationParams.setOperatorName(mno.name());
            registrationParams.setBanglaName(applicant_name_ben);
            registrationParams.setNominee(nominee);
            registrationParams.setNomineeRelation(nominee_relation);
            if (father_name.equalsIgnoreCase("none")) {
                registrationParams.setFatherName("");
            } else {
                registrationParams.setFatherName(father_name);
            }
            if (mother_name.equalsIgnoreCase("none")) {
                registrationParams.setMotherName("");
            } else {
                registrationParams.setMotherName(mother_name);
            }
            if (spouse_name.equalsIgnoreCase("none")) {
                registrationParams.setSpouseName("");
            } else {
                registrationParams.setSpouseName(spouse_name);
            }
            registrationParams.setPhotoString(encodedString);
            registrationParams.setIsLimitedKYC(false);

            return callTblRegistration(locale, response, registrationParams);

        } else {
            throw new DecodePinException(CommonConstant.DECODE_EXCEPTION_MESSAGE);
        }

    }

    private RegistrationResponse prepareLimitedRegistrationData(String pin, String firstName, String lastName, UserGroupEnum accountType, MobileOperatorEnum mno, GenderEnum gender, String profession, String mobile_number, Locale locale, RegistrationResponse response) {
        if (Pattern.matches("......==", pin)) {
            RegistrationParams registrationParams = new RegistrationParams();
            registrationParams.setUserNumber(CommonConstant.checkNumber(mobile_number));
            registrationParams.setAccountPin(new String(Base64.getDecoder().decode(pin)));
            registrationParams.setFirstName(firstName);
            registrationParams.setLastName(lastName);
//            registrationParams.setNationalIdNumber(nid_no);
            registrationParams.setSex(gender.name());
            registrationParams.setUserGroupKey(accountType.getUserGroup());
            registrationParams.setOccupation(profession);
            registrationParams.setOperatorName(mno.name());
            registrationParams.setIsLimitedKYC(true);

            return callTblRegistration(locale, response, registrationParams);

        } else {
            throw new DecodePinException(CommonConstant.DECODE_EXCEPTION_MESSAGE);

        }
    }

    @Override
    public RegistrationResponse callTblRegistration(Locale locale, RegistrationResponse response, RegistrationParams registrationParams) {
        String result = apiManagerGateway.userRegistration(registrationParams, locale);
        if (result.contains(applicationProperties.getSuccessPlaceholderList().get(2))) {
            response.setMessage(messageRetrieverService.getMessage(locale, ErrorKeyConstants.API_MANAGER_REGISTRATION_SUCCESS_KEY));
            return response;
        } else if (result.contains(TBLResponsePlaceHolder.REGISTER_USER_EXISTS)) {
            throw new ApiManagerRequestException(messageRetrieverService.getMessage(locale, ErrorKeyConstants.API_MANAGER_USER_EXISTS));
        } else if (result.contains(TBLResponsePlaceHolder.REGISTER_DATA_FAIL)) {
            throw new ApiManagerRequestException(messageRetrieverService.getMessage(locale, ErrorKeyConstants.API_MANAGER_DATA_ENTRY_FAIL));
        } else {
            throw new ApiManagerRequestException(messageRetrieverService.getMessage(locale, ErrorKeyConstants.API_MANAGER_REGISTRATION_FAIL_KEY));
        }
    }


    private void errorCheck(NIDVerificationResponse nidVerificationResponse, Locale locale) {
        if (nidVerificationResponse.getData().getStatus().equalsIgnoreCase(CommonConstant.FAILED)) {

            if (!nidVerificationResponse.getData().getTextual_info_match()) {
                if (!nidVerificationResponse.getData().getApplicant_photo_app_ec_match()) {
                    if (!nidVerificationResponse.getData().getApplicant_photo_card_ec_match()) {
                        throw new UserRegistrationRequestException(ErrorKeyConstants.TEXT_EC_PHOTO_MISMATCH_KEY);
                        //throw new UserRegistrationRequestException("NID textual Data mismatch & photo on NID & APP does not match with EC Database ");
                    } else {
                        throw new UserRegistrationRequestException(ErrorKeyConstants.TEXT_GIVEN_PHOTO_MISMATCH_KEY);

                        // throw new UserRegistrationRequestException("NID textual Data mismatch & photo provided does not match with EC Database ");
                    }
                } else if (!nidVerificationResponse.getData().getApplicant_photo_card_ec_match()) {
                    throw new UserRegistrationRequestException(ErrorKeyConstants.TEXT_NID_PHOTO_MISMATCH_KEY);
                    //throw new UserRegistrationRequestException("NID textual Data mismatch & photo on given NID does not match with EC Database ");
                } else {
                    throw new UserRegistrationRequestException(ErrorKeyConstants.NID_TEXT_MISMATCH_KEY);

                    //throw new UserRegistrationRequestException("NID textual Data mismatch");
                }
            } else if (!nidVerificationResponse.getData().getApplicant_photo_app_ec_match()) {
                if (!nidVerificationResponse.getData().getApplicant_photo_card_ec_match()) {
                    throw new UserRegistrationRequestException(ErrorKeyConstants.EC_PHOTO_MISMATCH_KEY);
                    //throw new UserRegistrationRequestException("Photo on NID & APP does not match with EC Database ");
                } else {
                    throw new UserRegistrationRequestException(ErrorKeyConstants.GIVEN_PHOTO_MISMATCH_KEY);
                    //throw new UserRegistrationRequestException("Photo provided does not match with EC Database ");
                }
            } else if (!nidVerificationResponse.getData().getApplicant_photo_card_ec_match()) {
                throw new UserRegistrationRequestException(ErrorKeyConstants.NID_PHOTO_MISMATCH_KEY);
                //throw new UserRegistrationRequestException("Photo on given NID does not match with EC Database ");
            } else {
                throw new UserRegistrationRequestException(messageRetrieverService.getMessage(locale, ErrorKeyConstants.REGISTRATION_KYC_FAIL_KEY));
                //throw new UserRegistrationRequestException("User registration with KYC failed");
            }
        }
    }

    private RegistrationResponse getCommonResponse(String pin, String firstName, String lastName, UserGroupEnum accountType, MobileOperatorEnum mno, String nid_no, String dob, GenderEnum gender, String profession, String mobile_number, UserRegistrationRequest userRegistrationRequest, Locale locale) {
        log.info("NID verification passed");
        userRegistrationRequest.setMobileNumber(mobile_number);
        userRegistrationRequest.setAccountType(accountType);
        userRegistrationRequest.setFirstName(firstName.replace(" ", "_"));
        userRegistrationRequest.setLastName(lastName.replace(" ", "_"));
        userRegistrationRequest.setPin(pin);
        userRegistrationRequest.setMno(mno);
        userRegistrationRequest.setNid(nid_no);
        userRegistrationRequest.setGender(gender);
        userRegistrationRequest.setOccupation(profession.replace(" ", "_"));
        userRegistrationRequest.setDob(dob);
        return userRegistration(userRegistrationRequest, locale);
    }
}
