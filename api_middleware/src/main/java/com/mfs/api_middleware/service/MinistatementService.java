package com.mfs.api_middleware.service;

import com.mfs.api_middleware.dto.MiniStatementResponse;

import java.util.Locale;

public interface MinistatementService {
    MiniStatementResponse fetchMiniStatement(String userAccount, Locale locale);
}
