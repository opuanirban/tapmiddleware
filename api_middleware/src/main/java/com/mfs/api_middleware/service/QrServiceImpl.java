package com.mfs.api_middleware.service;

import com.mfs.api_middleware.Exception.CommonException;
import com.mfs.api_middleware.config.ApplicationProperties;
import com.mfs.api_middleware.dto.MerchantInfoResponse;
import com.mfs.api_middleware.gateway.ApiManagerGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;

@Service
public class QrServiceImpl implements QrService {
    public static final String API_MIDDLEWARE_MERCHANT_QR = "merchant-qr";
    public static final String MERCHANT_QR_SALT = "merchant-qr-tap";

    @Autowired
    ApiManagerGateway apiManagerGateway;

    @Autowired
    ApplicationProperties applicationProperties;

    @Autowired
    CryptoService cryptoService;

    @Override
    public MerchantInfoResponse getMerchantInfo(String id, String secret){
        String url = applicationProperties.getApiMiddlewareUrl() + File.separator + API_MIDDLEWARE_MERCHANT_QR +
                "?id=" + id;
        String hash = cryptoService.getSHA_512_hashValue(url, MERCHANT_QR_SALT);
        if(hash.equalsIgnoreCase(secret)){
            return apiManagerGateway.fetchMerchantInfoByAccountId(id);
        }
        else{
            throw new CommonException("Invalid QR code");
        }




    }
}
