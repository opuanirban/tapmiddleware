package com.mfs.api_middleware.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

import com.google.gson.Gson;
import com.mfs.api_middleware.Exception.ApiManagerRequestException;
import com.mfs.api_middleware.Exception.ApiRequestException;
import com.mfs.api_middleware.Exception.EmptyInputException;
import com.mfs.api_middleware.dto.*;
import com.mfs.api_middleware.gateway.*;
import com.mfs.api_middleware.gateway.desco.DescoServiceGateway;
import com.mfs.api_middleware.gateway.ekpay.EkpayServiceGateway;
import com.mfs.api_middleware.gateway.model.BaseResponse;
import com.mfs.api_middleware.gateway.model.BillFetchResponse;
import com.mfs.api_middleware.gateway.model.BillPreviewRestInput;
import com.mfs.api_middleware.gateway.nesco.NescoServiceGateway;
import com.mfs.api_middleware.gateway.nesco.model.BillPreviewOutput;
import com.mfs.api_middleware.gateway.reb.REBServiceGateway;
import com.mfs.api_middleware.util.CommonConstant;
import com.mfs.api_middleware.util.TBLResponsePlaceHolder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class BillInfoServiceImpl implements BillInfoService {

    @Autowired
    ApiManagerGateway apiManagerGateway;
    @Autowired
    NescoServiceGateway nescoServiceGateway;
    @Autowired
    EkpayServiceGateway ekpayServiceGateway;
    @Autowired
    DescoServiceGateway descoServiceGateway;
    @Autowired
    TvServiceGateway tvServiceGateway;
    @Autowired
    JGTDSLServiceGateway jgtdslServiceGateway;
    @Autowired
    IvacGateway ivacGateway;
    @Autowired
    REBServiceGateway rebServiceGateway;

    @Autowired
    InsuranceServiceGateway insuranceServiceGateway;

    private static final String EMPTY_STRING = "\"\"";

    @Override
    public BillInfoResponse getDpdcBill(DPDCBillInfo dpdcBillInfo, Locale locale) {
        String bill = apiManagerGateway.getDPDCBill(dpdcBillInfo, locale);
        return getBillInfoResponse(bill);
    }

    @Override
    public BillInfoResponse getDescoBill(String billNo, Locale locale) {
        if (!billNo.isEmpty()) {
            String bill = apiManagerGateway.getBillAmount(billNo, locale);
            return getBillInfoResponse(bill);
        } else {
            throw new EmptyInputException("Please provide a bill number");
        }
    }

    @Override
    public DescoPrepaidBillDetailsResponse getDescoPrepaidInfo(String meterNo, String amount, Locale locale, String customerAccountNumber) {
        DescoPrepaidBillDetailsResponse billDetailsResponse = new DescoPrepaidBillDetailsResponse();
        if (!meterNo.isEmpty() && !amount.isEmpty()) {
            // check for unified
            PrepaidBillDetailsResponse commonResponse = new PrepaidBillDetailsResponse();
            BillPreviewRestInput input = new BillPreviewRestInput();
            input.setAccountNumber(customerAccountNumber);
            input.setBillerId("Desco-Prepaid-unified");
            input.setCode("DESPR");
            input.setKey1(meterNo);
            input.setKey3(amount);

            // try {
            //     BillPreviewOutput billPreviewOutput = descoServiceGateway.summaryDescoPrepaidUnifiedBillPreview(input);
            //     BaseResponse baseResponse = billPreviewOutput.getResponse();
            //     if (baseResponse != null) {
            //         if (baseResponse.getResponseCode().equals(CommonConstant.SUCCESS_CODE)) {
            //             List<HashMap<String, Object>> tableDatas = billPreviewOutput.getTableData();
            //             List<String> feeDetailsPlaceholderList = Arrays.asList("Energy Cost,Due Amount,Demand Charge(25/kW),Meter Rent 3P(250/month),VAT(5%)".split("\\s*,\\s*"));
            //             StringBuilder feeDetails = new StringBuilder();
            //             boolean firstElement = true;
            //             for (int i = 0; i < tableDatas.size(); i++) {
            //                 for (String s : feeDetailsPlaceholderList) {
            //                     if (Pattern.compile(Pattern.quote(s), Pattern.CASE_INSENSITIVE).matcher(String.valueOf(tableDatas.get(i).get("label_en"))).find()) {
            //                         if(!(tableDatas.get(i).get("value").equals("0 "))) {
            //                             if (!firstElement) {
            //                                 feeDetails.append(" ; ");
            //                             }
            //                             firstElement = false;
            //                             feeDetails.append(String.valueOf(tableDatas.get(i).get("label_en").toString()));
            //                             feeDetails.append(": ");
            //                             feeDetails.append(String.valueOf(tableDatas.get(i).get("value").toString()));
            //                         }
            //                     }
            //                 }
            //             }
            //             billDetailsResponse.setMessage(feeDetails.toString());
            //             billDetailsResponse.setBillType("UNIFIED");
            //             billDetailsResponse.setTransactionId(billPreviewOutput.getResponse().getTransactionId());
            //             return billDetailsResponse;
            //         } else {
            //             if (!baseResponse.getResponseMessage().equals("404")) {
            //                 throw new InvalidTransactionException(baseResponse.getResponseMessage());
            //             }
            //         }
            //     }
            // } catch (InvalidTransactionException e) {
            //     throw new InvalidTransactionException(e.getMessage());
            // } catch (Exception e) {
            //     log.error("error received in Unified Gateway: {}", e);
            // }

            try{
                // check for kaifa
                String bill = apiManagerGateway.getDescoBillPrepaid(meterNo, amount, locale);
                bill = bill.replace("\\", "");
                CommonResponse response = new Gson().fromJson(bill, CommonResponse.class);
                if (Pattern.compile(Pattern.quote(TBLResponsePlaceHolder.DESCO_PREPAID_BREAKDOWN_SUCCESS),
                        Pattern.CASE_INSENSITIVE).matcher(response.getMessage()).find()) {
                    billDetailsResponse.setMessage(response.getMessage());
                    billDetailsResponse.setBillType("KAIFA");
                    return billDetailsResponse;
                } else {
                    throw new ApiManagerRequestException(response.getMessage());
                }           
            } catch (Exception e) {
                throw new ApiRequestException(e.getMessage());
            }

        } else {
            throw new EmptyInputException("Bill number and amount can not be empty");
        }
    }

    private BillInfoResponse getBillInfoResponse(String bill) {
        BillInfoResponse billInfoResponse = new BillInfoResponse();
        if (bill != null && !bill.equals(EMPTY_STRING)) {
            billInfoResponse.setBillAmount(bill.replace("\"", ""));
        } else {
            billInfoResponse.setBillAmount(bill);
        }
        return billInfoResponse;
    }

    @Override
    public PrepaidBillDetailsResponse getNescoPrepaidInfo(String customerAccountNumber, String customerNo, String meterNo, String amount, Locale locale) {
        PrepaidBillDetailsResponse commonResponse = new PrepaidBillDetailsResponse();
        BillPreviewRestInput input = new BillPreviewRestInput();
        input.setAccountNumber(customerAccountNumber);
        input.setBillerId("Nesco-Prepaid");
        input.setCode("NESPR");
        input.setKey1(customerNo);
        input.setKey2(meterNo);
        input.setKey3(amount);

        try {
            BillPreviewOutput billPreviewOutput = nescoServiceGateway.summaryPrepaidBillPreview(input);
            BaseResponse baseResponse = billPreviewOutput.getResponse();
            if (baseResponse != null) {
                if (baseResponse.getResponseCode().equals(CommonConstant.SUCCESS_CODE)) {
                    List<HashMap<String, Object>> tableDatas = billPreviewOutput.getTableData();
                    List<String> feeDetailsPlaceholderList = Arrays.asList("Energy Cost,Last Error,Paid Debt,PFC,meter rent,demand charge,vat,rebate".split("\\s*,\\s*"));
                    StringBuilder feeDetails = new StringBuilder();
                    boolean firstElement = true;
                    for (int i = 0; i < tableDatas.size(); i++) {
                        for (String s : feeDetailsPlaceholderList) {
                            if (Pattern.compile(Pattern.quote(s), Pattern.CASE_INSENSITIVE).matcher(String.valueOf(tableDatas.get(i).get("label_en"))).find()) {
                                if(String.valueOf(tableDatas.get(i).get("value")).equals("0.00") ||
                                    String.valueOf(tableDatas.get(i).get("value")).equals("0.0") ||
                                    String.valueOf(tableDatas.get(i).get("value")).equals("0"))
                                    continue;

                                if (!firstElement) {
                                    feeDetails.append(" ; ");
                                }
                                firstElement = false;
                                feeDetails.append(String.valueOf(tableDatas.get(i).get("label_en").toString()));
                                feeDetails.append(": ");
                                feeDetails.append(String.valueOf(tableDatas.get(i).get("value").toString()));
                            }
                        }
                    }

                    commonResponse.setMessage(feeDetails.toString());
                    commonResponse.setTransactionId(String.valueOf(billPreviewOutput.getPayload().get("key4"))); // transaction id: key4
                    return commonResponse;
                } else {
                    throw new ApiRequestException(baseResponse.getResponseMessage());
                }
            } else {
                throw new ApiRequestException(CommonConstant.COMMON_ERROR);
            }
        } catch (Exception e) {
            throw new ApiRequestException(e.getMessage());
        }
    }

    @Override
    public NescoBillInfoResponse getNescoBill(String customerAccountNumber, NescoBillInfo nescoBillInfo, Locale locale) {
        NescoBillInfoResponse billInfoResponse = new NescoBillInfoResponse();

        BillPreviewRestInput input = new BillPreviewRestInput();
        input.setAccountNumber(customerAccountNumber);
        input.setBillerId("Nesco-Postpaid");
        input.setCode("NESCO");
        input.setKey1(nescoBillInfo.getAccountNo());

        DateFormat monthFormat = new SimpleDateFormat("MM");
        DateFormat yearFormat = new SimpleDateFormat("yyyy");
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date); // set to the current time
        calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) - 1); // set to the previous month
        date = calendar.getTime();

        input.setKey2(nescoBillInfo.getBillMonth() != null ? nescoBillInfo.getBillMonth() : monthFormat.format(date));
        input.setKey3(nescoBillInfo.getBillYear() != null ? nescoBillInfo.getBillYear() : yearFormat.format(date));

        try {
            BillFetchResponse billFetchResponse = nescoServiceGateway.fetchPostpaidBillDetails(input);
            log.info("NESCO Bill Fetch Response");
            log.info(billFetchResponse.toString());
            BaseResponse baseResponse = billFetchResponse.getResponse();
            if (baseResponse != null) {
                if (baseResponse.getResponseCode().equals(CommonConstant.SUCCESS_CODE)) {
                    billInfoResponse.setBillAmount(billFetchResponse.getBillAmount());
                    billInfoResponse.setTransactionId(String.valueOf(billFetchResponse.getPayload().get("key5"))); // key5 for transaction id

                    List<HashMap<String, Object>> tableDatas = billFetchResponse.getTableData();
                    for (int i = 0; i < tableDatas.size(); i++) {
                        HashMap<String, Object> item = tableDatas.get(i);
                        if (item.get("key").equals("customerName")) {
                            billInfoResponse.setCustomerName(String.valueOf(item.get("value")));
                        } else if (item.get("key").equals("dueDate")) {
                            billInfoResponse.setDueDate(String.valueOf(item.get("value")));
                        }
                    }
                    return billInfoResponse;
                } else if (baseResponse.getResponseMessage().toLowerCase().contains("paid")) {
                    billInfoResponse.setBillAmount("-1");
                    return billInfoResponse;
                } else {
                    throw new ApiRequestException(baseResponse.getResponseMessage());
                }
            } else {
                throw new ApiRequestException(CommonConstant.COMMON_ERROR);
            }
        } catch (Exception e) {
            throw new ApiRequestException(e.getMessage());
        }
    }

    @Override
    public EkpayBillInfoResponse getEkpayBillInfo(String customerAccountNumber, EkpayBillInfo billInfo, Locale locale) {
        EkpayBillInfoResponse billInfoResponse = new EkpayBillInfoResponse();

        BillPreviewRestInput input = new BillPreviewRestInput();
        input.setAccountNumber(customerAccountNumber);
        input.setBillerId(billInfo.getBillerType().toString());
        input.setCode(billInfo.getBillerType().toString());
        input.setBillNumber(billInfo.getBillNo());
        input.setBillAccountNumber(billInfo.getBillAccountNo());
        input.setKey1(billInfo.getBillType());
        input.setKey2(billInfo.getBillMobileNo());

        try {
            BillFetchResponse billFetchResponse = ekpayServiceGateway.fetchBill(input);
            BaseResponse baseResponse = billFetchResponse.getResponse();
            if (baseResponse != null) {
                if (baseResponse.getResponseCode().equals(CommonConstant.SUCCESS_CODE)) {
                    billInfoResponse.setBillAmount(billFetchResponse.getBillAmount());
                    billInfoResponse.setTransactionId(String.valueOf(billFetchResponse.getPayload().get("key3"))); // key3 for transaction id
                    String dueDate = String.valueOf(billFetchResponse.getPayload().get("key6"));
                    billInfoResponse.setDueDate(dueDate.equals("null") ? null : dueDate);
                    if (String.valueOf(billFetchResponse.getPayload().get("key5")).equals("true")) {
                        billInfoResponse.setBillAmount("-1");
                    }

                    return billInfoResponse;
                } else {
                    throw new ApiRequestException(baseResponse.getResponseMessage());
                }
            } else {
                throw new ApiRequestException(CommonConstant.COMMON_ERROR);
            }
        } catch (Exception e) {
            throw new ApiRequestException(e.getMessage());
        }
    }

    @Override
    public CommonResponse getAkashBillInfo(String aksahAccountNumber, String customerAccountNumber, Locale locale) {
        CommonResponse commonResponse = new CommonResponse();
        BillPreviewRestInput input = new BillPreviewRestInput();
        input.setAccountNumber(customerAccountNumber);
        input.setBillerId("AKASH");
        input.setCode("AKASH");
        input.setKey1(aksahAccountNumber);

        try {
            BillPreviewOutput billFetchResponse = tvServiceGateway.summaryAkashBillPreview(input);
            BaseResponse baseResponse = billFetchResponse.getResponse();
            if (baseResponse != null) {
                if (baseResponse.getResponseCode().equals(CommonConstant.SUCCESS_CODE)) {
                    List<HashMap<String, Object>> tableDatas = billFetchResponse.getTableData();
                    for (int i = 0; i < tableDatas.size(); i++) {
                        HashMap<String, Object> item = tableDatas.get(i);
                        if (item.get("key").equals("customerName")) {
                            commonResponse.setMessage(String.valueOf(item.get("value")));
                        }
                    }
                    return commonResponse;
                } else {
                    throw new ApiRequestException(baseResponse.getResponseMessage());
                }
            } else {
                throw new ApiRequestException(CommonConstant.COMMON_ERROR);
            }
        } catch (Exception e) {
            throw new ApiRequestException(e.getMessage());
        }        
    }

    @Override
    public JGTDSLResponse getJGTDSLBillInfo(String jgtdslAccountNumber, String customerAccountNumber, Locale locale) {
        JGTDSLResponse billInfoResponse = new JGTDSLResponse();

        BillPreviewRestInput input = new BillPreviewRestInput();
        input.setAccountNumber(customerAccountNumber);
        input.setBillerId("JGTDSL-Postpaid");
        input.setCode("JGTDSL");
        input.setKey1(jgtdslAccountNumber);
        try {
            BillFetchResponse billFetchResponse = jgtdslServiceGateway.fetchJGTDSLBill(input);
            BaseResponse baseResponse = billFetchResponse.getResponse();
            if (baseResponse != null) {
                if (baseResponse.getResponseCode().equals(CommonConstant.SUCCESS_CODE)) {
                    billInfoResponse.setBillAmount(billFetchResponse.getBillAmount());
                    billInfoResponse.setTransactionId(String.valueOf(billFetchResponse.getPayload().get("key3"))); // key3 for transaction id

                    List<HashMap<String, Object>> tableDatas = billFetchResponse.getTableData();
                    for (int i = 0; i < tableDatas.size(); i++) {
                        HashMap<String, Object> item = tableDatas.get(i);
                        if (item.get("key").equals("customerName")) {
                            billInfoResponse.setCustomerName(String.valueOf(item.get("value")));
                        } else if (item.get("key").equals("monthYear")) {
                            billInfoResponse.setMonthYear(String.valueOf(item.get("value")));
                        }
                    }
                    return billInfoResponse;
                } else {
                    throw new ApiRequestException(baseResponse.getResponseMessage());
                }
            } else {
                throw new ApiRequestException(CommonConstant.COMMON_ERROR);
            }
        } catch (Exception e) {
            throw new ApiRequestException(e.getMessage());
        }        
    }

    @Override
    public IvacBillInfoResponse getIvacBillIfo(String ivacAccountNumber, IvacBillInfo ivacBillInfo) {
        BillPreviewRestInput input = new BillPreviewRestInput();
        input.setAccountNumber(ivacAccountNumber);
        input.setBillerId("IVAC");
        input.setCode("IVAC");
        input.setAccountNumber(ivacAccountNumber);
        input.setKey1("");
        input.setKey2(ivacBillInfo.getAppointType());
        input.setKey3(ivacBillInfo.getIvacId());
        input.setKey4(ivacBillInfo.getWebFile());
        input.setKey5(ivacBillInfo.getMobileNo());

        try{
            IvacBillPreviewOutput ivacBillPreviewOutput = ivacGateway.summaryIvacBillPreview(input);
            BaseResponse baseResponse = ivacBillPreviewOutput.getResponse();
            if(baseResponse != null){
                if(baseResponse.getResponseCode().equals(CommonConstant.SUCCESS_CODE)){
                    IvacBillInfoResponse billInfoResponse = new IvacBillInfoResponse();
                    billInfoResponse.setBillAmount(ivacBillPreviewOutput.getBillAmount());
                    billInfoResponse.setTransaction_id(baseResponse.getTransactionId());
                    return billInfoResponse;
                }else {
                    throw new ApiRequestException(baseResponse.getResponseMessage());
                }
            }else {
                throw new ApiRequestException(CommonConstant.COMMON_ERROR);
            }
        }catch (Exception ex){
            throw new ApiRequestException(ex.getMessage());
        }

    }

    @Override
    public List<IVACCenterItem> getIVACCenterList() {
        try {
            return ivacGateway.ivacCenterList().getIvacCenterItems();
        } catch (Exception e) {
            log.error("error fetching ivac centers: {}", e);
            List<IVACCenterItem> ivacCenterItems = new ArrayList<>();
            
            IVACCenterItem centerItem01 = new IVACCenterItem();
            centerItem01.setOrder(1);
            centerItem01.setCenterName("IVAC , RAJSHAHI");
            centerItem01.setCenterValue("5");
            ivacCenterItems.add(centerItem01);

            IVACCenterItem centerItem02 = new IVACCenterItem();
            centerItem02.setOrder(2);
            centerItem02.setCenterName("IVAC , KHULNA");
            centerItem02.setCenterValue("6");
            ivacCenterItems.add(centerItem02);

            IVACCenterItem centerItem03 = new IVACCenterItem();
            centerItem03.setOrder(3);
            centerItem03.setCenterName("IVAC , SYLHET");
            centerItem03.setCenterValue("7");
            ivacCenterItems.add(centerItem03);

            IVACCenterItem centerItem04 = new IVACCenterItem();
            centerItem04.setOrder(4);
            centerItem04.setCenterName("IVAC , CHITTAGONG");
            centerItem04.setCenterValue("8");
            ivacCenterItems.add(centerItem04);

            IVACCenterItem centerItem05 = new IVACCenterItem();
            centerItem05.setOrder(5);
            centerItem05.setCenterName("IVAC , RANGPUR");
            centerItem05.setCenterValue("9");
            ivacCenterItems.add(centerItem05);

            IVACCenterItem centerItem06 = new IVACCenterItem();
            centerItem06.setOrder(6);
            centerItem06.setCenterName("IVAC , MYMENSINGH");
            centerItem06.setCenterValue("10");
            ivacCenterItems.add(centerItem06);

            IVACCenterItem centerItem07 = new IVACCenterItem();
            centerItem07.setOrder(7);
            centerItem07.setCenterName("IVAC , BARISAL");
            centerItem07.setCenterValue("11");
            ivacCenterItems.add(centerItem07);

            IVACCenterItem centerItem08 = new IVACCenterItem();
            centerItem08.setOrder(8);
            centerItem08.setCenterName("IVAC , JESSORE");
            centerItem08.setCenterValue("12");
            ivacCenterItems.add(centerItem08);

            IVACCenterItem centerItem09 = new IVACCenterItem();
            centerItem09.setOrder(9);
            centerItem09.setCenterName("IVAC , Dhaka (JFP)");
            centerItem09.setCenterValue("13");
            ivacCenterItems.add(centerItem09);

            IVACCenterItem centerItem10 = new IVACCenterItem();
            centerItem10.setOrder(10);
            centerItem10.setCenterName("IVAC , THAKURGAON");
            centerItem10.setCenterValue("14");
            ivacCenterItems.add(centerItem10);

            IVACCenterItem centerItem11 = new IVACCenterItem();
            centerItem11.setOrder(11);
            centerItem11.setCenterName("IVAC , BOGURA");
            centerItem11.setCenterValue("15");
            ivacCenterItems.add(centerItem11);

            IVACCenterItem centerItem12 = new IVACCenterItem();
            centerItem12.setOrder(12);
            centerItem12.setCenterName("IVAC , SATKHIRA");
            centerItem12.setCenterValue("16");
            ivacCenterItems.add(centerItem12);

            IVACCenterItem centerItem13 = new IVACCenterItem();
            centerItem13.setOrder(13);
            centerItem13.setCenterName("IVAC , CUMILLA");
            centerItem13.setCenterValue("17");
            ivacCenterItems.add(centerItem13);

            IVACCenterItem centerItem14 = new IVACCenterItem();
            centerItem14.setOrder(14);
            centerItem14.setCenterName("IVAC , NOAKHALI");
            centerItem14.setCenterValue("18");
            ivacCenterItems.add(centerItem14);

            IVACCenterItem centerItem15 = new IVACCenterItem();
            centerItem15.setOrder(15);
            centerItem15.setCenterName("IVAC , BRAHMANBARIA");
            centerItem15.setCenterValue("19");
            ivacCenterItems.add(centerItem15);

            return ivacCenterItems;
        }
    }

    @Override
    public PrimeLifeBillResponse getPrimeLifeInfo(String msisdn,PrimeLifeInsuranceBillFetchRequest request) {
        BillPreviewRestInput input = new BillPreviewRestInput();
        input.setBillerId(request.getCode());
        input.setCode(request.getCode());
        input.setAccountNumber(msisdn);
        input.setKey1(request.getPolicyNumber());
        PrimeLifeBillResponse response = new PrimeLifeBillResponse();
        try {
            BillFetchResponse billFetchResponse = insuranceServiceGateway.getPrimeLifeInsuracneFetch(input);
            log.info("received response");
            log.info(billFetchResponse.toString());
            BaseResponse baseResponse = billFetchResponse.getResponse();
            if(baseResponse != null){
                if(baseResponse.getResponseCode().equals(CommonConstant.SUCCESS_CODE)) {
                    response.setBillAmount(billFetchResponse.getBillAmount());
                    List<HashMap<String, Object>> tableDatas = billFetchResponse.getTableData();
                    for (int i = 0; i < tableDatas.size(); i++) {
                        HashMap<String, Object> item = tableDatas.get(i);
                        if (item.get("key").equals("customerNumber"))
                            response.setPolicyNumber(String.valueOf(item.get("value")));
                        else if (item.get("key").equals("customerName"))
                            response.setCustomerName(String.valueOf(item.get("value")));
                        else if (item.get("key").equals("billAmount"))
                            response.setBillAmount(String.valueOf(item.get("value")));
                        else if (item.get("key").equals("nextPremiumDueDate"))
                            response.setNextPremiumDueDate(String.valueOf(item.get("value")));
                        else if (item.get("key").equals("policyMaturityDate"))
                            response.setPolicyMatureDate(String.valueOf(item.get("value")));
//                    else if(item.get("key").equals(""))
                    }

                    HashMap<String, Object> payloads = billFetchResponse.getPayload();
                    response.setTransactionId(String.valueOf(payloads.get("key3")));
                    log.info("final repsonse");
                    log.info(response.toString());
                    return response;
                }else {
                    throw new ApiRequestException(baseResponse.getResponseMessage());
                }
//                else if()

            }else {
                throw new ApiRequestException(baseResponse.getResponseMessage());
            }
        }catch (Exception ex){
            throw new ApiRequestException(ex.getMessage());
        }
//        if(billFetchResponse.getResponse().getResponseCode().equals())
//
//        return response;
    }

    @Override
    public REBBillInfoResponse getREBPostpaidBill(String smsAccountNumber, String customerAccountNumber, Locale locale) {
        REBBillInfoResponse billInfoResponse = new REBBillInfoResponse();

        BillPreviewRestInput input = new BillPreviewRestInput();
        input.setAccountNumber(customerAccountNumber);
        input.setBillerId("REB-Postpaid");
        input.setCode("REB");
        input.setKey1(smsAccountNumber);
        input.setKey3("APP_CUSTOMER"); // for agent middleware it will be APP_AGENT

        try {
            BillFetchResponse billFetchResponse = rebServiceGateway.fetchPostpaidBillDetails(input);
            log.info(billFetchResponse.toString());
            BaseResponse baseResponse = billFetchResponse.getResponse();
            if (baseResponse != null) {
                if (baseResponse.getResponseCode().equals(CommonConstant.SUCCESS_CODE)) {
                    billInfoResponse.setBillAmount(billFetchResponse.getBillAmount());
                    List<HashMap<String, Object>> tableDatas = billFetchResponse.getTableData();
                    for (int i = 0; i < tableDatas.size(); i++) {
                        HashMap<String, Object> item = tableDatas.get(i);
                        if (item.get("key").equals("billNo")) {
                            billInfoResponse.setBillNo(String.valueOf(item.get("value")));
                        } else if (item.get("key").equals("dueDate")) {
                            billInfoResponse.setDueDate(String.valueOf(item.get("value")));
                        } else if (item.get("key").equals("lpcDate")) {
                            billInfoResponse.setLpcDate(String.valueOf(item.get("value")));
                        } else if (item.get("key").equals("paidStatus")) {
                            billInfoResponse.setPaymentStatus(String.valueOf(item.get("value")));
                        } else if (item.get("key").equals("billMonth")) {
                            billInfoResponse.setBillMonth(String.valueOf(item.get("value")));
                        } else if (item.get("key").equals("billYear")) {
                            billInfoResponse.setBillYear(String.valueOf(item.get("value")));
                        }
                    }
                    billInfoResponse.setResponse(baseResponse);
                    return billInfoResponse;
                } else if(baseResponse.getResponseCode().equals("1278")){
                    billInfoResponse.setBillAmount("-1");
                    billInfoResponse.setResponse(baseResponse);
                    return billInfoResponse;
                }else {
                    throw new ApiRequestException(baseResponse.getResponseMessage());
                }
            } else {
                throw new ApiRequestException(CommonConstant.COMMON_ERROR);
            }
        } catch (Exception e) {
            log.error("error: {}", e);
            throw new ApiRequestException(e.getMessage());
        }
    }
}
