package com.mfs.api_middleware.service;

import com.google.gson.Gson;
import com.mfs.api_middleware.Exception.InvalidTransactionException;
import com.mfs.api_middleware.dto.*;
import com.mfs.api_middleware.gateway.ApiManagerGateway;
import com.mfs.api_middleware.util.CommonConstant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Locale;

@Slf4j
@Service
public class FeesAndChargesServiceImpl implements FeesAndChargesService {

    @Autowired
    ApiManagerGateway apiManagerGateway;

    @Override
    public FeesChargesResponse feeAndCharge(FeesAndChargesModelWallet feesAndChargesModelWallet, Locale locale) {

        FeesAndChargesModel feesAndChargesModel = new FeesAndChargesModel();
        feesAndChargesModel.setFromUserNumber(feesAndChargesModelWallet.getFromUserNumber());
        if(feesAndChargesModelWallet.getToUserNumber() != null && !feesAndChargesModelWallet.getToUserNumber().isEmpty()) {
            feesAndChargesModel.setToUserNumber(feesAndChargesModelWallet.getToUserNumber());
        }

        feesAndChargesModel.setAmount(feesAndChargesModelWallet.getAmount());
        feesAndChargesModel.setTransactionType(feesAndChargesModelWallet.getTransactionType());

        feesAndChargesModel.setFromUserNumber(CommonConstant.checkNumber(feesAndChargesModel.getFromUserNumber()));
        if(feesAndChargesModel.getTransactionType().equalsIgnoreCase("FCCO")) {
            feesAndChargesModel.setToUserNumber("0");
        } else if (feesAndChargesModel.getToUserNumber() != null && !feesAndChargesModel.getToUserNumber().isEmpty()) {
            feesAndChargesModel.setToUserNumber(CommonConstant.checkNumber(feesAndChargesModel.getToUserNumber()));
        }else {
            feesAndChargesModel.setToUserNumber("0");
        }

        String apiResponse = (apiManagerGateway.feeAndCharges(feesAndChargesModel, locale));
        if (apiResponse != null && !apiResponse.isEmpty()) {
            FeesChargesResponse response = new Gson().fromJson(apiResponse.replace("\\", ""), FeesChargesResponse.class);
            return response;
        } else {
            throw new InvalidTransactionException("You are not allowed to make this transaction");
        }

    }

    @Override
    public NidDueResponse feesNidResponse(String nid_no, String correction_type, String service_type, Locale locale) {
        return apiManagerGateway.getNidResponse(nid_no,correction_type,service_type,locale);
    }

    @Override
    public FeesAndChargesWithCommissionResponse feesAndChargeCommissionDetails(FeesAndChargesModelWallet request, Locale locale) {
        FeesAndChargesWithCommissionRequest feesAndChargesWithCommissionRequest = new FeesAndChargesWithCommissionRequest();
        feesAndChargesWithCommissionRequest.setFromUserNumber(Long.parseLong(CommonConstant.checkNumber(request.getFromUserNumber().toString())));
        feesAndChargesWithCommissionRequest.setToUserNumber(Long.parseLong(CommonConstant.checkNumber(request.getToUserNumber().toString())));
        feesAndChargesWithCommissionRequest.setAmount(request.getAmount());
        feesAndChargesWithCommissionRequest.setTransactionType(request.getTransactionType());
        return apiManagerGateway.getFeesAndChargesWithCommissionResponse(feesAndChargesWithCommissionRequest,locale);
    }
}
