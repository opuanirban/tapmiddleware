package com.mfs.api_middleware.service;

import com.mfs.api_middleware.Exception.DecodePinException;
import com.mfs.api_middleware.Exception.InvalidTransactionException;
import com.mfs.api_middleware.Exception.TransactionProcessException;
import com.mfs.api_middleware.config.ApplicationProperties;
import com.mfs.api_middleware.dto.ApiManagerRequest;
import com.mfs.api_middleware.dto.CommonResponse;
import com.mfs.api_middleware.dto.DonationPayment;
import com.mfs.api_middleware.dto.MerchantPayment;
import com.mfs.api_middleware.enumeration.ContextUrl;
import com.mfs.api_middleware.enumeration.TransactionEvents;
import com.mfs.api_middleware.gateway.ApiManagerGateway;
import com.mfs.api_middleware.service.security.ISecurityService;
import com.mfs.api_middleware.util.CommonConstant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Base64;
import java.util.Locale;
import java.util.regex.Pattern;

@Service
@Slf4j
public class MerchantPaymentServiceImpl extends ProcessRequestService implements MerchantPaymentService {

    @Autowired
    ApiManagerGateway apiManagerGateway;

    @Autowired
    ISecurityService iSecurityService;

    @Autowired
    ApplicationProperties applicationProperties;

    @Autowired
    TransactionProcessService transactionProcessService;

    @Override
    public CommonResponse payMerchant(MerchantPayment merchantPayment, String account, Locale locale) {

        if (applicationProperties.getCheckEnabled()) {
            if (transactionProcessService.findProcess(account, merchantPayment.getAmount(),merchantPayment.getRecipientNumber(), TransactionEvents.MERCHANT_PAYMENT.toString())) {
                log.error("Same transaction within 10 minutes");
                throw new TransactionProcessException("User not allowed to do the same transaction within " +
                        applicationProperties.getDuplicateReqTimeOutInMills() / 60000 + " minutes of last transaction");
            } else {
                return doMerchantPayment(merchantPayment, account, locale);
            }
        } else {
            return doMerchantPayment(merchantPayment, account, locale);
        }
    }

    @Override
    public CommonResponse payDonation(DonationPayment donationPaymentPayment, String account, Locale locale) {
        MerchantPayment merchantPayment = new MerchantPayment();
        merchantPayment.setAmount(donationPaymentPayment.getAmount());
        merchantPayment.setPin(donationPaymentPayment.getPin());
        merchantPayment.setPurpose("donation");
        merchantPayment.setNotificationNumber(donationPaymentPayment.getNotificationNumber());

        if(donationPaymentPayment.getCode().equalsIgnoreCase("BIDYANONDO")) {
            // todo need to fetch merchant number from backend
            String merchantNumber = "01766685686";
            merchantPayment.setRecipientNumber(merchantNumber);
        } else {
            throw new InvalidTransactionException("Invalid request. Merchant not supported");
        }

        return payMerchant(merchantPayment, account, locale);
    }

    private CommonResponse doMerchantPayment(MerchantPayment merchantPayment, String account, Locale locale) {
        if (Pattern.matches("......==", merchantPayment.getPin())) {
            String formattedText = "TRUSTMM MPAY " + CommonConstant.checkNumber(merchantPayment.getRecipientNumber()) + " " + merchantPayment.getPurpose() + " " + merchantPayment.getAmount() + " " + new String(Base64.getDecoder().decode(merchantPayment.getPin())) + " " + CommonConstant.checkNumber(merchantPayment.getNotificationNumber());
            ApiManagerRequest apiManagerRequest = makeProcessedRequest(applicationProperties.getEncryptionEnabled() ?
                    iSecurityService.encrypt(formattedText, "", "") : formattedText, ContextUrl.API_MANAGER_CONTEXT.getUrl(), account);

            CommonResponse response = getTransactionFormattedResponse(apiManagerGateway, apiManagerRequest, locale, true);

            try {
                transactionProcessService.storeProcess(account, merchantPayment.getAmount(),merchantPayment.getRecipientNumber(), TransactionEvents.MERCHANT_PAYMENT.toString());
            } catch (Exception e) {
                log.error(e.getMessage());
            }
            return response;

        } else {
            throw new DecodePinException(CommonConstant.DECODE_EXCEPTION_MESSAGE);
        }

    }
}
