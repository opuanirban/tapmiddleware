package com.mfs.api_middleware.service;

import com.mfs.api_middleware.Exception.DecodePinException;
import com.mfs.api_middleware.Exception.TransactionProcessException;
import com.mfs.api_middleware.config.ApplicationProperties;
import com.mfs.api_middleware.dto.ApiManagerRequest;
import com.mfs.api_middleware.dto.CommonResponse;
import com.mfs.api_middleware.dto.P2PRequest;
import com.mfs.api_middleware.enumeration.ContextUrl;
import com.mfs.api_middleware.enumeration.TransactionEvents;
import com.mfs.api_middleware.gateway.ApiManagerGateway;
import com.mfs.api_middleware.service.security.ISecurityService;
import com.mfs.api_middleware.util.CommonConstant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Base64;
import java.util.Locale;
import java.util.regex.Pattern;

@Service
@Slf4j
public class P2PServiceImpl extends ProcessRequestService implements P2PService {

    @Autowired
    ApiManagerGateway apiManagerGateway;

    @Autowired
    ISecurityService iSecurityService;

    @Autowired
    ApplicationProperties applicationProperties;

    @Autowired
    TransactionProcessService transactionProcessService;

    @Override
    public CommonResponse p2p(P2PRequest p2PRequest, String accountNo, Locale locale) {

        if (applicationProperties.getCheckEnabled()) {
            if (transactionProcessService.findProcess(accountNo, p2PRequest.getAmount(),p2PRequest.getRecipientNumber(), TransactionEvents.P2P.toString())) {
                log.error("Same transaction within 10 minutes");
                throw new TransactionProcessException("User not allowed to do the same transaction within " +
                        applicationProperties.getDuplicateReqTimeOutInMills() / 60000 + " minutes of last transaction");
            } else {
                return doP2P(p2PRequest, accountNo, locale);
            }
        } else {
            return doP2P(p2PRequest, accountNo, locale);
        }

    }

    private CommonResponse doP2P(P2PRequest p2PRequest, String accountNo, Locale locale) {
        if (Pattern.matches("......==", p2PRequest.getPin())) {
            String formattedText = "TRUSTMM PAY " + CommonConstant.checkNumber(p2PRequest.getRecipientNumber()) + " " + p2PRequest.getAmount() + " " + new String(Base64.getDecoder().decode(p2PRequest.getPin()));
            ApiManagerRequest apiManagerRequest = makeProcessedRequest(applicationProperties.getEncryptionEnabled() ?
                    iSecurityService.encrypt(formattedText, "", "") : formattedText, ContextUrl.API_MANAGER_CONTEXT.getUrl(), accountNo);

            CommonResponse response = getTransactionFormattedResponse(apiManagerGateway, apiManagerRequest, locale, true);

            try {
                transactionProcessService.storeProcess(accountNo, p2PRequest.getAmount(),p2PRequest.getRecipientNumber(), TransactionEvents.P2P.toString());
            } catch (Exception e) {
                log.error(e.getMessage());
            }

            return response;

        } else {
            throw new DecodePinException(CommonConstant.DECODE_EXCEPTION_MESSAGE);
        }

    }
}
