package com.mfs.api_middleware.service;

import com.mfs.api_middleware.dto.LimitInfoResponse;

import java.util.List;
import java.util.Locale;

public interface LimitInfoService {
    List<LimitInfoResponse> fetchLimit(String userAccount, Locale locale);
}
