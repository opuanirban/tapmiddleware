package com.mfs.api_middleware.service;

import com.mfs.api_middleware.dto.EkycNotificationRequest;
import com.mfs.api_middleware.dto.TblEkycUpdateResponse;
import com.mfs.api_middleware.dto.TblRegistrationStatus;

import java.util.Locale;

public interface TblRegistrationStatusService {
    public TblEkycUpdateResponse passDataToTbl(EkycNotificationRequest data, Locale locale);

    public TblRegistrationStatus getKycFromTbl(String msisdn, Locale locale);
}
