package com.mfs.api_middleware.service;

import java.util.Locale;

import com.mfs.api_middleware.dto.*;

public interface REBService {
    public REBPrepaidResponse payREBPrepaidBill(REBPrepaidRequest prepaidRequest, String accountNo, Locale locale);
    public REBPrepaidResponse payREBPrepaidBill2(REBPrepaidRequest prepaidRequest, String accountNo, Locale locale);
    public REBTransactionDetails rebTransactionDetails(String transactionId, String mfsAccountNumber);
    public REBTransactionDetails rebTransactionDetails2(String transactionId);
    public REBPendingTransactionsResponse getPendingTransactionList(String mfsAccountNumber);
    public REBPendingTransactionsResponse getPendingTransactionList2(String mfsAccountNumber);
    public REBPrepaidResponse retryREBPrepaidBill(String transactionId, String accountNo, Locale locale);
    public REBPrepaidResponse retryREBPrepaidBill2(String transactionId);
    public CommonResponse payREBPostpaidBill(REBPostpaidPaymentRequest postpaidRequest, String accountNo, Locale locale);
}
