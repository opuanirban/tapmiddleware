package com.mfs.api_middleware.service;

public interface OtpBlacklistService {
    boolean checkMsisdn(String msisdn);
}
