package com.mfs.api_middleware.service;

import java.util.Locale;

import com.mfs.api_middleware.dto.CommonResponse;
import com.mfs.api_middleware.dto.IvacPaymentRequest;
import com.mfs.api_middleware.dto.JGTDSLPaymentRequest;

public interface UtilityBillService {
    CommonResponse payJGTDSLBill(JGTDSLPaymentRequest request, String tapAccount, Locale locale);
    CommonResponse payIvacBill(IvacPaymentRequest request,String tapAccount,Locale locale);
}
