package com.mfs.api_middleware.service;

import com.mfs.api_middleware.dto.CommonResponse;
import com.mfs.api_middleware.dto.CoreBankingAccountList;
import com.mfs.api_middleware.dto.FloraFundTransferRequest;

import java.util.Locale;

public interface FloraFundService {

    public CommonResponse fundTransfer(FloraFundTransferRequest floraFundTransferRequest, String accountNo, Locale locale);
    public CoreBankingAccountList getCoreBankingAccountInfo(String msisdn,Locale locale);

}
