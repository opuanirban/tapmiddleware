package com.mfs.api_middleware.service;

import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

import com.mfs.api_middleware.Exception.ApiRequestException;
import com.mfs.api_middleware.Exception.DecodePinException;
import com.mfs.api_middleware.config.ApplicationProperties;
import com.mfs.api_middleware.dto.ApiManagerRequest;
import com.mfs.api_middleware.dto.CommonResponse;
import com.mfs.api_middleware.dto.TuitionFeeBillRequest;
import com.mfs.api_middleware.dto.TuitionFeeDetailRequest;
import com.mfs.api_middleware.dto.TuitionFeeDetailResponse;
import com.mfs.api_middleware.dto.TuitionFeePaymentRequest;
import com.mfs.api_middleware.enumeration.ContextUrl;
import com.mfs.api_middleware.gateway.ApiManagerGateway;
import com.mfs.api_middleware.gateway.model.BaseResponse;
import com.mfs.api_middleware.gateway.model.BillFetchResponse;
import com.mfs.api_middleware.gateway.model.BillPaymentInput;
import com.mfs.api_middleware.gateway.model.BillPaymentResponse;
import com.mfs.api_middleware.gateway.model.BillPreviewRestInput;
import com.mfs.api_middleware.gateway.school.SchoolServiceGateway;
import com.mfs.api_middleware.service.security.ISecurityService;
import com.mfs.api_middleware.util.CommonConstant;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TuitionPaymentServiceImpl extends ProcessRequestService implements TuitionPaymentService {

    @Autowired
    ApiManagerGateway apiManagerGateway;

    @Autowired
    SchoolServiceGateway schoolServiceGateway;

    @Autowired
    ISecurityService iSecurityService;

    @Autowired
    ApplicationProperties applicationProperties;

    /*
        Special Tuition Fee Payment Message Format:
        BISC: TRUSTMM BISC ReqNo NoOfMonth PIN StudentName NotificationNumber
        BUP: TRUSTMM BUP ReferenceNumber PaymentToMonth ShortName/InvoiceNumber  NotificiationMobileNo Pin

    */

    @Override
    public CommonResponse payTuition(TuitionFeePaymentRequest tuitionFeePaymentRequest, String accountNo, Locale locale) {
        if (Pattern.matches("......==", tuitionFeePaymentRequest.getPin())) {
            String formattedText;

            List<String> specialFormattedList = Arrays.asList("MIS", "RCPSC", "CESC", "DPSC", "DCGPC", "LPSC",
                    "NCPSC", "PISE", "BGBPS", "CPSCR", "TMSR", "BISC");
            if(tuitionFeePaymentRequest.getInstitutionCode().equalsIgnoreCase("BUP")) {
                // TrustMM BUP EV1406097 1 202125625 8801727262490 1785
                formattedText = "TRUSTMM " + tuitionFeePaymentRequest.getInstitutionCode() + " " + tuitionFeePaymentRequest.getRegNo() + " " + tuitionFeePaymentRequest.getNumberOfMonth() + " " + tuitionFeePaymentRequest.getShortName() + " " + CommonConstant.checkNumber(accountNo) + " " + new String(Base64.getDecoder().decode(tuitionFeePaymentRequest.getPin()));
            } else if (specialFormattedList.contains(tuitionFeePaymentRequest.getInstitutionCode())) {
                formattedText = "TRUSTMM " + tuitionFeePaymentRequest.getInstitutionCode() + " " + tuitionFeePaymentRequest.getRegNo() + " " + tuitionFeePaymentRequest.getNumberOfMonth() + " " + new String(Base64.getDecoder().decode(tuitionFeePaymentRequest.getPin())) + " " + tuitionFeePaymentRequest.getShortName();
            } else {
                formattedText = "TRUSTMM " + tuitionFeePaymentRequest.getInstitutionCode() + " " + tuitionFeePaymentRequest.getRegNo() + " " + tuitionFeePaymentRequest.getNumberOfMonth() + " " + tuitionFeePaymentRequest.getShortName() + " " + new String(Base64.getDecoder().decode(tuitionFeePaymentRequest.getPin()));
            }

            ApiManagerRequest apiManagerRequest = makeProcessedRequest(applicationProperties.getEncryptionEnabled() ?
                    iSecurityService.encrypt(formattedText, "", "") : formattedText, ContextUrl.API_MANAGER_CONTEXT.getUrl(), accountNo);
            CommonResponse response = getTransactionFormattedResponse(apiManagerGateway, apiManagerRequest, locale, true);
            return response;

        } else {
            throw new DecodePinException(CommonConstant.DECODE_EXCEPTION_MESSAGE);
        }

    }

    @Override
    public TuitionFeeDetailResponse fetchTuitionDetails(TuitionFeeDetailRequest request, String accountNo, Locale locale) {
        TuitionFeeDetailResponse response = new TuitionFeeDetailResponse();

        BillPreviewRestInput input = new BillPreviewRestInput();
        input.setAccountNumber(accountNo);
        input.setBillerId(request.getInstitutionCode());
        input.setCode(request.getInstitutionCode());
        input.setBillNumber(request.getRegNo());
        input.setKey1(request.getBillDate());

        try {
            BillFetchResponse billFetchResponse = schoolServiceGateway.fetchBill(input);
            BaseResponse baseResponse = billFetchResponse.getResponse();
            if (baseResponse != null) {
                if (baseResponse.getResponseCode().equals(CommonConstant.SUCCESS_CODE)) {
                    response.setCustomerName(String.valueOf(billFetchResponse.getPayload().get("key2")));
                    response.setTransactionId(String.valueOf(billFetchResponse.getPayload().get("key3"))); // key3 for transaction id
                    response.setDueDate(request.getBillDate());
                    double amount = Double.parseDouble(billFetchResponse.getBillAmount());
                    if (amount <= 0) {
                        response.setBillAmount("-1");
                    } else {
                        response.setBillAmount(billFetchResponse.getBillAmount());
                    }
                    return response;
                } else {
                    throw new ApiRequestException(baseResponse.getResponseMessage());
                }
            } else {
                throw new ApiRequestException(CommonConstant.COMMON_ERROR);
            }
        } catch (Exception e) {
            throw new ApiRequestException(e.getMessage());
        }
    }

    @Override
    public CommonResponse makeTuitionPayment(TuitionFeeBillRequest request, String accountNo, Locale locale) {
        CommonResponse response = new CommonResponse();

        BillPaymentInput input = new BillPaymentInput();
        input.setAccountNumber(accountNo);
        input.setBillerCode(request.getInstitutionCode());
        input.setPaymentChannel("app");
        input.setFee(input.fee);
        input.setNotificationNumber(request.getNotificationNumber());
        input.setKey3(request.getTransactionId());
        input.setPin(request.getPin());

        try {
            BillPaymentResponse billPaymentResponse = schoolServiceGateway.payBill(input);
            BaseResponse baseResponse = billPaymentResponse.getResponse();
            if (baseResponse != null) {
                if (baseResponse.getResponseCode().equals(CommonConstant.SUCCESS_CODE)) {
                    response.setMessage(baseResponse.getResponseMessage());
                    return response;
                } else {
                    throw new ApiRequestException(baseResponse.getResponseMessage());
                }
            } else {
                throw new ApiRequestException(CommonConstant.COMMON_ERROR);
            }
        } catch (Exception e) {
            throw new ApiRequestException(e.getMessage());
        }
    }
}
