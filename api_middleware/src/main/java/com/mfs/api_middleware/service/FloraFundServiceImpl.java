package com.mfs.api_middleware.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mfs.api_middleware.Exception.ApiManagerRequestException;
import com.mfs.api_middleware.Exception.DecodePinException;
import com.mfs.api_middleware.Exception.TransactionProcessException;
import com.mfs.api_middleware.config.ApplicationProperties;
import com.mfs.api_middleware.dto.*;
import com.mfs.api_middleware.enumeration.ContextUrl;
import com.mfs.api_middleware.enumeration.TransactionEvents;
import com.mfs.api_middleware.gateway.ApiManagerGateway;
import com.mfs.api_middleware.service.security.ISecurityService;
import com.mfs.api_middleware.util.CommonConstant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

@Service
@Slf4j
public class FloraFundServiceImpl extends ProcessRequestService implements FloraFundService {

    @Autowired
    ApiManagerGateway apiManagerGateway;

    @Autowired
    ISecurityService iSecurityService;

    @Autowired
    ApplicationProperties applicationProperties;


    @Autowired
    TransactionProcessService transactionProcessService;

    @Override
    public CommonResponse fundTransfer(FloraFundTransferRequest floraFundTransferRequest, String accountNo, Locale locale) {

        if (applicationProperties.getCheckEnabled()) {

            return doFloraTransfer(floraFundTransferRequest, accountNo, locale);

        } else {
            return doFloraTransfer(floraFundTransferRequest, accountNo, locale);
        }


    }

    private CommonResponse doFloraTransfer(FloraFundTransferRequest floraFundTransferRequest, String accountNo, Locale locale) {
        if (Pattern.matches("......==", floraFundTransferRequest.getPin())) {
            String formattedText = "TRUSTMM FCDP " + floraFundTransferRequest.getRecipientAccount() + " " + floraFundTransferRequest.getAmount() + " " + new String(Base64.getDecoder().decode(floraFundTransferRequest.getPin()));
            ApiManagerRequest apiManagerRequest = makeProcessedRequest(applicationProperties.getEncryptionEnabled() ?
                    iSecurityService.encrypt(formattedText, "", "") : formattedText, ContextUrl.API_MANAGER_CONTEXT.getUrl(), accountNo);

            CommonResponse response = getTransactionFormattedResponse(apiManagerGateway, apiManagerRequest, locale, true);
            return response;


        } else {
            throw new DecodePinException(CommonConstant.DECODE_EXCEPTION_MESSAGE);
        }

    }

    @Override
    public CoreBankingAccountList getCoreBankingAccountInfo(String msisdn, Locale locale) {
        CoreBankingAccountList coreBankingAccountList = new CoreBankingAccountList();
        List<CoreBankingAccountInfo> coreBankingAccountInfos = new ArrayList<>();

        String responseFromTbl = apiManagerGateway.fetchUserCoreBankingAccountInfo(msisdn, locale);
        ObjectMapper mapper = new ObjectMapper();
        List<TBLCBSAccountInfo> participantJsonList = null;
        try {
            participantJsonList = mapper.readValue(responseFromTbl, new TypeReference<List<TBLCBSAccountInfo>>() {
            });
        } catch (JsonProcessingException e) {
            log.error("response parsing exception: " + e.getMessage());
            throw new ApiManagerRequestException(e.getMessage());
        }

        int index = 1;
        if (!participantJsonList.isEmpty()) {
            for (TBLCBSAccountInfo tblcbsAccountInfo : participantJsonList) {
                CoreBankingAccountInfo coreBankingAccountInfo = new CoreBankingAccountInfo();
                coreBankingAccountInfo.setSerialNo(index++);
                coreBankingAccountInfo.setMaskedAccountNo(tblcbsAccountInfo.getKey());
                // coreBankingAccountInfo.setAccountName(tblcbsAccountInfo.getValue());
                coreBankingAccountInfo.setAccountName("Trust Bank Account");

                coreBankingAccountInfos.add(coreBankingAccountInfo);
            }
        }

        coreBankingAccountList.setAccountInfo(coreBankingAccountInfos);
        return coreBankingAccountList;
    }
}
