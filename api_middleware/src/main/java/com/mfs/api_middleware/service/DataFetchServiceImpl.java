package com.mfs.api_middleware.service;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.*;

import com.mfs.api_middleware.dao.RedisDao;
import com.mfs.api_middleware.dto.*;
import com.mfs.api_middleware.enumeration.BillTypeEnum;
import com.mfs.api_middleware.gateway.ApiManagerGateway;
import com.mfs.api_middleware.gateway.CampaignGateway;
import com.mfs.api_middleware.util.CommonConstant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class DataFetchServiceImpl implements DataFetchService {

    @Autowired
    ApiManagerGateway apiManagerGateway;

    @Autowired
    RedisDao redisDao;
    
     private static final String biller_info_en = "Electricity#Electricity.png#1###DESCO Postpaid#DESCO#DESCO.png#1##DESCO Prepaid#DESPR#DESCO.png#1##NESCO Prepaid#NESPR#NESCO.png#1##NESCO Postpaid#NESCO#NESCO.png#1##DPDC Postpaid#DPDC#DPDC.png#1##WZPDC Postpaid#WZPDCE#WZPDC.png#1##Polli Bidyut Prepaid#REBPR#REB.png#1##Polli Bidyut Postpaid#REB#REB.png#1;Gas#Gas.png#1###Bakhrabad Gas#BAKHRE#BAKHRABAD.png#1;Water#Water.png#1###Dhaka WASA#WASADE#DWASA.png#1##Khulna WASA#WASAKE#KWASA.png#1;TV/DTH#tv.png#1###AKASH DTH#AKASH#akash.png#1;Internet#internet.png#1###Carnival#CARNIVAL#CARNIVAL.png#1##Trust Innovation Limited#TIL#TIL.png#1";
     private static final String biller_info_bn = "বিদ্যুৎ#Electricity.png#1###ডেসকো পোস্টপেইড#DESCO#DESCO.png#1##ডেসকো প্রিপেইড#DESPR#DESCO.png#1##নেসকো প্রিপেইড#NESPR#NESCO.png#1##নেসকো পোস্টপেইড#NESCO#NESCO.png#1##ডিপিডিসি পোস্টপেইড#DPDC#DPDC.png#1##WZPDC পোস্টপেইড#WZPDCE#WZPDC.png#1##পল্লীবিদ্যুৎ প্রিপেইড#REBPR#REB.png#1##পল্লীবিদ্যুৎ পোস্টপেইড#REB#REB.png#1;গ্যাস#Gas.png#1###বাখরাবাদ গ্যাস#BAKHRE#BAKHRABAD.png#1;পানি#Water.png#1###ঢাকা ওয়াসা#WASADE#DWASA.png#1##খুলনা ওয়াসা#WASAKE#KWASA.png#1;টিভি/ডিটিএইচ#tv.png#1###আকাশ DTH#AKASH#akash.png#1;ইন্টারনেট#internet.png#1###কার্নিভাল#CARNIVAL#CARNIVAL.png#1##Trust Innovation Limited#TIL#TIL.png#1";

    //private static final String biller_info_en = "Electricity#Electricity.png#1###DESCO Postpaid#DESCO#DESCO.png#1##DESCO Prepaid#DESPR#DESCO.png#1##NESCO Prepaid#NESPR#NESCO.png#1##NESCO Postpaid#NESCO#NESCO.png#1##DPDC Postpaid#DPDC#DPDC.png#1##WZPDC Postpaid#WZPDCE#WZPDC.png#1##Polli Bidyut Prepaid#REBPR#REB.png#1##Polli Bidyut Postpaid#REB#REB.png#1;Gas#Gas.png#1###Bakhrabad Gas#BAKHRE#BAKHRABAD.png#1;Water#Water.png#1###Dhaka WASA#WASADE#DWASA.png#1##Khulna WASA#WASAKE#KWASA.png#1;TV/DTH#tv.png#1###AKASH DTH#AKASH#akash.png#1;Internet#internet.png#1###Carnival#CARNIVAL#CARNIVAL.png#1##Trust Innovation Limited#TIL#TIL.png#1";
    //private static final String biller_info_bn = "বিদ্যুৎ#Electricity.png#1###ডেসকো পোস্টপেইড#DESCO#DESCO.png#1##ডেসকো প্রিপেইড#DESPR#DESCO.png#1##নেসকো প্রিপেইড#NESPR#NESCO.png#1##নেসকো পোস্টপেইড#NESCO#NESCO.png#1##ডিপিডিসি পোস্টপেইড#DPDC#DPDC.png#1##WZPDC পোস্টপেইড#WZPDCE#WZPDC.png#1##পল্লীবিদ্যুৎ প্রিপেইড#REBPR#REB.png#1##পল্লীবিদ্যুৎ পোস্টপেইড#REB#REB.png#1;গ্যাস#Gas.png#1###বাখরাবাদ গ্যাস#BAKHRE#BAKHRABAD.png#1;পানি#Water.png#1###ঢাকা ওয়াসা#WASADE#DWASA.png#1##খুলনা ওয়াসা#WASAKE#KWASA.png#1;টিভি/ডিটিএইচ#tv.png#1###আকাশ DTH#AKASH#akash.png#1;ইন্টারনেট#internet.png#1###কার্নিভাল#CARNIVAL#CARNIVAL.png#1##Trust Innovation Limited#TIL#TIL.png#1";

     private static final String ticket_info_en = "Train#train.png#1###Rail Sheba#Rail Sheba#railheba.png#1;";
     private static final String ticket_info_bn = "বিদ্যুৎ#Electricity.png#1###ডেসকো পোস্টপেইড#DESCO#DESCO.png#1##ডেসকো প্রিপেইড#DESPR#DESCO.png#1##নেসকো প্রিপেইড#NESPR#NESCO.png#1##নেসকো পোস্টপেইড#NESCO#NESCO.png#1##ডিপিডিসি পোস্টপেইড#DPDC#DPDC.png#1##WZPDC পোস্টপেইড#WZPDCE#WZPDC.png#1##পল্লীবিদ্যুৎ প্রিপেইড#REBPR#REB.png#1##পল্লীবিদ্যুৎ পোস্টপেইড#REB#REB.png#1;গ্যাস#Gas.png#1###বাখরাবাদ গ্যাস#BAKHRE#BAKHRABAD.png#1;পানি#Water.png#1###ঢাকা ওয়াসা#WASADE#DWASA.png#1##খুলনা ওয়াসা#WASAKE#KWASA.png#1;টিভি/ডিটিএইচ#tv.png#1###আকাশ DTH#AKASH#akash.png#1;ইন্টারনেট#internet.png#1###কার্নিভাল#CARNIVAL#CARNIVAL.png#1##Trust Innovation Limited#TIL#TIL.png#1";

//    private static final String biller_info_en = "Electricity#Electricity.png#1###DESCO Postpaid#DESCO#DESCO.png#1##DESCO Prepaid#DESPR#DESCO.png#1##NESCO Prepaid#NESPR#NESCO.png#1##NESCO Postpaid#NESCO#NESCO.png#1##DPDC Postpaid#DPDC#DPDC.png#1##WZPDC Postpaid#WZPDCE#WZPDC.png#1;Gas#Gas.png#1###Bakhrabad Gas#BAKHRE#BAKHRABAD.png#1;Water#Water.png#1###Dhaka WASA#WASADE#DWASA.png#1##Khulna WASA#WASAKE#KWASA.png#1;TV/DTH#tv.png#1###AKASH DTH#AKASH#akash.png#1";
//    private static final String biller_info_bn = "বিদ্যুৎ#Electricity.png#1###ডেসকো পোস্টপেইড#DESCO#DESCO.png#1##ডেসকো প্রিপেইড#DESPR#DESCO.png#1##নেসকো প্রিপেইড#NESPR#NESCO.png#1##নেসকো পোস্টপেইড#NESCO#NESCO.png#1##ডিপিডিসি পোস্টপেইড#DPDC#DPDC.png#1##WZPDC পোস্টপেইড#WZPDCE#WZPDC.png#1;গ্যাস#Gas.png#1###বাখরাবাদ গ্যাস#BAKHRE#BAKHRABAD.png#1;পানি#Water.png#1###ঢাকা ওয়াসা#WASADE#DWASA.png#1##খুলনা ওয়াসা#WASAKE#KWASA.png#1;টিভি/ডিটিএইচ#tv.png#1###আকাশ DTH#AKASH#akash.png#1";

    private static final String fees_info_en = "Indian Visa Fee#IVAC#ivac.png#1##Passport Fees#DIP#Passport.jpg#0##NID Fees#NID#Nid.jpg#1##Tuition Fees#TUTION#tution.png#1##Insurance Fees#INSURANCE#insurance.png#1";
    private static final String fees_info_bn = "ইন্ডিয়ান ভিসা ফি#IVAC#ivac.png#1##পাসপোর্ট ফি#DIP#Passport.jpg#0##NID ফি#NID#Nid.jpg#1##টিউশন ফি#TUTION#tution.png#1##ইন্সুরেন্স ফি#INSURANCE#insurance.png#1";
//    private static final String biller_icon_url = "https://api.bdkepler.com/images/biller-icon/";
    ///for uat
    private static final String fees_info_new_en = "Indian Visa Fee#IVAC#ivac_fee.png#1##Passport Fees#DIP#passport_fee.png#0##NID Fees#NID#nid_fees.png#1##Tuition Fees#TUTION#tution_fee.png#1##Insurance Fees#INSURANCE#insurance_fee.png#1";
    private static final String fees_info_new_bn = "ইন্ডিয়ান ভিসা ফি#IVAC#ivac_fee.png#1##পাসপোর্ট ফি#DIP#passport_fee.png#0##NID ফি#NID#nid_fees.png#1##টিউশন ফি#TUTION#tution_fee.png#1##ইন্সুরেন্স ফি#INSURANCE#insurance_fee.png#1";

    @Value("${biller_icon.url}")
    private String biller_icon_url;

    @Value("${biller_icon.new_url}")
    private String biller_icon_new_url;


    @Override
    public List<DataListResponse> getUtility(Locale locale) {
        List<DataListResponse> billerList = new ArrayList<>();
        DataListResponse dataListResponse;
        String[] dataList =  locale.getLanguage().equals("en") ? biller_info_en.split(";")
                    : biller_info_bn.split(";");

        Integer[] sequenceNumber = new Integer[]{4,3,1,2,5};

        if(dataList.length > 0) {
            int seq = dataList.length;
            for (int i = 0; i < dataList.length; i++) {
                dataListResponse = new DataListResponse();

                dataListResponse.setSequenceNumber(sequenceNumber[i]);
                String[] utilityItem = dataList[i].split("###");
                if(utilityItem.length > 0) {
                    // set utility data
                    String[] item = utilityItem[0].split("#");
                    if(item.length == 3) {
                        dataListResponse.setCategoryTitle(item[0]);
                        dataListResponse.setCategoryIconSource(biller_icon_url + item[1]);
                        dataListResponse.setCategoryId(i+1);
                        dataListResponse.setActiveStatus(item[2].equals("1"));
                    }
                    if(utilityItem.length == 2) {
                        String[] utilityItems = utilityItem[1].split("##");
                        DataJson[] utilityList= new DataJson[utilityItems.length];
                        // set utility item data
                        for (int j = 0; j < utilityItems.length; j++) {
                            DataJson dataJson = new DataJson();
                            String[] items = utilityItems[j].split("#");
                            if(items.length == 4) {
                                dataJson.setId(UUID.randomUUID().toString());
                                dataJson.setDescription(items[0]);
                                dataJson.setCode(items[1]);
                                dataJson.setIconSource(biller_icon_url + items[2]);
                                dataJson.setActiveStatus(items[3].equals("1"));
                                utilityList[j] = dataJson;
                            }
                        }
                        dataListResponse.setItemList(utilityList);
                    }
                }
                billerList.add(dataListResponse);
            }
        }
        return billerList;
    }

    @Override
    public List<DataListResponse> getTickets(Locale locale){
        List<DataListResponse> billerList = new ArrayList<>();
        DataListResponse dataListResponse;

        System.out.println("Language "+locale.getLanguage()+" String "+ticket_info_en);
        String[] dataList =  locale.getLanguage().equals("en") ? ticket_info_en.split(";")
                : ticket_info_bn.split(";");

        Integer[] sequenceNumber = new Integer[]{1,2,3};

        if(dataList.length > 0) {
            int seq = dataList.length;
            for (int i = 0; i < dataList.length; i++) {
                dataListResponse = new DataListResponse();

                dataListResponse.setSequenceNumber(sequenceNumber[i]);
                String[] utilityItem = dataList[i].split("###");
                if(utilityItem.length > 0) {
                    // set utility data
                    String[] item = utilityItem[0].split("#");
                    if(item.length == 3) {
                        dataListResponse.setCategoryTitle(item[0]);
                        dataListResponse.setCategoryIconSource(biller_icon_url + item[1]);
                        dataListResponse.setCategoryId(i+1);
                        dataListResponse.setActiveStatus(item[2].equals("1"));
                    }
                    if(utilityItem.length == 2) {
                        String[] utilityItems = utilityItem[1].split("##");
                        DataJson[] utilityList= new DataJson[utilityItems.length];
                        // set utility item data
                        for (int j = 0; j < utilityItems.length; j++) {
                            DataJson dataJson = new DataJson();
                            String[] items = utilityItems[j].split("#");
                            if(items.length == 4) {
                                dataJson.setId(UUID.randomUUID().toString());
                                dataJson.setDescription(items[0]);
                                dataJson.setCode(items[1]);
                                dataJson.setIconSource(biller_icon_url + items[2]);
                                dataJson.setActiveStatus(items[3].equals("1"));
                                utilityList[j] = dataJson;
                            }
                        }
                        dataListResponse.setItemList(utilityList);
                    }
                }
                billerList.add(dataListResponse);
            }
        }
        return billerList;

    }


    @Override
    public List<DataListResponse> getUtilityForNewApp(Locale locale) {
        List<DataListResponse> billerList = new ArrayList<>();
        DataListResponse dataListResponse;
        String[] dataList =  locale.getLanguage().equals("en") ? biller_info_en.split(";")
                : biller_info_bn.split(";");

        Integer[] sequenceNumber = new Integer[]{4,3,1,2,5};

        if(dataList.length > 0) {
            int seq = dataList.length;
            for (int i = 0; i < dataList.length; i++) {
                dataListResponse = new DataListResponse();

                dataListResponse.setSequenceNumber(sequenceNumber[i]);
                String[] utilityItem = dataList[i].split("###");
                if(utilityItem.length > 0) {
                    // set utility data
                    String[] item = utilityItem[0].split("#");
                    if(item.length == 3) {
                        dataListResponse.setCategoryTitle(item[0]);
                        dataListResponse.setCategoryIconSource(biller_icon_new_url + item[1]);
                        dataListResponse.setCategoryId(i+1);
                        dataListResponse.setActiveStatus(item[2].equals("1"));
                    }
                    if(utilityItem.length == 2) {
                        String[] utilityItems = utilityItem[1].split("##");
                        DataJson[] utilityList= new DataJson[utilityItems.length];
                        // set utility item data
                        for (int j = 0; j < utilityItems.length; j++) {
                            DataJson dataJson = new DataJson();
                            String[] items = utilityItems[j].split("#");
                            if(items.length == 4) {
                                dataJson.setId(UUID.randomUUID().toString());
                                dataJson.setDescription(items[0]);
                                dataJson.setCode(items[1]);
                                dataJson.setIconSource(biller_icon_url + items[2]);
                                dataJson.setActiveStatus(items[3].equals("1"));
                                utilityList[j] = dataJson;
                            }
                        }
                        dataListResponse.setItemList(utilityList);
                    }
                }
                billerList.add(dataListResponse);
            }
        }
        return billerList;
    }

    @Override
    public void modifyDonationMerchantList(String donationMerchantList) {
        redisDao.setValue(CommonConstant.DONATION_MERCHANT_KEY, donationMerchantList);
    }

    @Override
    public DataJson[] getInsurance(Locale locale) {
        String insuranceResponse = apiManagerGateway.getList(BillTypeEnum.INSURANCE.getBill(), locale);
        return getDataJsons(insuranceResponse);
    }

    @Override
    public DataJson[] getInstitution(Locale locale) {
        String institutionResponse = apiManagerGateway.getList(BillTypeEnum.INSTITUTION.getBill(), locale);
        return getDataJsons(institutionResponse);
    }

    @Override
    public SchoolDataJson[] getInstitutionV2(Locale locale) {
        String institutionResponse = apiManagerGateway.getList(BillTypeEnum.INSTITUTION.getBill(), locale);
//        return getDataJsons(institutionResponse);
        return getSchoolDataJsons(institutionResponse);
    }

    @Override
    public DataJson[] getOthers(Locale locale) {
        String insuranceResponse = apiManagerGateway.getList(BillTypeEnum.OTHERS.getBill(), locale);
        return getDataJsons(insuranceResponse);
    }

    @Override
    public DataJson[] getDonations(Locale locale) {
        String donationBillerList = null;
        try {
            donationBillerList = redisDao.getValue(CommonConstant.DONATION_MERCHANT_KEY);
        }catch (Exception e){
            log.error("data retrieval error from redis: " + e.getMessage());
        }

        if(donationBillerList == null || donationBillerList.isEmpty()){
            donationBillerList = "\"[\\u000d\\u000a   {\\u000d\\u000a      \\\"Id\\\":\\\"2024dc48-6b09-4631-921d-b8b2267c8000\\\",\\u000d\\u000a      \\\"Code\\\":\\\"01766685686\\\",\\u000d\\u000a      \\\"Description\\\":\\\"Bidyanondo Foundation\\\",\\u000d\\u000a      \\\"IconSource\\\":\\\"Bidyanondo.png\\\",   \\u000d\\u000a      \\\"LabelView\\\":\\\"\\\",\\u000d\\u000a      \\\"DefaultNoOfMonth\\\":\\\"false\\\",\\u000d\\u000a      \\\"TermsAndConditions\\\":\\\"\\\",\\u000d\\u000a      \\\"TargetType\\\":\\\"\\\"\\u000d\\u000a\\u0009},\\u000d\\u000a   {\\u000d\\u000a      \\\"Id\\\":\\\"2024dc48-6b09-4631-921d-b8b2267c8001\\\",\\u000d\\u000a      \\\"Code\\\":\\\"01730482278\\\",\\u000d\\u000a      \\\"Description\\\":\\\"MASTUL Foundation\\\",\\u000d\\u000a      \\\"IconSource\\\":\\\"MASTUL-Foundation.png\\\",   \\u000d\\u000a      \\\"LabelView\\\":\\\"\\\",\\u000d\\u000a      \\\"DefaultNoOfMonth\\\":\\\"false\\\",\\u000d\\u000a      \\\"TermsAndConditions\\\":\\\"\\\",\\u000d\\u000a      \\\"TargetType\\\":\\\"\\\"\\u000d\\u000a\\u0009},\\u000d\\u000a   {\\u000d\\u000a      \\\"Id\\\":\\\"2024dc48-6b09-4631-921d-b8b2267c8001\\\",\\u000d\\u000a      \\\"Code\\\":\\\"01937791254\\\",\\u000d\\u000a      \\\"Description\\\":\\\"Embassy of the State of Palestine\\\",\\u000d\\u000a      \\\"IconSource\\\":\\\"Palestine.jpg\\\",   \\u000d\\u000a      \\\"LabelView\\\":\\\"\\\",\\u000d\\u000a      \\\"DefaultNoOfMonth\\\":\\\"false\\\",\\u000d\\u000a      \\\"TermsAndConditions\\\":\\\"\\\",\\u000d\\u000a      \\\"TargetType\\\":\\\"\\\"\\u000d\\u000a\\u0009}\\u000d\\u000a]\"";
        }
        return getDataJsons(donationBillerList);
    }

    private DataJson[] getDataJsons(String response) {
        DataJson[] result = CommonConstant.formatResponse(response);
        Arrays.stream(result).forEach((e) -> {
            try {
                if(e.getIconSource() != null && !e.getIconSource().isEmpty()) {
                    e.setIconSource(CommonConstant.BILLER_IMAGE_URL + URLEncoder.encode(e.getIconSource(), "UTF-8"));
                }else{
                    e.setIconSource(null);
                }
            } catch (UnsupportedEncodingException unsupportedEncodingException) {
                unsupportedEncodingException.printStackTrace();
            }
        });
        return result;
    }

    private SchoolDataJson[] getSchoolDataJsons(String response){
        SchoolDataJson[] result = CommonConstant.formatInstituteResponse(response);
        Arrays.stream(result).forEach((e)->{
            try{

            }catch (Exception ex){
                ex.printStackTrace();
            }
        });
        return result;
    }

    public DataListResponse prepareListObject(Integer i, String title, DataJson[] list, String iconSource) {
        DataListResponse dataListResponse = new DataListResponse();
        dataListResponse.setCategoryId(i);
        dataListResponse.setCategoryTitle(title);
        try {
            if(iconSource != null && !iconSource.isEmpty()) {
                dataListResponse.setCategoryIconSource(iconSource != null ? CommonConstant.BILLER_IMAGE_URL + URLEncoder.encode(iconSource, "UTF-8") : null);
            }else{
                dataListResponse.setCategoryIconSource(null);
            }
        } catch (UnsupportedEncodingException e) {
            dataListResponse.setCategoryIconSource(null);
            log.error("url encoding exception: " + e.getMessage());
        }
        dataListResponse.setItemList(list);
        return dataListResponse;
    }

    @Override
    public BannerItem[] getBanners(Locale locale) {
        String[] banners = {};
        BannerItem[] bannerItems = new BannerItem[banners.length];
        for (int i = 0; i < banners.length; i++) {
            BannerItem bannerItem = new BannerItem();
            bannerItem.setId(UUID.randomUUID().toString());
            bannerItem.setCode("BANNER-" + (i+1));
            bannerItem.setImageSource("https://api.bdkepler.com/images/banner/" + banners[i]);
            bannerItems[i] = bannerItem;
        }
        return bannerItems;
    }

    @Override
    public BannerAndOffers getBannersAndOffers(Locale locale) {
        // Title + web link (banners and offers)
        BannerAndOffers bannerAndOffers = new BannerAndOffers();
        int bannerItemCount = 8;
        int offerItemCount = 5;
        String titles = "Burger King,Robi,Aarong,KFC,Yellow";
        String offer_links = "https://www.bk.com/offers ," +
                "https://www.robi.com.bd/en/personal/offers,"
                +"https://www.aarong.com/news,"
                +"https://kfcbd.com/," +
                "https://yellowclothing.net/";
        String banner_link = "https://www.kajanapacko.com/";
        BannerAndOfferItem[] bannerItems = new BannerAndOfferItem[bannerItemCount];
        BannerAndOfferItem[] offerItems = new BannerAndOfferItem[offerItemCount];
        int id = 5;
        for(int i = 0; i < bannerItemCount; i++){
            BannerAndOfferItem bannerItem = new BannerAndOfferItem();
            bannerItem.setId(UUID.randomUUID().toString());
            bannerItem.setCode("BANNER-" + (i+1));
            bannerItem.setImageSource("https://mwstaging.tadlbd.com/images/banner/banner_"+ "00" + (i+1) + ".png");
            bannerItem.setWebLink(banner_link);
            bannerItems[i] = bannerItem;
        }
        id = 5;
        for(int i = 0; i < offerItemCount; i++){
            BannerAndOfferItem offerItem = new BannerAndOfferItem();
            offerItem.setId(UUID.randomUUID().toString());
            offerItem.setCode("OFFER-"+(i+1));
            offerItem.setImageSource("https://mwstaging.tadlbd.com/images/banner/offer_"+ "00" + (i+1) + ".png");
            offerItem.setWebLink(offer_links.split(",")[i]);
            offerItem.setTitle(titles.split(",")[i]);
            offerItems[i] = offerItem;
        }
        bannerAndOffers.setBanners(bannerItems);
        bannerAndOffers.setOffers(offerItems);
        return bannerAndOffers;
    }

    @Autowired
    CampaignGateway campaignGateway;

    @Override
    public CampaignMain getBannersAndOffersV2() {
        CampaignMain campaign = new CampaignMain();
        try {
            campaign = campaignGateway.getBannersAndOffersV2();
            List<Campaign> banners = campaign.getBanners();
            for(Campaign banner: banners){
                banner.setImageSource(banner.getImageSource().replaceAll("http://localhost:8080","https://api.bdkepler.com"));
                banner.setWebLink(banner.getWebLink().replaceAll("http://localhost:8080","https://api.bdkepler.com"));
            }

            List<Campaign> offers = campaign.getOffers();
            for(Campaign offer: offers){
                offer.setImageSource(offer.getImageSource().replaceAll("http://localhost:8080","https://api.bdkepler.com"));
                offer.setWebLink(offer.getWebLink().replaceAll("http://localhost:8080","https://api.bdkepler.com"));
            }
        } catch (Exception e){
            log.error("error : "+e.getMessage());
        }
        return campaign;
    }

    @Override
    public DataJson[] getFees(Locale locale) {
        String feesItemsStr = locale.getLanguage().equals("bn") ? fees_info_bn : fees_info_en;
        String[] feesItems = feesItemsStr.split("##");
        
        DataJson[] feesList= new DataJson[feesItems.length];
        // set fees item data
        for (int j = 0; j < feesItems.length; j++) {
            DataJson dataJson = new DataJson();
            String[] items = feesItems[j].split("#");
            if(items.length == 4) {
                dataJson.setId(UUID.randomUUID().toString());
                dataJson.setDescription(items[0]);
                dataJson.setCode(items[1]);
                dataJson.setIconSource(biller_icon_url + items[2]);
                dataJson.setActiveStatus(items[3].equals("1"));
                feesList[j] = dataJson;
            }
        }
        return feesList;
    }

    @Override
    public DataJson[] getFeesForNewApp(Locale locale) {
        String feesItemsStr = locale.getLanguage().equals("bn") ? fees_info_new_bn : fees_info_new_en;
        String[] feesItems = feesItemsStr.split("##");

        DataJson[] feesList= new DataJson[feesItems.length];
        // set fees item data
        for (int j = 0; j < feesItems.length; j++) {
            DataJson dataJson = new DataJson();
            String[] items = feesItems[j].split("#");
            if(items.length == 4) {
                dataJson.setId(UUID.randomUUID().toString());
                dataJson.setDescription(items[0]);
                dataJson.setCode(items[1]);
                dataJson.setIconSource(biller_icon_new_url + items[2]);
                dataJson.setActiveStatus(items[3].equals("1"));
                feesList[j] = dataJson;
            }
        }
        return feesList;
    }
}


