package com.mfs.api_middleware.service;

import com.mfs.api_middleware.dto.FeesAndChargesModelWallet;
import com.mfs.api_middleware.dto.FeesAndChargesWithCommissionResponse;
import com.mfs.api_middleware.dto.FeesChargesResponse;
import com.mfs.api_middleware.dto.NidDueResponse;

import java.util.Locale;

public interface FeesAndChargesService {
    public FeesChargesResponse feeAndCharge(FeesAndChargesModelWallet feesAndChargesModel, Locale locale);
    public NidDueResponse feesNidResponse(String nid_no,
                                          String correction_type,
                                          String service_type
            ,Locale locale);
    public FeesAndChargesWithCommissionResponse
        feesAndChargeCommissionDetails(FeesAndChargesModelWallet request,
                                   Locale locale);
}
