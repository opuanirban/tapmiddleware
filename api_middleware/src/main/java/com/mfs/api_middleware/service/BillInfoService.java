package com.mfs.api_middleware.service;

import com.mfs.api_middleware.dto.*;

import java.util.List;
import java.util.Locale;

public interface BillInfoService {
    public BillInfoResponse getDpdcBill(DPDCBillInfo dpdcBillInfo, Locale locale);
    public NescoBillInfoResponse getNescoBill(String customerAccountNumber, NescoBillInfo nescoBillInfo, Locale locale);
    public BillInfoResponse getDescoBill(String billNo,Locale locale);
    public DescoPrepaidBillDetailsResponse getDescoPrepaidInfo(String meterNo, String amount, Locale locale, String customerAccountNumber);
    public PrepaidBillDetailsResponse getNescoPrepaidInfo(String customerAccountNumber, String customerNo, String meterNo, String amount, Locale locale);
    public EkpayBillInfoResponse getEkpayBillInfo(String customerAccountNumber, EkpayBillInfo billInfo, Locale locale);
    public CommonResponse getAkashBillInfo(String aksahAccountNumber, String customerAccountNumber, Locale locale);
    public JGTDSLResponse getJGTDSLBillInfo(String jgtdslAccountNumber, String customerAccountNumber, Locale locale);
    public IvacBillInfoResponse getIvacBillIfo(String ivacAccountNumber,IvacBillInfo ivacBillInfo);
    public List<IVACCenterItem> getIVACCenterList();
    public PrimeLifeBillResponse getPrimeLifeInfo(String msisdn,PrimeLifeInsuranceBillFetchRequest request);
    public REBBillInfoResponse getREBPostpaidBill(String smsAccountNumber, String customerAccountNumber, Locale locale);
}
