package com.mfs.api_middleware.service;

import com.mfs.api_middleware.dto.CommonResponse;

public interface LogoutService {
    public CommonResponse addTokenToblacklist(String jwt);
}
