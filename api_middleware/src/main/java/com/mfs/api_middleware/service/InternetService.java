package com.mfs.api_middleware.service;

import javax.validation.Valid;

import com.mfs.api_middleware.dto.*;
import com.mfs.api_middleware.gateway.model.BillFetchResponse;

import java.util.Locale;

public interface InternetService {
    CarnivalBillInfoResponse fetchCarnivalBillDetails(String carnivalCustomerId, String mfsAccountNumber);
    CommonResponse payCarnivalBill(@Valid CarnivalBillPaymentRequest billPaymentRequest, String mfsAccountNumber);

    TILBillInfoResponse fetchTilBillDetails(String tilUsername, String mfsAccountNumber);
    CommonResponse payTilBill(@Valid TilBillPaymentRequest billPaymentRequest, String mfsAccountNumber);
}
