package com.mfs.api_middleware.service;

import java.util.Base64;
import java.util.Locale;
import java.util.regex.Pattern;

import com.mfs.api_middleware.Exception.ApiManagerRequestException;
import com.mfs.api_middleware.Exception.DecodePinException;
import com.mfs.api_middleware.config.ApplicationProperties;
import com.mfs.api_middleware.dto.CommonResponse;
import com.mfs.api_middleware.dto.RegistrationParams;
import com.mfs.api_middleware.dto.RegistrationResponse;
import com.mfs.api_middleware.dto.UserRegistrationRequest;
import com.mfs.api_middleware.gateway.ApiManagerGateway;
import com.mfs.api_middleware.gateway.model.CustomerRegistrationParams;
import com.mfs.api_middleware.util.CommonConstant;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class AsyncRegistrationServiceImpl implements AsyncRegistrationService {

    @Autowired
    ApiManagerGateway apiManagerGateway;
    @Autowired
    ApplicationProperties applicationProperties;
    @Autowired
    UserRegistrationService userRegistrationService;

    @Override
    public CommonResponse asyncRegister(UserRegistrationRequest userRegistrationRequest, Locale locale) {
        log.debug("Async Registration with TBL for " + userRegistrationRequest.getMobileNumber());
        if (Pattern.matches("......==", userRegistrationRequest.getPin())) {
            RegistrationParams registrationParams = new RegistrationParams();
            registrationParams.setUserNumber(CommonConstant.checkNumber(userRegistrationRequest.getMobileNumber()));
            registrationParams.setAccountPin(new String(Base64.getDecoder().decode(userRegistrationRequest.getPin())));
            registrationParams.setFirstName(userRegistrationRequest.getFirstName());
            registrationParams.setLastName(userRegistrationRequest.getLastName()==null?"":userRegistrationRequest.getLastName());
//            registrationParams.setNationalIdNumber(userRegistrationRequest.getNid());
            registrationParams.setSex(userRegistrationRequest.getGender().name());
            registrationParams.setUserGroupKey(userRegistrationRequest.getAccountType().getUserGroup());
            registrationParams.setOccupation(userRegistrationRequest.getOccupation());
            registrationParams.setOperatorName(userRegistrationRequest.getMno().name());
            registrationParams.setIsLimitedKYC(true);
            RegistrationResponse registrationResponse = userRegistrationService.callTblRegistration(locale, new RegistrationResponse(), registrationParams);
            CommonResponse response = new CommonResponse();
            response.setMessage(registrationResponse.getMessage());
            return response;

        } else {
            throw new DecodePinException(CommonConstant.DECODE_EXCEPTION_MESSAGE);

        }

    }

    @Override
    public CommonResponse customerRegistrationComplete(CustomerRegistrationParams customerRegistrationParams, Locale locale) {
        log.debug("customer complete request: {}", customerRegistrationParams);
        
        CommonResponse commonResponse = new CommonResponse();
        // basic registration
        RegistrationParams registrationParams = new RegistrationParams();
        registrationParams.setUserNumber(CommonConstant.checkNumber(customerRegistrationParams.getUserNumber()));
        registrationParams.setAccountPin(new String(Base64.getDecoder().decode(customerRegistrationParams.getUserPin())));
        registrationParams.setFirstName(customerRegistrationParams.getFirstName());
        registrationParams.setLastName(customerRegistrationParams.getLastName()==null?"":customerRegistrationParams.getLastName());
        registrationParams.setSex(customerRegistrationParams.getSex());
        registrationParams.setUserGroupKey(customerRegistrationParams.getUserGroupKey());
        registrationParams.setOccupation(customerRegistrationParams.getOccupation());
        registrationParams.setOperatorName(customerRegistrationParams.getOperatorName());
        registrationParams.setIsLimitedKYC(true);
        log.debug("basic registration request: {}", registrationParams);
        RegistrationResponse registrationResponse = userRegistrationService.callTblRegistration(locale, new RegistrationResponse(), registrationParams);
        log.debug("basic registration response: {}", registrationResponse);
        // KYC update
        RegistrationParams details = new RegistrationParams();
        details.setBanglaName(customerRegistrationParams.getBanglaName());
        details.setEnglishName(customerRegistrationParams.getFirstName());
        if (customerRegistrationParams.getFatherName()==null) {
            details.setFatherName("");
        } else {
            details.setFatherName(customerRegistrationParams.getFatherName());
        }
        if (customerRegistrationParams.getMotherName()==null) {
            details.setMotherName("");
        } else {
            details.setMotherName(customerRegistrationParams.getMotherName());
        }
        if (customerRegistrationParams.getSpouseName()==null) {
            details.setSpouseName("");
        } else {
            details.setSpouseName(customerRegistrationParams.getSpouseName());
        }
        details.setDoB(customerRegistrationParams.getDob());

        details.setUserKey(Long.valueOf(0));
        details.setSex(customerRegistrationParams.getSex());
        details.setOccupation(customerRegistrationParams.getOccupation());
        details.setUserNumber(CommonConstant.checkNumber(customerRegistrationParams.getUserNumber()));
        details.setNationalIdNumber(customerRegistrationParams.getNationalIdNumber());
        details.setNominee(customerRegistrationParams.getNominee());
        details.setNomineeRelation(customerRegistrationParams.getNomineeRelation());
        details.setPresentAddress(customerRegistrationParams.getPresentAddress());
        details.setPermanentAddress(customerRegistrationParams.getPermanentAddress());
        details.setPhotoName(customerRegistrationParams.getPhotoString());
        details.setIsLimitedKYC(false);
        log.debug("update kyc request: {}", details);
        String response = apiManagerGateway.registrationStatusUpdate(details, locale);
        log.debug("update registration response: {}", response);
        if (response.contains(applicationProperties.getSuccessPlaceholderList().get(2)) || response.contains(applicationProperties.getSuccessPlaceholderList().get(4))) {
            commonResponse.setMessage(registrationResponse.getMessage());
            return commonResponse;
        } else {
            throw new ApiManagerRequestException(response);
        }
    }
}
