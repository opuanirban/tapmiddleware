package com.mfs.api_middleware.service;

import com.mfs.api_middleware.dto.*;

import java.util.Locale;

public interface TuitionPaymentService {
    public CommonResponse payTuition(TuitionFeePaymentRequest tuitionFeePaymentRequest, String accountNo, Locale locale);
    public TuitionFeeDetailResponse fetchTuitionDetails(TuitionFeeDetailRequest tuitionFeeDetailRequestRequest, String accountNo, Locale locale);
    public CommonResponse makeTuitionPayment(TuitionFeeBillRequest tuitionFeePaymentRequest, String accountNo, Locale locale);
}
