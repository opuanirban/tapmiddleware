package com.mfs.api_middleware.service;

import com.mfs.api_middleware.config.ApplicationProperties;
import com.mfs.api_middleware.dto.ApiManagerRequest;
import com.mfs.api_middleware.dto.BnrfParam;
import com.mfs.api_middleware.dto.CommonResponse;
import com.mfs.api_middleware.enumeration.ContextUrl;
import com.mfs.api_middleware.gateway.ApiManagerGateway;
import com.mfs.api_middleware.service.security.ISecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Base64;
import java.util.Locale;

@Service
public class BnrfServiceImpl extends ProcessRequestService implements BnrfService{

    @Autowired
    ApiManagerGateway apiManagerGateway;

    @Autowired
    ISecurityService iSecurityService;

    @Autowired
    ApplicationProperties applicationProperties;

    @Override
    public CommonResponse bnrfRequest(BnrfParam bnrfParam,String accountNo,Locale locale) {

        String formattedText = "TRUSTMM BNRF " + bnrfParam.getRequirementType() + " " + new String(Base64.getDecoder().decode(bnrfParam.getPin()))+ " " +bnrfParam.getNotificationNumber();
        ApiManagerRequest apiManagerRequest = makeProcessedRequest(applicationProperties.getEncryptionEnabled()?
                iSecurityService.encrypt(formattedText,"",""):formattedText, ContextUrl.API_MANAGER_CONTEXT.getUrl(), accountNo);

        CommonResponse response = getTransactionFormattedResponse(apiManagerGateway, apiManagerRequest, locale, true);
        return response;
    }
}
