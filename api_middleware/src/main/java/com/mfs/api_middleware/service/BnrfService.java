package com.mfs.api_middleware.service;

import com.mfs.api_middleware.dto.BnrfParam;
import com.mfs.api_middleware.dto.CommonResponse;

import java.util.Locale;

public interface BnrfService {
    public CommonResponse bnrfRequest(BnrfParam bnrfParam, String accountNo, Locale locale);
}
