package com.mfs.api_middleware.service;

import com.mfs.api_middleware.Exception.CommonException;
import com.mfs.api_middleware.dao.RedisDao;
import com.mfs.api_middleware.dto.*;
import com.mfs.api_middleware.util.CommonConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Locale;

@Service
public class ServiceConfigServiceImpl implements ServiceConfigService {
    private static final Boolean DEVICE_BINDING_ENABLED=false;

    @Autowired
    RedisDao redisDao;

    @Override
    public ServiceConfigResponse getConfiguration() {
        ServiceConfigResponse serviceConfigResponse= new ServiceConfigResponse();

        ServerConfig serverConfig= new ServerConfig();
        serverConfig.setDeviceBindingEnabled(DEVICE_BINDING_ENABLED);

        ClientConfigs clientConfigs= new ClientConfigs();
        ForceUpdateList forceUpdateList= new ForceUpdateList();
        ForceUpdateHuawei forceUpdateHuawei = new ForceUpdateHuawei();
        ForceUpdateAndroid forceUpdateAndroid= new ForceUpdateAndroid();
        ForceUpdateIos forceUpdateIos = new ForceUpdateIos();

        forceUpdateAndroid.setForceUpdateVersion(redisDao.getValue(CommonConstant.FORCE_UPDATE_VERSION + "ANDROID"));
        forceUpdateAndroid.setBlacklistVersion(redisDao.getValue(CommonConstant.BLACKLIST_VERSION + "ANDROID"));
        if (redisDao.getValue(CommonConstant.FORCE_UPDATE_MESSAGE + "ANDROID") != null) {


            if (!redisDao.getValue(CommonConstant.FORCE_UPDATE_MESSAGE + "ANDROID").equalsIgnoreCase("")) {
                forceUpdateAndroid.setMessage(redisDao.getValue(CommonConstant.FORCE_UPDATE_MESSAGE + "ANDROID"));
            } else {
                forceUpdateAndroid.setMessage(CommonConstant.FORCE_UPDATE_RESPONSE_MESSAGE);
            }
        } else {
            forceUpdateAndroid.setMessage(CommonConstant.FORCE_UPDATE_RESPONSE_MESSAGE);
        }
        if (redisDao.getValue(CommonConstant.FORCE_UPDATE_LINK + "ANDROID") != null) {
            if (!redisDao.getValue(CommonConstant.FORCE_UPDATE_LINK + "ANDROID").equalsIgnoreCase("")) {
                forceUpdateAndroid.setLink(redisDao.getValue(CommonConstant.FORCE_UPDATE_LINK + "ANDROID"));
            } else {
                forceUpdateAndroid.setLink(CommonConstant.FORCE_UPDATE_DEFAULT_LINK);
            }
        } else {
            forceUpdateAndroid.setLink(CommonConstant.FORCE_UPDATE_DEFAULT_LINK);
        }

        forceUpdateHuawei.setForceUpdateVersion(redisDao.getValue(CommonConstant.FORCE_UPDATE_VERSION + "HUAWEI"));
        forceUpdateHuawei.setBlacklistVersion(redisDao.getValue(CommonConstant.BLACKLIST_VERSION + "HUAWEI"));
        if (redisDao.getValue(CommonConstant.FORCE_UPDATE_MESSAGE + "HUAWEI") != null) {


            if (!redisDao.getValue(CommonConstant.FORCE_UPDATE_MESSAGE + "HUAWEI").equalsIgnoreCase("")) {
                forceUpdateHuawei.setMessage(redisDao.getValue(CommonConstant.FORCE_UPDATE_MESSAGE + "HUAWEI"));
            } else {
                forceUpdateHuawei.setMessage(CommonConstant.FORCE_UPDATE_RESPONSE_MESSAGE);
            }
        } else {
            forceUpdateHuawei.setMessage(CommonConstant.FORCE_UPDATE_RESPONSE_MESSAGE);
        }
        if (redisDao.getValue(CommonConstant.FORCE_UPDATE_LINK + "HUAWEI") != null) {
            if (!redisDao.getValue(CommonConstant.FORCE_UPDATE_LINK + "HUAWEI").equalsIgnoreCase("")) {
                forceUpdateHuawei.setLink(redisDao.getValue(CommonConstant.FORCE_UPDATE_LINK + "HUAWEI"));
            } else {
                forceUpdateHuawei.setLink(CommonConstant.FORCE_UPDATE_DEFAULT_LINK);
            }
        } else {
            forceUpdateHuawei.setLink(CommonConstant.FORCE_UPDATE_DEFAULT_LINK);
        }

        forceUpdateIos.setForceUpdateVersion(redisDao.getValue(CommonConstant.FORCE_UPDATE_VERSION + "IOS"));
        forceUpdateIos.setBlacklistVersion(redisDao.getValue(CommonConstant.BLACKLIST_VERSION + "IOS"));
        if (redisDao.getValue(CommonConstant.FORCE_UPDATE_MESSAGE + "IOS") != null) {


            if (!redisDao.getValue(CommonConstant.FORCE_UPDATE_MESSAGE + "IOS").equalsIgnoreCase("")) {
                forceUpdateIos.setMessage(redisDao.getValue(CommonConstant.FORCE_UPDATE_MESSAGE + "IOS"));
            } else {
                forceUpdateIos.setMessage(CommonConstant.FORCE_UPDATE_RESPONSE_MESSAGE);
            }
        } else {
            forceUpdateIos.setMessage(CommonConstant.FORCE_UPDATE_RESPONSE_MESSAGE);
        }
        if (redisDao.getValue(CommonConstant.FORCE_UPDATE_LINK + "IOS") != null) {
            if (!redisDao.getValue(CommonConstant.FORCE_UPDATE_LINK + "IOS").equalsIgnoreCase("")) {
                forceUpdateIos.setLink(redisDao.getValue(CommonConstant.FORCE_UPDATE_LINK + "IOS"));
            } else {
                forceUpdateIos.setLink(CommonConstant.FORCE_UPDATE_DEFAULT_LINK);
            }
        } else {
            forceUpdateIos.setLink(CommonConstant.FORCE_UPDATE_DEFAULT_LINK);
        }

        forceUpdateList.setAndroid(forceUpdateAndroid);
        forceUpdateList.setHuawei(forceUpdateHuawei);
        forceUpdateList.setIOS(forceUpdateIos);


        clientConfigs.setForceUpdateList(forceUpdateList);

        HiddenFeatureInfo hiddenFeatureInfo = new HiddenFeatureInfo();
        hiddenFeatureInfo.setHiddenFeatureVersion(redisDao.getValue(CommonConstant.HIDE_FEATURE_VERSION));
        hiddenFeatureInfo.setHiddenFeatureStatus(Boolean.valueOf(redisDao.getValue(CommonConstant.HIDE_FEATURE_STATUS)));
        clientConfigs.setHiddenFeatureInfo(hiddenFeatureInfo);

        serviceConfigResponse.setClientConfig(clientConfigs);

        ServiceAvailibilityInfo serviceAvailibilityInfo = new ServiceAvailibilityInfo();
        String serviceAvailibilty = redisDao.getValue(CommonConstant.SERVICE_AVAILABLE_STATUS);
        if(serviceAvailibilty == null) {
            serviceAvailibilityInfo.setIsServiceAvailable(true);
        } else {
            serviceAvailibilityInfo.setIsServiceAvailable(serviceAvailibilty.equals("true"));
            serviceAvailibilityInfo.setNotificationMessageEn(redisDao.getValue(CommonConstant.SERVICE_UNAVAILABLE_KEY_EN)==null?CommonConstant.SERVICE_UNAVAILABLE_MESSAGE_EN:redisDao.getValue(CommonConstant.SERVICE_UNAVAILABLE_KEY_EN));
            serviceAvailibilityInfo.setNotificationMessageBn(redisDao.getValue(CommonConstant.SERVICE_UNAVAILABLE_KEY_BN)==null?CommonConstant.SERVICE_UNAVAILABLE_MESSAGE_BN:redisDao.getValue(CommonConstant.SERVICE_UNAVAILABLE_KEY_BN));
        }
        serverConfig.setServiceInfo(serviceAvailibilityInfo);
        serviceConfigResponse.setServerConfig(serverConfig);

        return  serviceConfigResponse;
    }

    @Override
    public CommonResponse setHiddenFeatureVersion(FeatureHidePermissionRequest featureHidePermissionRequest, Locale locale) {
        if(!featureHidePermissionRequest.getRequestAuthKey().equalsIgnoreCase(CommonConstant.CONFIG_UPDATE_AUTH_KEY)){
            throw new CommonException("Request authorization failed!");
        }

        try {
            redisDao.setValue(CommonConstant.HIDE_FEATURE_VERSION, featureHidePermissionRequest.getHiddenFeatureVersion());
            redisDao.setValue(CommonConstant.HIDE_FEATURE_STATUS, String.valueOf(featureHidePermissionRequest.isFeatureIsHidden()));
            CommonResponse response = new CommonResponse();
            response.setMessage("Success");
            return response;
        } catch (Exception e) {
            e.printStackTrace();
            throw new CommonException("Saving to Redis Unsuccessful!");
        }
    }

    @Override
    public void setServiceAvailibility(ServiceAvailibilityInfo serviceAvailibilityInfo) {
        try {
            redisDao.setValue(CommonConstant.SERVICE_AVAILABLE_STATUS, String.valueOf(serviceAvailibilityInfo.getIsServiceAvailable()));
            redisDao.setValue(CommonConstant.SERVICE_UNAVAILABLE_KEY_EN, serviceAvailibilityInfo.getNotificationMessageEn());
            redisDao.setValue(CommonConstant.SERVICE_UNAVAILABLE_KEY_BN, serviceAvailibilityInfo.getNotificationMessageBn());
        } catch (Exception e) {
            e.printStackTrace();
            throw new CommonException("Saving to Redis Unsuccessful!");
        }      
    }
}
