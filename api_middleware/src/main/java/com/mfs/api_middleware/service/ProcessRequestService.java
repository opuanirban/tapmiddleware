package com.mfs.api_middleware.service;

import com.mfs.api_middleware.Exception.ApiManagerRequestException;
import com.mfs.api_middleware.config.ApplicationProperties;
import com.mfs.api_middleware.dto.ApiManagerRequest;
import com.mfs.api_middleware.dto.CommonResponse;
import com.mfs.api_middleware.gateway.ApiManagerGateway;
import com.mfs.api_middleware.util.CommonConstant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Locale;
import java.util.regex.Pattern;

@Slf4j
public class ProcessRequestService {
    @Autowired
    ApplicationProperties applicationProperties;

    public ApiManagerRequest makeProcessedRequest(String formattedText, String contextPath, String accountNo){
        ApiManagerRequest apiManagerRequest = new ApiManagerRequest();
        apiManagerRequest.setMsgId(1);
        apiManagerRequest.setUserNumber(CommonConstant.checkNumber(accountNo));
        apiManagerRequest.setShortCode("app");
        apiManagerRequest.setTelcoId(1);
        apiManagerRequest.setContext(contextPath);
        apiManagerRequest.setSmsText(formattedText);

        return apiManagerRequest;
    }

    public CommonResponse getTransactionFormattedResponse(ApiManagerGateway apiManagerGateway, ApiManagerRequest apiManagerRequest, Locale locale, Boolean isTransaction){
        CommonResponse response = new CommonResponse();

        String responseFromApi = apiManagerGateway.apiManagerRequestMethod(apiManagerRequest,locale, isTransaction)
                .replace("\\", "");
        if(isMatched(responseFromApi)) {
            response.setMessage(responseFromApi);
        }else {
            if(apiManagerRequest.getSmsText().contains("REBPR") && responseFromApi.toLowerCase().contains("your recharge is pending")) {
                response.setMessage(responseFromApi);
            } else {
                if(responseFromApi.contains("###")){
                    responseFromApi = responseFromApi.split("###")[0];
                }
                throw new ApiManagerRequestException(responseFromApi);
            }
        }

        return response;
    }

    public boolean isMatched(String responseMessage){
        boolean match = false;
        for (String s : applicationProperties.getSuccessPlaceholderList()) {
            if(Pattern.compile(Pattern.quote(s), Pattern.CASE_INSENSITIVE).matcher(responseMessage).find() &&
             !responseMessage.toLowerCase().contains("insufficient balance")){
                match = true;
                break;
            }
        }
        return match;
    }
}
