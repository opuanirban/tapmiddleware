package com.mfs.api_middleware.service;

import com.mfs.api_middleware.Exception.CommonException;
import com.mfs.api_middleware.dao.RedisDao;
import com.mfs.api_middleware.dto.CommonResponse;
import com.mfs.api_middleware.dto.ForceUpdateRequest;
import com.mfs.api_middleware.dto.ForceUpdateResponse;
import com.mfs.api_middleware.util.CommonConstant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Locale;

@Service
@Slf4j
public class ForceUpdateServiceImpl implements ForceUpdateService {

    @Autowired
    RedisDao redisDao;
    @Autowired
    Environment environment;

    public CommonResponse setForceUpdateVersion(ForceUpdateRequest forceUpdateRequest, Locale locale) {
        if(Arrays.toString(environment.getActiveProfiles()).contains(CommonConstant.PROD_PROFILE)){
            if(!forceUpdateRequest.getRequestAuthKey().equalsIgnoreCase(CommonConstant.CONFIG_UPDATE_AUTH_KEY)){
                throw new CommonException("Request authorization failed!");
            }
        }

        try {
            redisDao.setValue(CommonConstant.FORCE_UPDATE_VERSION + forceUpdateRequest.getPlatform(), forceUpdateRequest.getForceUpdateVersion());
            redisDao.setValue(CommonConstant.BLACKLIST_VERSION + forceUpdateRequest.getPlatform(), forceUpdateRequest.getBlacklistVersion());
            redisDao.setValue(CommonConstant.FORCE_UPDATE_MESSAGE + forceUpdateRequest.getPlatform(), (forceUpdateRequest.getMessage() != null) ? forceUpdateRequest.getMessage() : "");
            redisDao.setValue(CommonConstant.FORCE_UPDATE_LINK + forceUpdateRequest.getPlatform(), (forceUpdateRequest.getLink() != null) ? forceUpdateRequest.getLink() : "");
            CommonResponse response = new CommonResponse();
            response.setMessage("Success");
            return response;
        } catch (Exception e) {
            e.printStackTrace();
            throw new CommonException("Saving to Redis Unsuccessful!");
        }
    }

    @Override
    public ForceUpdateResponse checkForceUpdateVersion(String platform, Locale locale) {
        try {
            ForceUpdateResponse forceUpdateResponse = new ForceUpdateResponse();
            forceUpdateResponse.setForceUpdateVersion(redisDao.getValue(CommonConstant.FORCE_UPDATE_VERSION + platform));

            String var = redisDao.getValue(CommonConstant.FORCE_UPDATE_MESSAGE + platform);
            if (redisDao.getValue(CommonConstant.FORCE_UPDATE_MESSAGE + platform) != null) {


                if (!redisDao.getValue(CommonConstant.FORCE_UPDATE_MESSAGE + platform).equalsIgnoreCase("")) {
                    forceUpdateResponse.setMessage(redisDao.getValue(CommonConstant.FORCE_UPDATE_MESSAGE + platform));
                } else {
                    forceUpdateResponse.setMessage(CommonConstant.FORCE_UPDATE_RESPONSE_MESSAGE);
                }
            } else {
                forceUpdateResponse.setMessage(CommonConstant.FORCE_UPDATE_RESPONSE_MESSAGE);
            }
            if (redisDao.getValue(CommonConstant.FORCE_UPDATE_LINK + platform) != null) {
                if (!redisDao.getValue(CommonConstant.FORCE_UPDATE_LINK + platform).equalsIgnoreCase("")) {
                    forceUpdateResponse.setLink(redisDao.getValue(CommonConstant.FORCE_UPDATE_LINK + platform));
                } else {
                    forceUpdateResponse.setLink(CommonConstant.FORCE_UPDATE_DEFAULT_LINK);
                }
            } else {
                forceUpdateResponse.setLink(CommonConstant.FORCE_UPDATE_DEFAULT_LINK);
            }
            //forceUpdateResponse.setMessage((redisDao.getValue(CommonConstant.FORCE_UPDATE_MESSAGE+platform)!="")? redisDao.getValue(CommonConstant.FORCE_UPDATE_MESSAGE+platform):CommonConstant.FORCE_UPDATE_RESPONSE_MESSAGE);
            // forceUpdateResponse.setLink((redisDao.getValue(CommonConstant.FORCE_UPDATE_LINK+platform)!=null)?redisDao.getValue(CommonConstant.FORCE_UPDATE_LINK+platform):"");
            forceUpdateResponse.setBlacklistVersion(redisDao.getValue(CommonConstant.BLACKLIST_VERSION + platform));
            //   forceUpdateResponse.setResponse_message(CommonConstant.FORCE_UPDATE_RESPONSE_MESSAGE);

            return forceUpdateResponse;
        } catch (Exception e) {
            e.printStackTrace();
            throw new CommonException("Fetch from redis unsuccesful!");
        }


    }
}
