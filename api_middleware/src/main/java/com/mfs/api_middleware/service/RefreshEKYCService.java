package com.mfs.api_middleware.service;

import com.mfs.api_middleware.dto.RefreshEKYCResponse;

import java.util.Locale;

public interface RefreshEKYCService {
    public RefreshEKYCResponse refreshStatus(String msisdn, Locale locale);

}
