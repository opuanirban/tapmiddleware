package com.mfs.api_middleware.service;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

import com.mfs.api_middleware.Exception.ApiManagerRequestException;
import com.mfs.api_middleware.Exception.ApiRequestException;
import com.mfs.api_middleware.Exception.DecodePinException;
import com.mfs.api_middleware.Exception.TransactionProcessException;
import com.mfs.api_middleware.config.ApplicationProperties;
import com.mfs.api_middleware.dto.*;
import com.mfs.api_middleware.enumeration.ContextUrl;
import com.mfs.api_middleware.enumeration.TransactionEvents;
import com.mfs.api_middleware.gateway.ApiManagerGateway;
import com.mfs.api_middleware.gateway.model.REBTransactionDetailsResponse;
import com.mfs.api_middleware.gateway.model.TransactionDetailsRequest;
import com.mfs.api_middleware.gateway.nesco.model.BillPaymentInput;
import com.mfs.api_middleware.gateway.nesco.model.BillPaymentResponse;
import com.mfs.api_middleware.gateway.reb.REBServiceGateway;
import com.mfs.api_middleware.gateway.reb.apimanager.PendingTransactionItem;
import com.mfs.api_middleware.gateway.reb.apimanager.PendingTransactionRequest;
import com.mfs.api_middleware.gateway.reb.apimanager.RetryTransactionRequest;
import com.mfs.api_middleware.service.security.ISecurityService;
import com.mfs.api_middleware.util.CommonConstant;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class REBServiceImpl extends ProcessRequestService implements REBService {
    @Autowired
    ApiManagerGateway apiManagerGateway;

    @Autowired
    ISecurityService iSecurityService;

    @Autowired
    TransactionProcessService transactionProcessService;
    
    @Autowired
    ApplicationProperties appProperties;

    @Autowired
    REBServiceGateway rebServiceGateway;

    @Override
    public REBPrepaidResponse payREBPrepaidBill(REBPrepaidRequest prepaidRequest, String accountNo, Locale locale) {
        if (appProperties.getCheckEnabled()) {
            if (transactionProcessService.findProcess(accountNo, prepaidRequest.getBillAmount(),prepaidRequest.getMeterNumber(), TransactionEvents.REB_PREPAID.toString())) {
                log.error("Same transaction within 10 minutes");
                throw new TransactionProcessException("User not allowed to do the same transaction within " +
                        appProperties.getDuplicateReqTimeOutInMills() / 60000 + " minutes of last transaction");
            } else {
                return doPayREBBill(prepaidRequest, accountNo, locale);
            }
        } else {
            return doPayREBBill(prepaidRequest, accountNo, locale);
        }
    }

    @Override
    public REBTransactionDetails rebTransactionDetails(String transactionId, String mfsAccountNumber) {
        REBTransactionDetails rebTransactionDetails = new REBTransactionDetails();

        TransactionDetailsRequest request = new TransactionDetailsRequest();
        request.setTransactionNumber(transactionId);
        request.setUserNumber(CommonConstant.checkNumber(mfsAccountNumber));
        REBTransactionDetailsResponse rebTransactionDetailsResponse = apiManagerGateway.fetchREBTransactionDetails(request);
        if(rebTransactionDetailsResponse.getCode() != 200) {
            throw new ApiManagerRequestException("Unable to fetch transaction details");
        }

        String responseEntity = String.valueOf(rebTransactionDetailsResponse.getMessage());
        if(responseEntity.contains("Data not found")) {
            throw new ApiManagerRequestException(responseEntity);
        }

        responseEntity = responseEntity.replace("{", "").replace("}", "");
        String[] itemList = responseEntity.split(",");
        for (int i = 0; i < itemList.length; i++) {
            String[] item = itemList[i].trim().split("=");
            if(item[0].equalsIgnoreCase("Amount")) {
                rebTransactionDetails.setBillAmount(item[1]);
            } else if(item[0].equalsIgnoreCase("MeterNumber")) {
                rebTransactionDetails.setMeterNumber(item[1]);
            } else if(item[0].equalsIgnoreCase("FeeAmount")) {
                rebTransactionDetails.setChargeAmount(item[1]);
            } else if(item[0].equalsIgnoreCase("TransactionDate")) {
                rebTransactionDetails.setPaymentDate(item[1]);
            } else if(item[0].equalsIgnoreCase("Token")) {
                rebTransactionDetails.setToken(item[1]);
            } else if(item[0].equalsIgnoreCase("TxCode")) {
                rebTransactionDetails.setTransactionId(item[1]);
            } else if(item[0].equalsIgnoreCase("Sequence")) {
                rebTransactionDetails.setSequenceNumber(item[1]);
            }
        }
        rebTransactionDetails.setOrganizationName("Polli Bidyut");
        rebTransactionDetails.setTapAccountNumber(mfsAccountNumber);
        return rebTransactionDetails;
    }

    @Override
    public REBTransactionDetails rebTransactionDetails2(String transactionId) {
        return rebServiceGateway.getPaymentDetails(transactionId);
    }

    private REBPrepaidResponse doPayREBBill(REBPrepaidRequest prepaidRequest, String accountNo, Locale locale) {
        REBPrepaidResponse prepaidResponse = new REBPrepaidResponse();
        if (Pattern.matches("......==", prepaidRequest.getPin())) {
            // TrustMM REBPR MeterNumber Amount PIN NotificationNumber(Optional)
            String formattedText = "TrustMM REBPR " + prepaidRequest.getMeterNumber() + " " + prepaidRequest.getBillAmount() + " " + new String(Base64.getDecoder().decode(prepaidRequest.getPin())) + " " + CommonConstant.checkNumber(prepaidRequest.getNotificationNumber());
            ApiManagerRequest apiManagerRequest = makeProcessedRequest(appProperties.getEncryptionEnabled() ?
                    iSecurityService.encrypt(formattedText, "", "") : formattedText, ContextUrl.API_MANAGER_CONTEXT.getUrl(), accountNo);

            CommonResponse response = getTransactionFormattedResponse(apiManagerGateway, apiManagerRequest, locale, true);
            if(response.getMessage().toLowerCase().contains("your recharge is pending")) {
                prepaidResponse.setPendingRequest(true);
            } else {
                prepaidResponse.setPendingRequest(false);
            }

            // parse charge details & transaction response
            log.debug("REB response: {}", response);
            String[] responseItems = response.getMessage().split("###");
            prepaidResponse.setMessage(responseItems[0]);
            String rebPaymentResponse = responseItems[0];
            if(rebPaymentResponse.contains("Vending Amount")) {
                String[] responseItemsInternal = rebPaymentResponse.split(";");
                for (int i = 0; i < responseItemsInternal.length; i++) {
                    String[] responseItem = responseItemsInternal[i].split(":");
                    if(responseItemsInternal[i].contains("Vending Amount:")) {
                        prepaidResponse.setAmount(responseItem[1].trim());
                    } else if(responseItemsInternal[i].contains("TxId:")) {
                        prepaidResponse.setTransactionId(responseItem[1].trim());
                    }
                }
            }
            if(responseItems.length > 1) {
                prepaidResponse.setChargeDetails(responseItems[1]);
            }

            try {
                transactionProcessService.storeProcess(accountNo, prepaidRequest.getBillAmount(),prepaidRequest.getMeterNumber(), TransactionEvents.REB_PREPAID.toString());
            } catch (Exception e) {
                log.error(e.getMessage());
            }
            return prepaidResponse;

        } else {
            throw new DecodePinException(CommonConstant.DECODE_EXCEPTION_MESSAGE);
        }
    }

    @Override
    public REBPrepaidResponse payREBPrepaidBill2(REBPrepaidRequest prepaidRequest, String accountNo, Locale locale) {
        REBPrepaidResponse rebPrepaidResponse = new REBPrepaidResponse();

        BillPaymentInput request = new BillPaymentInput();
        request.setAccountNumber(accountNo);
        request.setBillerCode("REBPR");
        request.setFee("0");
        request.setKey1(prepaidRequest.getMeterNumber());
        request.setKey2(String.valueOf(prepaidRequest.getBillAmount()));
        request.setNotificationNumber(prepaidRequest.getNotificationNumber()==null?accountNo:prepaidRequest.getNotificationNumber());
        request.setPaymentChannel("APP");
        request.setPin(prepaidRequest.getPin());
        BillPaymentResponse response = new BillPaymentResponse();
        try {
            response = rebServiceGateway.paymentRebPrepaidBill(request);
        } catch (Exception e) {
            throw new ApiRequestException(CommonConstant.COMMON_ERROR);
        }

        rebPrepaidResponse.setMessage(response.getResponse().getResponseMessage());
        rebPrepaidResponse.setAmount(response.getBillAmount());
        rebPrepaidResponse.setTransactionId(response.getResponse().getTransactionId());
        if(response.getResponse().getResponseCode().equals("000")){
            rebPrepaidResponse.setPendingRequest(Boolean.FALSE);
        } else if(response.getResponse().getResponseCode().equals("503")) {
            rebPrepaidResponse.setPendingRequest(Boolean.TRUE);
        } else {
            throw new ApiRequestException(response.getResponse().getResponseMessage());
        }
        return rebPrepaidResponse;
    }

    @Override
    public REBPendingTransactionsResponse getPendingTransactionList(String mfsAccountNumber) {
        REBPendingTransactionsResponse rebPendingTransactionsResponse = new REBPendingTransactionsResponse();
        List<REBPendingTransactionItem> pendingTransactionItems = new ArrayList<REBPendingTransactionItem>();
        // fetch data from tcash core
        PendingTransactionRequest pendingTransactionRequest = new PendingTransactionRequest();
        pendingTransactionRequest.setUserNumber(CommonConstant.checkNumber(mfsAccountNumber));
        List<PendingTransactionItem> itemsFromApiManager = apiManagerGateway.fetchREBPendingTransactions(pendingTransactionRequest);
        for (PendingTransactionItem pendingTransactionItem : itemsFromApiManager) {
            REBPendingTransactionItem item = new REBPendingTransactionItem();
            item.setMeterNumber(pendingTransactionItem.getMeterNumber());
            item.setTransactionDate(pendingTransactionItem.getCreatedDate());
            item.setTransactionId(pendingTransactionItem.getTxCode());
            pendingTransactionItems.add(item);
        }

        rebPendingTransactionsResponse.setRebPendingTransactionList(pendingTransactionItems);
        return rebPendingTransactionsResponse;
    }

    @Override
    public REBPendingTransactionsResponse getPendingTransactionList2(String mfsAccountNumber) {
        return rebServiceGateway.getPendingPaymentList(mfsAccountNumber.substring(mfsAccountNumber.length()-11, mfsAccountNumber.length()));
    }

    @Override
    public REBPrepaidResponse retryREBPrepaidBill(String transactionId, String accountNo, Locale locale) {
        REBPrepaidResponse prepaidResponse = new REBPrepaidResponse();

        RetryTransactionRequest request = new RetryTransactionRequest();
        request.setChannel("APP");
        request.setTransactionId(transactionId);
        request.setUserNumber(CommonConstant.checkNumber(accountNo));
        String response = apiManagerGateway.retryREBPendingTransaction(request);

        if(isMatched(response) || response.contains("pending token")) {
            log.debug("REB response: {}", response);
            String[] responseItems = response.split("###");
            String rebPaymentResponse = responseItems[0];
            if(rebPaymentResponse.contains("Vending Amount")) {
                String[] responseItemsInternal = rebPaymentResponse.split(";");
                for (int i = 0; i < responseItemsInternal.length; i++) {
                    String[] responseItem = responseItemsInternal[i].split(":");
                    if(responseItemsInternal[i].contains("Vending Amount:")) {
                        prepaidResponse.setAmount(responseItem[1].trim());
                    } else if(responseItemsInternal[i].contains("TxId:")) {
                        prepaidResponse.setTransactionId(responseItem[1].trim());
                    }
                }
            }
            prepaidResponse.setMessage(rebPaymentResponse);
            if (responseItems.length > 1) {
                prepaidResponse.setChargeDetails(responseItems[1]);
            }
        } else {
            throw new ApiManagerRequestException(response);
        }
        return prepaidResponse;
    }

    @Override
    public REBPrepaidResponse retryREBPrepaidBill2(String transactionId) {
        REBPrepaidResponse rebPrepaidResponse = new REBPrepaidResponse();
        BillPaymentResponse response = new BillPaymentResponse();
        try {
            response = rebServiceGateway.retryPendingPayment(transactionId);
        } catch (Exception e) {
            throw new ApiRequestException(CommonConstant.COMMON_ERROR);
        }
        rebPrepaidResponse.setMessage(response.getResponse().getResponseMessage());
        rebPrepaidResponse.setAmount(response.getBillAmount());
        rebPrepaidResponse.setTransactionId(response.getResponse().getTransactionId());
        if(response.getResponse().getResponseCode().equals("000")){
            rebPrepaidResponse.setPendingRequest(Boolean.FALSE);
        } else if(response.getResponse().getResponseCode().equals("503")) {
            rebPrepaidResponse.setPendingRequest(Boolean.TRUE);
        } else {
            throw new ApiRequestException(response.getResponse().getResponseMessage());
        }
        return rebPrepaidResponse;
    }

    @Override
    public CommonResponse payREBPostpaidBill(REBPostpaidPaymentRequest postpaidRequest, String accountNo, Locale locale) {
             CommonResponse commonResponse = new CommonResponse();
             // populate bill payment request
             BillPaymentInput billPaymentInput = new BillPaymentInput();
             billPaymentInput.setAccountNumber(accountNo);
             billPaymentInput.setBillerCode("REB");
             billPaymentInput.setKey1(postpaidRequest.getSmsAccountNo());
             billPaymentInput.setKey2(String.valueOf(postpaidRequest.getBillAmount()));
             billPaymentInput.setKey4(postpaidRequest.getTransactionId());
             billPaymentInput.setNotificationNumber(postpaidRequest.getNotificationNumber());
             billPaymentInput.setPin(postpaidRequest.getPin());
             billPaymentInput.setPaymentChannel(CommonConstant.REQUEST_CHANNEL_APP);

             BillPaymentResponse billPaymentResponse = rebServiceGateway.paymentPostpaidBill(billPaymentInput);
             if(!billPaymentResponse.getResponse().getResponseCode().equals(CommonConstant.SUCCESS_CODE)) {
                 throw new ApiRequestException(billPaymentResponse.getResponse().getResponseMessage());
             }

             commonResponse.setMessage(billPaymentResponse.getResponse().getResponseMessage());
             return commonResponse;
    }

}
