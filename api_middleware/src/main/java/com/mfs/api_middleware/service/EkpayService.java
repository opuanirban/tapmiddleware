package com.mfs.api_middleware.service;

import java.util.Locale;

import com.mfs.api_middleware.dto.CommonResponse;
import com.mfs.api_middleware.dto.EkpayBillRequest;

public interface EkpayService {
    public CommonResponse payEkpayBill(EkpayBillRequest request, String accountNo, Locale locale);
}
