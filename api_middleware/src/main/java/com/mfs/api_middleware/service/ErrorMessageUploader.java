package com.mfs.api_middleware.service;

import com.mfs.api_middleware.dto.CommonResponse;
import org.springframework.web.multipart.MultipartFile;

public interface ErrorMessageUploader {
    public CommonResponse uploadFile(MultipartFile file);
}
