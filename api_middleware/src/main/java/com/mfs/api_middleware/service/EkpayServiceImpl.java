package com.mfs.api_middleware.service;

import com.mfs.api_middleware.Exception.ApiRequestException;
import com.mfs.api_middleware.Exception.TransactionProcessException;
import com.mfs.api_middleware.dto.CommonResponse;
import com.mfs.api_middleware.dto.EkpayBillInfoResponse;
import com.mfs.api_middleware.dto.EkpayBillRequest;
import com.mfs.api_middleware.dto.NescoPrepaidRequest;
import com.mfs.api_middleware.enumeration.TransactionEvents;
import com.mfs.api_middleware.gateway.ApiManagerGateway;
import com.mfs.api_middleware.gateway.ekpay.EkpayServiceGateway;
import com.mfs.api_middleware.gateway.model.*;
import com.mfs.api_middleware.gateway.nesco.NescoServiceGateway;
import com.mfs.api_middleware.service.security.ISecurityService;
import com.mfs.api_middleware.util.CommonConstant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Locale;

@Service
@Slf4j
public class EkpayServiceImpl extends ProcessRequestService implements EkpayService {
    @Autowired
    ApiManagerGateway apiManagerGateway;

    @Autowired
    ISecurityService iSecurityService;

    @Autowired
    TransactionProcessService transactionProcessService;

    @Autowired
    EkpayServiceGateway ekpayServiceGateway;

    @Override
    public CommonResponse payEkpayBill(EkpayBillRequest request, String accountNo, Locale locale) {
        CommonResponse response = new CommonResponse();

        BillPaymentInput input = new BillPaymentInput();
        input.setAccountNumber(accountNo);
        input.setBillerCode(request.getBillerType().toString());
        input.setPaymentChannel("app");
        input.setFee(input.fee);
        input.setNotificationNumber(request.getNotificationNumber());
        input.setKey3(request.getTransactionId());
        input.setPin(request.getPin());

        try {
            BillPaymentResponse billPaymentResponse = ekpayServiceGateway.payBill(input);
            BaseResponse baseResponse = billPaymentResponse.getResponse();
            if (baseResponse != null) {
                if (baseResponse.getResponseCode().equals(CommonConstant.SUCCESS_CODE)) {
                    response.setMessage(baseResponse.getResponseMessage());
                    return response;
                } else {
                    throw new ApiRequestException(baseResponse.getResponseMessage());
                }
            } else {
                throw new ApiRequestException(CommonConstant.COMMON_ERROR);
            }
        } catch (Exception e) {
            throw new ApiRequestException(e.getMessage());
        }
    }
}
