package com.mfs.api_middleware.service;

import com.mfs.api_middleware.dto.CommonResponse;
import com.mfs.api_middleware.dto.ForceUpdateRequest;
import com.mfs.api_middleware.dto.ForceUpdateResponse;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Locale;

public interface ForceUpdateService {
    public CommonResponse setForceUpdateVersion(ForceUpdateRequest forceUpdateRequest, Locale locale);
    public ForceUpdateResponse checkForceUpdateVersion(String platform, Locale locale);
}
