package com.mfs.api_middleware.service;

import com.mfs.api_middleware.Exception.DecodePinException;
import com.mfs.api_middleware.Exception.PinUpdateException;
import com.mfs.api_middleware.config.ApplicationProperties;
import com.mfs.api_middleware.dto.CommonResponse;
import com.mfs.api_middleware.dto.PinInfo;
import com.mfs.api_middleware.gateway.ApiManagerGateway;
import com.mfs.api_middleware.service.security.ISecurityService;
import com.mfs.api_middleware.util.CommonConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Base64;
import java.util.Locale;
import java.util.regex.Pattern;

@Service
public class PinUpdateServiceImpl implements PinUpdateService {
    @Autowired
    ApiManagerGateway apiManagerGateway;

    @Autowired
    ISecurityService securityService;

    @Autowired
    ApplicationProperties applicationProperties;

    @Override
    public CommonResponse update_pin(String msisdn, PinInfo pinInfo, Locale locale) {

        if (Pattern.matches("......==", pinInfo.getOldPin())) {
            CommonResponse response = new CommonResponse();
            String message = apiManagerGateway.updatePin(msisdn, new String(Base64.getDecoder().decode(pinInfo.getOldPin())), new String(Base64.getDecoder().decode(pinInfo.getNewPin())), locale);
            if (message.contains(applicationProperties.getSuccessPlaceholderList().get(2))) {
                response.setMessage(message);
                return response;
            } else {
                throw new PinUpdateException(message);
            }

        } else {
            throw new DecodePinException(CommonConstant.DECODE_EXCEPTION_MESSAGE);
        }

    }


}


