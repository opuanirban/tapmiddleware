package com.mfs.api_middleware.service;

import com.mfs.api_middleware.dto.*;
import com.mfs.api_middleware.enumeration.GenderEnum;
import com.mfs.api_middleware.enumeration.MobileOperatorEnum;
import com.mfs.api_middleware.enumeration.UserGroupEnum;
import org.springframework.web.multipart.MultipartFile;

import java.util.Locale;

public interface UserRegistrationService {
    public RegistrationResponse userRegistration(UserRegistrationRequest userRegistrationRequest, Locale locale);

    public UserStatusInfo checkForUser(String msisdn, Locale locale);

    public RegistrationResponse userRegistrationKyc(String pin, String firstName, String lastName, UserGroupEnum accountType, MobileOperatorEnum mno, String nid_no, String dob, String applicant_name_ben, String applicant_name_eng, String father_name, String mother_name, String spouse_name, String pres_address, String perm_address, String id_front_name, String id_back_name, GenderEnum gender, String profession, String nominee, String nominee_relation, String mobile_number, MultipartFile applicant_photo, Locale locale);

    public UserLoginInfo fetchUserLoginInfo(String msisdn, String pin);

    public RegistrationResponse callTblRegistration(Locale locale, RegistrationResponse response, RegistrationParams registrationParams);
}
