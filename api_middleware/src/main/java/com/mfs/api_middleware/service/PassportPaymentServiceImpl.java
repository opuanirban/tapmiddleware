package com.mfs.api_middleware.service;

import com.mfs.api_middleware.Exception.ApiManagerRequestException;
import com.mfs.api_middleware.Exception.DecodePinException;
import com.mfs.api_middleware.Exception.TransactionProcessException;
import com.mfs.api_middleware.config.ApplicationProperties;
import com.mfs.api_middleware.dto.*;
import com.mfs.api_middleware.enumeration.ContextUrl;
import com.mfs.api_middleware.enumeration.TransactionEvents;
import com.mfs.api_middleware.gateway.ApiManagerGateway;
import com.mfs.api_middleware.service.security.ISecurityService;
import com.mfs.api_middleware.util.CommonConstant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Base64;
import java.util.Locale;
import java.util.regex.Pattern;

@Service
@Slf4j
public class PassportPaymentServiceImpl extends ProcessRequestService implements PassportPaymentService {

    @Autowired
    ApiManagerGateway apiManagerGateway;

    @Autowired
    ISecurityService iSecurityService;

    @Autowired
    ApplicationProperties applicationProperties;

    @Autowired
    TransactionProcessService transactionProcessService;


    @Override
    public CommonResponse payPassportFee(PassportFeePayment passportNidFeePayment, String account, Locale locale) {
        throw new ApiManagerRequestException("Passport Fee payment is temporarily unavailable. Sorry for the inconvenience.");

        // if (applicationProperties.getCheckEnabled()) {
        //     if (transactionProcessService.findProcess(account, passportNidFeePayment.getAmount(),passportNidFeePayment.getFirstName()+passportNidFeePayment.getLastName(), TransactionEvents.PASSPORT.toString())) {
        //         log.error("Same transaction within 10 minutes");
        //         throw new TransactionProcessException("User not allowed to do the same transaction within " +
        //                 applicationProperties.getDuplicateReqTimeOutInMills() / 60000 + " minutes of last transaction");
        //     } else {
        //         return doPayPassportFee(passportNidFeePayment, account, locale);
        //     }
        // } else {
        //     return doPayPassportFee(passportNidFeePayment, account, locale);
        // }

    }

    public CommonResponse doPayPassportFee(PassportFeePayment passportNidFeePayment, String account, Locale locale) {

        if (Pattern.matches("......==", passportNidFeePayment.getPin())) {
            String formattedText = "TRUSTMM DIP " + passportNidFeePayment.getAmount() + " " + new String(Base64.getDecoder().decode(passportNidFeePayment.getPin())) + " " + passportNidFeePayment.getFirstName() + " " + passportNidFeePayment.getLastName() + " " + passportNidFeePayment.getEmail() + " " + CommonConstant.checkNumber(passportNidFeePayment.getNotificationNumber());
            ApiManagerRequest apiManagerRequest = makeProcessedRequest(applicationProperties.getEncryptionEnabled() ?
                    iSecurityService.encrypt(formattedText, "", "") : formattedText, ContextUrl.API_MANAGER_CONTEXT.getUrl(), account);

            CommonResponse response = getTransactionFormattedResponse(apiManagerGateway, apiManagerRequest, locale, true);
            try {
                transactionProcessService.storeProcess(account, passportNidFeePayment.getAmount(),passportNidFeePayment.getFirstName()+passportNidFeePayment.getLastName(), TransactionEvents.PASSPORT.toString());
            } catch (Exception e) {
                log.error(e.getMessage());
            }
            return response;

        } else {
            throw new DecodePinException(CommonConstant.DECODE_EXCEPTION_MESSAGE);
        }
    }

    @Override
    public CommonResponse payNIDFee(NIDFeePayment nidFeePayment, String account, Locale locale) {

        if (applicationProperties.getCheckEnabled()) {
            if (transactionProcessService.findProcess(account, new BigDecimal(0), nidFeePayment.getNidNo(), TransactionEvents.NID.toString())) {
                log.error("Same transaction within 10 minutes");
                throw new TransactionProcessException("User not allowed to do the same transaction within " +
                        applicationProperties.getDuplicateReqTimeOutInMills() / 60000 + " minutes of last transaction");
            } else {
                return doPayNIDFee(nidFeePayment,account,locale);
            }
        } else {
            return doPayNIDFee(nidFeePayment, account, locale);
        }


    }

    public CommonResponse doPayNIDFee(NIDFeePayment nidFeePayment, String account, Locale locale) {

        if (Pattern.matches("......==", nidFeePayment.getPin())) {
            String formattedText = "TrustMM NID " + nidFeePayment.getNidNo() + " " + new String(Base64.getDecoder().decode(nidFeePayment.getPin())) + " " + nidFeePayment.getCorrectionType() + " " + nidFeePayment.getServiceType() + " " + CommonConstant.checkNumber(nidFeePayment.getNotificationNumber());
            ApiManagerRequest apiManagerRequest = makeProcessedRequest(applicationProperties.getEncryptionEnabled() ?
                    iSecurityService.encrypt(formattedText, "", "") : formattedText, ContextUrl.API_MANAGER_CONTEXT.getUrl(), account);

            CommonResponse response = getTransactionFormattedResponse(apiManagerGateway, apiManagerRequest, locale, true);
            try {
                transactionProcessService.storeProcess(account, new BigDecimal(0),nidFeePayment.getNidNo(),TransactionEvents.NID.toString());
            } catch (Exception e) {
                log.error(e.getMessage());
            }
            return response;


        } else {
            throw new DecodePinException(CommonConstant.DECODE_EXCEPTION_MESSAGE);
        }


    }

    @Override
    public NidServiceListParams[] getNidServices(Locale locale) {
        String response = apiManagerGateway.getNidServiceList(locale);
        return CommonConstant.formatNidServices(response);
    }
}
