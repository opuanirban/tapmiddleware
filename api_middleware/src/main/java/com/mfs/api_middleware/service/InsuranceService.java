package com.mfs.api_middleware.service;

import com.mfs.api_middleware.dto.CommonResponse;
import com.mfs.api_middleware.dto.InsurancePaymentRequest;
import com.mfs.api_middleware.dto.PrimeLifeInsuranceBillPaymentRequest;

import java.util.Locale;

public interface InsuranceService {
    public CommonResponse insurancePayment(InsurancePaymentRequest insurancePaymentRequest, String accountNo, Locale locale);
    public CommonResponse primeLifeInsurancePayment(PrimeLifeInsuranceBillPaymentRequest request,String msisdn,Locale locale);
}
