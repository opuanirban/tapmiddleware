package com.mfs.api_middleware.service;

import com.mfs.api_middleware.dto.CommonResponse;
import com.mfs.api_middleware.dto.UserRegistrationRequest;
import com.mfs.api_middleware.gateway.model.CustomerRegistrationParams;

import java.util.Locale;

public interface AsyncRegistrationService {

    public CommonResponse asyncRegister(UserRegistrationRequest registrationParams, Locale locale);
    public CommonResponse customerRegistrationComplete(CustomerRegistrationParams customerRegistrationParams, Locale locale); 
}
