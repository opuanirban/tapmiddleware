package com.mfs.api_middleware.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class IvacBillInfoResponse extends BillInfoResponse{
    private String transaction_id;
}
