package com.mfs.api_middleware.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.math.BigDecimal;

@Data
public class TilBillPaymentRequest {

    @NotNull
    @NotEmpty
    private String tilUserName;

    @NotNull
    @NotEmpty
    private String tilUserId;

    @NotNull
    @NotEmpty
    private String transactionId;

    @NotNull
    @NotEmpty
    @Pattern(regexp="[0-9]+")
    private String notificationNumber;

    @NotNull
    private BigDecimal billAmount;

    @NotNull
    @NotEmpty
    private String pin;

}