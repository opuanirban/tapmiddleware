package com.mfs.api_middleware.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
public class PrimeLifeInsuranceBillFetchRequest {
//    @NotNull
//    @NotEmpty
//    @Pattern(regexp="[0-9]+")
//    private String accountNumber;//msisdn theke ashbe

//    @NotNull
//    @NotEmpty
//    private String billerId;

    @NotNull
    @NotEmpty
    private String code;

    @NotNull
    @NotEmpty
    private String policyNumber;
}
