package com.mfs.api_middleware.dto;

import lombok.Data;

@Data
public class TuitionFeeDetailResponse extends BillInfoResponse{
    private String customerName;
    private String dueDate;
    private String transactionId;
}