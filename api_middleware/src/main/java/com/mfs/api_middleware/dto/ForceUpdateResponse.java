package com.mfs.api_middleware.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class ForceUpdateResponse {
    private String forceUpdateVersion;

    private String blacklistVersion;

    private String message;

    private String link;

}
