package com.mfs.api_middleware.dto;

import lombok.Data;

@Data
public class ForceUpdateHuawei {
    private String forceUpdateVersion;

    private String blacklistVersion;

    private String message;

    private String link;
}
