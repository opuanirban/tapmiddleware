package com.mfs.api_middleware.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class NIDValidationRequest {
    @JsonProperty("NationalIdNumber")
    private String NationalIdNumber;

    @JsonProperty("UserNumber")
    private long userNumber;
}
