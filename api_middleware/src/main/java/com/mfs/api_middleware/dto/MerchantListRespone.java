package com.mfs.api_middleware.dto;

import lombok.Data;

import java.util.List;

@Data
public class MerchantListRespone {
    private Integer Count;
    private List<MerchantInfoFetchData> Users;
}
