package com.mfs.api_middleware.dto;

import lombok.Data;

@Data
public class BannerAndOffers {
    BannerAndOfferItem[] banners;
    BannerAndOfferItem[] offers;
}
