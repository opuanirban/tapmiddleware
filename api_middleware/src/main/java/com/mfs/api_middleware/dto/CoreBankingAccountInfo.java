package com.mfs.api_middleware.dto;

import lombok.Data;

@Data
public class CoreBankingAccountInfo {
    private int serialNo;
    private String maskedAccountNo;
    private String accountName;
}
