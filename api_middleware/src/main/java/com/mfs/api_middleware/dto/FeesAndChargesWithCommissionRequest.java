package com.mfs.api_middleware.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
public class FeesAndChargesWithCommissionRequest {
//        @NotNull
//        @JsonProperty("ChannelId")
//        private Long channelId;

    @NotNull
    @JsonProperty("FromUserNumber")
    private Long fromUserNumber;

    @NotNull
    @JsonProperty("ToUserNumber")
    private Long toUserNumber;

    @NotNull
    @NotEmpty
    @JsonProperty("TransactionType")
    private String transactionType;

    @NotNull
    @JsonProperty("Amount")
    @DecimalMin(value = "0.0", inclusive = false)
    private BigDecimal amount;

}
