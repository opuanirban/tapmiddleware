package com.mfs.api_middleware.dto;

import lombok.Data;

@Data
public class ExcelDto {
    private String key;
    private String value;
}
