package com.mfs.api_middleware.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class MerchantInfoRequest {
    @JsonProperty("AccountNumber")
    private long accountNumber;

    @JsonProperty("AccountId")
    private long accountId;
}
