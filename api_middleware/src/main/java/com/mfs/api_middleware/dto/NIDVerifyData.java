package com.mfs.api_middleware.dto;

import lombok.Data;

@Data
public class NIDVerifyData {
    private String status;
    private Boolean textual_info_match;
    private Boolean applicant_photo_card_ec_match;
    private Boolean applicant_photo_app_ec_match;
    private NIDVerificationStatus detail;
}
