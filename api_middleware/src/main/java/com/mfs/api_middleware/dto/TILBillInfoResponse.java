package com.mfs.api_middleware.dto;

import lombok.Data;

@Data
public class TILBillInfoResponse {

    private String transactionId;
    private String billAmount;
    private String customerName;
    private String customerNumber;
}
