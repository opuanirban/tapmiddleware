package com.mfs.api_middleware.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class DescoPrepaidBillDetailsResponse extends CommonResponse{
    private String billType;
    private String transactionId;
}
