package com.mfs.api_middleware.dto;

import com.mfs.api_middleware.gateway.model.BaseResponse;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class REBBillInfoResponse extends BillInfoResponse{
    private String billNo;
    private String dueDate;
    private String lpcDate;
    private String paymentStatus;
    private String billMonth;
    private String billYear;
    private BaseResponse response;
}
