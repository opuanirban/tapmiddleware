package com.mfs.api_middleware.dto;

import lombok.Data;

@Data
public class UserLoginInfo {
    private String userName;
    private String balance;
    private Boolean ekycComplete;
    private String userStatus;
    private Integer userGroupId;
    private FeatureAccesibilityMatrix featureAccesibilityMatrix;
}
