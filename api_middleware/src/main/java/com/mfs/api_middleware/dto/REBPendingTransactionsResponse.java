package com.mfs.api_middleware.dto;

import java.util.List;

import lombok.Data;

@Data
public class REBPendingTransactionsResponse {
    private List<REBPendingTransactionItem> rebPendingTransactionList;
}
