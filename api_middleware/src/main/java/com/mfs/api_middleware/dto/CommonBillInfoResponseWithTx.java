package com.mfs.api_middleware.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class CommonBillInfoResponseWithTx extends BillInfoResponse{
    private String transactionId;
}
