package com.mfs.api_middleware.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class MiniStatementResponseEntity {
    private String TxCode;
    private String TransactionDate;
    private String TransactionType;
    private Long SenderNumber;
    private Long ReceiverNumber;
    private String SenderName;
    private String ReceiverName;
    private String ActionType;
    private BigDecimal Amount;
}
