package com.mfs.api_middleware.dto;

import lombok.Data;

@Data
public class REBTransactionDetails {
    private String organizationName;
    private String meterNumber;
    private String billAmount;
    private String paymentDate;
    private String tapAccountNumber;
    private String transactionId;
    private String chargeAmount;
    private String token;
    private String sequenceNumber;
    private String customerName;
}
