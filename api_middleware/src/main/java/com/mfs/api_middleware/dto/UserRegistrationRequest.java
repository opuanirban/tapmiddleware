package com.mfs.api_middleware.dto;

import com.mfs.api_middleware.enumeration.GenderEnum;
import com.mfs.api_middleware.enumeration.MobileOperatorEnum;
import com.mfs.api_middleware.enumeration.UserGroupEnum;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
public class UserRegistrationRequest {
    @NotNull
    @NotEmpty
    @Pattern(regexp="[0-9]+")
    private String mobileNumber;
    @NotNull
    @NotEmpty
    private String pin;
//    @NotNull
//    @NotEmpty
    private String nid;
    @NotNull
    @NotEmpty
    private String firstName;
//    @NotNull
//    @NotEmpty
    private String lastName;
    @NotNull
    private GenderEnum gender;
    @NotNull
    private UserGroupEnum accountType;
//    @NotNull
//    @NotEmpty
    private String dob;
    @NotNull
    @NotEmpty
    private String occupation;
    @NotNull
    private MobileOperatorEnum mno;

}
