package com.mfs.api_middleware.dto;

import lombok.Data;

@Data
public class ClientConfigs {
    private ForceUpdateList forceUpdateList;
    private HiddenFeatureInfo hiddenFeatureInfo;
}
