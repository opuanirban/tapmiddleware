package com.mfs.api_middleware.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
public class FeesAndChargesWithCommissionResponse {
    @NotNull
    @JsonProperty("Amount")
    private BigDecimal amount;

    @NotNull
    @JsonProperty("ChannelId")
    private Long channelId;

    @NotNull
    @JsonProperty("FromUserNumber")
    private Long fromUserNumber;

    @NotNull
    @JsonProperty("ToUserNumber")
    private Long toUserNumber;

    @NotNull
    @NotEmpty
    @JsonProperty("TransactionType")
    private String transactionType;

    @NotNull
    @JsonProperty("TransactionTypeKey")
    private Long transactionTypeKey;

    @NotNull
    @JsonProperty("ChargeAmount")
    private  Double chargeAmount;

    @NotNull
    @JsonProperty("CommissionAmount")
    private Double commissionAmount;

    @NotNull
    @JsonProperty("HasSufficientBalance")
    private Boolean hasSufficientBalance;

    @NotNull
    @JsonProperty("NewBalance")
    private Double newBalance;

    @NotNull
    @JsonProperty("OldBalance")
    private Double oldBalance;

    @NotNull
    @JsonProperty("TotalPayable")
    private Double totalPayable;
}
