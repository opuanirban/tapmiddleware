package com.mfs.api_middleware.dto;

import lombok.Data;

@Data
public class IVACCenterItem {
    private int order;
    private String centerName;
    private String centerValue;
}
