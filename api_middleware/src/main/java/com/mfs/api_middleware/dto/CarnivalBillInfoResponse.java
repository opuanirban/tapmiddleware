package com.mfs.api_middleware.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class CarnivalBillInfoResponse extends CommonBillInfoResponseWithTx {
    private String accountName;
    private String accountId;
}
