package com.mfs.api_middleware.dto;


import lombok.Data;

@Data
public class PrimeLifeBillFetchResponse {
    private String policyNumber;
    private String billAmount;
    private String customerName;
    private String nextPremiumDueDate;
    private String policyMaturityDate;
    private String mobileNumber;
    private String transactionId;
    private String billerCode;
    private String fee;
    private String accountNumber;

}
