package com.mfs.api_middleware.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class DataListResponse {
    private Integer categoryId;
    private String categoryTitle;
    private String categoryIconSource;
    private Boolean activeStatus;
    private int sequenceNumber;
    private DataJson[] itemList;
}
