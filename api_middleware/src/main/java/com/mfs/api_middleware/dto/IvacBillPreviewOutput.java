package com.mfs.api_middleware.dto;

import com.mfs.api_middleware.gateway.nesco.model.BillPreviewOutput;
import lombok.Data;

@Data
public class IvacBillPreviewOutput extends BillPreviewOutput {
    private String billAmount;
}
