package com.mfs.api_middleware.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
public class ValidateOTPRequest {
    @NotNull
    private Integer otp;
    @NotNull
    @NotEmpty
    private String otpReferenceId;
    @NotNull
    @NotEmpty
    @Pattern(regexp="[0-9]+")
    private String receiverInfo;

}
