package com.mfs.api_middleware.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class PrepaidBillDetailsResponse extends CommonResponse{
    private String transactionId;
}
