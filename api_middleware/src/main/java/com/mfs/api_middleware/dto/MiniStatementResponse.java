package com.mfs.api_middleware.dto;

import lombok.Data;

import java.util.List;

@Data
public class MiniStatementResponse {
    private List<MiniStatementResponseEntity> statement;
}
