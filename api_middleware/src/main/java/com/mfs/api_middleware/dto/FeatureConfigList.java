package com.mfs.api_middleware.dto;

import lombok.Data;

import java.util.List;

@Data
public class FeatureConfigList {
    private List<FeatureConfigItem> featuresConfigList;
}
