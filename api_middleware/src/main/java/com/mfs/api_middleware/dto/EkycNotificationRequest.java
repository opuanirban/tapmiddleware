package com.mfs.api_middleware.dto;

import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class EkycNotificationRequest {
    @NotNull
    @NotEmpty
    private String status;
    @NotNull
    @Valid
    private EkycVerificationDetails detail;
    @NotNull
    private Boolean textual_info_match;
    @NotNull
    private Boolean applicant_photo_card_ec_match;
    @NotNull
    private Boolean applicant_photo_app_ec_match;


}
