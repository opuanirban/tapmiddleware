package com.mfs.api_middleware.dto;

import lombok.Data;

@Data
public class BannerItem {
    private String id;
    private String code;
    private String imageSource;
}
