package com.mfs.api_middleware.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class JGTDSLResponse extends BillInfoResponse {
    private String customerName;
    private String monthYear;
    private String transactionId;
}
