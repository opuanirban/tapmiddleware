package com.mfs.api_middleware.dto;

import lombok.Data;

@Data
public class RefreshEKYCResponse {
    private String gigatechStatus;
    private String tblStatus;
    private String tblMessage;
}
