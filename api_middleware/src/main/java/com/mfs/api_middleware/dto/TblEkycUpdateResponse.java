package com.mfs.api_middleware.dto;
import lombok.Data;

@Data
public class TblEkycUpdateResponse {
    private String kycUpdateStatus;
    private String kycUpdateMessage;
}
