package com.mfs.api_middleware.dto;

import com.mfs.api_middleware.enumeration.BillStatus;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class EkpayBillInfoResponse extends BillInfoResponse{
    private String customerName;
    private String dueDate;
    private String transactionId;
}
