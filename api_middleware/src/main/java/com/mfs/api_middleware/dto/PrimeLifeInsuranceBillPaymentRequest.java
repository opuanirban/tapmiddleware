package com.mfs.api_middleware.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
public class PrimeLifeInsuranceBillPaymentRequest {
    @NotNull
    @NotEmpty
    private String billerCode;

//    @NotNull
//    @NotEmpty
//    private String paymentChannel;

//    @NotNull
//    @NotEmpty
//    @Pattern(regexp="[0-9]+")
//    private String accountNumber;

    @NotNull
    @NotEmpty
    private String pin;

    @NotNull
    @NotEmpty
    private String policyNumber;

    @NotNull
    @NotEmpty
    private String billAmount;

    @NotNull
    @NotEmpty
    private String transactionId;


    @Pattern(regexp="[0-9]+")
    private String notificationNumber;

}
