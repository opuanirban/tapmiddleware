package com.mfs.api_middleware.dto;

import lombok.Data;

@Data
public class ForceUpdateList {
    private ForceUpdateAndroid android;
    private ForceUpdateIos iOS;
    private ForceUpdateHuawei huawei;
}
