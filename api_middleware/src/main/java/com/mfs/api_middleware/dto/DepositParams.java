package com.mfs.api_middleware.dto;

import lombok.Data;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.math.BigDecimal;

@Data
public class DepositParams {
    @NotEmpty
    @NotNull
    @Pattern(regexp="[0-9]+(-[0-9]+)+")
    private String accountNumber;
    @NotNull
    @DecimalMin(value = "0.0", inclusive = false)
    private BigDecimal amount;
    @NotEmpty
    @NotNull
    private String pin;
}
