package com.mfs.api_middleware.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class PinInfo {
    @NotNull
    @NotEmpty
    private String oldPin;

    @NotNull
    @NotEmpty
    private String newPin;
}
