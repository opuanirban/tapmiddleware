package com.mfs.api_middleware.dto;

import com.mfs.api_middleware.enumeration.EkpayBillerType;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.math.BigDecimal;

@Getter
@Setter
@ToString
public class EkpayBillRequest {
    private EkpayBillerType billerType;
    private String billNo;
    private String billAccountNo;
    private String billMobileNo;
    private String billType;
    private String fee;
    @NotNull
    @NotEmpty
    private String transactionId;
    @NotNull
    @NotEmpty
    @Pattern(regexp="[0-9]+")
    private String notificationNumber;
    @NotNull
    @DecimalMin(value = "0.0", inclusive = false)
    private BigDecimal billAmount;
    @NotNull
    @NotEmpty
    private String pin;
}
