package com.mfs.api_middleware.dto;

import lombok.Data;

@Data
public class REBPendingTransactionItem {
    private String transactionId;
    private String transactionDate;
    private String meterNumber;
    private String amount;
}
