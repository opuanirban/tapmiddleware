package com.mfs.api_middleware.dto;

import lombok.Data;

@Data
public class BaseResponse {
    private String code;
    private String message;

}
