package com.mfs.api_middleware.dto;

import lombok.Data;
import lombok.NonNull;

import javax.validation.constraints.NotEmpty;

@Data
public class IvacBillInfo {
    private String passportNo;
    private String appointType;
    @NonNull
    @NotEmpty
    private String ivacId;
    @NonNull
    @NotEmpty
    private String webFile;
    @NonNull
    @NotEmpty
    private String mobileNo;
}
