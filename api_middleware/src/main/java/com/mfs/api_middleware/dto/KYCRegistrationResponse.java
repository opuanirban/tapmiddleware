package com.mfs.api_middleware.dto;

import lombok.Data;

@Data
public class KYCRegistrationResponse {
    private String status;
    private Integer status_code;
    private String message;
}
