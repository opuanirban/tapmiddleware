package com.mfs.api_middleware.dto;

import com.mfs.api_middleware.enumeration.EkpayBillerType;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class EkpayBillInfo {
    private EkpayBillerType billerType;
    private String billNo;
    private String billAccountNo;
    private String billMobileNo;
    private String billType;
}

