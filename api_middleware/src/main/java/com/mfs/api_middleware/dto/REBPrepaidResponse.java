package com.mfs.api_middleware.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class REBPrepaidResponse extends CommonResponse {
    private String chargeDetails;
    private String amount;
    private String transactionId;
    private boolean isPendingRequest;
}
