package com.mfs.api_middleware.dto;

import lombok.Data;

@Data
public class NIDData {
    private String nid_no;
    private String dob;
    private String applicant_name_ben;
    private String applicant_name_eng;
    private String father_name;
    private String mother_name;
    private String spouse_name;
    private String address;
    private String id_front_image;
    private String id_back_image;
    private String id_front_name;
    private String id_back_name;
}
