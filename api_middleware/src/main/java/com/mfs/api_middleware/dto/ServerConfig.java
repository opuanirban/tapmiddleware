package com.mfs.api_middleware.dto;

import lombok.Data;

@Data
public class ServerConfig {
    private Boolean deviceBindingEnabled;
    private ServiceAvailibilityInfo serviceInfo;
}
