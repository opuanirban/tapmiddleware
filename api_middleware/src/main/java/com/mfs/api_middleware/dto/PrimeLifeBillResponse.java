package com.mfs.api_middleware.dto;

import lombok.Data;

@Data
public class PrimeLifeBillResponse {
    private String billAmount;
    private String customerName;
    private String nextPremiumDueDate;
    private String policyMatureDate;
    private String policyNumber;
    private String transactionId;
//    private String
}
