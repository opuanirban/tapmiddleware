package com.mfs.api_middleware.dto;

import lombok.Data;

@Data
public class TblRegistrationStatus {
    private String status;
    private RegistrationParams detail;
}
