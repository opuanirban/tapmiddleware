package com.mfs.api_middleware.dto;

import lombok.Data;

@Data
public class NIDUploadApiResponse {
    private String status;
    private Integer status_code;
    private NIDData data;
}
