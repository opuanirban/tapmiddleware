package com.mfs.api_middleware.dto;

import lombok.Data;

@Data
public class FeatureAccesibilityMatrixItem {
    private String featureName;
    private Boolean featureAccess;
}
