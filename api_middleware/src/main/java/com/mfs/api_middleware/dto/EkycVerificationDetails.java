package com.mfs.api_middleware.dto;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class EkycVerificationDetails {
    @NotNull
    @NotEmpty
    private String nid_no;
    @NotNull
    @NotEmpty
    private String dob;
    private String gender;
    private String profession;
    private String applicant_name_ben;
    private int applicant_name_ben_score;
    private String applicant_name_eng;
    private int applicant_name_eng_score;
    private String father_name;
    private int father_name_score;
    private String mother_name;
    private int mother_name_score;
    private String spouse_name;
    private int spouse_name_score;
    private String pres_address;
    private int pres_address_score;
    private String prem_address;
    @NotNull
    @NotEmpty
    private String applicant_photo;
    private int applicant_photo_score;
    @NotNull
    @NotEmpty
    private String mobile_number;
    private String nominee;
    private String nominee_relation;

    @Override
    public String toString() {
        return "EkycVerificationDetails [applicant_name_ben=" + applicant_name_ben + ", applicant_name_ben_score="
                + applicant_name_ben_score + ", applicant_name_eng=" + applicant_name_eng
                + ", applicant_name_eng_score=" + applicant_name_eng_score + ", applicant_photo_score="
                + applicant_photo_score + ", dob=" + dob + ", father_name=" + father_name + ", father_name_score="
                + father_name_score + ", gender=" + gender + ", mobile_number=" + mobile_number + ", mother_name="
                + mother_name + ", mother_name_score=" + mother_name_score + ", nid_no=" + nid_no + ", nominee="
                + nominee + ", nominee_relation=" + nominee_relation + ", prem_address=" + prem_address
                + ", pres_address=" + pres_address + ", pres_address_score=" + pres_address_score + ", profession="
                + profession + ", spouse_name=" + spouse_name + ", spouse_name_score=" + spouse_name_score + "]";
    }

}
