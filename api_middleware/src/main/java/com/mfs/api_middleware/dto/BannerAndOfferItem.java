package com.mfs.api_middleware.dto;

import lombok.Data;

@Data
public class BannerAndOfferItem extends BannerItem{
    private String webLink;
    private String title;
}
