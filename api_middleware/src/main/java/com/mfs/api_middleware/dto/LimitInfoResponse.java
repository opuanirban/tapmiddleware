package com.mfs.api_middleware.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class LimitInfoResponse {

    private int ServiceTypeId;
    private String ServiceName;
    private int DailyNumber;
    private BigDecimal DailyAmount;
    private int MonthlyNumber;
    private BigDecimal MonthlyAmount;
    private int DailyUsedNumber;
    private BigDecimal DailyUsedAmount;
    private int MonthlyUsedNumber;
    private BigDecimal MonthlyUsedAmount;

}
