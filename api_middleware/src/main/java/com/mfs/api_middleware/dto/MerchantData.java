package com.mfs.api_middleware.dto;

import lombok.Data;

@Data
public class MerchantData {
    private Long id;
    private String merchantNumber;
    private String merchantName;
}
