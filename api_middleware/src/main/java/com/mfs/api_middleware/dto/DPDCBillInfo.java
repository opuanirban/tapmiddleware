package com.mfs.api_middleware.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class DPDCBillInfo {

    @NotNull
    @NotEmpty
    private String locationCode;
    @NotNull
    @NotEmpty
    private String billMonth;
    @NotNull
    @NotEmpty
    private String accountNumber;

}
