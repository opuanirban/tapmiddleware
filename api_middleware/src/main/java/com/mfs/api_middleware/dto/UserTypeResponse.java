package com.mfs.api_middleware.dto;

import lombok.Data;

@Data
public class UserTypeResponse {
    private String key;
    private String value;
}
