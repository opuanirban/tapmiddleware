package com.mfs.api_middleware.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.sql.Date;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@ToString
public class TopUpBundleRequest {

    private String id;

    @NotEmpty(message = "Operator Name can not be empty")
    @NotNull(message = "Operator Name not be null")
    @NotBlank(message = "Operator Name can not be blank")
    private String operatorName;

    @NotEmpty(message = "Sim Type can not be empty")
    @NotNull(message = "Sim Type not be null")
    @NotBlank(message = "Sim Type can not be blank")
    private String simType;
    private String isForPostpaid;
    private String isForPrepaid;

    private String itemHeadTitle;
    private String callRate;
    private String minute;
    private String sms;
    private String data;
    private String validity;
    private Integer amount;

    private String itemHeadTitleBn;
    private String callRateBn;
    private String minuteBn;
    private String smsBn;
    private String dataBn;
    private String validityBn;

    private String offerType;
    private Date fromDate, toDate;

    private String sortField;
    private String sortOrder;

}