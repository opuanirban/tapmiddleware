package com.mfs.api_middleware.dto;

import lombok.Data;

@Data
public class TopUpResponse {
    private int Code;
    private String Message;
}
