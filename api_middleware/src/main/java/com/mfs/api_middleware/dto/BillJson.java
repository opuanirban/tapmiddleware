package com.mfs.api_middleware.dto;

import lombok.Data;

@Data
public class BillJson {
    private String Code;
    private String Description;
    private String IconSource;
}

