package com.mfs.api_middleware.dto;

import lombok.Data;

@Data
public class NidServiceListParams {
    private Integer Id;
    private String Description;
    private String Amount;
}
