package com.mfs.api_middleware.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
public class FeesAndChargesModel {

    @NotNull
    @NotEmpty
    @JsonProperty("FromUserNumber")
    private String fromUserNumber;
    @JsonProperty("ToUserNumber")
    private String toUserNumber;
    @NotNull
    @NotEmpty
    @JsonProperty("TransactionType")
    private String transactionType;
    @NotNull
    @DecimalMin(value = "0.0", inclusive = false)
    @JsonProperty("Amount")
    private BigDecimal amount;
}
