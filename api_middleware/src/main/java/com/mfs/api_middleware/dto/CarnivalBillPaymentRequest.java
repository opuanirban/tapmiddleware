package com.mfs.api_middleware.dto;

import java.math.BigDecimal;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import lombok.Data;

@Data
public class CarnivalBillPaymentRequest {
    @NotNull
    @NotEmpty
    private String carnivalCustomerId;
    @NotNull
    @NotEmpty
    private String transactionId;
    @NotNull
    @NotEmpty
    @Pattern(regexp="[0-9]+")
    private String notificationNumber;
    @NotNull
    private BigDecimal billAmount;
    @NotNull
    @NotEmpty
    private String pin;
}
