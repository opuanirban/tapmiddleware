package com.mfs.api_middleware.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.mfs.api_middleware.enumeration.ConnectionTypeEnum;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Locale;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class TopUpRequest {

    @NotNull
    @NotEmpty
    @Pattern(regexp="[0-9]+")
    private String recipientNumber;

    @NotNull
//    @Pattern(regexp = "[\\s]*[0-9]*[1-9]+",message="msg")
    @Min(value = 1)
    private int amount;

    @NotNull
    private ConnectionTypeEnum connectionType;

    @NotNull
    @NotEmpty
    private String operator;

    @NotNull
    @NotEmpty
    private String pin;

    private String accountNumber;
    private Locale requestTime;
    private boolean isBundle;

}
