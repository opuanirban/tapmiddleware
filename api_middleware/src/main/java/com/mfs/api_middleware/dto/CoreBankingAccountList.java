package com.mfs.api_middleware.dto;

import lombok.Data;

import java.util.List;

@Data
public class CoreBankingAccountList {
    private List<CoreBankingAccountInfo> accountInfo;
}
