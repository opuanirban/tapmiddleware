package com.mfs.api_middleware.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.math.BigDecimal;

@Setter
@Getter
@ToString
public class REBPostpaidPaymentRequest {
    @NotNull
    @NotEmpty
    private String smsAccountNo;
    @NotNull
    @NotEmpty
    private String transactionId;
    @NotNull
    @NotEmpty
    @Pattern(regexp="[0-9]+")
    private String notificationNumber;
    @NotNull
    @DecimalMin(value = "0.0", inclusive = false)
    private BigDecimal billAmount;
    @NotNull
    @NotEmpty
    private String pin;
}
