package com.mfs.api_middleware.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class NidDueResponse {
    @JsonProperty("Amount")
    private String Amount;
}
