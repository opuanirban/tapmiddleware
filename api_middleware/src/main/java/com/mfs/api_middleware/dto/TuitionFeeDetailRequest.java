package com.mfs.api_middleware.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class TuitionFeeDetailRequest {
    @NotNull
    @NotEmpty
    private String regNo;
    @NotNull
    @NotEmpty
    private String billDate;
    @NotNull
    @NotEmpty
    private String institutionCode;
}
