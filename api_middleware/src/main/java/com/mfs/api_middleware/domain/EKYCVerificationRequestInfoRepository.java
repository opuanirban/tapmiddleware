package com.mfs.api_middleware.domain;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EKYCVerificationRequestInfoRepository extends MongoRepository<EKYCVerificationRequestInfo, String> {
    EKYCVerificationRequestInfo findTopByOrderByRequestDateDesc();
}
