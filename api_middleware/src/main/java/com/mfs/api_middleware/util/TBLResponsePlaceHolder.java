package com.mfs.api_middleware.util;

public class TBLResponsePlaceHolder {
    public static final String CHECK_USER_STATUS_SUCCESS = "true";
    public static final String REGISTER_USER_SUCCESS = "Thank you. Account created successfully";
    public static final String REGISTER_USER_EXISTS= "User already exist";
    public static final String REGISTER_DATA_FAIL= "Registration data insert fail";
    public static final String DESCO_PREPAID_BREAKDOWN_SUCCESS = "Breakup";
    public static final String SUCCESSFUL_KYC_INFO_UPDATE = "KYC info updated successfully in TBL";
}
