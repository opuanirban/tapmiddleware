package com.mfs.api_middleware.util;

import java.util.HashMap;
import java.util.Map;

public class GigatechStatusCodes {

    private static Map<Integer, String> ekycStatus;

    static {
        ekycStatus = new HashMap<>();
        ekycStatus.put(5001, "Please check your image fields");
        ekycStatus.put(6001, "Nid or Date of Birth not found");
        ekycStatus.put(6002, "Nid and Date of Birth not found");
        ekycStatus.put(4001, "Extracted Nid information");
        ekycStatus.put(5003, "id_front_name or id_back_name field can not be more than 100 characters");
        ekycStatus.put(5002, "All fields are mandatory");
        ekycStatus.put(5004, "Please provide a valid Bangladeshi mobile number without country code");
        ekycStatus.put(5005, "Please upload a valid photo");
        ekycStatus.put(6003, "Applicant with this NID or mobile number already exists");
        ekycStatus.put(4002, "Customer is successfully re-enrolled, waiting verification");
        ekycStatus.put(4003, "Customer is successfully enrolled waiting verification");
        ekycStatus.put(6004, "No applicant with this mobile number found");
        ekycStatus.put(4004, "Customer data");
        ekycStatus.put(5006, "Please provide a valid status field");
        ekycStatus.put(6005, "No applicants record found");
        ekycStatus.put(4005, "Response data");
    }

}