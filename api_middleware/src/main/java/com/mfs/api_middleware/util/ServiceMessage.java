package com.mfs.api_middleware.util;

public class ServiceMessage {
    public static final String REGISTRATION_SUCCESS = "User Registration Successful.";
    public static final String REGISTRATION_FAILED = "User Registration Failed. Please try again";
    public static final String MINISTATEMENT_FAILED = "MiniStatement Failed. Please try again";
}
