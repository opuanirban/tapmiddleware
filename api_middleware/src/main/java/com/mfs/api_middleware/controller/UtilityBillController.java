package com.mfs.api_middleware.controller;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.mfs.api_middleware.dto.CommonResponse;
import com.mfs.api_middleware.dto.EkpayBillRequest;
import com.mfs.api_middleware.dto.IvacPaymentRequest;
import com.mfs.api_middleware.dto.JGTDSLPaymentRequest;
import com.mfs.api_middleware.service.EkpayService;
import com.mfs.api_middleware.service.UtilityBillService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UtilityBillController {

    @Autowired
    EkpayService ekpayService;
    @Autowired
    UtilityBillService utilityBillService;

    @ResponseBody
    @PostMapping(path = "/ekpay_payment", produces = "application/json")
    public CommonResponse EkpayPayment(@Valid @RequestBody EkpayBillRequest request, HttpServletRequest httpServletRequest, Locale locale) throws Exception {
        return ekpayService.payEkpayBill(request,httpServletRequest.getHeader("msisdn"),locale);
    }

    @ResponseBody
    @PostMapping(path = "/jgtdsl_payment", produces = "application/json")
    public CommonResponse jgtdslPayment(@Valid @RequestBody JGTDSLPaymentRequest request, HttpServletRequest httpServletRequest, Locale locale) throws Exception {
        return utilityBillService.payJGTDSLBill(request,httpServletRequest.getHeader("msisdn"),locale);
    }

    @ResponseBody
    @PostMapping(path = "/ivac_payment", produces = "application/json")
    public CommonResponse ivacPayment(@Valid @RequestBody IvacPaymentRequest ivacPaymentRequest,HttpServletRequest httpServletRequest,Locale locale){
        return utilityBillService.payIvacBill(ivacPaymentRequest,httpServletRequest.getHeader("msisdn"),locale);
    }

}
