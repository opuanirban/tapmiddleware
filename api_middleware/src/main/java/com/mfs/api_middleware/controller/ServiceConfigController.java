package com.mfs.api_middleware.controller;

import com.mfs.api_middleware.dto.ServiceAvailibilityInfo;
import com.mfs.api_middleware.dto.ServiceConfigResponse;
import com.mfs.api_middleware.service.ServiceConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ServiceConfigController {

    @Autowired
    ServiceConfigService serviceConfigService;
    
    @ResponseBody
    @GetMapping(path = "/get-service-config", produces = "application/json")
    public ServiceConfigResponse getConfig(){
        return serviceConfigService.getConfiguration();
    }

    @ResponseBody
    @PostMapping(path = "/set-service-availibility", produces = "application/json")
    public void setServiceAvailibilty(@RequestBody ServiceAvailibilityInfo serviceAvailibilityInfo){
        serviceConfigService.setServiceAvailibility(serviceAvailibilityInfo);
    }
}
