package com.mfs.api_middleware.controller;

import com.mfs.api_middleware.dto.CommonResponse;
import com.mfs.api_middleware.dto.PinInfo;
import com.mfs.api_middleware.service.PinUpdateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Locale;

@RestController
public class PinUpdateController {
    @Autowired
    PinUpdateService pinUpdateService;

//    @ResponseBody
//    @PostMapping(path = "/update_pin", produces = "application/json")
//    public CommonResponse updatePin(HttpServletRequest httpServletRequest, @Valid @RequestParam("oldPin") String oldPin, @Valid @RequestParam("newPin") String newPin, Locale locale) throws Exception{
//        return pinUpdateService.update_pin(httpServletRequest.getHeader("msisdn"),oldPin,newPin,locale);
//
//    }

    @ResponseBody
    @PostMapping(path = "/update_pin", produces = "application/json")
    public CommonResponse updatePin(HttpServletRequest httpServletRequest, @Valid @RequestBody PinInfo pinInfo, Locale locale) throws Exception{
        return pinUpdateService.update_pin(httpServletRequest.getHeader("msisdn"),pinInfo,locale);

    }

}
