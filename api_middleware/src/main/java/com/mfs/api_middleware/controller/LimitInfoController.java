package com.mfs.api_middleware.controller;

import com.mfs.api_middleware.dto.LimitInfoResponse;
import com.mfs.api_middleware.service.LimitInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Locale;

@RestController
public class LimitInfoController {

    @Autowired
    LimitInfoService limitInfoService;

    @ResponseBody
    @PostMapping(path = "/limit_info", produces = "application/json")
    public List<LimitInfoResponse> limitInfo(HttpServletRequest httpServletRequest, Locale locale) {
        return limitInfoService.fetchLimit(httpServletRequest.getHeader("msisdn"), locale);
    }
}
