package com.mfs.api_middleware.controller;

import com.mfs.api_middleware.dto.CommonResponse;
import com.mfs.api_middleware.dto.InsurancePaymentRequest;
import com.mfs.api_middleware.dto.PrimeLifeInsuranceBillPaymentRequest;
import com.mfs.api_middleware.service.InsuranceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Locale;

@RestController
public class InsuranceController {

    @Autowired
    InsuranceService insuranceService;

    @PostMapping(path = "/insurancePayment", produces = "application/json")
    public CommonResponse insurancePayment(@Valid @RequestBody InsurancePaymentRequest insurancePaymentRequest, HttpServletRequest httpServletRequest, Locale locale) {
        return insuranceService.insurancePayment(insurancePaymentRequest, httpServletRequest.getHeader("msisdn"),locale);
    }

    @ResponseBody
    @PostMapping(path = "/prime_life_insurance_pay_bill",produces = "application/json")
    public CommonResponse payPrimeLifeInsuranceBill(@RequestBody PrimeLifeInsuranceBillPaymentRequest request,HttpServletRequest httpServletRequest,
                                                    Locale locale){
        return insuranceService.primeLifeInsurancePayment(request,httpServletRequest.getHeader("msisdn"),locale);
    }

//    public

}
