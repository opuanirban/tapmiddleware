package com.mfs.api_middleware.controller;

import com.mfs.api_middleware.dto.*;
import com.mfs.api_middleware.service.TblRegistrationStatusService;
import com.mfs.api_middleware.util.CommonConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Locale;

@RestController
public class TestController {

    @Autowired
    TblRegistrationStatusService tblRegistrationStatusService;

    @ResponseBody
    @PostMapping(path = "/test", produces = "application/json")
    public RefreshEKYCResponse test(@RequestBody NIDVerificationResponse nidVerificationResponse, Locale locale) {

        RefreshEKYCResponse response = new RefreshEKYCResponse();
        EkycNotificationRequest ekycNotificationRequest = new EkycNotificationRequest();
        ekycNotificationRequest.setStatus(nidVerificationResponse.getData().getStatus());
        ekycNotificationRequest.setTextual_info_match(nidVerificationResponse.getData().getTextual_info_match());
        ekycNotificationRequest.setApplicant_photo_app_ec_match(nidVerificationResponse.getData().getApplicant_photo_app_ec_match());
        ekycNotificationRequest.setApplicant_photo_card_ec_match(nidVerificationResponse.getData().getApplicant_photo_card_ec_match());
        EkycVerificationDetails details = new EkycVerificationDetails();
        details.setApplicant_name_ben(nidVerificationResponse.getData().getDetail().getApplicant_name_ben());
        details.setApplicant_name_ben_score(nidVerificationResponse.getData().getDetail().getApplicant_name_ben_score());
        details.setApplicant_name_eng(nidVerificationResponse.getData().getDetail().getApplicant_name_eng());
        details.setApplicant_name_eng_score(nidVerificationResponse.getData().getDetail().getApplicant_name_eng_score());
        details.setFather_name(nidVerificationResponse.getData().getDetail().getFather_name());
        details.setMother_name(nidVerificationResponse.getData().getDetail().getMother_name());
        details.setFather_name_score(nidVerificationResponse.getData().getDetail().getFather_name_score());
        details.setMother_name_score(nidVerificationResponse.getData().getDetail().getMother_name_score());
        details.setSpouse_name(nidVerificationResponse.getData().getDetail().getSpouse_name());
        details.setSpouse_name_score(nidVerificationResponse.getData().getDetail().getSpouse_name_score());
        details.setDob(nidVerificationResponse.getData().getDetail().getDob());
        details.setPres_address(nidVerificationResponse.getData().getDetail().getPres_address());
        details.setPres_address_score(nidVerificationResponse.getData().getDetail().getPres_address_score());
        details.setPrem_address(nidVerificationResponse.getData().getDetail().getPrem_address());
        details.setApplicant_photo(nidVerificationResponse.getData().getDetail().getApplicantPhoto());
        details.setApplicant_photo_score(nidVerificationResponse.getData().getDetail().getApplicant_photo_score());
        details.setNid_no(nidVerificationResponse.getData().getDetail().getNid_no());
        details.setMobile_number(CommonConstant.checkNumber(nidVerificationResponse.getData().getDetail().getMobile_number()));
        details.setNominee(nidVerificationResponse.getData().getDetail().getNominee());
        details.setNominee_relation(nidVerificationResponse.getData().getDetail().getNomineeRelation());
        details.setDob(nidVerificationResponse.getData().getDetail().getDob());
        details.setGender(nidVerificationResponse.getData().getDetail().getGender());
        details.setProfession(nidVerificationResponse.getData().getDetail().getProfession());
        ekycNotificationRequest.setDetail(details);
        TblEkycUpdateResponse commonResponse = (tblRegistrationStatusService.passDataToTbl(ekycNotificationRequest, locale));
        response.setGigatechStatus(CommonConstant.PASSED);
        response.setTblStatus(commonResponse.getKycUpdateStatus());
        response.setTblMessage(commonResponse.getKycUpdateMessage());
        return response;

    }
}
