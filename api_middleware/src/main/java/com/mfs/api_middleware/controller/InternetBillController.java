package com.mfs.api_middleware.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.mfs.api_middleware.dto.*;
import com.mfs.api_middleware.gateway.model.BillFetchResponse;
import com.mfs.api_middleware.service.InternetService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.*;

import java.util.Locale;

@RestController
public class InternetBillController {
    @Autowired
    InternetService internetService;

    @ResponseBody
    @GetMapping(path = "/carnival_fetch_bill",produces = "application/json")
    public CarnivalBillInfoResponse getCarnivalTransactionDetails(@Valid @RequestParam("carnivalCustomerId") String carnivalCustomerId, HttpServletRequest httpServletRequest){
        return internetService.fetchCarnivalBillDetails(carnivalCustomerId, httpServletRequest.getHeader("msisdn"));
    }

    @ResponseBody
    @PostMapping(path = "/carnival_pay_bill",produces = "application/json")
    public CommonResponse payCarnivalBill(@Valid @RequestBody CarnivalBillPaymentRequest billPaymentRequest , HttpServletRequest httpServletRequest){
        return internetService.payCarnivalBill(billPaymentRequest, httpServletRequest.getHeader("msisdn"));
    }

    @ResponseBody
    @GetMapping(path = "/til_fetch_bill",produces = "application/json")
    public TILBillInfoResponse getTilTransactionDetails(@Valid @RequestParam("customerId") String customerId, HttpServletRequest httpServletRequest){
        return internetService.fetchTilBillDetails(customerId, httpServletRequest.getHeader("msisdn"));
    }

    @ResponseBody
    @PostMapping(path = "/til_pay_bill",produces = "application/json")
    public CommonResponse payTilBill(@Valid @RequestBody TilBillPaymentRequest billPaymentRequest , HttpServletRequest httpServletRequest){
        return internetService.payTilBill(billPaymentRequest, httpServletRequest.getHeader("msisdn"));
    }
}
