package com.mfs.api_middleware.controller;

import com.mfs.api_middleware.dto.CommonResponse;
import com.mfs.api_middleware.dto.NIDFeePayment;
import com.mfs.api_middleware.dto.NidServiceListParams;
import com.mfs.api_middleware.dto.PassportFeePayment;
import com.mfs.api_middleware.service.PassportPaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Locale;

@RestController
public class PassportNidFeeController {

    @Autowired
    PassportPaymentService passportPaymentService;

    @PostMapping(path = "/passport_payment", produces = "application/json")
    public CommonResponse payDipFee(@Valid @RequestBody PassportFeePayment passportNidFeePayment, HttpServletRequest httpServletRequest, Locale locale) {
        return passportPaymentService.payPassportFee(passportNidFeePayment, httpServletRequest.getHeader("msisdn"), locale);
    }

    @PostMapping(path = "/nid_payment", produces = "application/json")
    public CommonResponse payNidFee(@Valid @RequestBody NIDFeePayment nidFeePayment, HttpServletRequest httpServletRequest, Locale locale) {
        return passportPaymentService.payNIDFee(nidFeePayment, httpServletRequest.getHeader("msisdn"), locale);
    }

    @GetMapping(path = "/nid_services", produces = "application/json")
    public NidServiceListParams[] nidServices(Locale locale) {
        return passportPaymentService.getNidServices(locale);
    }
}
