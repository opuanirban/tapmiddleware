package com.mfs.api_middleware.controller;

import com.mfs.api_middleware.dto.MiniStatementResponse;
import com.mfs.api_middleware.service.MinistatementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

@RestController
public class MiniStatementController {

    @Autowired
    MinistatementService ministatementService;

    @ResponseBody
    @PostMapping(path = "/mini_statement", produces = "application/json")
    public MiniStatementResponse checkBalance(HttpServletRequest httpServletRequest, Locale locale) throws Exception {
        return ministatementService.fetchMiniStatement(httpServletRequest.getHeader("msisdn"),locale);
    }
}
