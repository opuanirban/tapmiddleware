package com.mfs.api_middleware.controller;

import com.mfs.api_middleware.dto.*;
import com.mfs.api_middleware.service.TuitionPaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Locale;

@RestController
public class TuitionFeeController {

    @Autowired
    TuitionPaymentService tuitionPaymentService;

    @PostMapping(path = "/tuitionPayment",produces = "application/json")
    public CommonResponse payTuition(@Valid @RequestBody TuitionFeePaymentRequest tuitionFeePaymentRequest, HttpServletRequest httpServletRequest, Locale locale){
        return tuitionPaymentService.payTuition(tuitionFeePaymentRequest,httpServletRequest.getHeader("msisdn"),locale);
    }

    @PostMapping(path = "/fetch_tuition_details",produces = "application/json")
    public TuitionFeeDetailResponse fetchTuitionDetails(@Valid @RequestBody TuitionFeeDetailRequest tuitionFeeDetailRequest, HttpServletRequest httpServletRequest, Locale locale){
        return tuitionPaymentService.fetchTuitionDetails(tuitionFeeDetailRequest,httpServletRequest.getHeader("msisdn"),locale);
    }

    @PostMapping(path = "/make_tuition_payment",produces = "application/json")
    public CommonResponse MakeTuitionPayment(@Valid @RequestBody TuitionFeeBillRequest tuitionFeeBillRequest, HttpServletRequest httpServletRequest, Locale locale){
        return tuitionPaymentService.makeTuitionPayment(tuitionFeeBillRequest,httpServletRequest.getHeader("msisdn"),locale);
    }
}
