package com.mfs.api_middleware.controller;

import com.mfs.api_middleware.Exception.GigatechException;
import com.mfs.api_middleware.dto.EkycNotificationRequest;
import com.mfs.api_middleware.dto.GigaResponse;
import com.mfs.api_middleware.dto.TblEkycUpdateResponse;
import com.mfs.api_middleware.service.TblRegistrationStatusService;
import com.mfs.api_middleware.util.CommonConstant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Locale;

@Slf4j
@RestController
public class RegistrationStatusController {

    @Value("${gigatech.token}")
    private String token;

    @Autowired
    TblRegistrationStatusService tblRegistrationStatusService;

    @CrossOrigin
    @ResponseBody
    @PostMapping(path = "/ekyc_notification", produces = "application/json")
    public GigaResponse registrationResponse(@Valid @RequestBody EkycNotificationRequest data, HttpServletRequest httpServletRequest, Locale locale) {

        log.debug("ekyc notification request: {}", data);
        String apiToken = httpServletRequest.getHeader(CommonConstant.GIGATECH_TOKEN);
        if (apiToken != null && apiToken.equalsIgnoreCase(token)) {
            GigaResponse response = new GigaResponse();
            log.info("eKYC notification received for: " + data.getDetail().getMobile_number());
            TblEkycUpdateResponse commonResponse = tblRegistrationStatusService.passDataToTbl(data, locale);
            response.setStatus(commonResponse.getKycUpdateStatus());
            return response;

        } else {
            throw new GigatechException("Token not present or invalid token");
        }

    }
}
