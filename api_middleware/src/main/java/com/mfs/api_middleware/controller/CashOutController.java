package com.mfs.api_middleware.controller;

import com.mfs.api_middleware.dto.CashOutRequest;
import com.mfs.api_middleware.dto.CommonResponse;
import com.mfs.api_middleware.service.CashOutService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Locale;

@RestController
public class CashOutController {

    @Autowired
    CashOutService cashOutService;

    @ResponseBody
    @PostMapping(path = "/cash_out", produces = "application/json")
    public CommonResponse cashOut(@Valid @RequestBody CashOutRequest cashOutRequest, HttpServletRequest httpServletRequest, Locale locale) throws Exception {
        return cashOutService.sendCashOut(cashOutRequest,httpServletRequest.getHeader("msisdn"),locale);
    }

}
