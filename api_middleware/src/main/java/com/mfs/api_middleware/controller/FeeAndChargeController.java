package com.mfs.api_middleware.controller;

import com.mfs.api_middleware.dto.FeesAndChargesModelWallet;
import com.mfs.api_middleware.dto.FeesAndChargesWithCommissionResponse;
import com.mfs.api_middleware.dto.FeesChargesResponse;
import com.mfs.api_middleware.dto.NidDueResponse;
import com.mfs.api_middleware.service.FeesAndChargesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Locale;

@RestController
public class FeeAndChargeController {

    @Autowired
    FeesAndChargesService feesAndChargesService;

    @ResponseBody
    @PostMapping(path = "/fee_charge", produces = "application/json")
    public FeesChargesResponse feeCharge(@Valid @RequestBody FeesAndChargesModelWallet feesAndChargesModel, HttpServletRequest httpServletRequest, Locale locale) {
        return feesAndChargesService.feeAndCharge(feesAndChargesModel, locale);
    }

    @GetMapping("/nid_fee_amount")
    public NidDueResponse getNidFessAmount(@RequestParam("nidNo") String nid_no,
                                           @RequestParam("correctionType")String correction_type,
                                           @RequestParam("serviceType") String service_type
            , Locale locale){
        return feesAndChargesService.feesNidResponse(nid_no,correction_type,service_type,locale);
    }

    @PostMapping("/GetFeesAndChargesWithCommissionDetail")
    public FeesAndChargesWithCommissionResponse feesAndChargesWithCommissionResponse(
            @Valid @RequestBody FeesAndChargesModelWallet request,
            Locale locale){
        return feesAndChargesService.feesAndChargeCommissionDetails(request,locale);
    }
}
