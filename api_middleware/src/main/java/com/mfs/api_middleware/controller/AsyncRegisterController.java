package com.mfs.api_middleware.controller;

import java.util.Locale;

import javax.validation.Valid;

import com.mfs.api_middleware.dto.CommonResponse;
import com.mfs.api_middleware.dto.KYCRegistrationResponse;
import com.mfs.api_middleware.dto.UserRegistrationRequest;
import com.mfs.api_middleware.gateway.model.CustomerRegistrationParams;
import com.mfs.api_middleware.service.AsyncRegistrationService;
import com.mfs.api_middleware.service.KYCService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
public class AsyncRegisterController {

    @Autowired
    AsyncRegistrationService asyncRegistrationService;

    @Autowired
    KYCService kycService;

    @ResponseBody
    @PostMapping(path = "/async_registration", produces = "application/json")
    public CommonResponse userRegistration(@Valid @RequestBody UserRegistrationRequest userRegistrationRequest, Locale locale) {
        return asyncRegistrationService.asyncRegister(userRegistrationRequest, locale);
    }

    @PostMapping(path = "/ekyc_verify")
    public KYCRegistrationResponse ekycVerify(@RequestParam("nid_no") String nid_no, @RequestParam("dob") String dob, @RequestParam("applicant_name_ben") String applicant_name_ben, @RequestParam("applicant_name_eng") String applicant_name_eng, @RequestParam("father_name") String father_name, @RequestParam("mother_name") String mother_name, @RequestParam("spouse_name") String spouse_name, @RequestParam("pres_address") String pres_address, @RequestParam("perm_address") String perm_address, @RequestParam("id_front_name") String id_front_name, @RequestParam("id_back_name") String id_back_name, @RequestParam("gender") String gender, @RequestParam("profession") String profession, @RequestParam("nominee") String nominee, @RequestParam("nominee_relation") String nominee_relation, @RequestParam("mobile_number") String mobile_number, @RequestParam("applicant_photo") MultipartFile applicant_photo, Locale locale) {
        return kycService.kycRegister(nid_no, dob, applicant_name_ben, applicant_name_eng, father_name, mother_name, spouse_name, pres_address, perm_address, id_front_name, id_back_name, gender, profession, nominee, nominee_relation, mobile_number, applicant_photo, locale);
    }

    @ResponseBody
    @PostMapping(path = "/customer_registration", produces = "application/json")
    public CommonResponse customerRegistration(@Valid @RequestBody CustomerRegistrationParams userRegistrationRequest, Locale locale) {
        return asyncRegistrationService.customerRegistrationComplete(userRegistrationRequest, locale);
    }
}
