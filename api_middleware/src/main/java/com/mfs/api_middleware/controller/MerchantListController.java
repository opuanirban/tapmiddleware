package com.mfs.api_middleware.controller;

import com.mfs.api_middleware.service.MerchantListService;
import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MerchantListController {

    @Autowired
    MerchantListService merchantListService;

    @RequestMapping(value = "/merchant-list",method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> getMerchantList(){
        return ResponseEntity.ok(merchantListService.responseMerchantList().getMerchantDataList());
    }
}
