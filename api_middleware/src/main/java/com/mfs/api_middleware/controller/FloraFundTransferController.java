package com.mfs.api_middleware.controller;

import com.mfs.api_middleware.dto.CommonResponse;
import com.mfs.api_middleware.dto.CoreBankingAccountList;
import com.mfs.api_middleware.dto.FloraFundTransferRequest;
import com.mfs.api_middleware.service.FloraFundService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Locale;

@RestController
public class FloraFundTransferController {

    @Autowired
    FloraFundService floraFundService;


    @ResponseBody
    @PostMapping(path = "/flora_fund", produces = "application/json")
    public CommonResponse floraFund(@Valid @RequestBody FloraFundTransferRequest floraFundTransferRequest, HttpServletRequest httpServletRequest, Locale locale) throws Exception {
        return floraFundService.fundTransfer(floraFundTransferRequest,httpServletRequest.getHeader("msisdn"),locale);
    }

    @ResponseBody
    @PostMapping(path = "/core_banking_account_info", produces = "application/json")
    public CoreBankingAccountList floraFund(HttpServletRequest httpServletRequest,Locale locale) {
        return floraFundService.getCoreBankingAccountInfo(httpServletRequest.getHeader("msisdn"),locale);
    }

}
