package com.mfs.api_middleware.controller;

import com.mfs.api_middleware.Exception.ExcelUploadException;
import com.mfs.api_middleware.dto.CommonResponse;
import com.mfs.api_middleware.service.ErrorMessageUploader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;

@RestController
public class MessageUploadController {

    @Autowired
    ErrorMessageUploader errorMessageUploader;

    @PostMapping(path = "/uploadErrorMessage")
    public CommonResponse uploadFile(@RequestParam("file") @Valid MultipartFile file){
        if(!file.isEmpty()){
            return errorMessageUploader.uploadFile(file);
        }else {
            throw new ExcelUploadException("Please upload a valid file");
        }

    }
}
