package com.mfs.api_middleware.controller;

import com.mfs.api_middleware.dto.CommonResponse;
import com.mfs.api_middleware.dto.FeatureConfigList;
import com.mfs.api_middleware.dto.FeatureHidePermissionRequest;
import com.mfs.api_middleware.dto.ForceUpdateRequest;
import com.mfs.api_middleware.service.FetchTransactionTypeService;
import com.mfs.api_middleware.service.ServiceConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Locale;

@RestController
public class DataConfigController {

    @Autowired
    FetchTransactionTypeService transactionTypeService;
    @Autowired
    ServiceConfigService serviceConfigService;

    @ResponseBody
    @GetMapping(path = "/config-data", produces = "application/json")
    public FeatureConfigList feeCharge(Locale locale) {
        return transactionTypeService.fetchTypes(locale);
    }

    @PostMapping(path = "/feature-hide-config", produces = "application/json")
    public CommonResponse setHideFeatureConfig(@RequestBody FeatureHidePermissionRequest featureHidePermissionRequest, Locale locale){
        return serviceConfigService.setHiddenFeatureVersion(featureHidePermissionRequest,locale);
    }
}
