package com.mfs.api_middleware.controller;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.mfs.api_middleware.dto.AkashPaymentRequest;
import com.mfs.api_middleware.dto.CommonResponse;
import com.mfs.api_middleware.service.TvService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/tv-service")
public class TvServiceController {
    @Autowired
    TvService tvService;

    @ResponseBody
    @PostMapping(path = "/akash", produces = "application/json")
    public CommonResponse descoPrepaid(@Valid @RequestBody AkashPaymentRequest paymentRequest, HttpServletRequest httpServletRequest, Locale locale) throws Exception {
        return tvService.payAkashService(paymentRequest,httpServletRequest.getHeader("msisdn"),locale);
    }

}
