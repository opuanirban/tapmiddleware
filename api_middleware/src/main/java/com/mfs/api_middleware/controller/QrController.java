package com.mfs.api_middleware.controller;

import com.mfs.api_middleware.dto.MerchantInfoResponse;
import com.mfs.api_middleware.service.QrService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;

@RestController
public class QrController {

    @Autowired
    QrService qrService;

    @ResponseBody
    @GetMapping("/merchant-qr")
    public MerchantInfoResponse getMerchant(@RequestParam("id") String id, @RequestParam("secret") String secret ){

        return qrService.getMerchantInfo(id,secret);
    }
}
