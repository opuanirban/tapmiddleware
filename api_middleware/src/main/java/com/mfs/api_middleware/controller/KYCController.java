package com.mfs.api_middleware.controller;

import com.mfs.api_middleware.dto.KYCRegistrationResponse;
import com.mfs.api_middleware.dto.NIDUploadApiResponse;
import com.mfs.api_middleware.gateway.KYCGateway;
import com.mfs.api_middleware.service.KYCService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.Locale;

@Slf4j
@RestController
public class KYCController {

    @Autowired
    KYCService kycService;
    @Autowired
    KYCGateway kycGateway;

    @PostMapping(path = "/nidUpload")
    public NIDUploadApiResponse nidUpload(@RequestParam("id_front") MultipartFile id_front, @RequestParam("id_back") MultipartFile id_back,String msisdn,Boolean isNewRegistration, Locale locale) {
        long requestTime = System.currentTimeMillis();
        NIDUploadApiResponse nidUploadApiResponse = kycGateway.nidUpload(id_front, id_back,msisdn,isNewRegistration, locale);
        long elapsedTime = System.currentTimeMillis() - requestTime;
        log.info("NID Upload Elapsed time for msisdn: " + msisdn + " Elapsed: " + elapsedTime);
        return nidUploadApiResponse;
    }

    @PostMapping(path = "/kycRegistration")
    public KYCRegistrationResponse kycRegister(@RequestParam("nid_no") String nid_no, @RequestParam("dob") String dob, @RequestParam("applicant_name_ben") String applicant_name_ben, @RequestParam("applicant_name_eng") String applicant_name_eng, @RequestParam("father_name") String father_name, @RequestParam("mother_name") String mother_name, @RequestParam("spouse_name") String spouse_name, @RequestParam("pres_address") String pres_address, @RequestParam("perm_address") String perm_address, @RequestParam("id_front_name") String id_front_name, @RequestParam("id_back_name") String id_back_name, @RequestParam("gender") String gender, @RequestParam("profession") String profession, @RequestParam("nominee") String nominee, @RequestParam("nominee_relation") String nominee_relation, @RequestParam("mobile_number") String mobile_number, @RequestParam("applicant_photo") MultipartFile applicant_photo, Locale locale) {
        long requestTime = System.currentTimeMillis();
        KYCRegistrationResponse kycRegistrationResponse = kycService.kycRegister(nid_no, dob, applicant_name_ben, applicant_name_eng,
                father_name, mother_name, spouse_name, pres_address, perm_address, id_front_name, id_back_name, gender, profession, nominee, nominee_relation, mobile_number, applicant_photo, locale);
        long elapsedTime = System.currentTimeMillis() - requestTime;
        log.info("NID verify request Elapsed time for msisdn: " + mobile_number + " Elapsed: " + elapsedTime);
        return kycRegistrationResponse;
    }


}
