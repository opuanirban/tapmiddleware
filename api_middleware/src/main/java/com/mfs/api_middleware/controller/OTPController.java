package com.mfs.api_middleware.controller;

import com.google.common.util.concurrent.RateLimiter;
import com.mfs.api_middleware.Exception.ApiRequestException;
import com.mfs.api_middleware.config.ApplicationProperties;
import com.mfs.api_middleware.dto.BaseResponse;
import com.mfs.api_middleware.dto.GenerateOTPRequest;
import com.mfs.api_middleware.dto.GenerateOTPResponse;
import com.mfs.api_middleware.dto.ValidateOTPRequest;
import com.mfs.api_middleware.gateway.OTPserviceGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import javax.validation.Valid;
import java.util.Locale;

@RestController
public class OTPController {

    @Autowired
    OTPserviceGateway otPserviceGateway;

    @Autowired
    ApplicationProperties applicationProperties;

    private RateLimiter rateLimiter;

    @PostConstruct
    void init() {
        rateLimiter = RateLimiter.create(applicationProperties.getAllowedTps());
    }

    @ResponseBody
    @PostMapping(path = "/generate-otp", consumes = "application/json", produces = "application/json")
    public GenerateOTPResponse getOTP(@Valid @RequestBody GenerateOTPRequest generateOTPRequest, Locale locale) {
        if (rateLimiter.tryAcquire()) {
            return otPserviceGateway.getOtp(generateOTPRequest,locale);
        } else {
            throw new ApiRequestException("Please try again after some time");
        }
    }

    @ResponseBody
    @PostMapping(path = "/validate-otp", consumes = "application/json", produces = "application/json")
    public BaseResponse validateOTP(@Valid @RequestBody ValidateOTPRequest validateOTPRequest,Locale locale) {
        if (rateLimiter.tryAcquire()) {
            return otPserviceGateway.validateOTP(validateOTPRequest,locale);
        } else {
            throw new ApiRequestException("Please try again after some time");
        }
    }


}
