package com.mfs.api_middleware.controller;


import com.mfs.api_middleware.dto.RegistrationResponse;
import com.mfs.api_middleware.dto.UserRegistrationRequest;
import com.mfs.api_middleware.enumeration.GenderEnum;
import com.mfs.api_middleware.enumeration.MobileOperatorEnum;
import com.mfs.api_middleware.enumeration.UserGroupEnum;
import com.mfs.api_middleware.service.UserRegistrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Locale;

@RestController
public class UserRegistrationController {

    @Autowired
    UserRegistrationService userRegistrationService;

    @Autowired
    private MessageSource messageSource;


    @ResponseBody
    @PostMapping(path = "/user_registration", produces = "application/json")
    public RegistrationResponse userRegistration(@Valid @RequestBody UserRegistrationRequest userRegistrationRequest, Locale locale) throws Exception {
        return userRegistrationService.userRegistration(userRegistrationRequest,locale);
    }

    @PostMapping(path = "/user_registration_kyc")
    public RegistrationResponse kycRegister(@Valid @NotNull @RequestParam("pin") String pin, @NotNull @RequestParam("firstName") String firstName,
                                      @NotNull @RequestParam("lastName") String lastName, @NotNull @RequestParam("accountType") UserGroupEnum accountType,
                                      @NotNull @RequestParam("mno") MobileOperatorEnum mno, @NotNull @RequestParam("nid_no") String nid_no, @NotNull @RequestParam("dob") String dob,
                                      @NotNull @RequestParam("applicant_name_ben") String applicant_name_ben, @NotNull @RequestParam("applicant_name_eng") String applicant_name_eng,
                                      @NotNull @RequestParam("father_name") String father_name, @NotNull @RequestParam("mother_name") String mother_name, @NotNull @RequestParam("spouse_name") String spouse_name,
                                      @NotNull @RequestParam("pres_address") String pres_address, @NotNull @RequestParam("perm_address") String perm_address,
                                      @NotNull @RequestParam("id_front_name") String id_front_name, @NotNull @RequestParam("id_back_name") String id_back_name, @NotNull @RequestParam("gender") GenderEnum gender,
                                      @NotNull @RequestParam("profession") String profession, @NotNull @RequestParam("nominee") String nominee, @NotNull @RequestParam("nominee_relation") String nominee_relation,
                                      @NotNull @RequestParam("mobile_number") String mobile_number, @NotNull @RequestParam("applicant_photo") MultipartFile applicant_photo,Locale locale) {


        return userRegistrationService.userRegistrationKyc(pin, firstName, lastName, accountType, mno, nid_no, dob, applicant_name_ben, applicant_name_eng, father_name, mother_name, spouse_name, pres_address, perm_address, id_front_name, id_back_name, gender, profession, nominee, nominee_relation, mobile_number, applicant_photo,locale);
    }

}
