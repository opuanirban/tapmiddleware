package com.mfs.api_middleware.controller;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.mfs.api_middleware.dto.*;
import com.mfs.api_middleware.service.DataFetchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Locale;

@RestController
public class DataFetchController {

    @Autowired
    DataFetchService dataFetchService;

    @GetMapping(path = "/getInsurance")
    public DataJson[] getInsurance(Locale locale) {
        return dataFetchService.getInsurance(locale);
    }

    @GetMapping(path = "/getInstitution")
    public DataJson[] getInstitution(Locale locale) {
        return dataFetchService.getInstitution(locale);
    }

    @GetMapping(path = "/getInstitution/v2")
    public SchoolDataJson[] getInstitutionV2(Locale locale) {
        return dataFetchService.getInstitutionV2(locale);
    }

    @GetMapping(path = "/getUtility")
    public List<DataListResponse> getUtility(Locale locale) {
        return dataFetchService.getUtility(locale);
    }

    @GetMapping(path = "/getUtility/v2")
    public List<DataListResponse> getUtilityV2(Locale locale) {
        return dataFetchService.getUtilityForNewApp(locale);
    }

    @GetMapping(path = "/getUtilityWithEkycStatus")
    public List<DataListResponse> getUtilityWithEkycStatus(Locale locale){
        return dataFetchService.getUtility(locale);
    }

    @GetMapping(path = "/getOthers")
    public DataJson[] getOthers(Locale locale) {
        return dataFetchService.getOthers(locale);
    }

    @GetMapping(path = "/getDonations")
    public DataJson[] getDonations(Locale locale) {
        return dataFetchService.getDonations(locale);
    }

    @GetMapping(path = "/getBanners")
    public BannerItem[] getBanners(Locale locale) {
        return dataFetchService.getBanners(locale);
    }

    /*@GetMapping(path = "/getBannersAndOffers")
    public BannerAndOffers getBannersAndOffers(Locale locale){
        return dataFetchService.getBannersAndOffers(locale);
    }*/

    @GetMapping(path = "/getBannersAndOffers")
    public CampaignMain getBannersAndOffers(){
        return dataFetchService.getBannersAndOffersV2();
    }

    @PostMapping(path = "/modify-donation-merchant")
    public void modifyDonationMerchantList(@RequestBody DonationMerchantListRequest donationMerchantList){
        dataFetchService.modifyDonationMerchantList(donationMerchantList.getDonationMerchantList());
    }

    @GetMapping(path="/getFees")
    public DataJson[] getfeesItems(Locale locale){
        return dataFetchService.getFees(locale);
    }

    @GetMapping(path="/getFees/v2")
    public DataJson[] getfeesItemsForNewApp(Locale locale){
        return dataFetchService.getFeesForNewApp(locale);
    }

    //Development for getTickets
    @GetMapping(path="/getTickets")
    public List<DataListResponse> getTickets(Locale locale) {return dataFetchService.getTickets(locale);}
    /*public String getTickets (Locale locale) { return "Hello Tickets";}
    public List<DataListResponse> getTickets(Locale locale) {
        return dataFetchService.getUtility(locale);
    }*/

}
