package com.mfs.api_middleware.controller;

import com.mfs.api_middleware.dto.BnrfParam;
import com.mfs.api_middleware.dto.CommonResponse;
import com.mfs.api_middleware.service.BnrfService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Locale;

@RestController
public class BnrfController {

    @Autowired
    BnrfService bnrfService;

    @PostMapping(path = "/bnrf", produces = "application/json")
    public CommonResponse payDipFee(@Valid @RequestBody BnrfParam bnrfParam, HttpServletRequest httpServletRequest, Locale locale) {
        return bnrfService.bnrfRequest(bnrfParam, httpServletRequest.getHeader("msisdn"),locale);
    }
}
