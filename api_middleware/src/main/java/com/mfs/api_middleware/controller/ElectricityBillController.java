package com.mfs.api_middleware.controller;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.mfs.api_middleware.dto.*;
import com.mfs.api_middleware.service.DescoService;
import com.mfs.api_middleware.service.DpdcService;
import com.mfs.api_middleware.service.NescoService;
import com.mfs.api_middleware.service.REBService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ElectricityBillController {

    @Autowired
    DescoService descoService;
    @Autowired
    DpdcService dpdcService;
    @Autowired
    NescoService nescoService;
    @Autowired
    REBService rebService;

    @ResponseBody
    @PostMapping(path = "/desco_prepaid", produces = "application/json")
    public CommonResponse descoPrepaid(@Valid @RequestBody DescoPrepaidRequest prepaidRequest, HttpServletRequest httpServletRequest, Locale locale) throws Exception {
        return descoService.payDescoBill(prepaidRequest,httpServletRequest.getHeader("msisdn"),locale);
    }

    @ResponseBody
    @PostMapping(path = "/desco_postpaid", produces = "application/json")
    public CommonResponse descoPostpaid(@Valid @RequestBody DescoPostPaidRequest postPaidRequest,HttpServletRequest httpServletRequest,Locale locale) throws Exception {
        return descoService.payDescoBill(postPaidRequest,httpServletRequest.getHeader("msisdn"),locale);
    }

    @ResponseBody
    @PostMapping(path = "/dpdc_pay", produces = "application/json")
    public CommonResponse descoPrepaid(@Valid @RequestBody DPDCBillPayRequest dpdcBillPayRequestt, HttpServletRequest httpServletRequest, Locale locale){
        return dpdcService.payDpdcBill(dpdcBillPayRequestt,httpServletRequest.getHeader("msisdn"),locale);
    }

    @ResponseBody
    @PostMapping(path = "/nesco_prepaid", produces = "application/json")
    public CommonResponse nescoPrepaid(@Valid @RequestBody NescoPrepaidRequest prepaidRequest, HttpServletRequest httpServletRequest, Locale locale) {
        return nescoService.payNescoBill(prepaidRequest,httpServletRequest.getHeader("msisdn"),locale);
    }

    @ResponseBody
    @PostMapping(path = "/nesco_postpaid", produces = "application/json")
    public CommonResponse nescoPostpaid(@Valid @RequestBody NescoPostpaidRequest postPaidRequest,HttpServletRequest httpServletRequest,Locale locale) throws Exception {
        return nescoService.payNescoPostpaidBill(postPaidRequest,httpServletRequest.getHeader("msisdn"),locale);
    }

    @ResponseBody
    @PostMapping(path = "/reb_prepaid", produces = "application/json")
    public REBPrepaidResponse rebPrepaid(@Valid @RequestBody REBPrepaidRequest prepaidRequest, HttpServletRequest httpServletRequest, Locale locale) {
        return rebService.payREBPrepaidBill2(prepaidRequest,httpServletRequest.getHeader("msisdn"),locale);
    }

    @ResponseBody
    @PostMapping(path = "/reb_postpaid", produces = "application/json")
    public CommonResponse rebPostPaid(@Valid @RequestBody REBPostpaidPaymentRequest postpaidRequest, HttpServletRequest httpServletRequest, Locale locale) {
        return rebService.payREBPostpaidBill(postpaidRequest,httpServletRequest.getHeader("msisdn"),locale);
    }

    @ResponseBody
    @GetMapping(path = "/reb_details",produces = "application/json")
    public REBTransactionDetails getTransactionDetails(@Valid @RequestParam("transactionId") String transactionId, HttpServletRequest httpServletRequest){
        return rebService.rebTransactionDetails2(transactionId);
    }

    @ResponseBody
    @GetMapping(path = "/reb_pending_list", produces = "application/json")
    public REBPendingTransactionsResponse getPendingTransactions(HttpServletRequest httpServletRequest){
        return rebService.getPendingTransactionList2(httpServletRequest.getHeader("msisdn"));
    }

    @ResponseBody
    @PostMapping(path = "/reb_prepaid_retry", produces = "application/json")
    public REBPrepaidResponse rebPrepaidRetry(@RequestParam("transactionId") String transactionId, HttpServletRequest httpServletRequest, Locale locale) {
        return rebService.retryREBPrepaidBill2(transactionId);
    }
}
