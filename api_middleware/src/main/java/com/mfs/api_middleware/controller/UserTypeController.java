package com.mfs.api_middleware.controller;

import com.mfs.api_middleware.dto.UserTypeResponseWallet;
import com.mfs.api_middleware.service.UserTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Locale;

@RestController
public class UserTypeController {

    @Autowired
    UserTypeService userTypeService;

    @ResponseBody
    @GetMapping(path = "/user_type", produces = "application/json")
    public UserTypeResponseWallet limitInfo(@Valid @NotNull @RequestParam("userNumber") String userNumber, Locale locale) {
        return userTypeService.fetchUserType(userNumber, locale);
    }
}
