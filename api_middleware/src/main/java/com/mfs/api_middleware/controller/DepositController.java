package com.mfs.api_middleware.controller;

import com.mfs.api_middleware.dto.CommonResponse;
import com.mfs.api_middleware.dto.DepositParams;
import com.mfs.api_middleware.service.DepositService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Locale;

@RestController
public class DepositController {

    @Autowired
    DepositService depositService;

    @ResponseBody
    @PostMapping(path = "/deposit_money", produces = "application/json")
    public CommonResponse deposit(@Valid @RequestBody DepositParams depositParams, HttpServletRequest httpServletRequest, Locale locale) throws Exception {
        return depositService.depositMoney(depositParams, httpServletRequest.getHeader("msisdn"),locale);
    }
}
