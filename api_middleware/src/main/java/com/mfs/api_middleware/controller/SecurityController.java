package com.mfs.api_middleware.controller;

import com.mfs.api_middleware.service.security.ISecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SecurityController {
    @Autowired
    ISecurityService securityService;

    @ResponseBody
    @PostMapping(path = "/decrypt")
    public String decrypt(@RequestParam("input") String input) {
        return securityService.decrypt(input, null, null);
    }

    @ResponseBody
    @PostMapping(path = "/encrypt")
    public String encrypt(@RequestParam("input") String input) {
        return securityService.encrypt(input, null, null);
    }
}
