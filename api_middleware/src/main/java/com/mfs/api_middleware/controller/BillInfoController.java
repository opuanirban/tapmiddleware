package com.mfs.api_middleware.controller;

import com.mfs.api_middleware.dto.*;
import com.mfs.api_middleware.service.BillInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import java.util.List;
import java.util.Locale;
import java.util.PrimitiveIterator;

@RestController
public class BillInfoController {

    @Autowired
    BillInfoService billInfoService;

    @ResponseBody
    @PostMapping(path = "/dpdc_bill", produces = "application/json")
    public BillInfoResponse dpdcBill(@Valid @RequestBody DPDCBillInfo dpdcBillInfo, Locale locale){
        return billInfoService.getDpdcBill(dpdcBillInfo,locale);
    }

    @ResponseBody
    @PostMapping(path = "/desco_bill", produces = "application/json")
    public BillInfoResponse descoBill(@Valid @RequestParam("billNo") String billNo, Locale locale){
        return billInfoService.getDescoBill(billNo,locale);
    }

    @ResponseBody
    @PostMapping(path = "/nesco_bill", produces = "application/json")
    public NescoBillInfoResponse nescoBill(@Valid @RequestBody NescoBillInfo nescoBillInfo, Locale locale, HttpServletRequest httpServletRequest){
        final String customerAccountNumber = httpServletRequest.getHeader("msisdn");
        return billInfoService.getNescoBill(customerAccountNumber, nescoBillInfo, locale);
    }

    @ResponseBody
    @PostMapping(path = "/desco_prepaid_bill", produces = "application/json")
    public DescoPrepaidBillDetailsResponse descoBillPrepaid(@Valid @RequestParam("meterNo") String meterNo, 
    @Valid @RequestParam("amount") String amount, Locale locale, HttpServletRequest httpServletRequest){
        String customerAccountNumber = httpServletRequest.getHeader("msisdn");
        return billInfoService.getDescoPrepaidInfo(meterNo,amount,locale, customerAccountNumber);
    }

    @ResponseBody
    @PostMapping(path = "/nesco_prepaid_bill", produces = "application/json")
    public PrepaidBillDetailsResponse nescoBillPrepaid(@RequestParam(value = "customerNo", required = false) String customerNo, 
    @RequestParam(value = "meterNo", required = false) String meterNo, @RequestParam("amount") String amount, HttpServletRequest httpServletRequest, 
    Locale locale){
        String customerAccountNumber = httpServletRequest.getHeader("msisdn");
        return billInfoService.getNescoPrepaidInfo(customerAccountNumber, customerNo, meterNo, amount, locale);
    }

    @ResponseBody
    @PostMapping(path = "/ekpay_bill", produces = "application/json")
    public EkpayBillInfoResponse ekpayBill(@Valid @RequestBody EkpayBillInfo billInfo, Locale locale, HttpServletRequest httpServletRequest){
        final String customerAccountNumber = httpServletRequest.getHeader("msisdn");
        return billInfoService.getEkpayBillInfo(customerAccountNumber, billInfo, locale);
    }

    @ResponseBody
    @PostMapping(path = "/akash_bill", produces = "application/json")
    public CommonResponse akashBillInfo(@RequestParam(value = "akashAccountNumber") String accountNumber, HttpServletRequest httpServletRequest, 
    Locale locale){
        String customerAccountNumber = httpServletRequest.getHeader("msisdn");
        return billInfoService.getAkashBillInfo(accountNumber, customerAccountNumber, locale);
    }

    @ResponseBody
    @PostMapping(path = "/jgtdsl_bill", produces = "application/json")
    public JGTDSLResponse jgtdslBillInfo(@RequestParam(value = "accountNumber") String accountNumber, HttpServletRequest httpServletRequest, 
    Locale locale){
        String customerAccountNumber = httpServletRequest.getHeader("msisdn");
        return billInfoService.getJGTDSLBillInfo(accountNumber, customerAccountNumber, locale);
    }

    @ResponseBody
    @PostMapping(path="/ivac_bill",produces = "application/json")
    public IvacBillInfoResponse ivacBillInfo(@Valid @RequestBody IvacBillInfo ivacBillInfo,
                                       Locale locale,HttpServletRequest httpServletRequest){

        return billInfoService.getIvacBillIfo(httpServletRequest.getHeader("msisdn"),ivacBillInfo);
    }

    @ResponseBody
    @GetMapping(path="/ivac_center_info",produces = "application/json")
    public List<IVACCenterItem> ivacCenterInfo(){
        return billInfoService.getIVACCenterList();
    }


    @ResponseBody
    @PostMapping(path = "/prime_life_insurance_info",produces = "application/json")
    public PrimeLifeBillResponse getPrimeLifeInsuranceInfo(@RequestBody PrimeLifeInsuranceBillFetchRequest request,HttpServletRequest httpServletRequest,
                                                           Locale locale){

        return billInfoService.getPrimeLifeInfo(httpServletRequest.getHeader("msisdn"),request);
    }

    @ResponseBody
    @PostMapping(path = "/reb_bill", produces = "application/json")
    public REBBillInfoResponse rebBillInfo(@RequestParam(value = "smsAccountNumber") String smsAccountNumber, HttpServletRequest httpServletRequest,
                                         Locale locale){
        String customerAccountNumber = httpServletRequest.getHeader("msisdn");
        return billInfoService.getREBPostpaidBill(smsAccountNumber, customerAccountNumber, locale);
    }

}
