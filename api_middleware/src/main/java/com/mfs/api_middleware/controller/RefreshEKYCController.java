package com.mfs.api_middleware.controller;

import com.mfs.api_middleware.dto.RefreshEKYCResponse;
import com.mfs.api_middleware.dto.TblRegistrationStatus;
import com.mfs.api_middleware.service.RefreshEKYCService;
import com.mfs.api_middleware.service.TblRegistrationStatusService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

@Slf4j
@RestController
public class RefreshEKYCController {

    @Autowired
    RefreshEKYCService refreshEKYCService;

    @Autowired
    TblRegistrationStatusService tblRegistrationStatusService;

    @ResponseBody
    @PostMapping(path = "/refresh_ekyc", produces = "application/json")
    public RefreshEKYCResponse refreshEkyc(HttpServletRequest httpServletRequest, Locale locale) throws Exception {
        long startTime = System.currentTimeMillis();
        String msisdn = httpServletRequest.getHeader("msisdn");
        RefreshEKYCResponse refreshEKYCResponse = refreshEKYCService.refreshStatus(httpServletRequest.getHeader("msisdn"), locale);
        log.info("Refresh eKYC for user: {}, Elapsed time: {}", msisdn, (System.currentTimeMillis() - startTime));
        return refreshEKYCResponse;
    }

    @ResponseBody
    @PostMapping(path = "/get_ekyc_from_tbl", produces = "application/json")
    public TblRegistrationStatus getKyc(HttpServletRequest httpServletRequest, Locale locale) throws Exception {
        String msisdn = httpServletRequest.getHeader("msisdn");
        TblRegistrationStatus tblRegistrationStatus = tblRegistrationStatusService.getKycFromTbl(msisdn, locale);
        return tblRegistrationStatus;
    }
}
