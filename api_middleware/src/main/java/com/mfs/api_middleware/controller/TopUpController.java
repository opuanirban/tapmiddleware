package com.mfs.api_middleware.controller;

import com.mfs.api_middleware.dto.CommonResponse;
import com.mfs.api_middleware.dto.TopUpBundleRequest;
import com.mfs.api_middleware.dto.TopUpRequest;
import com.mfs.api_middleware.gateway.topup.BundlePlanMobileDTO;
import com.mfs.api_middleware.service.TopUpService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;
import java.util.Locale;

@Slf4j
@RestController
public class TopUpController {

    @Autowired
    TopUpService topUpService;

    @ResponseBody
    @PostMapping(path = "/top_up", produces = "application/json")
    public CommonResponse topup(@Valid @RequestBody TopUpRequest topUpRequest, HttpServletRequest httpServletRequest, Locale locale) {
        long requestTime = System.currentTimeMillis();
        log.info("Recharge request received: {}", topUpRequest);
        CommonResponse commonResponse = topUpService.sendTopUp(topUpRequest,httpServletRequest.getHeader("msisdn"),locale);
        long elapsedTime = System.currentTimeMillis() - requestTime;
        log.info("Recharge response: {} with elapsed time: {}", commonResponse, elapsedTime);
        return commonResponse;
    }

    @ResponseBody
    @PostMapping(path = "/bundle", produces = "application/json")
    public List<BundlePlanMobileDTO> bundleSearch(@Valid @RequestBody TopUpBundleRequest input) {
        log.debug("bundle request: {}", input);
        List<BundlePlanMobileDTO> output = topUpService.bundleSearch(input);
        log.debug("bundle response: {}", output);
        return output;
    }

}
