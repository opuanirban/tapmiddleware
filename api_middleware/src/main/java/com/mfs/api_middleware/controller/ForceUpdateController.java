package com.mfs.api_middleware.controller;

import com.mfs.api_middleware.dto.CommonResponse;
import com.mfs.api_middleware.dto.ForceUpdateRequest;
import com.mfs.api_middleware.dto.ForceUpdateResponse;
import com.mfs.api_middleware.dto.UserTypeResponseWallet;
import com.mfs.api_middleware.service.ForceUpdateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Locale;

@RestController
public class ForceUpdateController {
    @Autowired
    ForceUpdateService forceUpdateService;

    @PostMapping(path = "/force_update_blacklist", produces = "application/json")
    public CommonResponse setForceUpdateVersion(@RequestBody ForceUpdateRequest forceUpdateRequest, Locale locale){
        return forceUpdateService.setForceUpdateVersion(forceUpdateRequest,locale);
    }

    @ResponseBody
    @GetMapping(path = "/check_forceupdate_version", produces = "application/json")
    public ForceUpdateResponse checkForceUpdateVersion(@Valid @NotNull @RequestParam("platform") String platform, Locale locale) {
        return forceUpdateService.checkForceUpdateVersion(platform,locale);
    }
}
