package com.mfs.api_middleware.Exception;

public class DecodePinException extends RuntimeException {
    public DecodePinException(String message) {
        super(message);
    }

    public DecodePinException(String message, Throwable cause) {
        super(message, cause);
    }
}
