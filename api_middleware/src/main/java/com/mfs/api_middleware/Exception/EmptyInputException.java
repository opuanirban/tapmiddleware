package com.mfs.api_middleware.Exception;

public class EmptyInputException extends RuntimeException {

    public EmptyInputException(String message) {
        super(message);
    }

    public EmptyInputException(String message, Throwable cause) {
        super(message, cause);
    }
}
