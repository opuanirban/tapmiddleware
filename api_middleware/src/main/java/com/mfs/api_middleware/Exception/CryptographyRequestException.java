package com.mfs.api_middleware.Exception;

public class CryptographyRequestException extends RuntimeException {

    public CryptographyRequestException(String message) {
        super(message);
    }

    public CryptographyRequestException(String message, Throwable cause) {
        super(message, cause);
    }
}
