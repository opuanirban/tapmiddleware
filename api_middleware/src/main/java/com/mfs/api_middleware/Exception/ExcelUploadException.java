package com.mfs.api_middleware.Exception;

public class ExcelUploadException extends RuntimeException {

    public ExcelUploadException(String message) {
        super(message);
    }

    public ExcelUploadException(String message, Throwable cause) {
        super(message, cause);
    }
}
