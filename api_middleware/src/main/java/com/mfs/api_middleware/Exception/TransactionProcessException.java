package com.mfs.api_middleware.Exception;

public class TransactionProcessException extends RuntimeException {

    public TransactionProcessException(String message) {
        super(message);
    }

    public TransactionProcessException(String message, Throwable cause) {
        super(message, cause);
    }
}
