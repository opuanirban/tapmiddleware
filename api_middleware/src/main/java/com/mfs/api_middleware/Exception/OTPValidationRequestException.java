package com.mfs.api_middleware.Exception;

public class OTPValidationRequestException extends RuntimeException {

    public OTPValidationRequestException(String message) {
        super(message);
    }

    public OTPValidationRequestException(String message, Throwable cause) {
        super(message, cause);
    }
}
