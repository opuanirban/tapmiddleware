package com.mfs.api_middleware.Exception;

public class UserRegistrationRequestException extends RuntimeException {

    public UserRegistrationRequestException(String message) {
        super(message);
    }

    public UserRegistrationRequestException(String message, Throwable cause) {
        super(message, cause);
    }
}
