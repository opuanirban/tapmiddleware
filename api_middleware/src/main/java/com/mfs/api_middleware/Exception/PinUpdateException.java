package com.mfs.api_middleware.Exception;

public class PinUpdateException extends RuntimeException {

    public PinUpdateException(String message) {
        super(message);
    }

    public PinUpdateException(String message, Throwable cause) {
        super(message, cause);
    }
}
