package com.mfs.api_middleware.Exception;

import com.mfs.api_middleware.util.CommonConstant;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Locale;

@ControllerAdvice
public class CustomExceptionHandler {

    @ExceptionHandler(value = ApiManagerRequestException.class)
    public ResponseEntity<Object> handleApiRequestException(ApiManagerRequestException e, Locale locale) {
        String error_message = null;
        if (e.getMessage() == null) {
            if (locale.getLanguage().equalsIgnoreCase("bn")) {
                error_message = CommonConstant.COMMON_ERROR_BN;
            } else {
                error_message = CommonConstant.COMMON_ERROR;
            }
        } else {
            error_message = e.getMessage();
        }
        CustomException customException = new CustomException(
                CommonConstant.API_MANGER_REQUEST_FAIL,
                error_message
        );
        return new ResponseEntity<>(customException, org.springframework.http.HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = DecodePinException.class)
    public ResponseEntity<Object> handleApiRequestException(DecodePinException e, Locale locale) {
        String error_message = null;
        if (e.getMessage() == null) {
            if (locale.getLanguage().equalsIgnoreCase("bn")) {
                error_message = CommonConstant.COMMON_ERROR_BN;
            } else {
                error_message = CommonConstant.COMMON_ERROR;
            }
        } else {
            error_message = e.getMessage();
        }
        CustomException customException = new CustomException(
                CommonConstant.PIN_DECODE_EXCEPTION,
                error_message
        );
        return new ResponseEntity<>(customException, org.springframework.http.HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = OTPValidationRequestException.class)
    public ResponseEntity<Object> handleApiRequestException(OTPValidationRequestException e, Locale locale) {
        String error_message = null;
        if (e.getMessage() == null) {
            if (locale.getLanguage().equalsIgnoreCase("bn")) {
                error_message = CommonConstant.COMMON_ERROR_BN;
            } else {
                error_message = CommonConstant.COMMON_ERROR;
            }
        } else {
            error_message = e.getMessage();
        }
        CustomException customException = new CustomException(
                CommonConstant.OTP_VALIDATION_FAILED,
                error_message
        );
        return new ResponseEntity<>(customException, org.springframework.http.HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = CryptographyRequestException.class)
    public ResponseEntity<Object> handleApiRequestException(CryptographyRequestException e) {
        CustomException customException = new CustomException(
                CommonConstant.CRYPTOGRAPHY_OPERATION_FAILED,
                e.getMessage()
        );
        return new ResponseEntity<>(customException, org.springframework.http.HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = PinUpdateException.class)
    public ResponseEntity<Object> handleApiRequestException(PinUpdateException e) {
        CustomException customException = new CustomException(
                CommonConstant.PIN_UPDATE_EXCEPTION,
                e.getMessage()
        );
        return new ResponseEntity<>(customException, org.springframework.http.HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = GigatechException.class)
    public ResponseEntity<Object> handleApiRequestException(GigatechException e) {
        CustomException customException = new CustomException(
                CommonConstant.GIGATECH_EXCEPTION,
                e.getMessage()
        );
        return new ResponseEntity<>(customException, org.springframework.http.HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = UserRegistrationRequestException.class)
    public ResponseEntity<Object> handleApiRequestException(UserRegistrationRequestException e, Locale locale) {
        String error_message = null;
        if (e.getMessage() == null) {
            if (locale.getLanguage().equalsIgnoreCase("bn")) {
                error_message = CommonConstant.COMMON_ERROR_BN;
            } else {
                error_message = CommonConstant.COMMON_ERROR;
            }
        } else {
            error_message = e.getMessage();
        }
        CustomException customException = new CustomException(
                CommonConstant.USER_REGISTRATION_FAILED,
                error_message
        );
        return new ResponseEntity<>(customException, org.springframework.http.HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = TransactionProcessException.class)
    public ResponseEntity<Object> handleApiRequestException(TransactionProcessException e) {
        CustomException customException = new CustomException(
                CommonConstant.TRANSACTION_ERROR,
                e.getMessage()
        );
        return new ResponseEntity<>(customException, org.springframework.http.HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = InvalidTransactionException.class)
    public ResponseEntity<Object> handleApiRequestException(InvalidTransactionException e) {
        CustomException customException = new CustomException(
                CommonConstant.INVALID_TRANSACTION,
                e.getMessage()
        );
        return new ResponseEntity<>(customException, org.springframework.http.HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = ExcelUploadException.class)
    public ResponseEntity<Object> handleApiRequestException(ExcelUploadException e) {
        CustomException customException = new CustomException(
                CommonConstant.EXCEL_ERROR,
                e.getMessage()
        );
        return new ResponseEntity<>(customException, org.springframework.http.HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = EmptyInputException.class)
    public ResponseEntity<Object> handleApiRequestException(EmptyInputException e) {
        CustomException customException = new CustomException(
                CommonConstant.EMPTY_INPUT,
                e.getMessage()
        );
        return new ResponseEntity<>(customException, org.springframework.http.HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = CommonException.class)
    public ResponseEntity<Object> handleApiRequestException(CommonException e, Locale locale) {
        String error_message = null;
        if (e.getMessage() == null) {
            if (locale.getLanguage().equalsIgnoreCase("bn")) {
                error_message = CommonConstant.COMMON_ERROR_BN;
            } else {
                error_message = CommonConstant.COMMON_ERROR;
            }
        } else {
            error_message = e.getMessage();
        }
        CustomException customException = new CustomException(
                CommonConstant.COMMON_ERROR_CODE,
                error_message
        );
        return new ResponseEntity<>(customException, org.springframework.http.HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = ApiRequestException.class)
    public ResponseEntity<Object> handleApiRequestException(ApiRequestException e) {
        //1.Create payload containing exception details
        ApiException apiException = new ApiException(
                e.getMessage(),
                ZonedDateTime.now(ZoneId.of("Z"))
        );
        return new ResponseEntity<>(apiException, org.springframework.http.HttpStatus.BAD_REQUEST);
    }

}
