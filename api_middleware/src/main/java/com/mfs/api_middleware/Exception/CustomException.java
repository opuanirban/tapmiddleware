package com.mfs.api_middleware.Exception;


import lombok.Data;

@Data
public class CustomException {
    private final String code;
    private final String message;
}
