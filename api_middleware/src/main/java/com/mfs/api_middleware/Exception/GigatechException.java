package com.mfs.api_middleware.Exception;

public class GigatechException extends RuntimeException {

    public GigatechException(String message) {
        super(message);
    }

    public GigatechException(String message, Throwable cause) {
        super(message, cause);
    }
}
