package com.mfs.api_middleware.Exception;

public class ApiManagerRequestException extends RuntimeException {

    public ApiManagerRequestException(String message) {
        super(message);
    }

    public ApiManagerRequestException(String message, Throwable cause) {
        super(message, cause);
    }
}
