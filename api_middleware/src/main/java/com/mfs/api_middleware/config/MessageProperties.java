package com.mfs.api_middleware.config;

import lombok.Data;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotEmpty;

@Component
@Data
public class MessageProperties {

    @NotEmpty(message = "{kycfail1.text}")
    public String kycfail1;
}
