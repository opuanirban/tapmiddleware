package com.mfs.api_middleware.enumeration;

public enum PlatformTypeEnum {
    IOS,
    ANDROID,
    HUAWEI
}
