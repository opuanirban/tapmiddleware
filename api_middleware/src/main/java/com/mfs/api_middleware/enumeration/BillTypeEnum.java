package com.mfs.api_middleware.enumeration;

public enum BillTypeEnum {
    INSURANCE("data/insurance"),
    UTILITY("data/utilities"),
    BILLTYPE("data/billpayment"),
    INSTITUTION("data/Institutions"),
    OTHERS("data/others");

    private String bill;

    BillTypeEnum(String billType) {
        this.bill = billType;
    }

    public String getBill() {
        return bill;
    }
}
