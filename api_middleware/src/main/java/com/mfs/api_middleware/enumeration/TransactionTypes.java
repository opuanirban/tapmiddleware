package com.mfs.api_middleware.enumeration;

public enum TransactionTypes {

    P2P("PAY#10#25000"),
    MERCHANT_PAYMENT("MPAY#1#-1"),
    CASH_OUT("CO#50#25000"),
    ADD_MONEY_TBL("FCDP#20#30000"),
    TBL_ACCOUNT_DEPOSIT("FCCO#50#25000"),
    TOP_UP_PREPAID("TopUp#10#1000"),
    TOP_UP_POSTPAID("TopUp#10#5000"),
    ADD_MONEY_CREDIT_CARD("FCDC#100#20000"),
    INSURANCE_PAYMENT("INS#1#-1");

    private String transactionType;

    TransactionTypes(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getTransactionType() {
        return transactionType;
    }
}
