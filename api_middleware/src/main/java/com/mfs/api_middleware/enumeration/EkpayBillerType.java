package com.mfs.api_middleware.enumeration;

public enum EkpayBillerType {
    WZPDCE("wzpdce"),
    WASADE("wasade"),
    WASAKE("wasake"),
    BAKHRE("bakhre");

    private String billerType;

    EkpayBillerType(String billerType) {
        this.billerType = billerType;
    }

    public String getBillerType() {
        return billerType;
    }
}
