package com.mfs.api_middleware.enumeration;

public enum MobileOperatorEnum {
    Airtel, Robi, GrameenPhone, Teletalk, Banglalink
}
