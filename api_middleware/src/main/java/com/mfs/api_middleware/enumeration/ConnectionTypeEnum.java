package com.mfs.api_middleware.enumeration;

public enum ConnectionTypeEnum {

    Prepaid("PR"),
    Postpaid("PO"),
    Skitto("Skitto");

    private String connection;

    ConnectionTypeEnum(String con) {
        this.connection = con;
    }

    public String getConnection() {
        return connection;
    }

}