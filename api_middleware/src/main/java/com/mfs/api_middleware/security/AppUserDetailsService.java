package com.mfs.api_middleware.security;

import com.mfs.api_middleware.gateway.ApiManagerGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class AppUserDetailsService implements UserDetailsService {

    @Autowired
    ApiManagerGateway apiManagerGateway;


    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        String pin = apiManagerGateway.userRefresh(s);
        return new User(s, pin, new ArrayList<>());
    }

}