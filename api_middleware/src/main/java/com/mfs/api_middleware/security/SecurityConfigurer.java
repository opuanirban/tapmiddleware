package com.mfs.api_middleware.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(
        // securedEnabled = true,
        // jsr250Enabled = true,
        prePostEnabled = true)
public class SecurityConfigurer extends WebSecurityConfigurerAdapter {
    @Autowired
    private AppUserDetailsService appUserDetailService;
    @Autowired
    private JwtRequestFilter jwtRequestFilter;
    @Autowired
    private AuthEntryPointJwt unauthorizedHandler;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(appUserDetailService);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
//        http.csrf().disable().authorizeRequests()
//
//                .antMatchers("/login").permitAll()
//                .antMatchers("/actuator/*").permitAll()
//                .antMatchers("/user_registration").permitAll()
//                .antMatchers("/generate-otp").permitAll()
//                .antMatchers("/validate-otp").permitAll()
//                .antMatchers("/checkUser").permitAll()
//                .antMatchers("/userRefresh").permitAll()
//                .antMatchers("/encrypt").permitAll()
//                .antMatchers("/decrypt").permitAll()
//                .anyRequest().authenticated()
//                .and().sessionManagement()
//                .sessionCreationPolicy(SessionCreationPolicy.STATELESS);
//        http.addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter.class);


        http.cors().and().csrf().disable()
                .exceptionHandling().authenticationEntryPoint(unauthorizedHandler).and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
                .authorizeRequests().antMatchers("/login").permitAll()
                .antMatchers("/actuator/*").permitAll()
                .antMatchers("/user_registration").permitAll()
                .antMatchers("/user_registration_kyc").permitAll()
                .antMatchers("/generate-otp").permitAll()
                .antMatchers("/validate-otp").permitAll()
                .antMatchers("/checkUser").permitAll()
                .antMatchers("/userRefresh").permitAll()
                .antMatchers("/encrypt").permitAll()
                .antMatchers("/decrypt").permitAll()
                .antMatchers("/nidUpload").permitAll()
                .antMatchers("/kycRegistration").permitAll()
                .antMatchers("/verifyStatus").permitAll()
                .antMatchers("/actuator/health").permitAll()
                .antMatchers("/user_delete").permitAll()
                .antMatchers("/test").permitAll()
                .antMatchers("/ekyc_notification").permitAll()
                .antMatchers("/uploadErrorMessage").permitAll()
                .antMatchers("/async_registration").permitAll()
                .antMatchers("/ekyc_verify").permitAll()
                .antMatchers("/ekyc_notification").permitAll()
                .antMatchers("/force_update_blacklist").permitAll()
                .antMatchers("/check_forceupdate_version").permitAll()
                .antMatchers("/get-service-config").permitAll()
                .antMatchers("/merchant-qr").permitAll()
                .antMatchers("/feature-hide-config").permitAll()
                .antMatchers("/modify-donation-merchant").permitAll()
                .antMatchers("/getDonations").permitAll()
                .antMatchers("/set-service-availibility").permitAll()
                .antMatchers("/validate-token").permitAll()
                .antMatchers("/customer_registration").permitAll()
                .antMatchers("/getBanners").permitAll()
                .antMatchers("/getUtility").permitAll()
                .antMatchers("/getUtility/v2").permitAll()
                .antMatchers("/getFees").permitAll()
                .antMatchers("/getFees/v2").permitAll()
                .antMatchers("/getTickets").permitAll()
                .anyRequest().authenticated();

        http.addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter.class);
    }

    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return NoOpPasswordEncoder.getInstance();
    }
}