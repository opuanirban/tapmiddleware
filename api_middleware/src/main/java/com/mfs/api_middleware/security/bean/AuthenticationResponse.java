package com.mfs.api_middleware.security.bean;

import com.mfs.api_middleware.dto.UserLoginInfo;
import lombok.Data;

@Data
public class AuthenticationResponse {

    private String jwt;
    private Integer timeout;
    private UserLoginInfo userLoginInfo;

}
