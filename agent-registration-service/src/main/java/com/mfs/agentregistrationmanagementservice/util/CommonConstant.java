package com.mfs.agentregistrationmanagementservice.util;

public class CommonConstant {
    public static final String HTTP_4XX_ERROR = "Invalid request, Unable to process at this moment. Please try after sometime";
    public static final String HTTP_5XX_ERROR = "Internal processing error, Unable to process at this moment. Please try after sometime.";
    public static final String TIMEOUT_ERROR = "Request processing failed due to system delay. Please try after sometime.";
    public static final String COMMON_ERROR = "Request processing failed. Please try again";
}
