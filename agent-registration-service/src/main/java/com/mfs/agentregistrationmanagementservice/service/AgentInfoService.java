package com.mfs.agentregistrationmanagementservice.service;

import com.mfs.agentregistrationmanagementservice.model.AgentInfoDto;
import com.mfs.agentregistrationmanagementservice.model.AgentStatusResponse;
import com.mfs.agentregistrationmanagementservice.model.AgentVerificationRequest;

public interface AgentInfoService {
    void saveAgentInfo(AgentInfoDto agentInfo);
    void modifyAgentVerificationStatus(AgentVerificationRequest agentVerificationRequest);
    AgentStatusResponse fetchAgentVerificationStatus(String agentNumber);
}
