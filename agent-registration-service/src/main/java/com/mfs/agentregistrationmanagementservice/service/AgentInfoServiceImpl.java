package com.mfs.agentregistrationmanagementservice.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.mfs.agentregistrationmanagementservice.domain.AgentInfo;
import com.mfs.agentregistrationmanagementservice.domain.NIDDataInfo;
import com.mfs.agentregistrationmanagementservice.domain.ShopInfo;
import com.mfs.agentregistrationmanagementservice.exception.ApiRequestException;
import com.mfs.agentregistrationmanagementservice.gateway.ApiManagerGateway;
import com.mfs.agentregistrationmanagementservice.gateway.model.AgentDocumentItem;
import com.mfs.agentregistrationmanagementservice.gateway.model.AgentRegistrationParams;
import com.mfs.agentregistrationmanagementservice.model.AgentInfoDto;
import com.mfs.agentregistrationmanagementservice.model.AgentStatusResponse;
import com.mfs.agentregistrationmanagementservice.model.AgentVerificationRequest;
import com.mfs.agentregistrationmanagementservice.model.enumeration.AgentFileTypeEnum;
import com.mfs.agentregistrationmanagementservice.model.enumeration.AgentRegistrationStatusEnum;
import com.mfs.agentregistrationmanagementservice.model.enumeration.VerificationStatusEnum;
import com.mfs.agentregistrationmanagementservice.repository.AgentInfoRepository;
import com.mfs.agentregistrationmanagementservice.repository.NIDDataInfoRepository;
import com.mfs.agentregistrationmanagementservice.repository.ShopInfoRepository;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AgentInfoServiceImpl implements  AgentInfoService{
    @Autowired
    AgentInfoRepository agentInfoRepository;
    @Autowired
    NIDDataInfoRepository nidDataInfoRepository;
    @Autowired
    ShopInfoRepository shopInfoRepository;
    @Autowired
    ApiManagerGateway apiManagerGateway;

    @Value("${application.image.location}")
    private String imageLocation;

    private static final String HASH_SALT = "Tadl@1234a";

    @Override
    @Transactional
    public void saveAgentInfo(AgentInfoDto agentInfo) {
        Optional<AgentInfo> agentInfoFetch = agentInfoRepository.findFirstByAgentWalletNumberOrderByAgentRequestTimeDesc(
                agentInfo.getAgentNumber());
        if (agentInfoFetch.isPresent()) {
            if(agentInfoFetch.get().getAgentRegistrationStatus().equals(AgentRegistrationStatusEnum.PENDING.name())) {
                throw new ApiRequestException("Agent number already has pending verification.");
            } else if(agentInfoFetch.get().getAgentRegistrationStatus().equals(AgentRegistrationStatusEnum.SUCCESS.name())) {
                throw new ApiRequestException("Agent number already completed verification.");
            }
        }

        AgentInfo agentInfoEntity = new AgentInfo();
        agentInfoEntity.setKycVerificationStatus(VerificationStatusEnum.PENDING.name());
        agentInfoEntity.setTinCertificateVerificationStatus(VerificationStatusEnum.PENDING.name());
        agentInfoEntity.setTradeLicenseVerificationStatus(VerificationStatusEnum.PENDING.name());
        agentInfoEntity.setShopVerificationStatus(VerificationStatusEnum.PENDING.name());
        agentInfoEntity.setAgentGender(agentInfo.getAgentGender());
        agentInfoEntity.setAgentNominee(agentInfo.getAgentNominee());
        agentInfoEntity.setAgentNomineeRelation(agentInfo.getAgentNomineeRelation());
        agentInfoEntity.setAgentFirstName(agentInfo.getAgentFirstName());
        agentInfoEntity.setAgentLastName(agentInfo.getAgentLastName());
        agentInfoEntity.setAgentOccupation(agentInfo.getAgentOccupation());
        agentInfoEntity.setAgentWalletPin(agentInfo.getAgentWalletPin());
        agentInfoEntity.setAgentWalletPinHash(get_SHA_512_SecurePassword(agentInfo.getAgentWalletPin(), HASH_SALT));
        agentInfoEntity.setAgentOccupation(agentInfo.getAgentOccupation());
        agentInfoEntity.setAgentOperatorName(agentInfo.getAgentOperatorName());
        agentInfoEntity.setAgentTradeLicenseNumber(agentInfo.getAgentTradeLicenseNumber());
        agentInfoEntity.setAgentTinNumber(agentInfo.getAgentTinNumber());

        NIDDataInfo nidDataInfo = new NIDDataInfo();
        nidDataInfo.setNidNumber(agentInfo.getAgentNidInfoDto().getAgentNidNumber());
        nidDataInfo.setNidDateOfBirth(agentInfo.getAgentNidInfoDto().getAgentNidDob());
        nidDataInfo.setNidNameInEnglish(agentInfo.getAgentNidInfoDto().getAgentNidNameEn());
        nidDataInfo.setNidNameInBangla(agentInfo.getAgentNidInfoDto().getAgentNidNameBn());
        nidDataInfo.setNidFatherName(agentInfo.getAgentNidInfoDto().getAgentNidFatherName());
        nidDataInfo.setNidMotherName(agentInfo.getAgentNidInfoDto().getAgentNidMotherName());
        nidDataInfo.setNidSpouseName(agentInfo.getAgentNidInfoDto().getAgentNidSpouseName());
        nidDataInfo.setNidPresentAddress(agentInfo.getAgentNidInfoDto().getAgentNidPresentAddress());
        nidDataInfo.setNidFrontImage(agentInfo.getAgentNidInfoDto().getAgentNidFrontImageName());
        nidDataInfo.setNidBackImage(agentInfo.getAgentNidInfoDto().getAgentNidBackImageName());

        ShopInfo shopInfo = new ShopInfo();
        shopInfo.setShopName(agentInfo.getAgentShopInfo().getShopName());
        shopInfo.setShopDivision(agentInfo.getAgentShopInfo().getDivision());
        shopInfo.setShopDistrict(agentInfo.getAgentShopInfo().getDistrict());
        shopInfo.setShopType(agentInfo.getAgentShopInfo().getShopType());
        shopInfo.setShopUnion(agentInfo.getAgentShopInfo().getUnion());
        shopInfo.setShopUpazila(agentInfo.getAgentShopInfo().getUpazila());

        try {
            agentInfoEntity.setTradeLicenseImage(saveFileResource(agentInfo.getTradeLicense(), AgentFileTypeEnum.TRADE_LICENSE, agentInfo.getAgentNumber()));
            agentInfoEntity.setTinCertificateImage(saveFileResource(agentInfo.getTinCertificate(), AgentFileTypeEnum.TIN_CERTIFICATE, agentInfo.getAgentNumber()));
            agentInfoEntity.setAgentWalletNumber(agentInfo.getAgentNumber());
            agentInfoEntity.setAgentRegistrationStatus(AgentRegistrationStatusEnum.PENDING.name());
            agentInfoEntity.setAgentSelfieImage(saveFileResource(agentInfo.getAgentSelfie(), AgentFileTypeEnum.AGENT_SELFIE, agentInfo.getAgentNumber()));
            shopInfo.setShopPhoto(saveFileResource(agentInfo.getTinCertificate(), AgentFileTypeEnum.AGENT_SHOP, agentInfo.getAgentNumber()));
            agentInfoEntity.setAgentRequestTime(new Date());
            agentInfoEntity.setNidDataInfo(nidDataInfo);
            agentInfoEntity.setShopInfo(shopInfo);

            agentInfoRepository.save(agentInfoEntity);
        } catch (IOException e) {
            e.printStackTrace();
            log.error("error in file save: {}", e.getMessage());
        }
    }

    @Override
    public void modifyAgentVerificationStatus(AgentVerificationRequest agentVerificationRequest) {
        Optional<AgentInfo> agentInfo= agentInfoRepository.findFirstByAgentWalletNumberOrderByAgentRequestTimeDesc(
                agentVerificationRequest.getAgentNumber());
        
        if (!(agentInfo.isPresent() && agentInfo.get().getAgentRegistrationStatus().equals(AgentRegistrationStatusEnum.PENDING.name()))) {
            throw new ApiRequestException("Agent number is not in valid state to do verification.");
        }

        AgentInfo agentInfoDatabase = agentInfo.get();
        if(agentVerificationRequest.getNidVerificationStatus() != null){
            agentInfoDatabase.setKycVerificationStatus(agentVerificationRequest.getNidVerificationStatus());
            if(agentVerificationRequest.getNidVerificationStatus().equals(VerificationStatusEnum.FAILED.name())) {
                agentInfoDatabase.setAgentRegistrationStatus(AgentRegistrationStatusEnum.FAILED.name());
                agentInfoRepository.save(agentInfoDatabase);
                return;
            }
        } 
        if(agentVerificationRequest.getTinCertificateVerificationStatus() != null){
            agentInfoDatabase.setTinCertificateVerificationStatus(agentVerificationRequest.getTinCertificateVerificationStatus());
            if(agentVerificationRequest.getTinCertificateVerificationStatus().equals(VerificationStatusEnum.FAILED.name())) {
                agentInfoDatabase.setAgentRegistrationStatus(AgentRegistrationStatusEnum.FAILED.name());
                agentInfoRepository.save(agentInfoDatabase);
                return;
            }
        }
        if(agentVerificationRequest.getTradeLicenseVerificationStatus() != null){
            agentInfoDatabase.setTradeLicenseVerificationStatus(agentVerificationRequest.getTradeLicenseVerificationStatus());
            if(agentVerificationRequest.getTradeLicenseVerificationStatus().equals(VerificationStatusEnum.FAILED.name())) {
                agentInfoDatabase.setAgentRegistrationStatus(AgentRegistrationStatusEnum.FAILED.name());
                agentInfoRepository.save(agentInfoDatabase);
                return;
            }
        }
        if(agentVerificationRequest.getShopVerificationStatus() != null){
            agentInfoDatabase.setShopVerificationStatus(agentVerificationRequest.getShopVerificationStatus());
            if(agentVerificationRequest.getShopVerificationStatus().equals(VerificationStatusEnum.FAILED.name())) {
                agentInfoDatabase.setAgentRegistrationStatus(AgentRegistrationStatusEnum.FAILED.name());
                agentInfoRepository.save(agentInfoDatabase);
                return;
            }
        }

        if(agentInfoDatabase.getKycVerificationStatus().equals(VerificationStatusEnum.SUCCESS.name()) &&
            agentInfoDatabase.getTinCertificateVerificationStatus().equalsIgnoreCase(VerificationStatusEnum.SUCCESS.name()) &&
            agentInfoDatabase.getTradeLicenseVerificationStatus().equalsIgnoreCase(VerificationStatusEnum.SUCCESS.name()) &&
            agentInfoDatabase.getShopVerificationStatus().equalsIgnoreCase(VerificationStatusEnum.SUCCESS.name())) {
            String response = apiManagerGateway.userRegistration(populateAgentRegistrationParam(agentInfoDatabase));    
            if(response.contains("created successfully")) {
                agentInfoDatabase.setAgentRegistrationStatus(AgentRegistrationStatusEnum.SUCCESS.name());
            } else {
                throw new ApiRequestException(response);
            }
        }
        agentInfoRepository.save(agentInfoDatabase);
    }

    private String saveFileResource(MultipartFile content, AgentFileTypeEnum fileTypeEnum, String agentNumber) throws IOException {
        String imageName = agentNumber + "_" + fileTypeEnum.name() + "_" + String.valueOf(new Date().getTime()) + "." + FilenameUtils.getExtension(content.getOriginalFilename());
        File file = new File(imageLocation + File.separator + imageName);
        try (OutputStream os = new FileOutputStream(file)) {
            os.write(content.getBytes());
        }
        return imageName;
    }

    private String get_SHA_512_SecurePassword(String passwordToHash, String salt){
        String generatedPassword = null;
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-512");
            md.update(salt.getBytes(StandardCharsets.UTF_8));
            byte[] bytes = md.digest(passwordToHash.getBytes(StandardCharsets.UTF_8));
            StringBuilder sb = new StringBuilder();
            for(int i=0; i< bytes.length ;i++){
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            generatedPassword = sb.toString();
        } catch (NoSuchAlgorithmException e) {
            throw new ApiRequestException("Error generating pin hash");
        }
        return generatedPassword;
    }

    @Override
    public AgentStatusResponse fetchAgentVerificationStatus(String agentNumber) {
        AgentStatusResponse agentStatusResponse = new AgentStatusResponse();
        Optional<AgentInfo> agentInfo= agentInfoRepository.findFirstByAgentWalletNumberOrderByAgentRequestTimeDesc(agentNumber);
        if(!agentInfo.isPresent()) {
            agentStatusResponse.setAgentVerificationStatus(AgentRegistrationStatusEnum.NOTFOUND.name());
        } else {
            agentStatusResponse.setAgentVerificationStatus(agentInfo.get().getAgentRegistrationStatus());
        }
        return agentStatusResponse;
    }

    private AgentRegistrationParams populateAgentRegistrationParam(AgentInfo agentInfo) {
        AgentRegistrationParams agentRegistrationParams = new AgentRegistrationParams();
        agentRegistrationParams.setAccountPin("1234");
        agentRegistrationParams.setAgreementsigned(Boolean.TRUE);
        agentRegistrationParams.setBanglaName(agentInfo.getNidDataInfo().getNidNameInBangla());
        agentRegistrationParams.setBusinessOwnerName(agentInfo.getShopInfo().getShopName());
        agentRegistrationParams.setBusinesspremisesinspected(Boolean.TRUE);
        agentRegistrationParams.setBusinesstypeconfirmed(Boolean.TRUE);
        agentRegistrationParams.setDob(agentInfo.getNidDataInfo().getNidDateOfBirth());
        agentRegistrationParams.setDocumentcollected(Boolean.TRUE);
        agentRegistrationParams.setFatherName(agentInfo.getNidDataInfo().getNidFatherName());
        agentRegistrationParams.setFirstName(agentInfo.getAgentFirstName());
        agentRegistrationParams.setIsLimitedKyc(Boolean.FALSE);
        agentRegistrationParams.setLastName(agentInfo.getAgentLastName());
        agentRegistrationParams.setMotherName(agentInfo.getNidDataInfo().getNidMotherName());
        agentRegistrationParams.setNationalIdNumber(agentInfo.getNidDataInfo().getNidNumber());
        agentRegistrationParams.setNominee(agentInfo.getAgentNominee());
        agentRegistrationParams.setNomineeRelation(agentInfo.getAgentNomineeRelation());
        agentRegistrationParams.setOccupation(agentInfo.getAgentOccupation());
        agentRegistrationParams.setOperatorName(agentInfo.getAgentOperatorName());
        agentRegistrationParams.setPermanentAddress(agentInfo.getNidDataInfo().getNidPresentAddress());
        agentRegistrationParams.setPhotoName(agentInfo.getAgentSelfieImage());
        agentRegistrationParams.setPresentAddress(agentInfo.getNidDataInfo().getNidPresentAddress());
        agentRegistrationParams.setSex(agentInfo.getAgentGender());
        agentRegistrationParams.setSpouseName(agentInfo.getNidDataInfo().getNidSpouseName());
        agentRegistrationParams.setTinNumber(agentInfo.getAgentTinNumber());
        agentRegistrationParams.setTradeLicenseNo(agentInfo.getAgentTradeLicenseNumber());
        agentRegistrationParams.setUserGroupKey("69");
        agentRegistrationParams.setUserNumber("88"+ agentInfo.getAgentWalletNumber());
        agentRegistrationParams.setUserPin(agentInfo.getAgentWalletPin());

        List<AgentDocumentItem> documentList = new ArrayList<>();
        AgentDocumentItem agentDocumentItemTin = new AgentDocumentItem();
        agentDocumentItemTin.setDocumentTitle("TIN Certificate");
        agentDocumentItemTin.setDocumentContentType("Image");
        agentDocumentItemTin.setDocumentExtension(".jpeg");
        agentDocumentItemTin.setDocumentType("image/jpeg");
        agentDocumentItemTin.setDocumentPath(agentInfo.getTinCertificateImage());
        documentList.add(agentDocumentItemTin);
        AgentDocumentItem agentDocumentItemTradeLicense = new AgentDocumentItem();
        agentDocumentItemTradeLicense.setDocumentExtension(".jpeg");
        agentDocumentItemTradeLicense.setDocumentType("image/jpeg");
        agentDocumentItemTradeLicense.setDocumentContentType("Image");
        agentDocumentItemTradeLicense.setDocumentTitle("Trade License");
        agentDocumentItemTradeLicense.setDocumentPath(agentInfo.getTradeLicenseImage());
        documentList.add(agentDocumentItemTradeLicense);
        AgentDocumentItem agentDocumentItemAgentShop = new AgentDocumentItem();
        agentDocumentItemAgentShop.setDocumentExtension(".jpeg");
        agentDocumentItemAgentShop.setDocumentType("image/jpeg");
        agentDocumentItemAgentShop.setDocumentContentType("Image");
        agentDocumentItemAgentShop.setDocumentTitle("Agent Shop");
        agentDocumentItemAgentShop.setDocumentPath(agentInfo.getShopInfo().getShopPhoto());
        documentList.add(agentDocumentItemAgentShop);
        agentRegistrationParams.setDocumentList(documentList);
        
        return agentRegistrationParams;
    } 
}
