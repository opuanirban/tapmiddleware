package com.mfs.agentregistrationmanagementservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AgentRegistrationManagementServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(AgentRegistrationManagementServiceApplication.class, args);
	}

}
