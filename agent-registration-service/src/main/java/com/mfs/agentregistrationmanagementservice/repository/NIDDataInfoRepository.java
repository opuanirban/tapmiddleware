package com.mfs.agentregistrationmanagementservice.repository;

import com.mfs.agentregistrationmanagementservice.domain.NIDDataInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NIDDataInfoRepository extends JpaRepository<NIDDataInfo, Long> {
}
