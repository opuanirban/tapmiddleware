package com.mfs.agentregistrationmanagementservice.repository;

import com.mfs.agentregistrationmanagementservice.domain.AgentInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AgentInfoRepository extends JpaRepository<AgentInfo, Long> {
    Optional<AgentInfo> findByAgentWalletNumberAndAgentRegistrationStatus(String agentWalletNumber, String agentRegistrationStatus);
    Optional<AgentInfo> findFirstByAgentWalletNumberOrderByAgentRequestTimeDesc(String agentWalletNumber);
}
