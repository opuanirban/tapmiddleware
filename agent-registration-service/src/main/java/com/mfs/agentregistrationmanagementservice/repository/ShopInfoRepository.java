package com.mfs.agentregistrationmanagementservice.repository;

import com.mfs.agentregistrationmanagementservice.domain.ShopInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ShopInfoRepository extends JpaRepository<ShopInfo, Long> {
}
