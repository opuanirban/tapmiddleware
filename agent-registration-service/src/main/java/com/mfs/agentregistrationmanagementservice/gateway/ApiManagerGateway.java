package com.mfs.agentregistrationmanagementservice.gateway;

import com.mfs.agentregistrationmanagementservice.exception.ApiRequestException;
import com.mfs.agentregistrationmanagementservice.gateway.model.AgentRegistrationParams;
import com.mfs.agentregistrationmanagementservice.util.CommonConstant;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import java.util.concurrent.TimeoutException;

import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.http.HttpStatus;
import java.time.Duration;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ApiManagerGateway {
    @Value("${application.api-manager-url}")
    private String apiManagerUrl;

    private final WebClient webClient;

    public ApiManagerGateway(WebClient.Builder webClientBuilder) {
        this.webClient = webClientBuilder.build();
    }

    public String userRegistration(AgentRegistrationParams registrationParams) {
        log.debug("agent registration request: {}", registrationParams);
        String response =  webClient.post()
                .uri(apiManagerUrl + "/AgentRegistration")
                .body(BodyInserters.fromValue(registrationParams))
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, clientResponse -> {
                    throw new ApiRequestException(CommonConstant.HTTP_4XX_ERROR);
                })
                .onStatus(HttpStatus::is5xxServerError, clientResponse -> {
                    throw new ApiRequestException(CommonConstant.HTTP_5XX_ERROR);
                })
                .bodyToMono(String.class)
                .timeout(Duration.ofMillis(30000))
                .onTerminateDetach()
                .onErrorMap(throwable -> {
                    log.error("error: {}", throwable);
                    if(throwable instanceof ApiRequestException) {
                        throw new ApiRequestException(throwable.getMessage());
                    } else if (throwable instanceof TimeoutException) {
                        throw new ApiRequestException(CommonConstant.TIMEOUT_ERROR);
                    } else {
                        throw new ApiRequestException(CommonConstant.COMMON_ERROR);
                    }
                })
                .block();

        log.debug("response from api manager: {}", response);
        if (response.startsWith("\"") & response.endsWith("\"")) {
            return response.substring(1, response.length() - 1);
        } else {
            return response;
        }
    }
}
