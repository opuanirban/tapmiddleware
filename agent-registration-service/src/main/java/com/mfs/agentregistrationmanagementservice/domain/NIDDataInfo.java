package com.mfs.agentregistrationmanagementservice.domain;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "nid_data_info")
public class NIDDataInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nid_number")
    private String nidNumber;

    @Column(name = "nid_dob")
    private String nidDateOfBirth;

    @Column(name = "nid_name_eng")
    private String nidNameInEnglish;

    @Column(name = "nid_name_bn")
    private String nidNameInBangla;

    @Column(name = "nid_father_name")
    private String nidFatherName;

    @Column(name = "nid_mother_name")
    private String nidMotherName;

    @Column(name = "nid_spouse_name")
    private String nidSpouseName;

    @Column(name = "nid_present_address")
    private String nidPresentAddress;

    @Column(name = "nid_front_image")
    private String nidFrontImage;

    @Column(name = "nid_back_image")
    private String nidBackImage;

    @OneToOne(mappedBy = "nidDataInfo")
    private AgentInfo agentInfo;

}
