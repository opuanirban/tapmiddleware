package com.mfs.agentregistrationmanagementservice.domain;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "shop_info")
public class ShopInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "shop_name")
    private String shopName;

    @Column(name = "shop_type")
    private String shopType;

    @Column(name = "shop_division")
    private String shopDivision;

    @Column(name = "shop_district")
    private String shopDistrict;

    @Column(name = "shop_upazila")
    private String shopUpazila;

    @Column(name = "shop_union")
    private String shopUnion;

    @Column(name = "shop_photo")
    private String shopPhoto;

    @OneToOne(mappedBy = "shopInfo")
    private AgentInfo agentInfo;

}
