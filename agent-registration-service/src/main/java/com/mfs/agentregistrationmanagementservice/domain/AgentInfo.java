package com.mfs.agentregistrationmanagementservice.domain;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "agent_info")
public class AgentInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "agent_wallet_number")
    private String agentWalletNumber;

    @Column(name = "agent_gender")
    private String agentGender;

    @Column(name = "agent_nominee")
    private String agentNominee;

    @Column(name = "agent_nominee_relation")
    private String agentNomineeRelation;

    @Column(name = "trade_license_image")
    private String tradeLicenseImage;

    @Column(name = "tin_certificate_image")
    private String tinCertificateImage;

    @Column(name = "agent_selfie_image")
    private String agentSelfieImage;

    @Column(name = "kyc_verification_Status")
    private String kycVerificationStatus;

    @Column(name = "trade_license_verification_status")
    private String tradeLicenseVerificationStatus;

    @Column(name = "tin_certificate_verification_status")
    private String tinCertificateVerificationStatus;

    @Column(name = "shop_verification_status")
    private String shopVerificationStatus;

    @Column(name = "agent_registration_status")
    private String agentRegistrationStatus;

    @Column(name = "agent_request_time")
    private Date agentRequestTime;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "shop_id", referencedColumnName = "id")
    private ShopInfo shopInfo;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "nid_data_id", referencedColumnName = "id")
    private NIDDataInfo nidDataInfo;

    @Column(name = "agent_first_name")
    private String agentFirstName;

    @Column(name = "agent_last_name")
    private String agentLastName;

    @Column(name = "agent_wallet_pin")
    private String agentWalletPin;

    @Column(name = "agent_wallet_pin_hash")
    private String agentWalletPinHash;

    @Column(name = "agent_occupation")
    private String agentOccupation;

    @Column(name = "agent_operator_name")
    private String agentOperatorName;

    @Column(name = "agent_tin_number")
    private String agentTinNumber;

    @Column(name = "agent_trade_license_number")
    private String agentTradeLicenseNumber;

}
