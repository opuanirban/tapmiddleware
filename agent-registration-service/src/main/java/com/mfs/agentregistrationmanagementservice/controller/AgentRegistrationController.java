package com.mfs.agentregistrationmanagementservice.controller;

import com.mfs.agentregistrationmanagementservice.model.*;
import com.mfs.agentregistrationmanagementservice.model.enumeration.ShopTypeEnum;
import com.mfs.agentregistrationmanagementservice.service.AgentInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/agent")
public class AgentRegistrationController {

    @Autowired
    AgentInfoService agentInfoService;

    @ResponseBody
    @PostMapping(path = "/registration")
    public void registerAgent(@RequestParam(value = "agent_number") String agentNumber,
                              @RequestParam(value = "nid_no") String nidNo,
                              @RequestParam(value = "nid_dob") String nidDateOfBirth,
                              @RequestParam(value = "nid_name_en") String nidNameEnglish,
                              @RequestParam(value = "nid_name_bn") String nidNameBangla,
                              @RequestParam(value = "nid_father_name") String nidFatherName,
                              @RequestParam(value = "nid_mother_name") String nidMotherName,
                              @RequestParam(value = "nid_spouse_name") String nidSpouseName,
                              @RequestParam(value = "nid_present_address") String nidPresentAddress,
                              @RequestParam(value = "nid_front_image_name") String nidFrontImageName,
                              @RequestParam(value = "nid_back_image_name") String nidBackImageName,
                              @RequestParam(value = "agent_selfie") MultipartFile agentSelfie,
                              @RequestParam(value = "agent_first_name") String agentFirstName,
                              @RequestParam(value = "agent_last_name") String agentLastName,
                              @RequestParam(value = "agent_wallet_pin") String agentWalletPin,
                              @RequestParam(value = "agent_occupation") String agentOccupation,
                              @RequestParam(value = "agent_operator_name") String agentOperatorName,
                              @RequestParam(value = "trade_license") MultipartFile tradeLicense,
                              @RequestParam(value = "tin_certificate") MultipartFile tinCertificate,
                              @RequestParam(value = "agent_shop_image") MultipartFile agentShopImage,
                              @RequestParam(value = "agent_gender") String agentGender,
                              @RequestParam(value = "agent_nominee") String agentNominee,
                              @RequestParam(value = "agent_nominee_relation") String agentNomineeRelation,
                              @RequestParam(value = "shop_name") String shopName,
                              @RequestParam(value = "shop_type") String shopType,
                              @RequestParam(value = "division") String division,
                              @RequestParam(value = "district") String district,
                              @RequestParam(value = "upazila") String upazila,
                              @RequestParam(value = "agent_trade_license_number") String agentTradeLicenseNumber,
                              @RequestParam(value = "agent_tin_number") String agentTinNumber,
                              @RequestParam(value = "union") String union) {
        AgentInfoDto agentInfoDto = new AgentInfoDto();
        agentInfoDto.setAgentNumber(agentNumber);
        agentInfoDto.setTradeLicense(tradeLicense);
        agentInfoDto.setTinCertificate(tinCertificate);
        agentInfoDto.setAgentGender(agentGender);
        agentInfoDto.setAgentNominee(agentNominee);
        agentInfoDto.setAgentNomineeRelation(agentNomineeRelation);
        agentInfoDto.setAgentSelfie(agentSelfie);
        agentInfoDto.setAgentFirstName(agentFirstName);
        agentInfoDto.setAgentLastName(agentLastName);
        agentInfoDto.setAgentWalletPin(agentWalletPin);
        agentInfoDto.setAgentOccupation(agentOccupation);
        agentInfoDto.setAgentOperatorName(agentOperatorName);
        agentInfoDto.setAgentTradeLicenseNumber(agentTradeLicenseNumber);
        agentInfoDto.setAgentTinNumber(agentTinNumber);
        
        // populate NID dto from request
        AgentNidInfoDto agentNidInfoDto = new AgentNidInfoDto();
        agentNidInfoDto.setAgentNidDob(nidDateOfBirth);
        agentNidInfoDto.setAgentNidNameEn(nidNameEnglish);
        agentNidInfoDto.setAgentNidNameBn(nidNameBangla);
        agentNidInfoDto.setAgentNidNumber(nidNo);
        agentNidInfoDto.setAgentNidFatherName(nidFatherName);
        agentNidInfoDto.setAgentNidBackImageName(nidBackImageName);
        agentNidInfoDto.setAgentNidFrontImageName(nidFrontImageName);
        agentNidInfoDto.setAgentNidMotherName(nidMotherName);
        agentNidInfoDto.setAgentNidSpouseName(nidSpouseName);
        agentNidInfoDto.setAgentNidPresentAddress(nidPresentAddress);
        agentInfoDto.setAgentNidInfoDto(agentNidInfoDto);
        // populate shop dto from request
        AgentShopInfoDto agentShopInfoDto = new AgentShopInfoDto();
        agentShopInfoDto.setShopName(shopName);
        agentShopInfoDto.setDistrict(district);
        agentShopInfoDto.setDivision(division);
        agentShopInfoDto.setUpazila(upazila);
        agentShopInfoDto.setUnion(union);
        agentShopInfoDto.setShopType(shopType);
        agentShopInfoDto.setAgentShopImage(agentShopImage);
        agentInfoDto.setAgentShopInfo(agentShopInfoDto);

        agentInfoService.saveAgentInfo(agentInfoDto);
    }

    @ResponseBody
    @PostMapping(path = "/update-verification-status")
    public void updateVerificationStatus(@RequestBody AgentVerificationRequest agentVerificationRequest) {
        agentInfoService.modifyAgentVerificationStatus(agentVerificationRequest);
    }

    @ResponseBody
    @GetMapping(path = "/fetch-verification-status")
    public AgentStatusResponse fetchAgentStatus(@RequestParam(value = "agent_number") String agentNumber) {
        return agentInfoService.fetchAgentVerificationStatus(agentNumber);
    }

}
