package com.mfs.agentregistrationmanagementservice.exception;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.Locale;

@ControllerAdvice
public class CustomExceptionHandler {

    @ExceptionHandler(value = ApiManagerRequestException.class)
    public ResponseEntity<Object> handleApiRequestException(ApiManagerRequestException e, Locale locale) {

        CustomException customException = new CustomException(
                "01_001", e.getMessage()
        );
        return new ResponseEntity<>(customException, org.springframework.http.HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = ApiRequestException.class)
    public ResponseEntity<Object> handleApiRequestException(ApiRequestException e) {
        CustomException customException = new CustomException(
                "01_002", e.getMessage()
        );
        return new ResponseEntity<>(customException, org.springframework.http.HttpStatus.BAD_REQUEST);
    }

}
