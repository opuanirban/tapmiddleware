package com.mfs.agentregistrationmanagementservice.model;

import lombok.Data;

@Data
public class AgentStatusResponse {
    private String agentVerificationStatus;
}
