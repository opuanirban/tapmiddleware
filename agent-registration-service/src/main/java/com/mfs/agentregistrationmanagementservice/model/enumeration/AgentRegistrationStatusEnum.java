package com.mfs.agentregistrationmanagementservice.model.enumeration;

public enum AgentRegistrationStatusEnum {
    PENDING, SUCCESS, FAILED, NOTFOUND
}
