package com.mfs.agentregistrationmanagementservice.model;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class AgentVerificationRequest {
    @NotEmpty(message = "Agent number can not be empty")
    @NotNull(message = "Agent number can not be null")
    private String agentNumber;
    private String nidVerificationStatus;
    private String tradeLicenseVerificationStatus;
    private String tinCertificateVerificationStatus;
    private String shopVerificationStatus;
}
