package com.mfs.agentregistrationmanagementservice.model;

import lombok.Data;

@Data
public class AgentNidInfoDto {
    private String agentNidNumber;
    private String agentNidDob;
    private String agentNidNameEn;
    private String agentNidNameBn;
    private String agentNidFatherName;
    private String agentNidMotherName;
    private String agentNidSpouseName;
    private String agentNidPresentAddress;
    private String agentNidFrontImageName;
    private String agentNidBackImageName;
}
