package com.mfs.agentregistrationmanagementservice.model.enumeration;

public enum AgentFileTypeEnum {
    TRADE_LICENSE, TIN_CERTIFICATE, AGENT_SELFIE, AGENT_SHOP
}
