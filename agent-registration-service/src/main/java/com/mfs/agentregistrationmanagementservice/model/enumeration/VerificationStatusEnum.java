package com.mfs.agentregistrationmanagementservice.model.enumeration;

public enum VerificationStatusEnum {
    PENDING, SUCCESS, FAILED
}
