package com.mfs.qrgeneration.service;

import net.lingala.zip4j.ZipFile;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import lombok.extern.slf4j.Slf4j;

import javax.annotation.Resource;
import java.io.*;
import java.util.Iterator;

@Service
@Slf4j
public class ExcelService {
    @Resource
    private MerchantQRService merchantQRService;
    @Value("${pdf_files_location}")
    private String merchantQRPdfPath;

    @Value("${zip_file_location}")
    private String zipPath;

    @Value("${multipart_target_file_location}")
    private String targetFilePath;


    public void callGenerate(MultipartFile multipartFile) {
        try {
//            FileOutputStream file = new FileOutputStream(multipartFile.getOriginalFilename());
//            file.write(multipartFile.getBytes());
           // File file = File.createTempFile("targetFile",".xlsx", new File("src/main/resources"));
            File file = new File(targetFilePath+"targetFile.xlsx");

            multipartFile.transferTo(file);

            FileInputStream excelFile = new FileInputStream(file);
            Workbook workbook = new XSSFWorkbook(excelFile);
            Sheet datatypeSheet = workbook.getSheetAt(0);
            Iterator<Row> iterator = datatypeSheet.iterator();
            while (iterator.hasNext()) {


                Row currentRow = iterator.next();
                Iterator<Cell> cellIterator = currentRow.iterator();
                log.debug("cell data: {}", currentRow.getCell(0).toString());
                if(currentRow.getCell(0).toString().startsWith("1")){
                    try{
                        String number= String.valueOf((long)currentRow.getCell(0).getNumericCellValue());
                        String counter= String.valueOf((int)currentRow.getCell(1).getNumericCellValue());
                        File merchantQrPdf = merchantQRService.generateMerchantQr(number,counter);
                        log.debug("QR path: " + merchantQrPdf.getPath());
                        merchantQrPdf.renameTo(new File(merchantQRPdfPath+number+"-"+counter+".pdf"));
                        FileOutputStream fileOut = new FileOutputStream(merchantQrPdf);
                        fileOut.close();
                    }catch(Exception e) {
                        log.error("unable to generate qr: {}", currentRow.getCell(0).toString());
                    }
                }
                else{
                    continue;
                }
                
              //  ZipUtil.pack(new File("E:\\MFS_REDDOT\\release-tap-1.0.2\\reddotmfs\\qr-generation-service\\src\\main\\resources\\pdf"), new File("E:\\MFS_REDDOT\\release-tap-1.0.2\\reddotmfs\\qr-generation-service\\src\\main\\resources\\pdf.zip"));
//                ZipFile zipfile = new ZipFile(new File(zipPath));
//                zipfile.addFolder(new File(merchantQRPdfPath));
              //  return zipfile;
            }
            workbook.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
