package com.mfs.qrgeneration.service;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.mfs.qrgeneration.config.ApplicationProperties;
import com.mfs.qrgeneration.gateway.ApiManagerGateway;
import com.mfs.qrgeneration.model.MerchantInfoResponse;
import lombok.extern.slf4j.Slf4j;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.ui.jasperreports.JasperReportsUtils;

import javax.imageio.ImageIO;
import java.io.*;
import java.util.Collections;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

@Slf4j
@Service
public class MerchantQRService {
    public static final String API_MIDDLEWARE_MERCHANT_QR = "merchant-qr";
    public static final String MERCHANT_QR_SALT = "merchant-qr-tap";
    @Value("${merchant.qr.template.path}")
    private String merchantQRTemplatePath;
    @Value("${pdf_files_location}")
    private String pdfFileLocation;
    @Value("${agent.qr.template.path}")
    private String agentQRTemplatePath;
//    @Value("${background.image.path}")
//    private String backgroundImagePath;
    @Autowired
    ApiManagerGateway apiManagerGateway;
    @Autowired
    CryptoService cryptoService;
    @Autowired
    ApplicationProperties applicationProperties;

    public File generateMerchantQr(String merchantAccountNo,String counter) throws IOException {
        if (merchantAccountNo.startsWith("0")) {
            merchantAccountNo = "88" + merchantAccountNo;
        } else if(merchantAccountNo.startsWith("1")) {
            merchantAccountNo = "880" + merchantAccountNo;
        }

        File pdfFile = File.createTempFile("mc_"+merchantAccountNo, ".pdf");
//        pdfFile.renameTo(new File(pdfFileLocation+"\\"+merchantAccountNo+".pdf"));
        log.info(String.format("Invoice pdf path : %s", pdfFile.getAbsolutePath()));

        try(FileOutputStream pos = new FileOutputStream(pdfFile))
        {
            final JasperReport report = loadTemplate();
            MerchantInfoResponse merchantInfoResponse = apiManagerGateway.fetchMerchantInfoByAccountNumber(merchantAccountNo);
            final Map<String, Object> parameters = parameters(merchantInfoResponse,counter);

            // Create an empty datasource.
            final JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(Collections.singletonList("QRCode"));

            // Render the invoice as a PDF file.
            JasperReportsUtils.renderAsPdf(report, parameters, dataSource, pos);
            return pdfFile;
        }
        catch (final Exception e)
        {
            log.error(String.format("An error occurred during PDF creation: %s", e));
            throw new RuntimeException(e);
        }
    }

    public File generateAgentQr(String agentAccountNo) throws IOException{
        File pdfFile = File.createTempFile("ag_"+agentAccountNo, ".pdf");
        log.info(String.format("Invoice pdf path : %s", pdfFile.getAbsolutePath()));
        try(FileOutputStream pos = new FileOutputStream(pdfFile))
        {
            final JasperReport report = loadAgentTemplate();
            final Map<String, Object> parameters = parameters(agentAccountNo);

            // Create an empty datasource.
            final JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(Collections.singletonList("QRCode"));

            // Render the invoice as a PDF file.
            JasperReportsUtils.renderAsPdf(report, parameters, dataSource, pos);
            return pdfFile;
        }
        catch (final Exception e)
        {
            log.error(String.format("An error occurred during PDF creation: %s", e));
            throw new RuntimeException(e);
        }

    }

    private Map<String, Object> parameters( MerchantInfoResponse merchantInfoResponse,String counter) throws IOException, WriterException {
        final Map<String, Object> parameters = new HashMap<>();
        String merchantAccountNumber = String.valueOf(merchantInfoResponse.getAccountNumber());
        if(merchantAccountNumber.startsWith("88"))
            merchantAccountNumber = merchantAccountNumber.substring(2);

        log.debug("merchant account number: {}", merchantAccountNumber);
        for (int i = 0; i < 11 ; i++) {
            String key = new StringBuffer().append("msisdn")
                    .append(i<9?"0":"")
                    .append(i+1).toString();
            parameters.put(key, String.valueOf(merchantAccountNumber.charAt(i)));
        }
        parameters.put("merchant_code",  merchantInfoResponse.getAccountName());
        parameters.put("counter_no",  counter);

        // Generating Hash value
        String url = applicationProperties.getApiMiddlewareUrl() + "/" + API_MIDDLEWARE_MERCHANT_QR +
                "?id=" + merchantInfoResponse.getAccountId();
        String hash = cryptoService.getSHA_512_hashValue(url, MERCHANT_QR_SALT);
        String finalUrl = url + "&secret=" + hash;
        parameters.put("qrcode", new ByteArrayInputStream(generateQrCode(finalUrl).toByteArray()));
        //        parameters.put("qrcode", new ByteArrayInputStream(generateQrCode(finalUrl, String.valueOf(
        //                merchantInfoResponse.getAccountNumber())).toByteArray()));

        return parameters;
    }

    private Map<String, Object> parameters( String agentNo) throws IOException, WriterException {
        final Map<String, Object> parameters = new HashMap<>();
        String agentAccountNumber = agentNo;
        if(agentAccountNumber.startsWith("88"))
            agentAccountNumber = agentAccountNumber.substring(2);

        log.debug("agent account number: {}", agentAccountNumber);
        for (int i = 0; i < 11 ; i++) {
            String key = new StringBuffer().append("msisdn")
                    .append(i<9?"0":"")
                    .append(i+1).toString();
            parameters.put(key, String.valueOf(agentAccountNumber.charAt(i)));
        }

        // Generating Hash value
//        String url = applicationProperties.getApiMiddlewareUrl() + File.separator + API_MIDDLEWARE_MERCHANT_QR +
//                "?id=" + merchantInfoResponse.getAccountId();
//        String hash = cryptoService.getSHA_512_hashValue(url, MERCHANT_QR_SALT);
//        String finalUrl = url + "&secret=" + hash;
        parameters.put("qrcode", new ByteArrayInputStream(generateQrCode(agentAccountNumber).toByteArray()));

        return parameters;
    }

    private ByteArrayOutputStream generateQrCode(String inputText) throws WriterException, IOException {
        QRCodeWriter qrCodeWriter = new QRCodeWriter();
        BitMatrix bitMatrix = qrCodeWriter.encode(inputText, BarcodeFormat.QR_CODE, 200, 200);

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ImageIO.write(MatrixToImageWriter.toBufferedImage(bitMatrix), "PNG", outputStream);
        return outputStream;
    }

//    private ByteArrayOutputStream generateQrCode(String inputText, String merchantAccountNo) throws WriterException, IOException {
//        QRCodeWriter qrCodeWriter = new QRCodeWriter();
//        BitMatrix bitMatrix = qrCodeWriter.encode(inputText, BarcodeFormat.QR_CODE, 200, 200);
//
//        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
//        ImageIO.write(MatrixToImageWriter.toBufferedImage(bitMatrix), "PNG", outputStream);
//        return outputStream;
//    }

    // Load invoice JRXML template
    private JasperReport loadTemplate() throws JRException {
        log.info(String.format("Merchant QR template path : %s", merchantQRTemplatePath));
        final InputStream reportInputStream = getClass().getResourceAsStream(merchantQRTemplatePath);
        final JasperDesign jasperDesign = JRXmlLoader.load(reportInputStream);

        return JasperCompileManager.compileReport(jasperDesign);
    }

    private JasperReport loadAgentTemplate() throws JRException {
        log.info(String.format("Merchant QR template path : %s", agentQRTemplatePath));
        final InputStream reportInputStream = getClass().getResourceAsStream(agentQRTemplatePath);
        final JasperDesign jasperDesign = JRXmlLoader.load(reportInputStream);

        return JasperCompileManager.compileReport(jasperDesign);
    }

}
