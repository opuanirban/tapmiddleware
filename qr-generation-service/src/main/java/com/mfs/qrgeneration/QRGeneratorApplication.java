package com.mfs.qrgeneration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;


@SpringBootApplication
public class QRGeneratorApplication {

	public static void main(String[] args) {
		SpringApplication.run(QRGeneratorApplication.class, args);
	}
}
