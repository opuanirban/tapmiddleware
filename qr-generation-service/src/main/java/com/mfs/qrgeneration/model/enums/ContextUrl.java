package com.mfs.qrgeneration.model.enums;


public enum ContextUrl {
    MERCHANT_INFO_FROM_ACC_NUMBER("/GenerateQR"),
    MERCHANT_INFO_FROM_ACC_ID("/ReadQR");

    private String url;
    ContextUrl(String envUrl) {
        this.url = envUrl;
    }
    public String getUrl() {
        return url;
    }
}