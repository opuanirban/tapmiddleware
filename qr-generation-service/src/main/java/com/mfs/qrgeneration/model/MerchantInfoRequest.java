package com.mfs.qrgeneration.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class MerchantInfoRequest {
    @JsonProperty("AccountNumber")
    private long accountNumber;
}
