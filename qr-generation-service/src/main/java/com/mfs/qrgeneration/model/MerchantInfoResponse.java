package com.mfs.qrgeneration.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class MerchantInfoResponse {
    private long AccountId;
    private long AccountNumber;
    private String AccountName;
    private String AccountType;
}
