package com.mfs.qrgeneration.gateway;

import com.google.gson.Gson;
import com.mfs.qrgeneration.model.MerchantInfoRequest;
import com.mfs.qrgeneration.model.MerchantInfoResponse;
import com.mfs.qrgeneration.model.enums.ContextUrl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.lang.reflect.Type;
import java.util.Locale;

@Slf4j
@Service
public class ApiManagerGateway {

    @Autowired
    RestTemplate restTemplate;

    @Value("${api_manager_app.url}")
    private String apiManagerUrl;

    @Value("${api_manager_auth_code}")
    private String apiManagerAuthCode;

    public MerchantInfoResponse fetchMerchantInfoByAccountNumber(String merchantAccountNumber){
        String url = new StringBuilder(apiManagerUrl)
                .append(ContextUrl.MERCHANT_INFO_FROM_ACC_NUMBER.getUrl()).toString();
        MerchantInfoResponse merchantInfoResponse;

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Authorization", apiManagerAuthCode);
        httpHeaders.add("Content-Type", "application/json");

        MerchantInfoRequest merchantInfoRequest = new MerchantInfoRequest();
        merchantInfoRequest.setAccountNumber(Long.valueOf(merchantAccountNumber));
        HttpEntity<MerchantInfoRequest> entity = new HttpEntity<>(merchantInfoRequest, httpHeaders);
        log.debug("calling url: {}, request: {}", url, merchantInfoRequest);
        try {
            ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.POST,entity,
                    String.class);
            if (responseEntity.getStatusCode() == HttpStatus.OK) {
                String response = responseEntity.getBody().replace("\\", "");
                log.debug("merchant raw response: {}", response);
                if (response.startsWith("\"") & response.endsWith("\"")) {
                    merchantInfoResponse = new Gson().fromJson(response.substring(1, response.length() - 1), MerchantInfoResponse.class);
                } else {
                    merchantInfoResponse = new Gson().fromJson(response, MerchantInfoResponse.class);
                }
                log.debug("merchant response: {}", merchantInfoResponse);
                return  merchantInfoResponse;
            }
            else {
                throw new RuntimeException("Error processing request in the core service.");
            }
        } catch (Exception e){
            throw new RuntimeException("Unable to process at this moment. Please try again later.");
        }
    }
}
