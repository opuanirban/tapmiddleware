package com.mfs.qrgeneration.controller;

import com.mfs.qrgeneration.service.ExcelService;
import com.mfs.qrgeneration.service.MerchantQRService;
import lombok.extern.slf4j.Slf4j;
import net.lingala.zip4j.ZipFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import javax.annotation.Resource;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.zip.ZipInputStream;

import static java.lang.String.format;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_PDF;

@Controller
@Slf4j
//@RequestMapping("/qr/merchant")
public class MerchantController {
    @Resource
    private MerchantQRService merchantQRService;

    @Autowired
    ExcelService excelService;

    @Value("${zip_file_location}")
    private String zipPath;

    @GetMapping("/login")
    public String login() {
        return "login";
    }
    @GetMapping("/home")
    public String layout() {
        return "dashboard";
    }

//    @GetMapping("/home")
//    public String layout() {
//        return "layout";
//    }

    @GetMapping("/forms")
    public String invoiceForms() {
        return "forms";
    }

    @GetMapping("/merchant-form")
    public String merchantForms() {
        return "merchant_tab";
    }

    // generate invoice pdf
    @PostMapping(value = "/generate", produces = "application/pdf")
    public ResponseEntity<InputStreamResource> qrGenerate( @RequestParam(name = "merchant_account_no") String merchantAccountNo,@RequestParam(name = "counterNo", defaultValue = "1") String counterNo) throws IOException {
        try{
            String merchantNo= merchantAccountNo.substring(merchantAccountNo.length()-11);
            String regex = "^\\d{11}$";
            if(merchantNo.matches(regex)){
                log.info("Starting merchant qr generation...");
                final File merchantQrPdf = merchantQRService.generateMerchantQr(merchantNo,counterNo);
                log.info("merchant qr generated successfully...");

                final HttpHeaders httpHeaders = getHttpHeaders(merchantAccountNo, merchantQrPdf);
                return new ResponseEntity<>(new InputStreamResource(new FileInputStream(merchantQrPdf)), httpHeaders, OK);
            }
            else{
                throw new RuntimeException("Invalid Number.Please input proper merchant account number.");
            }
        }
        catch (Exception e){
            throw new RuntimeException("Please input proper merchant account number.");
        }


    }

    @PostMapping(value = "/generate-bulk",produces = "application/octet-stream")
    public ResponseEntity<Object> qrGenerateBulk(@RequestParam("file") MultipartFile file) throws IOException {
        String filename = null;
        try {
            //   final File zipPdf= excelService.callGenerate(file);
            excelService.callGenerate(file);
//             ZipInputStream zipInputStream= new ZipInputStream();
//            return new ResponseEntity<>(new StreamingResponseBody(new ZipInputStream(zipPdf)) );
//            filename = zipPath;
//            File zip = new File(filename);
//            InputStreamResource resource = new InputStreamResource(new FileInputStream(zip));
//
//            HttpHeaders headers = new HttpHeaders();
//            headers.add("Content-Disposition",
//                    "attachment;filename=QR-pdf.zip");
//            headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
//            headers.add("Pragma", "no-cache");
//            headers.add("Expires", "0");
//            ResponseEntity<Object> responseEntity = ResponseEntity.ok().headers(headers)
//                    .contentLength(zip.length())
//                    .contentType(MediaType.parseMediaType("application/octet-stream")).body(resource);

            return ResponseEntity.ok().body("success");
        } catch (Exception e) {
            throw new RuntimeException("Something went wrong.");
        }
    }

    private HttpHeaders getHttpHeaders(String code,  File invoicePdf) {
        HttpHeaders respHeaders = new HttpHeaders();
        respHeaders.setContentType(APPLICATION_PDF);
        respHeaders.setContentLength(invoicePdf.length());
        respHeaders.setContentDispositionFormData("attachment", format("%s.pdf", code));
        return respHeaders;
    }
}
