package com.mfs.qrgeneration.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Data
public class ApplicationProperties {
    @Value("${api_middleware_url}")
    private String apiMiddlewareUrl;
}
