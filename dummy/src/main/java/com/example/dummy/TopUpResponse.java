package com.example.dummy;

import lombok.Data;

@Data
public class TopUpResponse {
    private int Code;
    private String Message;
}
