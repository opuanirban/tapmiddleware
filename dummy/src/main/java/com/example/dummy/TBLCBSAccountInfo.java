package com.example.dummy;

import lombok.Data;

@Data
public class TBLCBSAccountInfo {
    private String key;
    private String value;
}
