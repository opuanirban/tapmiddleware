package com.example.dummy;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.annotation.PostConstruct;

import com.example.dummy.dao.RedisDao;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.gson.Gson;

import org.apache.tomcat.util.codec.binary.Base64;
//import java.util.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@Component
public class TestController {
    public static final String DATA_SEPARATOR = "###";
    public static final String BALANCE_FORMAT = "###.##";

    @Value("${jwt.password}")
    private String jwtPassword;

    @Autowired
    RedisDao redisDao;

    public static final String INSURANCE = "data/insurance";
    public static final String UTILITY = "data/utilities";
    public static final String INSTITUTION = "data/Institutions";
    public static final String OTHERS = "data/others";


    @PostConstruct
    public void setRedisKey() {
//        DPDC
//        DPDC Customer Id: 35159703, 154383 location: F2/A8, Year:2020, Month: Nov(11)
//        44, 15468051,  35159737, 35159775, 35159718, 35159722, 35159741, 35159756, 35159760
        redisDao.setValue("661120014510", "1");
        redisDao.setValue("112013134972", "1");
        redisDao.setValue("112013135582", "1");
        redisDao.setValue("112013132182", "1");
        redisDao.setValue("112013137095", "1");
        redisDao.setValue("112013137105", "1");
        redisDao.setValue("112013137194", "1");
        redisDao.setValue("112013137271", "1");
        redisDao.setValue("112013137294", "1");
        redisDao.setValue("112013137358", "1");
        redisDao.setValue("112013137363", "1");
        redisDao.setValue("112013137377", "1");
        redisDao.setValue("202011", "1");
        redisDao.setValue("202104", "1");
        redisDao.setValue("202108", "1");
        redisDao.setValue("35159703", "1");
        redisDao.setValue("35159760", "1");
        redisDao.setValue("35159756", "1");
        redisDao.setValue("35159741", "1");
        redisDao.setValue("35159722", "1");
        redisDao.setValue("35159718", "1");
        redisDao.setValue("35159775", "1");
        redisDao.setValue("35159737", "1");
        redisDao.setValue("15468051", "1");
        redisDao.setValue("15438344", "1");
        redisDao.setValue("8801844590344", "1");
        redisDao.setValue("8801817182040", "1");
        redisDao.setValue("8801867981406", "1");
        redisDao.setValue("191779", "1");
        redisDao.setValue("191778", "1");
        redisDao.setValue("191780", "1");
        redisDao.setValue("8801729069555", "1");
        redisDao.setValue("88001704320043","1");
        redisDao.setValue("8801898762360", "1");
        redisDao.setValue("8801913057116", "1");
        redisDao.setValue("01913057116","1");
        redisDao.setValue("8801580120644", "1");
        redisDao.setValue("3381170", "1");
        redisDao.setValue("3307461", "1");
        redisDao.setValue("2663907", "1");
        redisDao.setValue("F2", "1");
        redisDao.setValue("f2", "1");
        redisDao.setValue("A8", "1");
        redisDao.setValue("a8", "1");
        redisDao.setValue("8801766685686", "1");
        redisDao.setValue("8801937791254", "1");
        redisDao.setValue("8801704320043","1");
        redisDao.setValue("35159703","1");
    }

    @ResponseBody
    @PostMapping(path = "/set_key", produces = "application/json")
    public String setKey(@RequestParam("key") String key) {
        try {
            redisDao.setValue(key, "1");
            return "Key set successful";
        } catch (Exception e) {
            log.error(e.getMessage());
            return "Failed to set key";

        }
    }

    @ResponseBody
    @PostMapping(path = "/remove_key",produces = "application/json")
    public String removeKey(@RequestParam("key") String key){
//        redisDao.
        if(redisDao.hasKey(key))
            redisDao.deleteValue(key);
        return "Key Removed";
    }
    @ResponseBody
    @PostMapping(path = "/reset_redis_keys", produces = "application/json")
    public String resetRedisKey() {
        try {
            redisDao.setValue("661120014510", "1");
            redisDao.setValue("112013134972", "1");
            redisDao.setValue("112013135582", "1");
            redisDao.setValue("112013132182", "1");
            redisDao.setValue("112013137095", "1");
            redisDao.setValue("112013137105", "1");
            redisDao.setValue("112013137194", "1");
            redisDao.setValue("112013137271", "1");
            redisDao.setValue("112013137294", "1");
            redisDao.setValue("112013137358", "1");
            redisDao.setValue("112013137363", "1");
            redisDao.setValue("112013137377", "1");
            redisDao.setValue("202011", "1");
            redisDao.setValue("202104", "1");
            redisDao.setValue("202105", "1");
            redisDao.setValue("35159703", "1");
            redisDao.setValue("35159760", "1");
            redisDao.setValue("35159756", "1");
            redisDao.setValue("35159741", "1");
            redisDao.setValue("35159722", "1");
            redisDao.setValue("35159718", "1");
            redisDao.setValue("35159775", "1");
            redisDao.setValue("35159737", "1");
            redisDao.setValue("15468051", "1");
            redisDao.setValue("15438344", "1");
            redisDao.setValue("8801844590344", "1");
            redisDao.setValue("8801817182040", "1");
            redisDao.setValue("8801867981406", "1");
            redisDao.setValue("191779", "1");
            redisDao.setValue("191778", "1");
            redisDao.setValue("191780", "1");
            redisDao.setValue("8801729069555", "1");
            redisDao.setValue("8801898762360", "1");
            redisDao.setValue("8801913057116", "1");
            redisDao.setValue("8801580120644", "1");
            redisDao.setValue("3381170", "1");
            redisDao.setValue("3307461", "1");
            redisDao.setValue("2663907", "1");
            redisDao.setValue("8801766685686", "1");
            redisDao.setValue("8801937791254", "1");
            redisDao.setValue("35159703","1");
//            redisDao.setValue("");
            return "reset successful";
        } catch (Exception e) {
            log.error(e.getMessage());
            return "Failed to reset";

        }

    }

    @PostMapping(path = "/GetUserStatus", produces = "application/json")
    public String userStatus(@RequestParam("userNumber") String msisdn, @RequestParam("shortCode") String shortCode) {
        log.debug("userNumber: " + msisdn);
        if (redisDao.getValue(msisdn) != null)
            return "true";
        else
            return "false";

    }

    @PostMapping(path = "/GenerateQR", produces = "application/json", consumes = "application/json")
    public MerchantInfoResponse generateQR(@RequestBody String AccountNumber) {
        log.debug("AccountNumber: " + AccountNumber);
        MerchantInfoResponse merchantInfoResponse = new MerchantInfoResponse();
        merchantInfoResponse.setAccountId(148640);
        merchantInfoResponse.setAccountNumber(8801729069555L);
        merchantInfoResponse.setAccountName("T-cash  Test Merchant");
        merchantInfoResponse.setAccountType("Merchant");

        return merchantInfoResponse;

    }

    @PostMapping(path = "/ReadQR", produces = "application/json")
    public MerchantInfoResponse readQR(@RequestBody String AccountId) {
        log.debug("AccountNumber: " + AccountId);
        MerchantInfoResponse merchantInfoResponse = new MerchantInfoResponse();
        merchantInfoResponse.setAccountId(148640);
        merchantInfoResponse.setAccountNumber(8801729069555L);
        merchantInfoResponse.setAccountName("T-cash  Test Merchant");
        merchantInfoResponse.setAccountType("Merchant");

        return merchantInfoResponse;

    }


    @PostMapping(path = "/UserRefresh", produces = "application/json")
    public String userRefresh(@RequestParam("userNumber") String msisdn, @RequestParam("encKey") String encKey) {
        if (redisDao.getValue(msisdn) != null) {
            String pin = redisDao.getValue(msisdn).split(DATA_SEPARATOR)[0];
            return "\"" + Base64.encodeBase64String(pin.getBytes()) + "\"";
        } else {
            return "Invalid mobile number, please enter registered mobile number.";
        }
    }

    @PostMapping(path = "/UserLogin", produces = "application/json")
    public String userLogin(@RequestParam("userNumber") String msisdn, @RequestParam("pin") String pin) {
//        if(msisdn.equals("8801704320043") || msisdn.equals("01704320043")) {
//            redisDao.setValue("8801704320043"+"ekyc","1");
////            redisDao.setValue("01704320043"+);
//            return "\"" + "TEST USER|" + "500" + "|True|Active" + "\"";
//        }
            if (redisDao.getValue(msisdn) != null) {
            String balance = redisDao.getValue(msisdn).split(DATA_SEPARATOR)[1];
            String decodedPin=new String(Base64.decodeBase64(pin));
            String pinRedis = redisDao.getValue(msisdn).split(DATA_SEPARATOR)[0];
            log.info("msisdn : "+msisdn);
            log.info("pin in redis : "+pinRedis);
            log.info("pin by user : "+pin);
            log.info("decoded pin by user : "+decodedPin);
            if(!pinRedis.equalsIgnoreCase(decodedPin)){
                return "Invalid mobile number or password";
            }
            if(redisDao.getValue(msisdn+"ekyc") != null) {
                if (redisDao.getValue(msisdn + "ekyc").equalsIgnoreCase("1")) {
                    return "\"" + "TEST USER|" + balance + "|False|Authorized" + "\"";
                } else {
                    return "\"" + "TEST USER|" + balance + "|True|Active" + "\"";
                }
            }
            else{
                redisDao.setValue(msisdn + "ekyc", "1");
                return "\"" + "TEST USER|" + balance + "|False|Authorized" + "\"";
            }
        } else {

            return "Invalid mobile number, please enter registered mobile number.";
        }
    }


    @PostMapping(path = "/GetDescoBillInfo", produces = "application/json")
    public String getBillAmount(@RequestParam("BillNo") String billNo) {

        if (!billNo.isEmpty()) {
            if (redisDao.getValue(billNo) != null) {
                if (redisDao.getValue(billNo).equalsIgnoreCase("1")) {
                    return "\"50\"";
                } else {
                    return "\"-2\"";

                }

            } else {
                return "\"-2\"";
            }
        }
        return "\"Please provide a bill number\"";
    }

    @PostMapping(path = "/GetDescoPrepaidBreakdownDetail", produces = "application/json")
    public String descoPrepaid(@RequestParam("meterNo") String meterNo, @RequestParam("amount") String amount) {

        if (!meterNo.isEmpty() && !amount.isEmpty()) {
            if(amount.matches(".*[a-zA-Z]+.*")){
                return "\"{\\\"transactionId\\\":\\\"2bf2cf3d-a4ba-4559-b006-49d216639cb8\\\",\\\"accountNo\\\":\\\"34051491\\\",\\\"meterNo\\\":\\\"661120014510\\\",\\\"amount\\\":500,\\\"energyCost\\\":480.95,\\\"charges\\\":{\\\"Rebate\\\":-4.76},\\\"revenue\\\":476.19,\\\"vat\\\":23.81,\\\"message\\\":\\\"Invalid request, Unable to process at this moment. Please try after sometime\\\"}\"";
//                return "Invalid request, Unable to process at this moment. Please try after sometime";

            }

            if (Double.parseDouble(amount) <= 0) {
                return "Invalid data given.";
            }

            if (Double.parseDouble(amount) < 500) {
                return "\"{\\\"transactionId\\\":\\\"2bf2cf3d-a4ba-4559-b006-49d216639cb8\\\",\\\"accountNo\\\":\\\"34051491\\\",\\\"meterNo\\\":\\\"661120014510\\\",\\\"amount\\\":500,\\\"energyCost\\\":480.95,\\\"charges\\\":{\\\"Rebate\\\":-4.76},\\\"revenue\\\":476.19,\\\"vat\\\":23.81,\\\"message\\\":\\\"Purchase amount can't be lower than min threshold(500)!\\\"}\"";

//                return "Purchase amount can't be lower than min threshold(500)!";
            }
            if (amount.startsWith(".")) {
                return "এই মুহু ্তে অনুরোধ প্রক্রিয়া করতে অক্ষম";
            }
            // redisDao.setValue(meterNo,"1");
            //  System.out.println(redisDao.getValue(meterNo));
            if (redisDao.getValue(meterNo) != null) {
                return "\"{\\\"transactionId\\\":\\\"2bf2cf3d-a4ba-4559-b006-49d216639cb8\\\",\\\"accountNo\\\":\\\"34051491\\\",\\\"meterNo\\\":\\\"661120014510\\\",\\\"amount\\\":500,\\\"energyCost\\\":480.95,\\\"charges\\\":{\\\"Rebate\\\":-4.76},\\\"revenue\\\":476.19,\\\"vat\\\":23.81,\\\"message\\\":\\\"Breakup for Tk 500: Energy: 480.95; Rebate: -4.76; VAT: 23.81.\\\"}\"";

                //return "\"{\\\"transactionId\\\":\\\"\\\",\\\"accountNo\\\":\\\"\\\",\\\"meterNo\\\":\\\"066112002259\\\",\\\"amount\\\":500,\\\"energyCost\\\":0,\\\"charges\\\":null,\\\"revenue\\\":0,\\\"vat\\\":0,\\\"message\\\":\\\"Meter Not Registered.\\\"}\"";
                //return "\"{\\\"transactionId\\\":\\\"\\\",\\\"accountNo\\\":\\\"\\\",\\\"meterNo\\\":\\\"661120022597\\\",\\\"amount\\\":0,\\\"energyCost\\\":0,\\\"charges\\\":null,\\\"revenue\\\":0,\\\"vat\\\":0,\\\"message\\\":\\\"Invalid data given.\\\"}\"";
            } else {
             //   throw new RuntimeException("Meter does not exist in the storage");
                return "\"{\\\"transactionId\\\":\\\"2bf2cf3d-a4ba-4559-b006-49d216639cb8\\\",\\\"accountNo\\\":\\\"34051491\\\",\\\"meterNo\\\":\\\"661120014510\\\",\\\"amount\\\":500,\\\"energyCost\\\":480.95,\\\"charges\\\":{\\\"Rebate\\\":-4.76},\\\"revenue\\\":476.19,\\\"vat\\\":23.81,\\\"message\\\":\\\"Meter does not exist in the storage.\\\"}\"";

                //return "Meter does not exist in the storage.";
            }
        } else {
            return "\"Bill number and amount can not be empty\"";
        }

    }


    @PostMapping(path = "/GetDPDCBillInfo", produces = "application/json")
    public String getDPDCBill(@RequestParam("LocationCode") String locationCode, @RequestParam("BillMonth") String billMonth, @RequestParam("accountNumber") String accountNo) {

        if (!locationCode.isEmpty() && !billMonth.isEmpty() && !accountNo.isEmpty()) {
            if(redisDao.getValue(locationCode) == null){
                return "\"100\"";
            }
            if (redisDao.getValue(billMonth) != null) {
                if (redisDao.getValue(accountNo).equalsIgnoreCase("1")) {
                    return "\"991.00\"";
                } else if (redisDao.getValue(accountNo).equalsIgnoreCase("0")) {
                    return "\"-1\"";
                } else {
                    return "\"\"";
                }

            } else {
                return "\"\"";
            }
        }
        return null;
    }

    @ResponseBody
    @PostMapping(path = "/RechargeGateway", consumes = "application/json", produces = "application/json")
    public String topUp(@RequestBody TopUpRequest2 topUpRequest2) {
        TopUpResponse topUpResponse= new TopUpResponse();
        Gson gson = new Gson();
        if (redisDao.getValue(topUpRequest2.getFromAccount()) != null) {
            String pin = redisDao.getValue(topUpRequest2.getFromAccount()).split(DATA_SEPARATOR)[0];
            String userpin=topUpRequest2.getPin();
            String decodedPin=new String(Base64.decodeBase64(userpin));
            log.info("msisdn : "+topUpRequest2.getFromAccount());
            log.info("pin in redis : "+pin);
            log.info("pin by user : "+userpin);
            log.info("decoded pin by user : "+decodedPin);
            if (pin.equalsIgnoreCase(decodedPin)){
                Double amount = Double.valueOf(topUpRequest2.getAmount());
                String recipAcc = topUpRequest2.getRecipientMsisdn();
//                if (recipAcc.length() != 13) {
//                    return "\"The cell number has to be valid, please provide cell number start with 88.\"";
//                }
                if(amount<10){
                    topUpResponse.setCode(400);
                    topUpResponse.setMessage("The recharge amount is either invalid or out of operator assigned valid range.");
                    return gson.toJson(topUpResponse);
                //    return "\"The recharge amount is either invalid or out of operator assigned valid range.\"";
                }
                if (debitAccount(topUpRequest2.getFromAccount(), amount+10.0)) {
                    topUpResponse.setCode(200);
                    topUpResponse.setMessage("Recharge request has been initiated for processing.");
                    return gson.toJson(topUpResponse);
                //    return "\"Recharge request has been initiated for processing.\"";
                } else {
                    topUpResponse.setCode(400);
                    topUpResponse.setMessage("Insufficient balance to perform the transaction, your current balance is Tk. " + redisDao.getValue(topUpRequest2.getFromAccount()).split(DATA_SEPARATOR)[1]);
                    return gson.toJson(topUpResponse);
                  //  return "\"Insufficient balance to perform the transaction, your current balance is Tk. " + redisDao.getValue(topUpRequest2.getFromAccount()).split(DATA_SEPARATOR)[1] + ".\"";
                }
            } else {
                topUpResponse.setCode(400);
                topUpResponse.setMessage("Incorrect PIN! If you have lost your PIN, please contact customer service for assistance");
                return gson.toJson(topUpResponse);
              //  return "\"Incorrect PIN! If you have lost your PIN, please contact customer service for assistance\"";
            }
        } else {
            topUpResponse.setCode(400);
            topUpResponse.setMessage("User not registered");
            return gson.toJson(topUpResponse);
            // return "\"User not registered\"";
        }

    }

    @ResponseBody
    @PostMapping(path = "/UserPOST", consumes = "application/json", produces = "application/json")
    public String register(@RequestBody RegistrationParams registrationParams) {

        String userNumber = registrationParams.getUserNumber().replace("\\xac\\xed\\x00\\x05t\\x00\\r", "");
        log.debug("user number: " + userNumber + " length: " + userNumber.length());
        if (redisDao.getValue(userNumber) != null)
            return "User already exists";
        else {
            String value = registrationParams.getAccountPin() + DATA_SEPARATOR + getFormatedBalance((double) 0);
            redisDao.setValue(userNumber, value);
            redisDao.setValue(userNumber+"ekyc","1");
            //tblStatus
            if (redisDao.getValue(userNumber) != null) {
                return "\"Thank you. Account created successfully\"";
            } else {
                return "Registration data insert fail";
            }
        }

    }
    @ResponseBody
    @PostMapping(path = "/GetFeesAndCharges", consumes = "application/json", produces = "application/json")
    public String feeCharge(@RequestBody FeesAndChargesModel feesAndChargesModel) {

        if (redisDao.getValue(feesAndChargesModel.getFromUserNumber()) != null) {
            String response = "\"{\\\"ChargeAmount\\\":\"10\"}\"";
            return response;
        }
        return "\"You are not allowed to make this transaction\"";
    }

    @ResponseBody
    @PostMapping(path = "/ValidateNID", consumes = "application/json", produces = "application/json")
    public Boolean validateNID(@RequestBody NIDValidationRequest nidValidationRequest){
        return true;
    }

    @ResponseBody
    @PostMapping(path = "/ValidateNIDIneKYCUpdate", consumes = "application/json", produces = "application/json")
    public Boolean validateNIDIneKYCUpdate(@RequestBody NIDValidationRequest nidValidationRequest){
        return true;
    }

    @ResponseBody
    @PostMapping(path = "/EkycUpdate", consumes = "application/json", produces = "application/json")
    public String ekycUpdate(@RequestBody RegistrationParams registrationParams) {

        if (redisDao.getValue(registrationParams.getUserNumber()) != null) {
            redisDao.setValue(registrationParams.getUserNumber()+"ekyc","0");
            return "KYC info updated successfully in TBL";
        } else {
            return "User number missing in TBL DB";
        }

    }

    @PostMapping(path = "/GetUserBalance", produces = "application/json")
    public String balanceCheck(@RequestParam("userNumber") String userNumber, @RequestParam("encKey") String encKey) {
        log.debug("userNumber: " + userNumber);
        if (redisDao.getValue(userNumber) != null) {
            String balance = redisDao.getValue(userNumber).split(DATA_SEPARATOR)[1];
            return balance;
        } else {
            return "\"User not registered\"";
        }


    }

    @PostMapping(path = "/UpdatePin", produces = "application/json")
    public String updatePin(@RequestParam("userNumber") String msisdn, @RequestParam("oldPin") String oldPin, @RequestParam("newPin") String newPin, @RequestParam("encKey") String encKey) {
        if (redisDao.getValue(msisdn) != null) {
            String pin = redisDao.getValue(msisdn).split(DATA_SEPARATOR)[0];
            String balance = redisDao.getValue(msisdn).split(DATA_SEPARATOR)[1];
            if (pin.equalsIgnoreCase(oldPin)) {
                if (oldPin.equalsIgnoreCase(newPin)) {
                    return "New pin cannot match old pin";
                }
                String value = newPin + DATA_SEPARATOR + balance;
                redisDao.setValue(msisdn, value);
                System.out.println(redisDao.getValue(msisdn).split(DATA_SEPARATOR)[0]);
                return "\"Your pin has been updated successfully.\"";
            } else {
                return "Your old pin is incorrect";
            }

        } else {
            return "Pin update failed.Please try again.";

        }
    }


    @PostMapping(path = "/UserStatement", produces = "application/json")
    public String userStatement(@RequestParam("userNumber") String userNumber, @RequestParam("encKey") String encKey) {
        String statement = "[{\"ActionType\":\"DEBIT\",\"Amount\":5.00000,\"ReceiverName\":\"Commission Distribution\",\"ReceiverNumber\":8800000670002,\"SenderName\":\"MIZANUR RAHMAN\",\"SenderNumber\":8801710754881,\"TransactionDate\":\"08-09-2020\",\"TransactionType\":\"Pay\",\"TxCode\":\"PABSKEXW16CF56330148\"},{\"ActionType\":\"DEBIT\",\"Amount\":1000.00000,\"ReceiverName\":\"MD. MOSFIQUR RAHMAN\",\"ReceiverNumber\":8801833184036,\"SenderName\":\"MIZANUR RAHMAN\",\"SenderNumber\":8801710754881,\"TransactionDate\":\"08-09-2020\",\"TransactionType\":\"Pay\",\"TxCode\":\"PABSKEXW16CF56330148\"},{\"ActionType\":\"DEBIT\",\"Amount\":20.00000,\"ReceiverName\":\"Arena BD. Ltd\",\"ReceiverNumber\":8801835416692,\"SenderName\":\"MIZANUR RAHMAN\",\"SenderNumber\":8801710754881,\"TransactionDate\":\"08-09-2020\",\"TransactionType\":\"TopUp\",\"TxCode\":\"TOPUPGLV7KXIUJL82846483\"},{\"ActionType\":\"CREDIT\",\"Amount\":5000.00000,\"ReceiverName\":\"MIZANUR RAHMAN\",\"ReceiverNumber\":8801710754881,\"SenderName\":\"t- Cash\",\"SenderNumber\":8800000699999,\"TransactionDate\":\"07-09-2020\",\"TransactionType\":\"TransferFloraToTBMM\",\"TxCode\":\"FCDPSOIKDV33XL73664966\"},{\"ActionType\":\"DEBIT\",\"Amount\":100.00000,\"ReceiverName\":\"Arena BD. Ltd\",\"ReceiverNumber\":8801835416692,\"SenderName\":\"MIZANUR RAHMAN\",\"SenderNumber\":8801710754881,\"TransactionDate\":\"07-09-2020\",\"TransactionType\":\"TopUp\",\"TxCode\":\"TOPUPTUBR4ZO36D8908117\"},{\"ActionType\":\"CREDIT\",\"Amount\":50.00000,\"ReceiverName\":\"MIZANUR RAHMAN\",\"ReceiverNumber\":8801710754881,\"SenderName\":\"t- Cash\",\"SenderNumber\":8800000699999,\"TransactionDate\":\"06-09-2020\",\"TransactionType\":\"TransferFloraToTBMM\",\"TxCode\":\"FCDPWOVFF3MUZ447115349\"},{\"ActionType\":\"DEBIT\",\"Amount\":140.00000,\"ReceiverName\":\"MD Imran\",\"ReceiverNumber\":8801756200700,\"SenderName\":\"MIZANUR RAHMAN\",\"SenderNumber\":8801710754881,\"TransactionDate\":\"06-09-2020\",\"TransactionType\":\"MPAY\",\"TxCode\":\"MPAYZCFCLXVWM952725845\"},{\"ActionType\":\"CREDIT\",\"Amount\":100.00000,\"ReceiverName\":\"MIZANUR RAHMAN\",\"ReceiverNumber\":8801710754881,\"SenderName\":\"t- Cash\",\"SenderNumber\":8800000699999,\"TransactionDate\":\"01-09-2020\",\"TransactionType\":\"TransferFloraToTBMM\",\"TxCode\":\"FCDPW63PC35AKF73040661\"},{\"ActionType\":\"DEBIT\",\"Amount\":50.00000,\"ReceiverName\":\"MD Imran\",\"ReceiverNumber\":8801756200700,\"SenderName\":\"MIZANUR RAHMAN\",\"SenderNumber\":8801710754881,\"TransactionDate\":\"01-09-2020\",\"TransactionType\":\"MPAY\",\"TxCode\":\"MPAYBLL7U8EJWW84918476\"},{\"ActionType\":\"CREDIT\",\"Amount\":200.00000,\"ReceiverName\":\"MIZANUR RAHMAN\",\"ReceiverNumber\":8801710754881,\"SenderName\":\"Arena BD. Ltd\",\"SenderNumber\":8801835416692,\"TransactionDate\":\"12-08-2020\",\"TransactionType\":\"TopUp\",\"TxCode\":\"TopUp4JYHPQV1RK86645477\"},{\"ActionType\":\"CREDIT\",\"Amount\":10.00000,\"ReceiverName\":\"MIZANUR RAHMAN\",\"ReceiverNumber\":8801710754881,\"SenderName\":\"t- Cash\",\"SenderNumber\":8800000699999,\"TransactionDate\":\"06-08-2020\",\"TransactionType\":\"TransferFloraToTBMM\",\"TxCode\":\"FCDPJADEJEA1L35689619\"},{\"ActionType\":\"CREDIT\",\"Amount\":200.00000,\"ReceiverName\":\"MIZANUR RAHMAN\",\"ReceiverNumber\":8801710754881,\"SenderName\":\"t- Cash\",\"SenderNumber\":8800000699999,\"TransactionDate\":\"09-07-2020\",\"TransactionType\":\"TransferFloraToTBMM\",\"TxCode\":\"FCDP51UUE94C3N39844190\"},{\"ActionType\":\"CREDIT\",\"Amount\":50.00000,\"ReceiverName\":\"MIZANUR RAHMAN\",\"ReceiverNumber\":8801710754881,\"SenderName\":\"t- Cash\",\"SenderNumber\":8800000699999,\"TransactionDate\":\"29-06-2020\",\"TransactionType\":\"TransferFloraToTBMM\",\"TxCode\":\"FCDPB2HYFI6B3O89766186\"},{\"ActionType\":\"CREDIT\",\"Amount\":10.00000,\"ReceiverName\":\"MIZANUR RAHMAN\",\"ReceiverNumber\":8801710754881,\"SenderName\":\"t- Cash\",\"SenderNumber\":8800000699999,\"TransactionDate\":\"24-06-2020\",\"TransactionType\":\"TransferFloraToTBMM\",\"TxCode\":\"FCDP3JIGG6IPBB2657891\"},{\"ActionType\":\"CREDIT\",\"Amount\":50.00000,\"ReceiverName\":\"MIZANUR RAHMAN\",\"ReceiverNumber\":8801710754881,\"SenderName\":\"Arena BD. Ltd\",\"SenderNumber\":8801835416692,\"TransactionDate\":\"19-06-2020\",\"TransactionType\":\"TopUp\",\"TxCode\":\"TopUp4NA4B6YR561461471\"},{\"ActionType\":\"CREDIT\",\"Amount\":10.00000,\"ReceiverName\":\"MIZANUR RAHMAN\",\"ReceiverNumber\":8801710754881,\"SenderName\":\"t- Cash\",\"SenderNumber\":8800000699999,\"TransactionDate\":\"19-03-2020\",\"TransactionType\":\"TransferFloraToTBMM\",\"TxCode\":\"FCDPCFR4PSMWK653535710\"},{\"ActionType\":\"CREDIT\",\"Amount\":10.00000,\"ReceiverName\":\"MIZANUR RAHMAN\",\"ReceiverNumber\":8801710754881,\"SenderName\":\"t- Cash\",\"SenderNumber\":8800000699999,\"TransactionDate\":\"09-03-2020\",\"TransactionType\":\"TransferFloraToTBMM\",\"TxCode\":\"FCDPLG94BO5LJ117236494\"},{\"ActionType\":\"CREDIT\",\"Amount\":10.00000,\"ReceiverName\":\"MIZANUR RAHMAN\",\"ReceiverNumber\":8801710754881,\"SenderName\":\"t- Cash\",\"SenderNumber\":8800000699999,\"TransactionDate\":\"23-02-2020\",\"TransactionType\":\"TransferFloraToTBMM\",\"TxCode\":\"FCDPENSTF5B5NO50342975\"},{\"ActionType\":\"CREDIT\",\"Amount\":10.00000,\"ReceiverName\":\"MIZANUR RAHMAN\",\"ReceiverNumber\":8801710754881,\"SenderName\":\"t- Cash\",\"SenderNumber\":8800000699999,\"TransactionDate\":\"16-02-2020\",\"TransactionType\":\"TransferFloraToTBMM\",\"TxCode\":\"FCDP13YGR36RHU8811993\"},{\"ActionType\":\"CREDIT\",\"Amount\":10.00000,\"ReceiverName\":\"MIZANUR RAHMAN\",\"ReceiverNumber\":8801710754881,\"SenderName\":\"Arena BD. Ltd\",\"SenderNumber\":8801835416692,\"TransactionDate\":\"13-02-2020\",\"TransactionType\":\"TopUp\",\"TxCode\":\"TopUpJDA4ZAMU9C44575541\"},{\"ActionType\":\"CREDIT\",\"Amount\":5.00000,\"ReceiverName\":\"MIZANUR RAHMAN\",\"ReceiverNumber\":8801710754881,\"SenderName\":\"Mehedi Arif\",\"SenderNumber\":8801913292769,\"TransactionDate\":\"01-01-2020\",\"TransactionType\":\"Pay\",\"TxCode\":\"PA8SFRJ3C5DE5603166\"},{\"ActionType\":\"DEBIT\",\"Amount\":50.00000,\"ReceiverName\":\"Arena BD. Ltd\",\"ReceiverNumber\":8801835416692,\"SenderName\":\"MIZANUR RAHMAN\",\"SenderNumber\":8801710754881,\"TransactionDate\":\"15-12-2019\",\"TransactionType\":\"TopUp\",\"TxCode\":\"TOPUP7OTAJA85B49183820\"},{\"ActionType\":\"DEBIT\",\"Amount\":10.00000,\"ReceiverName\":\"Arena BD. Ltd\",\"ReceiverNumber\":8801835416692,\"SenderName\":\"MIZANUR RAHMAN\",\"SenderNumber\":8801710754881,\"TransactionDate\":\"15-12-2019\",\"TransactionType\":\"TopUp\",\"TxCode\":\"TOPUPF4NTDG3J6W69196585\"},{\"ActionType\":\"DEBIT\",\"Amount\":199.00000,\"ReceiverName\":\"Arena BD. Ltd\",\"ReceiverNumber\":8801835416692,\"SenderName\":\"MIZANUR RAHMAN\",\"SenderNumber\":8801710754881,\"TransactionDate\":\"22-11-2019\",\"TransactionType\":\"TopUp\",\"TxCode\":\"TOPUPJVNSIHKTWT62954920\"},{\"ActionType\":\"DEBIT\",\"Amount\":20.00000,\"ReceiverName\":\"Arena BD. Ltd\",\"ReceiverNumber\":8801835416692,\"SenderName\":\"MIZANUR RAHMAN\",\"SenderNumber\":8801710754881,\"TransactionDate\":\"05-11-2019\",\"TransactionType\":\"TopUp\",\"TxCode\":\"TOPUPGL5YLHIQFR92012823\"},{\"ActionType\":\"CREDIT\",\"Amount\":300.00000,\"ReceiverName\":\"MIZANUR RAHMAN\",\"ReceiverNumber\":8801710754881,\"SenderName\":\"t- Cash\",\"SenderNumber\":8800000699999,\"TransactionDate\":\"29-10-2019\",\"TransactionType\":\"TransferFloraToTBMM\",\"TxCode\":\"FCDPE1XH9REA4F55051870\"},{\"ActionType\":\"DEBIT\",\"Amount\":50.00000,\"ReceiverName\":\"Arena BD. Ltd\",\"ReceiverNumber\":8801835416692,\"SenderName\":\"MIZANUR RAHMAN\",\"SenderNumber\":8801710754881,\"TransactionDate\":\"21-10-2019\",\"TransactionType\":\"TopUp\",\"TxCode\":\"TOPUPACMP74W1FC76437528\"},{\"ActionType\":\"DEBIT\",\"Amount\":50.00000,\"ReceiverName\":\"Arena BD. Ltd\",\"ReceiverNumber\":8801835416692,\"SenderName\":\"MIZANUR RAHMAN\",\"SenderNumber\":8801710754881,\"TransactionDate\":\"14-10-2019\",\"TransactionType\":\"TopUp\",\"TxCode\":\"TOPUP6IGVBH6XBT94499\"},{\"ActionType\":\"DEBIT\",\"Amount\":199.00000,\"ReceiverName\":\"Arena BD. Ltd\",\"ReceiverNumber\":8801835416692,\"SenderName\":\"MIZANUR RAHMAN\",\"SenderNumber\":8801710754881,\"TransactionDate\":\"28-09-2019\",\"TransactionType\":\"TopUp\",\"TxCode\":\"TOPUPSMP5ODYFJZ3810677\"},{\"ActionType\":\"DEBIT\",\"Amount\":20.00000,\"ReceiverName\":\"Arena BD. Ltd\",\"ReceiverNumber\":8801835416692,\"SenderName\":\"MIZANUR RAHMAN\",\"SenderNumber\":8801710754881,\"TransactionDate\":\"22-09-2019\",\"TransactionType\":\"TopUp\",\"TxCode\":\"TOPUPPAECEKUTDK35937033\"},{\"ActionType\":\"DEBIT\",\"Amount\":50.00000,\"ReceiverName\":\"Arena BD. Ltd\",\"ReceiverNumber\":8801835416692,\"SenderName\":\"MIZANUR RAHMAN\",\"SenderNumber\":8801710754881,\"TransactionDate\":\"22-09-2019\",\"TransactionType\":\"TopUp\",\"TxCode\":\"TOPUPSLCFJEW3YY94387362\"},{\"ActionType\":\"DEBIT\",\"Amount\":50.00000,\"ReceiverName\":\"Arena BD. Ltd\",\"ReceiverNumber\":8801835416692,\"SenderName\":\"MIZANUR RAHMAN\",\"SenderNumber\":8801710754881,\"TransactionDate\":\"18-09-2019\",\"TransactionType\":\"TopUp\",\"TxCode\":\"TOPUPN94K7H1OU737618266\"},{\"ActionType\":\"DEBIT\",\"Amount\":20.00000,\"ReceiverName\":\"t- Cash\",\"ReceiverNumber\":8800000699999,\"SenderName\":\"MIZANUR RAHMAN\",\"SenderNumber\":8801710754881,\"TransactionDate\":\"17-09-2019\",\"TransactionType\":\"Withdraw\",\"TxCode\":\"COFPIWRLVWK693892498\"},{\"ActionType\":\"DEBIT\",\"Amount\":10.00000,\"ReceiverName\":\"t- Cash\",\"ReceiverNumber\":8800000699999,\"SenderName\":\"MIZANUR RAHMAN\",\"SenderNumber\":8801710754881,\"TransactionDate\":\"17-09-2019\",\"TransactionType\":\"Withdraw\",\"TxCode\":\"CORU6P574EAP49022520\"},{\"ActionType\":\"DEBIT\",\"Amount\":10.00000,\"ReceiverName\":\"Arena BD. Ltd\",\"ReceiverNumber\":8801835416692,\"SenderName\":\"MIZANUR RAHMAN\",\"SenderNumber\":8801710754881,\"TransactionDate\":\"16-09-2019\",\"TransactionType\":\"TopUp\",\"TxCode\":\"TOPUPX9ZVF4YO1861562549\"},{\"ActionType\":\"DEBIT\",\"Amount\":50.00000,\"ReceiverName\":\"Arena BD. Ltd\",\"ReceiverNumber\":8801835416692,\"SenderName\":\"MIZANUR RAHMAN\",\"SenderNumber\":8801710754881,\"TransactionDate\":\"14-09-2019\",\"TransactionType\":\"TopUp\",\"TxCode\":\"TOPUPSOBNKNJSVQ80553757\"},{\"ActionType\":\"DEBIT\",\"Amount\":10.00000,\"ReceiverName\":\"Arena BD. Ltd\",\"ReceiverNumber\":8801835416692,\"SenderName\":\"MIZANUR RAHMAN\",\"SenderNumber\":8801710754881,\"TransactionDate\":\"11-09-2019\",\"TransactionType\":\"TopUp\",\"TxCode\":\"TOPUPVNF5GX2YUR35968674\"},{\"ActionType\":\"CREDIT\",\"Amount\":300.00000,\"ReceiverName\":\"MIZANUR RAHMAN\",\"ReceiverNumber\":8801710754881,\"SenderName\":\"t- Cash\",\"SenderNumber\":8800000699999,\"TransactionDate\":\"09-09-2019\",\"TransactionType\":\"TransferFloraToTBMM\",\"TxCode\":\"FCDP3EWDTPLONL10110130\"},{\"ActionType\":\"DEBIT\",\"Amount\":20.00000,\"ReceiverName\":\"Walletmix Limited\",\"ReceiverNumber\":8801974002244,\"SenderName\":\"MIZANUR RAHMAN\",\"SenderNumber\":8801710754881,\"TransactionDate\":\"01-09-2019\",\"TransactionType\":\"ATIME\",\"TxCode\":\"ATIME8LIZQBDQLF70549441\"},{\"ActionType\":\"CREDIT\",\"Amount\":20.00000,\"ReceiverName\":\"MIZANUR RAHMAN\",\"ReceiverNumber\":8801710754881,\"SenderName\":\"Walletmix Limited\",\"SenderNumber\":8801974002244,\"TransactionDate\":\"01-09-2019\",\"TransactionType\":\"ATIME\",\"TxCode\":\"ATIMEEJHNHEMHMZ49374123\"},{\"ActionType\":\"DEBIT\",\"Amount\":20.00000,\"ReceiverName\":\"Walletmix Limited\",\"ReceiverNumber\":8801974002244,\"SenderName\":\"MIZANUR RAHMAN\",\"SenderNumber\":8801710754881,\"TransactionDate\":\"01-09-2019\",\"TransactionType\":\"ATIME\",\"TxCode\":\"ATIMEOJJ8ORF7N597496797\"},{\"ActionType\":\"DEBIT\",\"Amount\":199.00000,\"ReceiverName\":\"Walletmix Limited\",\"ReceiverNumber\":8801974002244,\"SenderName\":\"MIZANUR RAHMAN\",\"SenderNumber\":8801710754881,\"TransactionDate\":\"28-08-2019\",\"TransactionType\":\"ATIME\",\"TxCode\":\"ATIMENGTY4LYSVF87092362\"},{\"ActionType\":\"DEBIT\",\"Amount\":199.00000,\"ReceiverName\":\"Walletmix Limited\",\"ReceiverNumber\":8801974002244,\"SenderName\":\"MIZANUR RAHMAN\",\"SenderNumber\":8801710754881,\"TransactionDate\":\"06-08-2019\",\"TransactionType\":\"ATIME\",\"TxCode\":\"ATIMEPPK1JKM81R56274691\"},{\"ActionType\":\"DEBIT\",\"Amount\":50.00000,\"ReceiverName\":\"Walletmix Limited\",\"ReceiverNumber\":8801974002244,\"SenderName\":\"MIZANUR RAHMAN\",\"SenderNumber\":8801710754881,\"TransactionDate\":\"06-08-2019\",\"TransactionType\":\"ATIME\",\"TxCode\":\"ATIMEQEJ78P6RDK71911743\"},{\"ActionType\":\"DEBIT\",\"Amount\":20.00000,\"ReceiverName\":\"Walletmix Limited\",\"ReceiverNumber\":8801974002244,\"SenderName\":\"MIZANUR RAHMAN\",\"SenderNumber\":8801710754881,\"TransactionDate\":\"29-07-2019\",\"TransactionType\":\"ATIME\",\"TxCode\":\"ATIMEVHCDDS722V60025174\"},{\"ActionType\":\"CREDIT\",\"Amount\":100.00000,\"ReceiverName\":\"MIZANUR RAHMAN\",\"ReceiverNumber\":8801710754881,\"SenderName\":\"t- Cash\",\"SenderNumber\":8800000699999,\"TransactionDate\":\"23-07-2019\",\"TransactionType\":\"TransferFloraToTBMM\",\"TxCode\":\"FCDPLIEX6OOKDX48685557\"},{\"ActionType\":\"CREDIT\",\"Amount\":500.00000,\"ReceiverName\":\"MIZANUR RAHMAN\",\"ReceiverNumber\":8801710754881,\"SenderName\":\"t- Cash\",\"SenderNumber\":8800000699999,\"TransactionDate\":\"23-07-2019\",\"TransactionType\":\"TransferFloraToTBMM\",\"TxCode\":\"FCDPN1DCDCZOPQ52972187\"},{\"ActionType\":\"DEBIT\",\"Amount\":199.00000,\"ReceiverName\":\"Walletmix Limited\",\"ReceiverNumber\":8801974002244,\"SenderName\":\"MIZANUR RAHMAN\",\"SenderNumber\":8801710754881,\"TransactionDate\":\"19-07-2019\",\"TransactionType\":\"ATIME\",\"TxCode\":\"ATIMEN4H5YC5Q7H90869230\"},{\"ActionType\":\"CREDIT\",\"Amount\":200.00000,\"ReceiverName\":\"MIZANUR RAHMAN\",\"ReceiverNumber\":8801710754881,\"SenderName\":\"t- Cash\",\"SenderNumber\":8800000699999,\"TransactionDate\":\"19-07-2019\",\"TransactionType\":\"TransferFloraToTBMM\",\"TxCode\":\"FCDPRDNIM5RIMQ77069886\"},{\"ActionType\":\"DEBIT\",\"Amount\":100.00000,\"ReceiverName\":\"Walletmix Limited\",\"ReceiverNumber\":8801974002244,\"SenderName\":\"MIZANUR RAHMAN\",\"SenderNumber\":8801710754881,\"TransactionDate\":\"07-07-2019\",\"TransactionType\":\"ATIME\",\"TxCode\":\"ATIMEKPANTDIHGK44166618\"}]\n";
//        String statement ="\"[]\"";
        // String statement= "\"[{\\\"TxCode\\\":\\\"PAX7NKB84ZOF12940049\\\",\\\"TransactionDate\\\":\\\"21-09-2020\\\",\\\"TransactionType\\\":\\\"Pay\\\",\\\"SenderNumber\\\":8801913439865,\\\"ReceiverNumber\\\":8801833184026,\\\"SenderName\\\":\\\"MD.EMRAN HOSSAIN\\\",\\\"ReceiverName\\\":\\\"MONWAR JAHAN\\\",\\\"ActionType\\\":\\\"DEBIT\\\",\\\"Amount\\\":15.00000},{\\\"TxCode\\\":\\\"PAX7NKB84ZOF12940049\\\",\\\"TransactionDate\\\":\\\"21-09-2020\\\",\\\"TransactionType\\\":\\\"Pay\\\",\\\"SenderNumber\\\":8801913439865,\\\"ReceiverNumber\\\":8800000670002,\\\"SenderName\\\":\\\"MD.EMRAN HOSSAIN\\\",\\\"ReceiverName\\\":\\\"Commission Distribution\\\",\\\"ActionType\\\":\\\"DEBIT\\\",\\\"Amount\\\":5.00000},{\\\"TxCode\\\":\\\"PA3FLR72YNVL75248610\\\",\\\"TransactionDate\\\":\\\"21-09-2020\\\",\\\"TransactionType\\\":\\\"Pay\\\",\\\"SenderNumber\\\":8801833184036,\\\"ReceiverNumber\\\":8801913439865,\\\"SenderName\\\":\\\"MD. MOSFIQUR RAHMAN\\\",\\\"ReceiverName\\\":\\\"MD.EMRAN HOSSAIN\\\",\\\"ActionType\\\":\\\"CREDIT\\\",\\\"Amount\\\":110.00000}]\"";
        return statement;
    }

    @GetMapping(path = "/GetUserType", produces = "application/json")
    public String userType(@RequestParam("userNumber") String userNumber) {
        if (redisDao.getValue(userNumber) != null) {
            String value = redisDao.getValue(userNumber);
            if(value.contains("ekyc")){
                String usertype = "{\"key\":\"Merchant\",\"value\":\"T-cash Test Merchant\"}";
            }
            if (userNumber.equalsIgnoreCase("8801729069555") || userNumber.equalsIgnoreCase("8801844590344") || userNumber.equalsIgnoreCase("8801898762360")) {
                String usertype = "{\"key\":\"Merchant\",\"value\":\"T-cash Test Merchant\"}";
                return usertype;
            } else if (userNumber.equalsIgnoreCase("8801867981406") || userNumber.equalsIgnoreCase("8801817182040")
                || userNumber.equalsIgnoreCase("8801812345238")
            ) {
                String usertype = "{\"key\":\"Paypoint\",\"value\":\"TADL Agent Test\"}";
                return usertype;
            } else {
                String usertype = "{\"key\":\"User\",\"value\":\"MIZANUR  RAHMAN\"}";
                return usertype;
            }
        }
        String usertype = "{\"key\":\"Validation\",\"value\":\"User not found\"}";
        return usertype;
    }

    @GetMapping(path = "/GetNIDServiceList", produces = "application/json")
    public String nidServiceList() {
        return "\"[\\u000d\\u000a{\\u000d\\u000a  \\\"Id\\\":\\\"1\\\",\\u000d\\u000a  \\\"Description\\\":\\\"NID Information Correction\\\",\\u000d\\u000a  \\\"Amount\\\":\\\"0\\\"\\u000d\\u000a},\\u000d\\u000a{\\u000d\\u000a  \\\"Id\\\":\\\"2\\\",\\u000d\\u000a  \\\"Description\\\":\\\"Other Information Correction\\\",\\u000d\\u000a  \\\"Amount\\\":\\\"0\\\"\\u000d\\u000a},\\u000d\\u000a{\\u000d\\u000a  \\\"Id\\\":\\\"3\\\",\\u000d\\u000a  \\\"Description\\\":\\\"Both Information Correction\\\",\\u000d\\u000a  \\\"Amount\\\":\\\"0\\\"\\u000d\\u000a},\\u000d\\u000a{\\u000d\\u000a  \\\"Id\\\":\\\"4\\\",\\u000d\\u000a  \\\"Description\\\":\\\"Duplicate Regular\\\",\\u000d\\u000a  \\\"Amount\\\":\\\"0\\\"\\u000d\\u000a},\\u000d\\u000a{\\u000d\\u000a  \\\"Id\\\":\\\"5\\\",\\u000d\\u000a  \\\"Description\\\":\\\"Duplicate Urgent\\\",\\u000d\\u000a  \\\"Amount\\\":\\\"0\\\"\\u000d\\u000a}\\u000d\\u000a]\"";
    }

    @PostMapping(path = "/GetAllTransactionTypes", produces = "application/json")
    public String getTransactionType() {
        return "\"[{\\\"TransactionTypeCode\\\":\\\"DP\\\",\\\"TransactionType\\\":\\\"Deposit\\\",\\\"UsedKeyword\\\":\\\"DP\\\"},{\\\"TransactionTypeCode\\\":\\\"CO\\\",\\\"TransactionType\\\":\\\"Withdraw\\\",\\\"UsedKeyword\\\":\\\"CO\\\"},{\\\"TransactionTypeCode\\\":\\\"BR\\\",\\\"TransactionType\\\":\\\"Bill Raised\\\",\\\"UsedKeyword\\\":\\\"BR\\\"},{\\\"TransactionTypeCode\\\":\\\"PY\\\",\\\"TransactionType\\\":\\\"Pay\\\",\\\"UsedKeyword\\\":\\\"PAY\\\"},{\\\"TransactionTypeCode\\\":\\\"RL\\\",\\\"TransactionType\\\":\\\"Reload\\\",\\\"UsedKeyword\\\":\\\"Reload\\\"},{\\\"TransactionTypeCode\\\":\\\"PR\\\",\\\"TransactionType\\\":\\\"PPREG\\\",\\\"UsedKeyword\\\":\\\"PPREG\\\"},{\\\"TransactionTypeCode\\\":\\\"TU\\\",\\\"TransactionType\\\":\\\"TopUp\\\",\\\"UsedKeyword\\\":\\\"TopUp\\\"},{\\\"TransactionTypeCode\\\":\\\"RC\\\",\\\"TransactionType\\\":\\\"Request CashOut\\\",\\\"UsedKeyword\\\":\\\"RC\\\"},{\\\"TransactionTypeCode\\\":\\\"BA\\\",\\\"TransactionType\\\":\\\"BAL\\\",\\\"UsedKeyword\\\":\\\"BAL\\\"},{\\\"TransactionTypeCode\\\":\\\"CP\\\",\\\"TransactionType\\\":\\\"CP\\\",\\\"UsedKeyword\\\":\\\"CP\\\"},{\\\"TransactionTypeCode\\\":\\\"HELP\\\",\\\"TransactionType\\\":\\\"HELP\\\",\\\"UsedKeyword\\\":\\\"HELP\\\"},{\\\"TransactionTypeCode\\\":\\\"REG\\\",\\\"TransactionType\\\":\\\"REG\\\",\\\"UsedKeyword\\\":\\\"REG\\\"},{\\\"TransactionTypeCode\\\":\\\"BP\\\",\\\"TransactionType\\\":\\\"Bill Pay\\\",\\\"UsedKeyword\\\":\\\"BP\\\"},{\\\"TransactionTypeCode\\\":\\\"HIST\\\",\\\"TransactionType\\\":\\\"HIST\\\",\\\"UsedKeyword\\\":\\\"HIST\\\"},{\\\"TransactionTypeCode\\\":\\\"IP\\\",\\\"TransactionType\\\":\\\"INTERESTPAY\\\",\\\"UsedKeyword\\\":\\\"INTERESTPAY\\\"},{\\\"TransactionTypeCode\\\":\\\"AlicoPay\\\",\\\"TransactionType\\\":\\\"AlicoPay\\\",\\\"UsedKeyword\\\":\\\"MA\\\"},{\\\"TransactionTypeCode\\\":\\\"ACPS\\\",\\\"TransactionType\\\":\\\"ACPS\\\",\\\"UsedKeyword\\\":\\\"ACPS\\\"},{\\\"TransactionTypeCode\\\":\\\"BNRF\\\",\\\"TransactionType\\\":\\\"BNRF\\\",\\\"UsedKeyword\\\":\\\"BNRF\\\"},{\\\"TransactionTypeCode\\\":\\\"ACC\\\",\\\"TransactionType\\\":\\\"ACC\\\",\\\"UsedKeyword\\\":\\\"ACC\\\"},{\\\"TransactionTypeCode\\\":\\\"RUMC\\\",\\\"TransactionType\\\":\\\"RUMC\\\",\\\"UsedKeyword\\\":\\\"RUMC\\\"},{\\\"TransactionTypeCode\\\":\\\"DESCO\\\",\\\"TransactionType\\\":\\\"DESCO\\\",\\\"UsedKeyword\\\":\\\"DESCO\\\"},{\\\"TransactionTypeCode\\\":\\\"DIP\\\",\\\"TransactionType\\\":\\\"DIP\\\",\\\"UsedKeyword\\\":\\\"DIP\\\"},{\\\"TransactionTypeCode\\\":\\\"MBR\\\",\\\"TransactionType\\\":\\\"MBR\\\",\\\"UsedKeyword\\\":\\\"MBR\\\"},{\\\"TransactionTypeCode\\\":\\\"MBP\\\",\\\"TransactionType\\\":\\\"MBP\\\",\\\"UsedKeyword\\\":\\\"MBP\\\"},{\\\"TransactionTypeCode\\\":\\\"SAGC\\\",\\\"TransactionType\\\":\\\"SAGC\\\",\\\"UsedKeyword\\\":\\\"SAGC\\\"},{\\\"TransactionTypeCode\\\":\\\"AIMS\\\",\\\"TransactionType\\\":\\\"AIMS\\\",\\\"UsedKeyword\\\":\\\"AIMS\\\"},{\\\"TransactionTypeCode\\\":\\\"SRCS\\\",\\\"TransactionType\\\":\\\"SRCS\\\",\\\"UsedKeyword\\\":\\\"SRCS\\\"},{\\\"TransactionTypeCode\\\":\\\"SPHS\\\",\\\"TransactionType\\\":\\\"SPHS\\\",\\\"UsedKeyword\\\":\\\"SPHS\\\"},{\\\"TransactionTypeCode\\\":\\\"ABN\\\",\\\"TransactionType\\\":\\\"ABN\\\",\\\"UsedKeyword\\\":\\\"ABN\\\"},{\\\"TransactionTypeCode\\\":\\\"MMA\\\",\\\"TransactionType\\\":\\\"MMA\\\",\\\"UsedKeyword\\\":\\\"MMA\\\"},{\\\"TransactionTypeCode\\\":\\\"MCSK\\\",\\\"TransactionType\\\":\\\"MCSK\\\",\\\"UsedKeyword\\\":\\\"MCSK\\\"},{\\\"TransactionTypeCode\\\":\\\"BNCD\\\",\\\"TransactionType\\\":\\\"BNCD\\\",\\\"UsedKeyword\\\":\\\"BNCD\\\"},{\\\"TransactionTypeCode\\\":\\\"PAYC\\\",\\\"TransactionType\\\":\\\"PAYC\\\",\\\"UsedKeyword\\\":\\\"PAYC\\\"},{\\\"TransactionTypeCode\\\":\\\"MLIC\\\",\\\"TransactionType\\\":\\\"MLIC\\\",\\\"UsedKeyword\\\":\\\"MLIC\\\"},{\\\"TransactionTypeCode\\\":\\\"Interest\\\",\\\"TransactionType\\\":\\\"Interest\\\",\\\"UsedKeyword\\\":\\\"Interest\\\"},{\\\"TransactionTypeCode\\\":\\\"MIST\\\",\\\"TransactionType\\\":\\\"MIST\\\",\\\"UsedKeyword\\\":\\\"MIST\\\"},{\\\"TransactionTypeCode\\\":\\\"BUP\\\",\\\"TransactionType\\\":\\\"BUP\\\",\\\"UsedKeyword\\\":\\\"BUP\\\"},{\\\"TransactionTypeCode\\\":\\\"RCPSC\\\",\\\"TransactionType\\\":\\\"RCPSC\\\",\\\"UsedKeyword\\\":\\\"RCPSC\\\"},{\\\"TransactionTypeCode\\\":\\\"CPSCR\\\",\\\"TransactionType\\\":\\\"CPSCR\\\",\\\"UsedKeyword\\\":\\\"CPSCR\\\"},{\\\"TransactionTypeCode\\\":\\\"DPDC\\\",\\\"TransactionType\\\":\\\"DPDC\\\",\\\"UsedKeyword\\\":\\\"DPDC\\\"},{\\\"TransactionTypeCode\\\":\\\"PILIL\\\",\\\"TransactionType\\\":\\\"PILIL\\\",\\\"UsedKeyword\\\":\\\"PILIL\\\"},{\\\"TransactionTypeCode\\\":\\\"Demo\\\",\\\"TransactionType\\\":\\\"Demo\\\",\\\"UsedKeyword\\\":\\\"Demo\\\"},{\\\"TransactionTypeCode\\\":\\\"CESC\\\",\\\"TransactionType\\\":\\\"CESC\\\",\\\"UsedKeyword\\\":\\\"CESC\\\"},{\\\"TransactionTypeCode\\\":\\\"RUMC11\\\",\\\"TransactionType\\\":\\\"RUMC11\\\",\\\"UsedKeyword\\\":\\\"RUMC11\\\"},{\\\"TransactionTypeCode\\\":\\\"BAF\\\",\\\"TransactionType\\\":\\\"BAF\\\",\\\"UsedKeyword\\\":\\\"BAF\\\"},{\\\"TransactionTypeCode\\\":\\\"PISE\\\",\\\"TransactionType\\\":\\\"PISE\\\",\\\"UsedKeyword\\\":\\\"PISE\\\"},{\\\"TransactionTypeCode\\\":\\\"ZAKAT\\\",\\\"TransactionType\\\":\\\"ZAKAT\\\",\\\"UsedKeyword\\\":\\\"ZAKAT\\\"},{\\\"TransactionTypeCode\\\":\\\"BLRSC\\\",\\\"TransactionType\\\":\\\"BLRSC\\\",\\\"UsedKeyword\\\":\\\"BLRSC\\\"},{\\\"TransactionTypeCode\\\":\\\"TMSR\\\",\\\"TransactionType\\\":\\\"TMSR\\\",\\\"UsedKeyword\\\":\\\"TMSR\\\"},{\\\"TransactionTypeCode\\\":\\\"NID\\\",\\\"TransactionType\\\":\\\"NID\\\",\\\"UsedKeyword\\\":\\\"NID\\\"},{\\\"TransactionTypeCode\\\":\\\"KDA\\\",\\\"TransactionType\\\":\\\"KDA\\\",\\\"UsedKeyword\\\":\\\"KDA\\\"},{\\\"TransactionTypeCode\\\":\\\"DRMC\\\",\\\"TransactionType\\\":\\\"DRMC\\\",\\\"UsedKeyword\\\":\\\"DRMC\\\"},{\\\"TransactionTypeCode\\\":\\\"BAUET\\\",\\\"TransactionType\\\":\\\"BAUET\\\",\\\"UsedKeyword\\\":\\\"BAUET\\\"},{\\\"TransactionTypeCode\\\":\\\"NCPSC\\\",\\\"TransactionType\\\":\\\"NCPSC\\\",\\\"UsedKeyword\\\":\\\"NCPSC\\\"},{\\\"TransactionTypeCode\\\":\\\"DCESC\\\",\\\"TransactionType\\\":\\\"DCESC\\\",\\\"UsedKeyword\\\":\\\"DCESC\\\"},{\\\"TransactionTypeCode\\\":\\\"FCDP\\\",\\\"TransactionType\\\":\\\"TransferFloraToTBMM\\\",\\\"UsedKeyword\\\":\\\"FCDP\\\"},{\\\"TransactionTypeCode\\\":\\\"FCCO\\\",\\\"TransactionType\\\":\\\"TransferTBMMTOFlora\\\",\\\"UsedKeyword\\\":\\\"FCCO\\\"},{\\\"TransactionTypeCode\\\":\\\"CHARGE\\\",\\\"TransactionType\\\":\\\"CHARGE\\\",\\\"UsedKeyword\\\":\\\"CHARGE\\\"},{\\\"TransactionTypeCode\\\":\\\"Salary\\\",\\\"TransactionType\\\":\\\"Salary\\\",\\\"UsedKeyword\\\":\\\"Salary\\\"},{\\\"TransactionTypeCode\\\":\\\"Soinik\\\",\\\"TransactionType\\\":\\\"Soinik\\\",\\\"UsedKeyword\\\":\\\"Soinik\\\"},{\\\"TransactionTypeCode\\\":\\\"JBA\\\",\\\"TransactionType\\\":\\\"JBA\\\",\\\"UsedKeyword\\\":\\\"JBA\\\"},{\\\"TransactionTypeCode\\\":\\\"BISC\\\",\\\"TransactionType\\\":\\\"BISC\\\",\\\"UsedKeyword\\\":\\\"BISC\\\"},{\\\"TransactionTypeCode\\\":\\\"MGSC\\\",\\\"TransactionType\\\":\\\"MGSC\\\",\\\"UsedKeyword\\\":\\\"MGSC\\\"},{\\\"TransactionTypeCode\\\":\\\"MCPSC\\\",\\\"TransactionType\\\":\\\"MCPSC\\\",\\\"UsedKeyword\\\":\\\"MCPSC\\\"},{\\\"TransactionTypeCode\\\":\\\"WZPDC\\\",\\\"TransactionType\\\":\\\"WZPDC\\\",\\\"UsedKeyword\\\":\\\"WZPDC\\\"},{\\\"TransactionTypeCode\\\":\\\"BGBPS\\\",\\\"TransactionType\\\":\\\"BGBPS\\\",\\\"UsedKeyword\\\":\\\"BGBPS\\\"},{\\\"TransactionTypeCode\\\":\\\"DPSC\\\",\\\"TransactionType\\\":\\\"DPSC\\\",\\\"UsedKeyword\\\":\\\"DPSC\\\"},{\\\"TransactionTypeCode\\\":\\\"LORDS\\\",\\\"TransactionType\\\":\\\"LORDS\\\",\\\"UsedKeyword\\\":\\\"LORDS\\\"},{\\\"TransactionTypeCode\\\":\\\"DCGPC\\\",\\\"TransactionType\\\":\\\"DCGPC\\\",\\\"UsedKeyword\\\":\\\"DCGPC\\\"},{\\\"TransactionTypeCode\\\":\\\"SPAY\\\",\\\"TransactionType\\\":\\\"SPAY\\\",\\\"UsedKeyword\\\":\\\"SPAY\\\"},{\\\"TransactionTypeCode\\\":\\\"LPSC\\\",\\\"TransactionType\\\":\\\"LPSC\\\",\\\"UsedKeyword\\\":\\\"LPSC\\\"},{\\\"TransactionTypeCode\\\":\\\"BTCL\\\",\\\"TransactionType\\\":\\\"BTCL\\\",\\\"UsedKeyword\\\":\\\"BTCL\\\"},{\\\"TransactionTypeCode\\\":\\\"ITHS\\\",\\\"TransactionType\\\":\\\"ITHS\\\",\\\"UsedKeyword\\\":\\\"ITHS\\\"},{\\\"TransactionTypeCode\\\":\\\"SRCC\\\",\\\"TransactionType\\\":\\\"SRCC\\\",\\\"UsedKeyword\\\":\\\"SRCC\\\"},{\\\"TransactionTypeCode\\\":\\\"ARMW\\\",\\\"TransactionType\\\":\\\"ARMW\\\",\\\"UsedKeyword\\\":\\\"ARMW\\\"},{\\\"TransactionTypeCode\\\":\\\"REMI\\\",\\\"TransactionType\\\":\\\"REMI\\\",\\\"UsedKeyword\\\":\\\"REMI\\\"},{\\\"TransactionTypeCode\\\":\\\"DESPR\\\",\\\"TransactionType\\\":\\\"DESPR\\\",\\\"UsedKeyword\\\":\\\"DESPR\\\"},{\\\"TransactionTypeCode\\\":\\\"IPSC\\\",\\\"TransactionType\\\":\\\"IPSC\\\",\\\"UsedKeyword\\\":\\\"IPSC\\\"},{\\\"TransactionTypeCode\\\":\\\"BCPSC\\\",\\\"TransactionType\\\":\\\"BCPSC\\\",\\\"UsedKeyword\\\":\\\"BCPSC\\\"},{\\\"TransactionTypeCode\\\":\\\"HCPSC\\\",\\\"TransactionType\\\":\\\"HCPSC\\\",\\\"UsedKeyword\\\":\\\"HCPSC\\\"},{\\\"TransactionTypeCode\\\":\\\"SSHSR\\\",\\\"TransactionType\\\":\\\"SSHSR\\\",\\\"UsedKeyword\\\":\\\"SSHSR\\\"},{\\\"TransactionTypeCode\\\":\\\"CBGSR\\\",\\\"TransactionType\\\":\\\"CBGSR\\\",\\\"UsedKeyword\\\":\\\"CBGSR\\\"},{\\\"TransactionTypeCode\\\":\\\"MPAY\\\",\\\"TransactionType\\\":\\\"MPAY\\\",\\\"UsedKeyword\\\":\\\"MPAY\\\"},{\\\"TransactionTypeCode\\\":\\\"ZCPSC\\\",\\\"TransactionType\\\":\\\"ZCPSC\\\",\\\"UsedKeyword\\\":\\\"ZCPSC\\\"},{\\\"TransactionTypeCode\\\":\\\"CBSMS\\\",\\\"TransactionType\\\":\\\"CBSMS\\\",\\\"UsedKeyword\\\":\\\"CBSMS\\\"},{\\\"TransactionTypeCode\\\":\\\"CPSCL\\\",\\\"TransactionType\\\":\\\"CPSCL\\\",\\\"UsedKeyword\\\":\\\"CPSCL\\\"},{\\\"TransactionTypeCode\\\":\\\"CPSCS\\\",\\\"TransactionType\\\":\\\"CPSCS\\\",\\\"UsedKeyword\\\":\\\"CPSCS\\\"},{\\\"TransactionTypeCode\\\":\\\"CCPC\\\",\\\"TransactionType\\\":\\\"CCPC\\\",\\\"UsedKeyword\\\":\\\"CCPC\\\"},{\\\"TransactionTypeCode\\\":\\\"GPCPS\\\",\\\"TransactionType\\\":\\\"GPCPS\\\",\\\"UsedKeyword\\\":\\\"GPCPS\\\"},{\\\"TransactionTypeCode\\\":\\\"AFMC\\\",\\\"TransactionType\\\":\\\"AFMC\\\",\\\"UsedKeyword\\\":\\\"AFMC\\\"},{\\\"TransactionTypeCode\\\":\\\"TITAS METERED\\\",\\\"TransactionType\\\":\\\"TITAS METERED\\\",\\\"UsedKeyword\\\":\\\"TITAS METERED\\\"},{\\\"TransactionTypeCode\\\":\\\"ATIME\\\",\\\"TransactionType\\\":\\\"ATIME\\\",\\\"UsedKeyword\\\":\\\"ATIME\\\"},{\\\"TransactionTypeCode\\\":\\\"DMMC\\\",\\\"TransactionType\\\":\\\"DMMC\\\",\\\"UsedKeyword\\\":\\\"DMMC\\\"},{\\\"TransactionTypeCode\\\":\\\"JESC\\\",\\\"TransactionType\\\":\\\"JESC\\\",\\\"UsedKeyword\\\":\\\"JESC\\\"},{\\\"TransactionTypeCode\\\":\\\"BHBFC\\\",\\\"TransactionType\\\":\\\"BHBFC\\\",\\\"UsedKeyword\\\":\\\"BHBFC\\\"},{\\\"TransactionTypeCode\\\":\\\"TITASPR\\\",\\\"TransactionType\\\":\\\"TITASPR\\\",\\\"UsedKeyword\\\":\\\"TITASPR\\\"},{\\\"TransactionTypeCode\\\":\\\"REB\\\",\\\"TransactionType\\\":\\\"REB\\\",\\\"UsedKeyword\\\":\\\"REB\\\"},{\\\"TransactionTypeCode\\\":\\\"GCPSC\\\",\\\"TransactionType\\\":\\\"GCPSC\\\",\\\"UsedKeyword\\\":\\\"GCPSC\\\"},{\\\"TransactionTypeCode\\\":\\\"RCPSC1\\\",\\\"TransactionType\\\":\\\"RCPSC1\\\",\\\"UsedKeyword\\\":\\\"RCPSC1\\\"},{\\\"TransactionTypeCode\\\":\\\"CPSCK\\\",\\\"TransactionType\\\":\\\"CPSCK\\\",\\\"UsedKeyword\\\":\\\"CPSCK\\\"},{\\\"TransactionTypeCode\\\":\\\"JCPSC\\\",\\\"TransactionType\\\":\\\"JCPSC\\\",\\\"UsedKeyword\\\":\\\"JCPSC\\\"},{\\\"TransactionTypeCode\\\":\\\"RCES\\\",\\\"TransactionType\\\":\\\"RCES\\\",\\\"UsedKeyword\\\":\\\"RCES\\\"},{\\\"TransactionTypeCode\\\":\\\"MESC\\\",\\\"TransactionType\\\":\\\"MESC\\\",\\\"UsedKeyword\\\":\\\"MESC\\\"},{\\\"TransactionTypeCode\\\":\\\"GES\\\",\\\"TransactionType\\\":\\\"GES\\\",\\\"UsedKeyword\\\":\\\"GES\\\"},{\\\"TransactionTypeCode\\\":\\\"BSISC\\\",\\\"TransactionType\\\":\\\"BSISC\\\",\\\"UsedKeyword\\\":\\\"BSISC\\\"},{\\\"TransactionTypeCode\\\":\\\"KCPSC\\\",\\\"TransactionType\\\":\\\"KCPSC\\\",\\\"UsedKeyword\\\":\\\"KCPSC\\\"},{\\\"TransactionTypeCode\\\":\\\"BNSCC\\\",\\\"TransactionType\\\":\\\"BNSCC\\\",\\\"UsedKeyword\\\":\\\"BNSCC\\\"},{\\\"TransactionTypeCode\\\":\\\"CPSCM\\\",\\\"TransactionType\\\":\\\"CPSCM\\\",\\\"UsedKeyword\\\":\\\"CPSCM\\\"},{\\\"TransactionTypeCode\\\":\\\"MSSC\\\",\\\"TransactionType\\\":\\\"MSSC\\\",\\\"UsedKeyword\\\":\\\"MSSC\\\"},{\\\"TransactionTypeCode\\\":\\\"NASCK\\\",\\\"TransactionType\\\":\\\"NASCK\\\",\\\"UsedKeyword\\\":\\\"NASCK\\\"},{\\\"TransactionTypeCode\\\":\\\"NASCC\\\",\\\"TransactionType\\\":\\\"NASCC\\\",\\\"UsedKeyword\\\":\\\"NASCC\\\"},{\\\"TransactionTypeCode\\\":\\\"B2B\\\",\\\"TransactionType\\\":\\\"B2B\\\",\\\"UsedKeyword\\\":\\\"B2B\\\"},{\\\"TransactionTypeCode\\\":\\\"CPSCSH\\\",\\\"TransactionType\\\":\\\"CPSCSH\\\",\\\"UsedKeyword\\\":\\\"CPSCSH\\\"},{\\\"TransactionTypeCode\\\":\\\"ALIC\\\",\\\"TransactionType\\\":\\\"ALIC\\\",\\\"UsedKeyword\\\":\\\"ALIC\\\"}]\"";
    }


    @PostMapping(path = "/GetLimitInfo", produces = "application/json")
    public String limitInfo(@RequestParam("userNumber") String userNumber) {
        if (redisDao.getValue(userNumber) != null) {
            String limit = "\"[   \\u000d\\u000a{\\u000d\\u000a  \\\"ServiceTypeId\\\":2,\\u000d\\u000a  \\\"ServiceName\\\":\\\"Cash In\\\",\\u000d\\u000a  \\\"DailyNumber\\\":5,\\u000d\\u000a  \\\"DailyAmount\\\":30000,\\u000d\\u000a  \\\"MonthlyNumber\\\":25,\\u000d\\u000a  \\\"DailyUsedNumber\\\":3,\\u000d\\u000a  \\\"DailyUsedAmount\\\":3000,\\u000d\\u000a  \\\"MonthlyUsedNumber\\\":15,\\u000d\\u000a  \\\"MonthlyUsedAmount\\\":153000,\\u000d\\u000a  \\\"MonthlyAmount\\\":200000\\u000d\\u000a},\\u000d\\u000a{\\u000d\\u000a  \\\"ServiceTypeId\\\":3,\\u000d\\u000a  \\\"ServiceName\\\":\\\"Cash OUT\\\",\\u000d\\u000a  \\\"DailyNumber\\\":5,\\u000d\\u000a  \\\"DailyAmount\\\":25000,\\u000d\\u000a  \\\"MonthlyNumber\\\":25,\\u000d\\u000a  \\\"DailyUsedNumber\\\":3,\\u000d\\u000a  \\\"DailyUsedAmount\\\":3000,\\u000d\\u000a  \\\"MonthlyUsedNumber\\\":15,\\u000d\\u000a  \\\"MonthlyUsedAmount\\\":103000,\\u000d\\u000a  \\\"MonthlyAmount\\\":150000\\u000d\\u000a},\\u000d\\u000a{\\u000d\\u000a  \\\"ServiceTypeId\\\":5,\\u000d\\u000a  \\\"ServiceName\\\":\\\"Send Money\\\",\\u000d\\u000a  \\\"DailyNumber\\\":20,\\u000d\\u000a  \\\"DailyAmount\\\":25000,\\u000d\\u000a  \\\"MonthlyNumber\\\":60,\\u000d\\u000a  \\\"DailyUsedNumber\\\":11,\\u000d\\u000a  \\\"DailyUsedAmount\\\":5000,\\u000d\\u000a  \\\"MonthlyUsedNumber\\\":35,\\u000d\\u000a  \\\"MonthlyUsedAmount\\\":53000,\\u000d\\u000a  \\\"MonthlyAmount\\\":75000\\u000d\\u000a},\\u000d\\u000a{\\u000d\\u000a  \\\"ServiceTypeId\\\":8,\\u000d\\u000a  \\\"ServiceName\\\":\\\"TOPUP\\\",\\u000d\\u000a  \\\"DailyNumber\\\":0,\\u000d\\u000a  \\\"DailyAmount\\\":0,\\u000d\\u000a  \\\"MonthlyNumber\\\":0,\\u000d\\u000a  \\\"DailyUsedNumber\\\":0,\\u000d\\u000a  \\\"DailyUsedAmount\\\":0,\\u000d\\u000a  \\\"MonthlyUsedNumber\\\":0,\\u000d\\u000a  \\\"MonthlyUsedAmount\\\":0,\\u000d\\u000a  \\\"MonthlyAmount\\\":0\\u000d\\u000a},\\u000d\\u000a{\\u000d\\u000a  \\\"ServiceTypeId\\\":97,\\u000d\\u000a  \\\"ServiceName\\\":\\\"Merchant Payment\\\",\\u000d\\u000a  \\\"DailyNumber\\\":0,\\u000d\\u000a  \\\"DailyAmount\\\":0,\\u000d\\u000a  \\\"MonthlyNumber\\\":0,\\u000d\\u000a  \\\"DailyUsedNumber\\\":0,\\u000d\\u000a  \\\"DailyUsedAmount\\\":0,\\u000d\\u000a  \\\"MonthlyUsedNumber\\\":0,\\u000d\\u000a  \\\"MonthlyUsedAmount\\\":0,\\u000d\\u000a  \\\"MonthlyAmount\\\":0\\u000d\\u000a},\\u000d\\u000a{\\u000d\\u000a  \\\"ServiceTypeId\\\":0,\\u000d\\u000a  \\\"ServiceName\\\":\\\"Bill Payment\\\",\\u000d\\u000a  \\\"DailyNumber\\\":0,\\u000d\\u000a  \\\"DailyAmount\\\":0,\\u000d\\u000a  \\\"MonthlyNumber\\\":0,\\u000d\\u000a  \\\"DailyUsedNumber\\\":0,\\u000d\\u000a  \\\"DailyUsedAmount\\\":0,\\u000d\\u000a  \\\"MonthlyUsedNumber\\\":0,\\u000d\\u000a  \\\"MonthlyUsedAmount\\\":0,\\u000d\\u000a  \\\"MonthlyAmount\\\":0\\u000d\\u000a}\\u000d\\u000a]\"";
            return limit;
        }
        return null;
    }

    @GetMapping(path = "/FileLoadRequest", produces = "application/json", params = "fileName")
    public String dataList(@RequestParam("fileName") String fileName, @RequestParam(name = "code", required = false) String code) {
        if (fileName.equals(INSURANCE)) {
            // return "\"[\\u000d\\u000a   {\\u000d\\u000a      \\\"Id\\\":\\\"2024dc48-6b09-4631-921d-b8b2267c85d8\\\",\\u000d\\u000a      \\\"Code\\\":\\\"ALICO\\\",\\u000d\\u000a      \\\"Description\\\":\\\"MetLife Alico\\\",\\u000d\\u000a      \\\"IconSource\\\":\\\"Metlife.png\\\",   \\u000d\\u000a\\u0009},\\u000d\\u000a   {\\u000d\\u000a      \\\"Id\\\":\\\"3cf506b7-d195-493e-b1e1-472ada7263c3\\\",\\u000d\\u000a      \\\"Code\\\":\\\"MLIC\\\",\\u000d\\u000a      \\\"Description\\\":\\\"Meghna Life Insurance\\\",\\u000d\\u000a      \\\"IconSource\\\":\\\"Meghna_Life.jpg\\\",\\u000d\\u000a\\u0009},\\u000d\\u000a   {\\u000d\\u000a      \\\"Id\\\":\\\"38cd4f96-dc39-45ce-81ce-4b2ddd3c4368\\\",\\u000d\\u000a      \\\"Code\\\":\\\"PILIL\\\",\\u000d\\u000a      \\\"Description\\\":\\\"Prime Islami Life Insurance\\\",\\u000d\\u000a      \\\"IconSource\\\":\\\"Prime_Islami.jpg\\\",\\u000d\\u000a\\u0009}\\u000d\\u000a]\"";
            return "\"[\\u000d\\u000a   {\\u000d\\u000a      \\\"Id\\\":\\\"2024dc48-6b09-4631-921d-b8b2267c85d8\\\",\\u000d\\u000a      \\\"Code\\\":\\\"MA\\\",\\u000d\\u000a      \\\"Description\\\":\\\"MetLife Alico\\\",\\u000d\\u000a      \\\"IconSource\\\":\\\"Metlife.png\\\",   \\u000d\\u000a      \\\"LabelView\\\":\\\"\\\",\\u000d\\u000a      \\\"DefaultNoOfMonth\\\":\\\"false\\\",\\u000d\\u000a      \\\"TermsAndConditions\\\":\\\"\\\",\\u000d\\u000a      \\\"TargetType\\\":\\\"\\\"\\u000d\\u000a\\u0009},\\u000d\\u000a   {\\u000d\\u000a      \\\"Id\\\":\\\"3cf506b7-d195-493e-b1e1-472ada7263c3\\\",\\u000d\\u000a      \\\"Code\\\":\\\"MLIC\\\",\\u000d\\u000a      \\\"Description\\\":\\\"Meghna Life Insurance\\\",\\u000d\\u000a      \\\"IconSource\\\":\\\"Meghna_Life.jpg\\\",\\u000d\\u000a      \\\"LabelView\\\":\\\"\\\",\\u000d\\u000a      \\\"DefaultNoOfMonth\\\":\\\"false\\\",\\u000d\\u000a      \\\"TermsAndConditions\\\":\\\"\\\",\\u000d\\u000a      \\\"TargetType\\\":\\\"\\\"\\u000d\\u000a\\u0009},\\u000d\\u000a   {\\u000d\\u000a      \\\"Id\\\":\\\"38cd4f96-dc39-45ce-81ce-4b2ddd3c4368\\\",\\u000d\\u000a      \\\"Code\\\":\\\"PILIL\\\",\\u000d\\u000a      \\\"Description\\\":\\\"Prime Islami Life Insurance\\\",\\u000d\\u000a      \\\"IconSource\\\":\\\"Prime_Islami.jpg\\\",\\u000d\\u000a\\u0009  \\\"LabelView\\\":\\\"\\\",\\u000d\\u000a\\u0009  \\\"DefaultNoOfMonth\\\":\\\"false\\\",\\u000d\\u000a\\u0009  \\\"TermsAndConditions\\\":\\\"\\\",\\u000d\\u000a      \\\"TargetType\\\":\\\"\\\"\\u000d\\u000a\\u0009},{\\u000d\\u000a      \\\"Id\\\":\\\"38cd4f96-dc39-45ce-81ce-4b2ddd3c4368\\\",\\u000d\\u000a      \\\"Code\\\":\\\"ALIC\\\",\\u000d\\u000a      \\\"Description\\\":\\\"Astha Life Insurance Company\\\",\\u000d\\u000a      \\\"IconSource\\\":\\\"Astha.png\\\",\\u000d\\u000a\\u0009  \\\"LabelView\\\":\\\"\\\",\\u000d\\u000a\\u0009  \\\"DefaultNoOfMonth\\\":\\\"false\\\",\\u000d\\u000a\\u0009  \\\"TermsAndConditions\\\":\\\"\\\",\\u000d\\u000a      \\\"TargetType\\\":\\\"\\\"\\u000d\\u000a\\u0009}\\u000d\\u000a]\"";
        }
//        else if (fileName.equals(UTILITY)) {
//
//           // return "\"[\\u000d\\u000a   {\\u000d\\u000a      \\\"Id\\\":\\\"2024dc48-6b09-4631-921d-b8b2267c85d8\\\",\\u000d\\u000a      \\\"Code\\\":\\\"DIP\\\",\\u000d\\u000a      \\\"Description\\\":\\\"Passport Fees\\\",\\u000d\\u000a      \\\"IconSource\\\":\\\"Passport.jpg\\\",\\u000d\\u000a\\u0009  \\\"TargetType\\\":\\\"MobileMoney.Views.Utilities.DIPFessNew, MobileMoney, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null\\\"\\u000d\\u000a\\u0009},\\u000d\\u000a   {\\u000d\\u000a      \\\"Id\\\":\\\"3cf506b7-d195-493e-b1e1-472ada7263c3\\\",\\u000d\\u000a      \\\"Code\\\":\\\"DESCO\\\",\\u000d\\u000a      \\\"Description\\\":\\\"DESCO Postpaid\\\",\\u000d\\u000a      \\\"IconSource\\\":\\\"DESCO.png\\\",\\u000d\\u000a\\u0009  \\\"TargetType\\\":\\\"MobileMoney.Views.Utilities.DESCOPostpaidNew, MobileMoney, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null\\\"\\u000d\\u000a\\u0009},\\u000d\\u000a\\u0009{\\u000d\\u000a      \\\"Id\\\":\\\"3cf506b7-d195-493e-b1e1-472ada7263c3\\\",\\u000d\\u000a      \\\"Code\\\":\\\"DESPR\\\",\\u000d\\u000a      \\\"Description\\\":\\\"DESCO Prepaid\\\",\\u000d\\u000a      \\\"IconSource\\\":\\\"DESCO.png\\\",\\u000d\\u000a\\u0009  \\\"TargetType\\\":\\\"MobileMoney.Views.Utilities.DESCOPrepaidNew, MobileMoney, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null\\\"\\u000d\\u000a\\u0009},\\u000d\\u000a   {\\u000d\\u000a      \\\"Id\\\":\\\"38cd4f96-dc39-45ce-81ce-4b2ddd3c4368\\\",\\u000d\\u000a      \\\"Code\\\":\\\"DPDC\\\",\\u000d\\u000a      \\\"Description\\\":\\\"DPDC\\\",\\u000d\\u000a      \\\"IconSource\\\":\\\"DPDC.png\\\",\\u000d\\u000a\\u0009  \\\"TargetType\\\":\\\"MobileMoney.Views.Utilities.DPDCBillNew, MobileMoney, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null\\\"\\u000d\\u000a\\u0009},\\u000d\\u000a\\u0009{\\u000d\\u000a      \\\"Id\\\":\\\"3cf506b7-d195-493e-b1e1-472ada7263c3\\\",\\u000d\\u000a      \\\"Code\\\":\\\"BTCL\\\",\\u000d\\u000a      \\\"Description\\\":\\\"BTCL\\\",\\u000d\\u000a      \\\"IconSource\\\":\\\"BTCL.jpg\\\",\\u000d\\u000a\\u0009  \\\"TargetType\\\":\\\"MobileMoney.Views.Utilities.BTCLNew, MobileMoney, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null\\\"\\u000d\\u000a\\u0009},\\u000d\\u000a\\u0009{\\u000d\\u000a      \\\"Id\\\":\\\"3cf506b7-d195-493e-b1e1-472ada7263c3\\\",\\u000d\\u000a      \\\"Code\\\":\\\"WZPDC\\\",\\u000d\\u000a      \\\"Description\\\":\\\"WZPDC\\\",\\u000d\\u000a      \\\"IconSource\\\":\\\"WZPDC.png\\\",\\u000d\\u000a\\u0009  \\\"TargetType\\\":\\\"MobileMoney.Views.Utilities.WZPDCNew, MobileMoney, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null\\\"\\u000d\\u000a\\u0009}\\u000d\\u000a]\"";
//            return "\"[\\u000d\\u000a   {\\u000d\\u000a      \\\"Id\\\":\\\"2024dc48-6b09-4631-921d-b8b2267c85d8\\\",\\u000d\\u000a      \\\"Code\\\":\\\"DIP\\\",\\u000d\\u000a      \\\"Description\\\":\\\"Passport Fees\\\",\\u000d\\u000a      \\\"IconSource\\\":\\\"Passport.jpg\\\",\\u000d\\u000a\\u0009  \\\"LabelView\\\":\\\"\\\",\\u000d\\u000a\\u0009  \\\"DefaultNoOfMonth\\\":\\\"false\\\",\\u000d\\u000a\\u0009  \\\"TermsAndConditions\\\":\\\"\\\",\\u000d\\u000a      \\\"TargetType\\\":\\\"\\\"\\u000d\\u000a\\u0009},\\u000d\\u000a   {\\u000d\\u000a      \\\"Id\\\":\\\"3cf506b7-d195-493e-b1e1-472ada7263c3\\\",\\u000d\\u000a      \\\"Code\\\":\\\"DESCO\\\",\\u000d\\u000a      \\\"Description\\\":\\\"DESCO Postpaid\\\",\\u000d\\u000a      \\\"IconSource\\\":\\\"DESCO.png\\\",\\u000d\\u000a\\u0009  \\\"LabelView\\\":\\\"\\\",\\u000d\\u000a\\u0009  \\\"DefaultNoOfMonth\\\":\\\"false\\\",\\u000d\\u000a\\u0009  \\\"TermsAndConditions\\\":\\\"\\\",\\u000d\\u000a      \\\"TargetType\\\":\\\"\\\"\\u000d\\u000a\\u0009},\\u000d\\u000a\\u0009{\\u000d\\u000a      \\\"Id\\\":\\\"3cf506b7-d195-493e-b1e1-472ada7263c3\\\",\\u000d\\u000a      \\\"Code\\\":\\\"DESPR\\\",\\u000d\\u000a      \\\"Description\\\":\\\"DESCO Prepaid\\\",\\u000d\\u000a      \\\"IconSource\\\":\\\"DESCO.png\\\",\\u000d\\u000a\\u0009  \\\"LabelView\\\":\\\"\\\",\\u000d\\u000a\\u0009  \\\"DefaultNoOfMonth\\\":\\\"false\\\",\\u000d\\u000a\\u0009  \\\"TermsAndConditions\\\":\\\"\\\",\\u000d\\u000a      \\\"TargetType\\\":\\\"\\\"\\u000d\\u000a\\u0009},\\u000d\\u000a   {\\u000d\\u000a      \\\"Id\\\":\\\"38cd4f96-dc39-45ce-81ce-4b2ddd3c4368\\\",\\u000d\\u000a      \\\"Code\\\":\\\"DPDC\\\",\\u000d\\u000a      \\\"Description\\\":\\\"DPDC\\\",\\u000d\\u000a      \\\"IconSource\\\":\\\"DPDC.png\\\",\\u000d\\u000a\\u0009  \\\"LabelView\\\":\\\"\\\",\\u000d\\u000a\\u0009  \\\"DefaultNoOfMonth\\\":\\\"false\\\",\\u000d\\u000a\\u0009  \\\"TermsAndConditions\\\":\\\"\\\",\\u000d\\u000a      \\\"TargetType\\\":\\\"\\\"\\u000d\\u000a\\u0009},\\u000d\\u000a\\u0009{\\u000d\\u000a      \\\"Id\\\":\\\"3cf506b7-d195-493e-b1e1-472ada7263c3\\\",\\u000d\\u000a      \\\"Code\\\":\\\"BTCL\\\",\\u000d\\u000a      \\\"Description\\\":\\\"BTCL\\\",\\u000d\\u000a      \\\"IconSource\\\":\\\"BTCL.jpg\\\",\\u000d\\u000a\\u0009  \\\"LabelView\\\":\\\"\\\",\\u000d\\u000a\\u0009  \\\"DefaultNoOfMonth\\\":\\\"false\\\",\\u000d\\u000a\\u0009  \\\"TermsAndConditions\\\":\\\"\\\",\\u000d\\u000a      \\\"TargetType\\\":\\\"\\\"\\u000d\\u000a\\u0009},\\u000d\\u000a\\u0009{\\u000d\\u000a      \\\"Id\\\":\\\"3cf506b7-d195-493e-b1e1-472ada7263c3\\\",\\u000d\\u000a      \\\"Code\\\":\\\"WZPDC\\\",\\u000d\\u000a      \\\"Description\\\":\\\"WZPDC\\\",\\u000d\\u000a      \\\"IconSource\\\":\\\"WZPDC.png\\\",\\u000d\\u000a\\u0009  \\\"LabelView\\\":\\\"\\\",\\u000d\\u000a\\u0009  \\\"DefaultNoOfMonth\\\":\\\"false\\\",\\u000d\\u000a\\u0009  \\\"TermsAndConditions\\\":\\\"\\\",\\u000d\\u000a      \\\"TargetType\\\":\\\"\\\"\\u000d\\u000a\\u0009}\\u000d\\u000a]\"";
//        }
        else if (fileName.equals(INSTITUTION)) {
            return "\"[\\u000d\\u000a   {\\u000d\\u000a      \\\"Id\\\":\\\"e96a8286-9bda-492c-be08-bb04b3524881\\\",\\u000d\\u000a      \\\"Code\\\":\\\"ACC\\\",\\u000d\\u000a      \\\"Description\\\":\\\"Adamjee Cantonment College\\\",\\u000d\\u000a      \\\"IconSource\\\":\\\"ACPS.png\\\",\\u0009  \\u000d\\u000a\\u0009  \\\"LabelView\\\":\\\"Student Id|Reg. No.|Ref. No.\\\",\\u000d\\u000a\\u0009  \\\"DefaultNoOfMonth\\\":\\\"false\\\",\\u000d\\u000a\\u0009  \\\"TermsAndConditions\\\":\\\"\\\",\\u000d\\u000a      \\\"TargetType\\\":\\\"\\\"   \\u000d\\u000a},\\u000d\\u000a   {\\u000d\\u000a      \\\"Id\\\":\\\"bcc45e7d-6dc0-45fc-9947-439a74c566e2\\\",\\u000d\\u000a      \\\"Code\\\":\\\"DMMC\\\",\\u000d\\u000a      \\\"Description\\\":\\\"Dhaka Mohanagar Mohila College\\\",\\u000d\\u000a      \\\"IconSource\\\":\\\"DMMC.jpg\\\",\\u000d\\u000a\\u0009  \\\"LabelView\\\":\\\"Student Id|Reg. No.|Ref. No.\\\",\\u000d\\u000a\\u0009  \\\"DefaultNoOfMonth\\\":\\\"false\\\",\\u000d\\u000a\\u0009  \\\"TermsAndConditions\\\":\\\"\\\",\\u000d\\u000a      \\\"TargetType\\\":\\\"\\\"\\u000d\\u000a},\\u000d\\u000a   {\\u000d\\u000a      \\\"Id\\\":\\\"c4b8f0f5-a4a5-4f78-9c9d-ce796530f8bb\\\",\\u000d\\u000a      \\\"Code\\\":\\\"DCPSC\\\",\\u000d\\u000a      \\\"Description\\\":\\\"Dhaka Cantonment Public School and College\\\",\\u000d\\u000a      \\\"IconSource\\\":\\\"DCGPC.gif\\\",\\u000d\\u000a      \\\"LabelView\\\":\\\"Student Id|Reg. No.|Ref. No.\\\",\\u000d\\u000a\\u0009  \\\"DefaultNoOfMonth\\\":\\\"false\\\",\\u000d\\u000a\\u0009  \\\"TermsAndConditions\\\":\\\"\\\",\\u000d\\u000a      \\\"TargetType\\\":\\\"\\\"\\u000d\\u000a},\\u000d\\u000a   {\\u000d\\u000a      \\\"Id\\\":\\\"2e660176-c0d9-401f-b96e-37a5ea49e7bd\\\",\\u000d\\u000a      \\\"Code\\\":\\\"MIS\\\",\\u000d\\u000a      \\\"Description\\\":\\\"Mainamati International School\\\",\\u000d\\u000a      \\\"IconSource\\\":\\\"MIS.png\\\",\\u000d\\u000a      \\\"LabelView\\\":\\\"Student Id|Reg. No.|Ref. No.\\\",\\u000d\\u000a\\u0009  \\\"DefaultNoOfMonth\\\":\\\"false\\\",\\u000d\\u000a\\u0009  \\\"TermsAndConditions\\\":\\\"\\\",\\u000d\\u000a      \\\"TargetType\\\":\\\"\\\"\\u000d\\u000a},\\u000d\\u000a   {\\u000d\\u000a      \\\"Id\\\":\\\"d4ae740a-4fb7-4b3f-9088-d761950d7a27\\\",\\u000d\\u000a      \\\"Code\\\":\\\"RCES\\\",\\u000d\\u000a      \\\"Description\\\":\\\"Ramu Cantonment English School\\\",\\u000d\\u000a      \\\"IconSource\\\":\\\"RCES.jpg\\\",\\u000d\\u000a      \\\"LabelView\\\":\\\"Student Id|Reg. No.|Ref. No.\\\",\\u000d\\u000a\\u0009  \\\"DefaultNoOfMonth\\\":\\\"false\\\",\\u000d\\u000a\\u0009  \\\"TermsAndConditions\\\":\\\"\\\",\\u000d\\u000a      \\\"TargetType\\\":\\\"\\\"\\u000d\\u000a},\\u000d\\u000a   {\\u000d\\u000a      \\\"Id\\\":\\\"e8215521-40fd-4e77-833c-75580f0566f1\\\",\\u000d\\u000a      \\\"Code\\\":\\\"GES\\\",\\u000d\\u000a      \\\"Description\\\":\\\"Gunners English School\\\",\\u000d\\u000a      \\\"IconSource\\\":\\\"GES.png\\\",\\u000d\\u000a      \\\"LabelView\\\":\\\"Student Id|Reg. No.|Ref. No.\\\",\\u000d\\u000a\\u0009  \\\"DefaultNoOfMonth\\\":\\\"false\\\",\\u000d\\u000a\\u0009  \\\"TermsAndConditions\\\":\\\"\\\",\\u000d\\u000a      \\\"TargetType\\\":\\\"\\\"\\u000d\\u000a},\\u000d\\u000a   {\\u000d\\u000a      \\\"Id\\\":\\\"cea09a24-398d-4043-9a0d-db38cb11f0c3\\\",\\u000d\\u000a      \\\"Code\\\":\\\"MIST\\\",\\u000d\\u000a      \\\"Description\\\":\\\"Military Institute of Science and Technology\\\",\\u000d\\u000a      \\\"IconSource\\\":\\\"MIST.png\\\",\\u000d\\u000a      \\\"LabelView\\\":\\\"Student Id|Reg. No.|Ref. No.\\\",\\u000d\\u000a\\u0009  \\\"DefaultNoOfMonth\\\":\\\"false\\\",\\u000d\\u000a\\u0009  \\\"TermsAndConditions\\\":\\\"\\\",\\u000d\\u000a      \\\"TargetType\\\":\\\"\\\"\\u000d\\u000a},\\u000d\\u000a   {\\u000d\\u000a      \\\"Id\\\":\\\"c294911f-8dd8-42b5-a0a4-f5c126944569\\\",\\u000d\\u000a      \\\"Code\\\":\\\"CPSCK\\\",\\u000d\\u000a      \\\"Description\\\":\\\"Cantonment Public School & College, Kholahati\\\",\\u000d\\u000a      \\\"IconSource\\\":\\\"CPSCK.jpg\\\",\\u000d\\u000a      \\\"LabelView\\\":\\\"Student Id|Reg. No.|Ref. No.\\\",\\u000d\\u000a\\u0009  \\\"DefaultNoOfMonth\\\":\\\"false\\\",\\u000d\\u000a\\u0009  \\\"TermsAndConditions\\\":\\\"\\\",\\u000d\\u000a      \\\"TargetType\\\":\\\"\\\"\\u000d\\u000a},\\u000d\\u000a   {\\u000d\\u000a      \\\"Id\\\":\\\"1e70ac57-65ea-4000-8e04-227ce62b89d7\\\",\\u000d\\u000a      \\\"Code\\\":\\\"MMA\\\",\\u000d\\u000a      \\\"Description\\\":\\\"Muslim Modern Academy\\\",\\u000d\\u000a      \\\"IconSource\\\":\\\"MMA.jpg\\\",\\u000d\\u000a      \\\"LabelView\\\":\\\"Student Id|Reg. No.|Ref. No.\\\",\\u000d\\u000a\\u0009  \\\"DefaultNoOfMonth\\\":\\\"false\\\",\\u000d\\u000a\\u0009  \\\"TermsAndConditions\\\":\\\"\\\",\\u000d\\u000a      \\\"TargetType\\\":\\\"\\\"\\u000d\\u000a},\\u000d\\u000a   {\\u000d\\u000a      \\\"Id\\\":\\\"ac1d7605-86bd-41ff-8e53-4cabb67a8dfa\\\",\\u000d\\u000a      \\\"Code\\\":\\\"RCPSC1\\\",\\u000d\\u000a      \\\"Description\\\":\\\"Rajendrapur Cantonment Public School & College\\\",\\u000d\\u000a      \\\"IconSource\\\":\\\"RCPSC1.png\\\",\\u000d\\u000a      \\\"LabelView\\\":\\\"Student Id|Reg. No.|Ref. No.\\\",\\u000d\\u000a\\u0009  \\\"DefaultNoOfMonth\\\":\\\"false\\\",\\u000d\\u000a\\u0009  \\\"TermsAndConditions\\\":\\\"\\\",\\u000d\\u000a      \\\"TargetType\\\":\\\"\\\"\\u000d\\u000a},\\u000d\\u000a   {\\u000d\\u000a      \\\"Id\\\":\\\"f44fe9ea-d45e-4352-b0d9-324a9147f12a\\\",\\u000d\\u000a      \\\"Code\\\":\\\"GCPSC\\\",\\u000d\\u000a      \\\"Description\\\":\\\"Ghatail Cantonment Public School & College\\\",\\u000d\\u000a      \\\"IconSource\\\":\\\"GCPSC.jpg\\\",\\u000d\\u000a      \\\"LabelView\\\":\\\"Student Id|Reg. No.|Ref. No.\\\",\\u000d\\u000a\\u0009  \\\"DefaultNoOfMonth\\\":\\\"false\\\",\\u000d\\u000a\\u0009  \\\"TermsAndConditions\\\":\\\"\\\",\\u000d\\u000a      \\\"TargetType\\\":\\\"\\\"\\u000d\\u000a},\\u000d\\u000a   {\\u000d\\u000a      \\\"Id\\\":\\\"8ca9008a-8819-4587-a6d5-9f7d797e7ad7\\\",\\u000d\\u000a      \\\"Code\\\":\\\"LORDS\\\",\\u000d\\u000a      \\\"Description\\\":\\\"LORDS, An English Medium School\\\",\\u000d\\u000a      \\\"IconSource\\\":\\\"LORDS.png\\\",\\u000d\\u000a      \\\"LabelView\\\":\\\"Student Id|Reg. No.|Ref. No.\\\",\\u000d\\u000a\\u0009  \\\"DefaultNoOfMonth\\\":\\\"false\\\",\\u000d\\u000a\\u0009  \\\"TermsAndConditions\\\":\\\"\\\",\\u000d\\u000a      \\\"TargetType\\\":\\\"\\\"\\u000d\\u000a},\\u000d\\u000a   {\\u000d\\u000a      \\\"Id\\\":\\\"de94933c-5f79-46e5-b718-2258865c80b1\\\",\\u000d\\u000a      \\\"Code\\\":\\\"JCPSC\\\",\\u000d\\u000a      \\\"Description\\\":\\\"Jalalabad Cantonment Public School & College\\\",\\u000d\\u000a      \\\"IconSource\\\":\\\"JCPSC.png\\\",\\u000d\\u000a      \\\"LabelView\\\":\\\"Student Id|Reg. No.|Ref. No.\\\",\\u000d\\u000a\\u0009  \\\"DefaultNoOfMonth\\\":\\\"false\\\",\\u000d\\u000a\\u0009  \\\"TermsAndConditions\\\":\\\"\\\",\\u000d\\u000a      \\\"TargetType\\\":\\\"\\\"\\u000d\\u000a},\\u000d\\u000a   {\\u000d\\u000a      \\\"Id\\\":\\\"303ebcff-62c8-495e-9122-d7d6ee938d72\\\",\\u000d\\u000a      \\\"Code\\\":\\\"JESC\\\",\\u000d\\u000a      \\\"Description\\\":\\\"Jashore English School & College\\\",\\u000d\\u000a      \\\"IconSource\\\":\\\"JESC.png\\\",\\u000d\\u000a      \\\"LabelView\\\":\\\"Student Id|Reg. No.|Ref. No.\\\",\\u000d\\u000a\\u0009  \\\"DefaultNoOfMonth\\\":\\\"false\\\",\\u000d\\u000a\\u0009  \\\"TermsAndConditions\\\":\\\"\\\",\\u000d\\u000a      \\\"TargetType\\\":\\\"\\\"\\u000d\\u000a},\\u000d\\u000a   {\\u000d\\u000a      \\\"Id\\\":\\\"43011f8b-2175-4d2f-868f-e4982f3c7a1a\\\",\\u000d\\u000a      \\\"Code\\\":\\\"AFMC\\\",\\u000d\\u000a      \\\"Description\\\":\\\"Armed Forces Medical College\\\",\\u000d\\u000a      \\\"IconSource\\\":\\\"AFMC.jpg\\\",\\u000d\\u000a      \\\"LabelView\\\":\\\"Student Id|Reg. No.|Ref. No.\\\",\\u000d\\u000a\\u0009  \\\"DefaultNoOfMonth\\\":\\\"false\\\",\\u000d\\u000a\\u0009  \\\"TermsAndConditions\\\":\\\"\\\",\\u000d\\u000a      \\\"TargetType\\\":\\\"\\\"\\u000d\\u000a},\\u000d\\u000a   {\\u000d\\u000a      \\\"Id\\\":\\\"38cd4f96-dc39-45ce-81ce-4b2ddd3c4381\\\",\\u000d\\u000a      \\\"Code\\\":\\\"KCPSC\\\",\\u000d\\u000a      \\\"Description\\\":\\\"Khagrachari Cantonment Public School & College\\\",\\u000d\\u000a      \\\"IconSource\\\":\\\"KCPSC.jpg\\\",\\u000d\\u000a      \\\"LabelView\\\":\\\"Student Id\\|Short Name\\|Number of Month\\\",\\u000d\\u000a\\u0009  \\\"DefaultNoOfMonth\\\":\\\"true\\\",\\u000d\\u000a\\u0009  \\\"TermsAndConditions\\\":\\\"\\\",\\u000d\\u000a      \\\"TargetType\\\":\\\"\\\"\\u000d\\u000a},\\u000d\\u000a   {\\u000d\\u000a      \\\"Id\\\":\\\"2024dc48-6b09-4631-921d-b8b2267c85d8\\\",\\u000d\\u000a      \\\"Code\\\":\\\"CCPC\\\",\\u000d\\u000a      \\\"Description\\\":\\\"Chattogram Cantonment Public College\\\",\\u000d\\u000a      \\\"IconSource\\\":\\\"CCPC.jpg\\\",\\u000d\\u000a      \\\"LabelView\\\":\\\"Student Id|Reg. No.|Ref. No.\\\",\\u000d\\u000a\\u0009  \\\"DefaultNoOfMonth\\\":\\\"false\\\",\\u000d\\u000a\\u0009  \\\"TermsAndConditions\\\":\\\"\\\",\\u000d\\u000a      \\\"TargetType\\\":\\\"\\\"\\u000d\\u000a},\\u000d\\u000a   {\\u000d\\u000a      \\\"Id\\\":\\\"3cf506b7-d195-493e-b1e1-472ada7263c3\\\",\\u000d\\u000a      \\\"Code\\\":\\\"CPSCS\\\",\\u000d\\u000a      \\\"Description\\\":\\\"Cantonment Public School & College, Saidpur\\\",\\u000d\\u000a      \\\"IconSource\\\":\\\"CPSCS.jpg\\\",\\u000d\\u000a      \\\"LabelView\\\":\\\"Student Id|Reg. No.|Ref. No.\\\",\\u000d\\u000a\\u0009  \\\"DefaultNoOfMonth\\\":\\\"false\\\",\\u000d\\u000a\\u0009  \\\"TermsAndConditions\\\":\\\"\\\",\\u000d\\u000a      \\\"TargetType\\\":\\\"\\\"\\u000d\\u000a},\\u000d\\u000a   {\\u000d\\u000a      \\\"Id\\\":\\\"38cd4f96-dc39-45ce-81ce-4b2ddd3c4368\\\",\\u000d\\u000a      \\\"Code\\\":\\\"HCPSC\\\",\\u000d\\u000a      \\\"Description\\\":\\\"Halishahar Cantonment Public School & College, Halishahar\\\",\\u000d\\u000a      \\\"IconSource\\\":\\\"HCPSC.png\\\",\\u000d\\u000a      \\\"LabelView\\\":\\\"Student Id|Reg. No.|Ref. No.\\\",\\u000d\\u000a\\u0009  \\\"DefaultNoOfMonth\\\":\\\"false\\\",\\u000d\\u000a\\u0009  \\\"TermsAndConditions\\\":\\\"\\\",\\u000d\\u000a      \\\"TargetType\\\":\\\"\\\"\\u000d\\u000a}\\u000d\\u000a]\"";
        } else if (fileName.equals(UTILITY)) {
            if (code.equalsIgnoreCase("1")) {
                return "\"[{\\\"Id\\\":\\\"3cf506b7-d195-493e-b1e1-472ada7263c3\\\",\\\"Code\\\":\\\"DESCO\\\",\\\"Description\\\":\\\"DESCO Postpaid\\\",\\\"IconSource\\\":\\\"DESCO.png\\\",\\\"ParentCode\\\":\\\"1\\\",\\\"LabelView\\\":null,\\\"DefaultNoOfMonth\\\":null,\\\"TermsAndConditions\\\":null},{\\\"Id\\\":\\\"3cf506b7-d195-493e-b1e1-472ada7263c3\\\",\\\"Code\\\":\\\"DESPR\\\",\\\"Description\\\":\\\"DESCO Prepaid\\\",\\\"IconSource\\\":\\\"DESCO.png\\\",\\\"ParentCode\\\":\\\"1\\\",\\\"LabelView\\\":null,\\\"DefaultNoOfMonth\\\":null,\\\"TermsAndConditions\\\":null},{\\\"Id\\\":\\\"38cd4f96-dc39-45ce-81ce-4b2ddd3c4368\\\",\\\"Code\\\":\\\"DPDC\\\",\\\"Description\\\":\\\"DPDC\\\",\\\"IconSource\\\":\\\"DPDC.png\\\",\\\"ParentCode\\\":\\\"1\\\",\\\"LabelView\\\":null,\\\"DefaultNoOfMonth\\\":null,\\\"TermsAndConditions\\\":null},{\\\"Id\\\":\\\"38cd4f96-dc39-45ce-81ce-4b2ddd3c4369\\\",\\\"Code\\\":\\\"NESCOPR\\\",\\\"Description\\\":\\\"NESCO Prepaid\\\",\\\"IconSource\\\":\\\"NESCO.png\\\",\\\"ParentCode\\\":\\\"1\\\",\\\"LabelView\\\":null,\\\"DefaultNoOfMonth\\\":null,\\\"TermsAndConditions\\\":null},{\\\"Id\\\":\\\"38cd4f96-dc39-45ce-81ce-4b2ddd3c4370\\\",\\\"Code\\\":\\\"NESCO\\\",\\\"Description\\\":\\\"NESCO Postpaid\\\",\\\"IconSource\\\":\\\"NESCO.png\\\",\\\"ParentCode\\\":\\\"1\\\",\\\"LabelView\\\":null,\\\"DefaultNoOfMonth\\\":null,\\\"TermsAndConditions\\\":null},{\\\"Id\\\":\\\"38cd4f96-dc39-45ce-81ce-4b2ddd3c4371\\\",\\\"Code\\\":\\\"REBPR\\\",\\\"Description\\\":\\\"REB Prepaid\\\",\\\"IconSource\\\":\\\"REB.png\\\",\\\"ParentCode\\\":\\\"1\\\",\\\"LabelView\\\":null,\\\"DefaultNoOfMonth\\\":null,\\\"TermsAndConditions\\\":null}]\"";
            } else if (code.equalsIgnoreCase("2")) {
                return "\"[{\\\"Id\\\":\\\"2024dc48-6b09-4631-921d-b8b2267c85d8\\\",\\\"Code\\\":\\\"BAKHRE\\\",\\\"Description\\\":\\\"Bakhrabad Gas\\\",\\\"IconSource\\\":\\\"BAKHRABAD.png\\\",\\\"ParentCode\\\":\\\"3\\\",\\\"LabelView\\\":null,\\\"DefaultNoOfMonth\\\":null,\\\"TermsAndConditions\\\":null}]\"";
            } else {
//                return null;
                return "\"[{\\\"Id\\\":\\\"2024dc48-6b09-4631-921d-b8b2267c85d8\\\",\\\"Code\\\":\\\"WASAKE\\\",\\\"Description\\\":\\\"Khulna WASA\\\",\\\"IconSource\\\":\\\"KWASA.png\\\",\\\"ParentCode\\\":\\\"3\\\",\\\"LabelView\\\":null,\\\"DefaultNoOfMonth\\\":null,\\\"TermsAndConditions\\\":null},{\\\"Id\\\":\\\"2024dc48-6b09-4631-921d-b8b2267c85d8\\\",\\\"Code\\\":\\\"WASADE\\\",\\\"Description\\\":\\\"Dhaka WASA\\\",\\\"IconSource\\\":\\\"DWASA.png\\\",\\\"ParentCode\\\":\\\"3\\\",\\\"LabelView\\\":null,\\\"DefaultNoOfMonth\\\":null,\\\"TermsAndConditions\\\":null}]\"";
            }
        } else if (fileName.equals(OTHERS)) {
            return "\"[{\\\"Id\\\":\\\"2024dc48-6b09-4631-921d-b8b2267c85d8\\\",\\\"Code\\\":\\\"DIP\\\",\\\"Description\\\":\\\"Passport Fees\\\",\\\"IconSource\\\":\\\"Passport.jpg\\\",\\\"ParentCode\\\":\\\"3\\\",\\\"LabelView\\\":null,\\\"DefaultNoOfMonth\\\":null,\\\"TermsAndConditions\\\":null},{\\\"Id\\\":\\\"2024dc48-6b09-4631-921d-b8b2267c85d8\\\",\\\"Code\\\":\\\"NID\\\",\\\"Description\\\":\\\"NID Fees\\\",\\\"IconSource\\\":\\\"Nid.jpg\\\",\\\"ParentCode\\\":\\\"3\\\",\\\"LabelView\\\":null,\\\"DefaultNoOfMonth\\\":null,\\\"TermsAndConditions\\\":null}]\"";
        }

        return null;
    }

//    @GetMapping(path = "/FileLoadRequest", produces = "application/json")
//    public String utilityDetails(@RequestParam("fileName") String fileName,@RequestParam("code") String code) {
//        if (fileName.equals(UTILITY)){
//            if(code.equalsIgnoreCase("1")){
//                return "\"[{\\\"Id\\\":\\\"3cf506b7-d195-493e-b1e1-472ada7263c3\\\",\\\"Code\\\":\\\"DESCO\\\",\\\"Description\\\":\\\"DESCO Postpaid\\\",\\\"IconSource\\\":\\\"DESCO.png\\\",\\\"ParentCode\\\":\\\"1\\\",\\\"LabelView\\\":null,\\\"DefaultNoOfMonth\\\":null,\\\"TermsAndConditions\\\":null},{\\\"Id\\\":\\\"3cf506b7-d195-493e-b1e1-472ada7263c3\\\",\\\"Code\\\":\\\"DESPR\\\",\\\"Description\\\":\\\"DESCO Prepaid\\\",\\\"IconSource\\\":\\\"DESCO.png\\\",\\\"ParentCode\\\":\\\"1\\\",\\\"LabelView\\\":null,\\\"DefaultNoOfMonth\\\":null,\\\"TermsAndConditions\\\":null},{\\\"Id\\\":\\\"38cd4f96-dc39-45ce-81ce-4b2ddd3c4368\\\",\\\"Code\\\":\\\"DPDC\\\",\\\"Description\\\":\\\"DPDC\\\",\\\"IconSource\\\":\\\"DPDC.png\\\",\\\"ParentCode\\\":\\\"1\\\",\\\"LabelView\\\":null,\\\"DefaultNoOfMonth\\\":null,\\\"TermsAndConditions\\\":null}]\"";
//            }else if(code.equalsIgnoreCase("2")){
//                return "";
//            }else{
//                return "\"[{\\\"Id\\\":\\\"2024dc48-6b09-4631-921d-b8b2267c85d8\\\",\\\"Code\\\":\\\"DIP\\\",\\\"Description\\\":\\\"Passport Fees\\\",\\\"IconSource\\\":\\\"Passport.jpg\\\",\\\"ParentCode\\\":\\\"3\\\",\\\"LabelView\\\":null,\\\"DefaultNoOfMonth\\\":null,\\\"TermsAndConditions\\\":null},{\\\"Id\\\":\\\"2024dc48-6b09-4631-921d-b8b2267c85d8\\\",\\\"Code\\\":\\\"NID\\\",\\\"Description\\\":\\\"NID Fees\\\",\\\"IconSource\\\":\\\"Passport.jpg\\\",\\\"ParentCode\\\":\\\"3\\\",\\\"LabelView\\\":null,\\\"DefaultNoOfMonth\\\":null,\\\"TermsAndConditions\\\":null}]\"";
//            }
//        }
//        return null;
//    }

    @GetMapping(path = "/GetBillTypes", produces = "application/json")
    public String billType(@RequestParam("fileName") String fileName) {
        return "\"[\\u000d\\u000a{\\u000d\\u000a \\\"Code\\\":\\\"1\\\",\\u000d\\u000a \\\"Description\\\":\\\"Electricity\\\" \\u000d\\u000a},\\u000d\\u000a{\\u000d\\u000a \\\"Code\\\":\\\"2\\\",\\u000d\\u000a \\\"Description\\\":\\\"Gas\\\"\\u000d\\u000a},\\u000d\\u000a{\\u000d\\u000a \\\"Code\\\":\\\"3\\\",\\u000d\\u000a \\\"Description\\\":\\\"Water\\\"\\u000d\\u000a}\\u000d\\u000a]\"";
    }

    @PostMapping(path = "/ProcessRequest", produces = "application/json")
    public String testProcessRequest(@RequestParam("msgId") Integer msgId, @RequestParam("userNumber") String userNumber, @RequestParam("smsText") String smsText, @RequestParam("telcoId") Integer telcoId, @RequestParam("shortCode") String shortCode, @RequestParam("encKey") String encKey) {

        log.debug(userNumber);
        log.debug(smsText);
        if (smsText.startsWith("TRUSTMM DPDC")) {
            if (redisDao.getValue(userNumber) != null) {
                String pin = redisDao.getValue(userNumber).split(DATA_SEPARATOR)[0];
                if (pin.equalsIgnoreCase(smsText.split(" ")[5])) {
                    Double amount = Double.valueOf(100);
                    String account = smsText.split(" ")[2];
                    String billMonth = smsText.split(" ")[4];
                    if (redisDao.getValue(billMonth) != null) {
                        if (redisDao.getValue(account).equalsIgnoreCase("1")) {
                            redisDao.setValue(account, "0");
                            if (debitAccount(userNumber, amount+10.0)) {
                                return "\"An amount of Tk. " + amount + " has been paid to DPDC for bill number " + account + ". Your current balance is Tk. " +
                                        redisDao.getValue(userNumber).split(DATA_SEPARATOR)[1] + ". TxID:DPDCCQY23LA70119763\"";
                            } else {
                                return "\"Insufficient balance to perform the transaction, your current balance is Tk. " + redisDao.getValue(userNumber).split(DATA_SEPARATOR)[1] + ".\"";
                            }
                        } else {
                            return "\"DPDC Service is not available.Please try again after some time.\"";
                        }
                    } else {
                        return "\"DPDC Service is not available.Please try again after some time.\"";
                    }
                } else {
                    return "Incorrect PIN! If you have lost your PIN, please contact customer service for assistance";
                }
            } else {
                return "\"User not registered\"";
            }
        }
        else if(smsText.startsWith("TRUSTMM DP")){
            if(redisDao.getValue(userNumber) != null){
                String pin = redisDao.getValue(userNumber).split(DATA_SEPARATOR)[0];
                if(pin.equalsIgnoreCase(smsText.split(" ")[4])){
                    String amount = smsText.split(" ")[3];
                    String toUserNumber = smsText.split(" ")[2];
                    debitAccount(toUserNumber,Double.parseDouble(amount));
                    creditAccount(userNumber,Double.parseDouble(amount));
                    String balance = redisDao.getValue(userNumber).split(DATA_SEPARATOR)[1];

//                    {
//                        "message": "An amount of Tk. 100.00 deposited from  8801704320043. Your current balance is Tk. 904.52. Fees:  Received TK. 0.41. Paid TK. 100.00. TxID: DP86GIYYK9EI23840735"
//                    }
                    return "\"An amount of Tk. "+Double.parseDouble(amount)+" deposited from "+toUserNumber+"."
                            +"Your current balance is Tk. "+Double.parseDouble(balance)+". Fees: Received TK. 0.41"
                            +" Paid TK. "+Double.parseDouble(amount)+". TxID: "+ UUID.randomUUID().toString()+ "\"";
                }
                else {
                    return "\" Transaction Failed!";
                }
            }
        }
        if (smsText.startsWith("TRUSTMM BAL")) {
            if (redisDao.getValue(userNumber) != null) {
                String pin = redisDao.getValue(userNumber).split(DATA_SEPARATOR)[0];
                if (pin.equalsIgnoreCase(smsText.split(" ")[2])) {
                    String balance = redisDao.getValue(userNumber).split(DATA_SEPARATOR)[1];
                    return "\"Your t-cash Account Balance is Tk. " + balance + "\"";
                } else {
                    return "Incorrect PIN! If you have lost your PIN, please contact customer service for assistance";
                }
            } else {
                return "\"User not registered\"";
            }
        } else if (smsText.startsWith("TRUSTMM HIST")) {
            return "\"09.06.20-Commission.to.8800000670002-TK.10.00000; 09.06.20-BNSCC.to.8800200000036-TK.60.00000; 09.06.20-Commission.to.8800000670002-TK.10.00000;\"";
        } else if (smsText.startsWith("TRUSTMM PAY")) {
            if (redisDao.getValue(userNumber) != null) {
                String pin = redisDao.getValue(userNumber).split(DATA_SEPARATOR)[0];
                if (pin.equalsIgnoreCase(smsText.split(" ")[4])) {
                    Double amount = Double.valueOf(smsText.split(" ")[3]);
                    String recipientNumber = smsText.split(" ")[2];
                    if (recipientNumber.length() != 13) {
                        return "\"Current length of ToUserNumber has set is 13. Please check the length of ToUserNumber\"";
                    }
                    if (redisDao.getValue(recipientNumber) != null) {
                        if (debitAccount(userNumber, amount+10.0)) {


                            //   creditAccount(recipientNumber, amount);

                            return "\"Send Tk. " + amount + " to " + recipientNumber + ". Your current balance is Tk. " +
                                    redisDao.getValue(userNumber).split(DATA_SEPARATOR)[1] + ". TxID#PA22YCZA23LA70119763\"";
                        } else {
                            return "\"Insufficient balance to perform the transaction, your current balance is Tk. " + redisDao.getValue(userNumber).split(DATA_SEPARATOR)[1] + ".\"";
                        }
                    } else {
                        return "\"You are not authorized to perform this action. Please contact customer service.\"";
                    }
                } else {
                    return "\"Incorrect PIN! If you have lost your PIN, please contact customer service for assistance\"";
                }
            } else {
                return "\"User not registered\"";
            }
        } else if (smsText.startsWith("Trustmm Topup")) {
            if (redisDao.getValue(userNumber) != null) {
                String pin = redisDao.getValue(userNumber).split(DATA_SEPARATOR)[0];
                if (pin.equalsIgnoreCase(smsText.split(" ")[4])) {
                    Double amount = Double.valueOf(smsText.split(" ")[3]);
                    String recipAcc = smsText.split(" ")[2];
                    if (recipAcc.length() != 13) {
                        return "\"The cell number has to be valid, please provide cell number start with 88.\"";
                    }
                    if (debitAccount(userNumber, amount+10.0)) {
                        return "\"Your TopUp request of Amount Tk. " + amount + " to Prepaid number " + recipAcc + " has been submitted successfully.\"";
                    } else {
                        return "\"Insufficient balance to perform the transaction, your current balance is Tk. " + redisDao.getValue(userNumber).split(DATA_SEPARATOR)[1] + ".\"";
                    }
                } else {
                    return "\"Incorrect PIN! If you have lost your PIN, please contact customer service for assistance\"";
                }
            } else {
                return "\"User not registered\"";
            }
        } else if (smsText.startsWith("TRUSTMM MPAY")) {
            if (redisDao.getValue(userNumber) != null) {
                String pin = redisDao.getValue(userNumber).split(DATA_SEPARATOR)[0];
                if (pin.equalsIgnoreCase(smsText.split(" ")[5])) {
                    Double amount = Double.valueOf(smsText.split(" ")[4]);
                    String recipAcc = smsText.split(" ")[2];
                    String notificationNo = smsText.split(" ")[6];
                    if (notificationNo.length() != 13) {
                        return "Please provide valid notification number.";
                    }
                    if (redisDao.getValue(recipAcc) != null) {
                        if (debitAccount(userNumber, amount+10.0)) {
                            if(recipAcc.equalsIgnoreCase("8801937791254") || recipAcc.equalsIgnoreCase("8801766685686")){
                                return "\"Payment of Tk. " + amount + " paid to " + recipAcc + ". Purpose Loan. Your current balance is Tk. " + redisDao.getValue(userNumber).split(DATA_SEPARATOR)[1] + ". TxID: MPAYGTIPEGNQ3R11308461\"";
                            }
                            return "\"Payment of Tk. " + amount + " paid to " + recipAcc + ". Purpose 1. Your current balance is Tk. " + redisDao.getValue(userNumber).split(DATA_SEPARATOR)[1] + ". TxID: MPAYGTIPEGNQ3R11308461\"";
                        } else {
                            return "\"Insufficient balance to perform the transaction, your current balance is Tk. " + redisDao.getValue(userNumber).split(DATA_SEPARATOR)[1] + ".\"";
                        }
                    } else {
                        return "\"Your request could not be processed, the receipient is not valid or\n" +
                                "registered. Please try again\"";
                    }
                } else {
                    return "Incorrect PIN! If you have lost your PIN, please contact customer service for assistance";
                }
            } else {
                return "\"User not registered\"";
            }
        } else if (smsText.startsWith("TRUSTMM FCDP")) {
            if (redisDao.getValue(userNumber) != null) {
                String pin = redisDao.getValue(userNumber).split(DATA_SEPARATOR)[0];
                if (pin.equalsIgnoreCase(smsText.split(" ")[4])) {
                    String recipAccount = smsText.split(" ")[2];
                    Double amount = Double.valueOf(smsText.split(" ")[3]);
                    if (recipAccount.equalsIgnoreCase("1") || recipAccount.equalsIgnoreCase("2")) {
                        if (amount < 20) {
                            return "Minimum Transaction limit didnt match.";
                        }
                        creditAccount(userNumber, amount);
                        return "\"Received Tk. " + amount + " from your TBL Account. Fee Tk. 0.00. Your current balance is Tk. " +
                                redisDao.getValue(userNumber).split(DATA_SEPARATOR)[1] + ". TxID#PA22YCZA23LA70119763\"";
                    } else {
                        return "\"Core account is not mapped with t-cash.\"";

                    }
                } else {
                    return "Incorrect PIN! If you have lost your PIN, please contact customer service for assistance";
                }

            } else {
                return "\"User not registered\"";
            }
        } else if (smsText.startsWith("TRUSTMM FCCO")) {
            if (redisDao.getValue(userNumber) != null) {
                String pin = redisDao.getValue(userNumber).split(DATA_SEPARATOR)[0];
                if (pin.equalsIgnoreCase(smsText.split(" ")[4])) {
                    Double amount = Double.valueOf(smsText.split(" ")[3]);
                    String acc = smsText.split(" ")[2];
//                    if (redisDao.getValue(acc) != null) {
                    if (debitAccount(userNumber, amount)) {
                        return "\"You have transfer an amount of TK. " + amount + "to " + acc + " Your current balance is TK. " + redisDao.getValue(userNumber).split(DATA_SEPARATOR)[1] + ". \"";
                    } else {
                        return "\"Insufficient balance to perform the transaction, your current balance is Tk. " + redisDao.getValue(userNumber).split(DATA_SEPARATOR)[1] + ".\"";
                    }
//                    }
//                else {
//                        return "\"Wrong meter number\"";
//
//
//                    }
                } else {
                    return "Incorrect PIN! If you have lost your PIN, please contact customer service for assistance";
                }
            } else {
                return "\"User not registered\"";
            }
        } else if (smsText.startsWith("TRUSTMM DESPR")) {
            if (redisDao.getValue(userNumber) != null) {
                String pin = redisDao.getValue(userNumber).split(DATA_SEPARATOR)[0];
                if (pin.equalsIgnoreCase(smsText.split(" ")[4])) {
                    Double amount = Double.valueOf(smsText.split(" ")[3]);
                    String meter = smsText.split(" ")[2];
                    String notificationNo = smsText.split(" ")[5];
                    if (redisDao.getValue(meter) != null) {
                        if (redisDao.getValue(notificationNo) == null) {
                            return "\"Please provide valid notification number starting with 88\"";
                        }
                        if (amount < 500) {
                            return "\"Purchase amount can't be lower than min threshold(500)!\"";
                        }
                        if (amount > 50000) {
                            return "\"Purchase amount can't be greater than max threshold(50000)!\"";
                        }
                        if (debitAccount(userNumber, amount+10.0)) {
                            return "\"DESCO Pre Meter " + meter + "recharged TK. " + amount + ": Energy: 350.55; Meter Rent-Single\n" +
                                    "Phase Meter: 40; Demand Charge-All: 90; Rebate: -4.36; VAT: 23.81. 20-Oct-20\n" +
                                    "15:20:26.Bal Tk. " + redisDao.getValue(userNumber).split(DATA_SEPARATOR)[1] + ". TxID#PA22YCZA23LA70119763\"";
                        } else {
                            return "\"Insufficient balance to perform the transaction, your current balance is Tk. " + redisDao.getValue(userNumber).split(DATA_SEPARATOR)[1] + ".\"";
                        }
                    } else {
                        return "\"Meter Not Registered.\"";


                    }
                } else {
                    return "Incorrect PIN! If you have lost your PIN, please contact customer service for assistance";
                }
            } else {
                return "\"User not registered\"";
            }
        } else if (smsText.startsWith("TRUSTMM NESPR")) {
            // Todo match with actual api response
//            return "Transaction successfully completed. Ref Number: 123123123";
            return "An amount of Tk 100.0 has been paid to NESCO for " +
            "prepaid Customer Number 3319123. Your current Balance is 5000.0 Tk." +
                    " TxID:NESPR40fdb27a7ae0eca4017aee52d5550101";
        } else if (smsText.startsWith("TRUSTMM NESCO")) {
            // Todo match with actual api response
            return "Transaction successfully completed. Ref Number: 123123123";
        } else if (smsText.startsWith("TrustMM REBPR")) {
            // Todo match with actual api response
            return "Transaction successfully completed. Ref Number: 123123123###energy amount: 129.3;arrear amount: 20.0;fee amount:23";
        } else if (smsText.startsWith("TRUSTMM DESCO")) {
            if (redisDao.getValue(userNumber) != null) {
                String pin = redisDao.getValue(userNumber).split(DATA_SEPARATOR)[0];
                if (pin.equalsIgnoreCase(smsText.split(" ")[3])) {
//                    Double amount = Double.valueOf(smsText.split(" ")[3]);
                    Double amount = 1.00;
                    String bill = smsText.split(" ")[2];
                    String notificationNo = smsText.split(" ")[4];
                    if (redisDao.getValue(bill) != null) {
                        if (notificationNo.length() != 13) {
                            return "Current length of {notifynumber} has set is 13. Please check the length of {notifynumber}";
                        }
                        if (redisDao.getValue(bill).equalsIgnoreCase("1")) {
                            redisDao.setValue(bill, "0");
                            if (debitAccount(userNumber, amount+10.0)) {
                                return "\"An amount of Tk. " + amount + " has been paid to DESCO for bill no " + bill + ". Your current balance is Tk. " +
                                        redisDao.getValue(userNumber).split(DATA_SEPARATOR)[1] + ". TxID#PA22YCZA23LA70119763\"";
                            } else {
                                return "\"Insufficient balance to perform the transaction, your current balance is TK. " + redisDao.getValue(userNumber).split(DATA_SEPARATOR)[1] + "  and due amount: " + amount + "\"";
                            }
                        } else {
                            return "\"Bill already paid for this bill number\"";
                        }
                    } else {
                        return "\" Desco API Service is not available.\"";
                    }
                } else {
                    return "Incorrect PIN! If you have lost your PIN, please contact customer service for assistance";
                }
            } else {
                return "\"User not registered\"";
            }
        }  else if (smsText.startsWith("TRUSTMM DIP")) {
            if (redisDao.getValue(userNumber) != null) {
                String pin = redisDao.getValue(userNumber).split(DATA_SEPARATOR)[0];
                if (pin.equalsIgnoreCase(smsText.split(" ")[3])) {
                    Double amount = Double.valueOf(smsText.split(" ")[2]);
                    String notificationNo = smsText.split(" ")[7];
                    if (notificationNo.length() != 13) {
                        return "\"Please provide valid notification number starting with 8801\"";
                    }
                    // String passport=smsText.split(" ")[1];
                    if (debitAccount(userNumber, amount+10.0)) {
                        return "\"An amount of Tk. " + amount + " has been paid as Passport fees against 313461CF00370E. Your current balance is Tk. " +
                                redisDao.getValue(userNumber).split(DATA_SEPARATOR)[1] + ".\""; // TxID#PA22YCZA23LA70119763
                    } else {
                        return "\"Insufficient balance to perform the transaction, your current balance is Tk. " + redisDao.getValue(userNumber).split(DATA_SEPARATOR)[1] + ".\"";
                    }
                } else {
                    return "Incorrect PIN! If you have lost your PIN, please contact customer service for assistance";
                }
            } else {
                return "\"User not registered\"";
            }
        } else if (smsText.startsWith("TrustMM NID")) {
            if (redisDao.getValue(userNumber) != null) {
                String pin = redisDao.getValue(userNumber).split(DATA_SEPARATOR)[0];
                if (pin.equalsIgnoreCase(smsText.split(" ")[3])) {
                    Double amount = 230.00;
                    String nidNo = smsText.split(" ")[1];
                    String serviceType = smsText.split(" ")[5];
                    String correctiontype = smsText.split(" ")[4];
                    String notificationNo = smsText.split(" ")[6];
                    if (notificationNo.length() != 13) {
                        return "\"Please provide valid notification number starting with 8801\"";
                    }
//                    if (Integer.parseInt(correctiontype) > 4) {
//                        return "\"Correction type is not valid. It must be in 1,2,3,4.\"";
//                    }
//                    if (!serviceType.equalsIgnoreCase("1") && !serviceType.equalsIgnoreCase("2")) {
//                        return "\"Service type is not valid. It must be in 1(Regular),2(Express)\"";
//                    }
                    if (debitAccount(userNumber, amount)) {
                        return "\"An amount of Tk. " + amount + " has been paid to EC for NID Number " + nidNo + " . Your current balance is Tk. " +
                                redisDao.getValue(userNumber).split(DATA_SEPARATOR)[1] + ".\""; // TxID#PA22YCZA23LA70119763
                    } else {
                        return "\"Insufficient balance to perform the transaction, your current balance is Tk. " + redisDao.getValue(userNumber).split(DATA_SEPARATOR)[1] + ".\"";
                    }
                } else {
                    return "Incorrect PIN! If you have lost your PIN, please contact customer service for assistance";
                }
            } else {
                return "\"User not registered\"";
            }
        } else if (smsText.startsWith("TRUSTMM RC")) {
            if (redisDao.getValue(userNumber) != null) {
                String pin = redisDao.getValue(userNumber).split(DATA_SEPARATOR)[0];
                if (pin.equalsIgnoreCase(smsText.split(" ")[4])) {
                    Double amount = Double.valueOf(smsText.split(" ")[3]);
                    String recipientNumber = smsText.split(" ")[2];
                    if (recipientNumber.length() != 13) {
                        return "Current length of ToUserNumber has set is 13. Please check the length of ToUserNumber";
                    }
                    if (redisDao.getValue(recipientNumber) != null) {
                        if (debitAccount(userNumber, amount+10.0)) {
                            // creditAccount(recipientNumber, amount);

                            return "\"Cash Out Tk. " + amount + " to " + recipientNumber + ". Balance Tk. " +
                                    redisDao.getValue(userNumber).split(DATA_SEPARATOR)[1] + ". TxID#PA22YCZA23LA70119763\"";

                        } else {
                            return "\"Insufficient balance to perform the transaction, your current balance is Tk. " + redisDao.getValue(userNumber).split(DATA_SEPARATOR)[1] + ".\"";
                        }
                    } else {
                        return "\"You are not authorized to perform this action. Please contact customer service.\"";
                    }
                } else {
                    return "Incorrect PIN! If you have lost your PIN, please contact customer service for assistance.";
                }
            } else {
                return "\"User not registered\"";
            }
        } else if (smsText.startsWith("TRUSTMM ALIC") || smsText.startsWith("TRUSTMM MLIC") || smsText.startsWith("TRUSTMM PILIL") || smsText.startsWith("TRUSTMM MA")) {
            if (redisDao.getValue(userNumber) != null) {
                String pin = redisDao.getValue(userNumber).split(DATA_SEPARATOR)[0];
                if (pin.equalsIgnoreCase(smsText.split(" ")[4])) {
                    Double amount = Double.valueOf(smsText.split(" ")[3]);
                    String policy = smsText.split(" ")[2];
                    String notificationNo = smsText.split(" ")[5];
                    String insuranceName="";
                    if(smsText.split(" ")[1].equalsIgnoreCase("MA")){
                        insuranceName= "Metlife Alico";
                    }
                    else if(smsText.split(" ")[1].equalsIgnoreCase("MLIC")){
                        insuranceName= "Meghna Life Ins Co";
                    }
                    else if(smsText.split(" ")[1].equalsIgnoreCase("PILIL")){
                        insuranceName= "Prime Islami Life Insurance";
                    }
                    else{
                        insuranceName= "Astha Life Insurance Company";

                    }

                    if (redisDao.getValue(policy) != null) {
                        if (notificationNo.length() != 13) {
                            return "\"Please provide valid notification number starting with 88\"";
                        }
                        if (debitAccount(userNumber, amount+10.0)) {
                            return "\"An amount of Tk. " + amount + " has been paid to "+insuranceName+ " for Policy Number " + policy +
                                    ". Your current balance is Tk. " +
                                    redisDao.getValue(userNumber).split(DATA_SEPARATOR)[1] + ". TxID#PA22YCZA23LA70119763\"";
                        } else {
                            return "\"Insufficient balance to perform the transaction, your current balance is Tk. " + redisDao.getValue(userNumber).split(DATA_SEPARATOR)[1] + ".\"";
                        }
                    } else {
                        return "\"Wrong policy no.\"";
                    }
                } else {
                    return "Incorrect PIN! If you have lost your PIN, please contact customer service for assistance";
                }
            } else {
                return "\"User not registered\"";
            }
        } else if (smsText.startsWith("TRUSTMM ACC") || smsText.startsWith("TRUSTMM DMMC") || smsText.startsWith("TRUSTMM DCPSC") || smsText.startsWith("TRUSTMM MIS") || smsText.startsWith("TRUSTMM RCES") || smsText.startsWith("TRUSTMM GES") || smsText.startsWith("TRUSTMM MIST") || smsText.startsWith("TRUSTMM CPSCK") || smsText.startsWith("TRUSTMM MMA") || smsText.startsWith("TRUSTMM RCPSC1") || smsText.startsWith("TRUSTMM GCPSC") || smsText.startsWith("TRUSTMM LORDS") || smsText.startsWith("TRUSTMM JCPSC") || smsText.startsWith("TRUSTMM JESC") || smsText.startsWith("TRUSTMM AFMC") || smsText.startsWith("TRUSTMM CCPC") || smsText.startsWith("TRUSTMM CPSCS") || smsText.startsWith("TRUSTMM HCPSC") || smsText.startsWith("TRUSTMM KCPSC")) {
            if (redisDao.getValue(userNumber) != null) {
                String pin = redisDao.getValue(userNumber).split(DATA_SEPARATOR)[0];
                if (pin.equalsIgnoreCase(smsText.split(" ")[5])) {
                    Double amount = Double.valueOf(100);
                    String institute = smsText.split(" ")[1];
                    String regNo = smsText.split(" ")[2];
                    if (redisDao.getValue(regNo) != null) {
                        if (debitAccount(userNumber, amount+10.0)) {
                            return "\"An amount of Tk. " + amount + " has been paid to " + institute + " for Registration Number " + regNo + ". Your current balance is Tk. " +
                                    redisDao.getValue(userNumber).split(DATA_SEPARATOR)[1] + ". TxID#PA22YCZA23LA70119763\"";
                        } else {
                            return "\"Insufficient balance to perform the transaction, your current balance is Tk. " + redisDao.getValue(userNumber).split(DATA_SEPARATOR)[1] + ".\"";
                        }
                    } else {
                        if (institute.equalsIgnoreCase("BUP") || institute.equalsIgnoreCase("MSSC")) {
                            return "\"Index was outside the bounds of the array.\"";
                        } else if (institute.equalsIgnoreCase("MIS")) {
                            return "\"The SMS format is not valid, please try again or type {bankkeyword} Help and Send to {smssource}\"";
                        } else if (institute.equalsIgnoreCase("KCPSC") || institute.equalsIgnoreCase("MESC") || institute.equalsIgnoreCase("RCES")) {
                            return "\"Invalid data received: The remote server returned an error: (500) Internal Server Error.\"";
                        } else if (institute.equalsIgnoreCase("MIST")) {
                            return "\"Current value of {Paymentmonth} is LONG. Your provided {Paymentmonth}is not LONG\"";
                        } else if (institute.equalsIgnoreCase("LORDS")) {
                            return "\"Current length of {PIN} has set is 4. Please check the length of {PIN}\"";
                        }


                        return "\"The remote server returned an error: (500) Internal Server Error.\"";
                    }
                } else {
                    return "Incorrect PIN! If you have lost your PIN, please contact customer service for assistance";
                }
            } else {
                return "\"User not registered\"";
            }
        } else {
            return "successfully";
        }

    }

    @PostMapping(path = "/UserRegistration", produces = "application/json")
    public String userRegister(@RequestParam("userNumber") String userNumber, @RequestParam("smsText") String smsText, @RequestParam("telcoId") String telcoId, @RequestParam("shortCode") String shortCode, @RequestParam("encKey") String encKey) {
        String[] text = smsText.split(" ");
        String usrNumber = text[2].replace("\\xac\\xed\\x00\\x05t\\x00\\r", "");
        log.debug("user number: " + userNumber + " length: " + userNumber.length());

        if (redisDao.getValue(usrNumber) != null) {
            return "'\"User number already exists\"";
        } else {
            String value = text[12] + DATA_SEPARATOR + getFormatedBalance((double) 0);
            redisDao.setValue(usrNumber, value);
            if (redisDao.getValue(usrNumber) != null) {
                return "'\"Your requested account " + usrNumber + " has been registered successfully.\"";
            } else {
                return "User registration failed.Please try again.";
            }
        }

    }

    @PostMapping(path = "/deleteUser", produces = "application/json")
    public String deleteUSer(@RequestParam("userNumber") String userNumber) {
        redisDao.deleteValue(userNumber);
        return "User Deleted successfully";
    }

    @PostMapping(path = "/GetCoreBankAccountInfo", produces = "application/json")
    public String userRegister(@RequestParam("userNumber") String userNumber, @RequestParam("shortCode") String shortCode) throws JsonProcessingException {
        List<TBLCBSAccountInfo> tblcbsAccountInfos = new ArrayList<>();
        TBLCBSAccountInfo tblcbsAccountInfo = new TBLCBSAccountInfo();
        tblcbsAccountInfo.setKey("7017-0311044864");
        tblcbsAccountInfo.setValue("Trust Bank Account");

        TBLCBSAccountInfo tblcbsAccountInfo1 = new TBLCBSAccountInfo();
        tblcbsAccountInfo1.setKey("7022-0311044157");
        tblcbsAccountInfo1.setValue("Moshiul Huq");

        tblcbsAccountInfos.add(tblcbsAccountInfo);
        tblcbsAccountInfos.add(tblcbsAccountInfo1);

        return new Gson().toJson(tblcbsAccountInfos);
    }


    public String generateHash(String input) throws NoSuchAlgorithmException {

        MessageDigest md = MessageDigest.getInstance("SHA-256");
        byte[] hashInBytes = md.digest(input.getBytes(StandardCharsets.UTF_8));

        // bytes to hex
        StringBuilder sb = new StringBuilder();
        for (byte b : hashInBytes) {
            sb.append(String.format("%02x", b));
        }

        System.out.println(sb.toString());
        return sb.toString();
    }

    public String getFormatedBalance(Double balance) {
        DecimalFormat df2 = new DecimalFormat("###.##");
        return String.valueOf(Double.valueOf(df2.format(balance)));
    }

    public boolean debitAccount(String msisdn, Double amount) {
        String[] redisData = redisDao.getValue(msisdn).split(DATA_SEPARATOR);
        Double balance = Double.valueOf(redisData[1]);
        if (balance > amount) {
            String value = redisData[0] + DATA_SEPARATOR + getFormatedBalance(balance - amount);
            redisDao.setValue(msisdn, value);
            return true;
        } else {
            return false;
        }
    }

    public void creditAccount(String msisdn, Double amount) {
        String[] redisData = redisDao.getValue(msisdn).split(DATA_SEPARATOR);
        Double balance = Double.valueOf(redisData[1]);
        String value = redisData[0] + DATA_SEPARATOR + getFormatedBalance(balance + amount);
        redisDao.setValue(msisdn, value);
    }
}
