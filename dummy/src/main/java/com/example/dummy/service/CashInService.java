package com.example.dummy.service;

import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public interface CashInService {
    Boolean findProcess(String userNo, BigDecimal amount, String referenceNo, String event);
}
