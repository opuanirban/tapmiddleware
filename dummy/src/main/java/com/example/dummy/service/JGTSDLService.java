package com.example.dummy.service;

import com.example.dummy.pojo.JGTSDLBillFetchInput;
import com.example.dummy.pojo.JGTSDLBillFetchResponse;
import com.example.dummy.pojo.JGTSDLBillPaymentInput;
import com.example.dummy.pojo.JGTSDLBillPaymentResponse;
import org.springframework.stereotype.Service;

@Service
public interface JGTSDLService {
    JGTSDLBillFetchResponse loadMockResponseForBillFetch(JGTSDLBillFetchInput jgtsdlBillFetchInput);
    JGTSDLBillPaymentResponse loadMockResponseForBillPayment(JGTSDLBillPaymentInput jgtsdlBillPaymentInput);
}
