package com.example.dummy.service;

import com.example.dummy.dao.RedisDao;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DecimalFormat;

@Service
@Slf4j
public class BalanceServiceImpl implements BalanceService {
    public static final String DATA_SEPARATOR = "###";

    @Autowired
    RedisDao redisDao;
    @Override
    public String getFormatedBalance(Double balance) {
        DecimalFormat df2 = new DecimalFormat("###.##");
        return String.valueOf(Double.valueOf(df2.format(balance)));
    }

    @Override
    public void creditAccount(String msisdn, Double amount) {
        String[] redisData = redisDao.getValue(msisdn).split(DATA_SEPARATOR);
        Double balance = Double.valueOf(redisData[1]);
        String value = redisData[0] + DATA_SEPARATOR + getFormatedBalance(balance + amount);
        redisDao.setValue(msisdn, value);
    }
}
