package com.example.dummy.service;

//import com.example.dummy.dao.PrepaidBillTableDataObjectList;
import com.example.dummy.pojo.*;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class NescoServiceImpl implements NescoService {


    @Override
    public PrepaidBillPreviewResponse preparePrePaidBillMockUp() {
        PrepaidBillPreviewResponse prepaidBillPreviewResponse = new PrepaidBillPreviewResponse();

        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setResponseCode("000");
        baseResponse.setResponseMessage("Dummy Message!");
        baseResponse.setResponseType("");
        baseResponse.setTransactionId("40fdb27a7ae0eca4017aee52d5550101");
//        Energy Cost: 577.09 ; Demand Charge(30/kW): 0.00 ; Rebate(1%): -5.66 ; PFC: 0.00 ; Paid Debt: 0.00 ; VAT(5%): 28.57

//        List<HashMap<String, Object>>tableData = new ArrayList<>();

        List<Object> tableDataObjects = new ArrayList<>();
        PrepaidBillTableDataObject prepaidBillTableDataObject = new PrepaidBillTableDataObject();
        prepaidBillTableDataObject.setLabel_en("Energy Cost");
        prepaidBillTableDataObject.setValue("577.09");
        prepaidBillTableDataObject.setKey("energyCost");
        prepaidBillTableDataObject.setOrder("01");

        tableDataObjects.add(prepaidBillTableDataObject);
        PrepaidBillTableDataObject prepaidBillTableDataObject1 = new PrepaidBillTableDataObject();
        prepaidBillTableDataObject1.setLabel_en("Demand Charge");
        prepaidBillTableDataObject1.setValue("0.0");
        prepaidBillTableDataObject1.setKey("demandCharge");
        prepaidBillTableDataObject1.setOrder("02");

        PrepaidBillTableDataObject prepaidBillTableDataObject2 = new PrepaidBillTableDataObject();
        prepaidBillTableDataObject2.setLabel_en("Rebate (1%)");
        prepaidBillTableDataObject2.setValue("-5.66");
        prepaidBillTableDataObject2.setKey("rebate");
        prepaidBillTableDataObject2.setOrder("03");


        PrepaidBillTableDataObject prepaidBillTableDataObject3 = new PrepaidBillTableDataObject();
        prepaidBillTableDataObject3.setLabel_en("PFC");
        prepaidBillTableDataObject3.setValue("0.00");
        prepaidBillTableDataObject3.setKey("PFC");
        prepaidBillTableDataObject3.setOrder("04");


        PrepaidBillTableDataObject prepaidBillTableDataObject4 = new PrepaidBillTableDataObject();
        prepaidBillTableDataObject4.setLabel_en("Paid Debt");
        prepaidBillTableDataObject4.setKey("0.00");
        prepaidBillTableDataObject4.setValue("paidDebt");
        prepaidBillTableDataObject4.setOrder("05");


        PrepaidBillTableDataObject prepaidBillTableDataObject5 = new PrepaidBillTableDataObject();
        prepaidBillTableDataObject5.setLabel_en("VAT(5%)");
        prepaidBillTableDataObject5.setValue("0.00");
        prepaidBillTableDataObject5.setKey("vat");
        prepaidBillTableDataObject5.setOrder("06");


        tableDataObjects.add(prepaidBillTableDataObject1);
        tableDataObjects.add(prepaidBillTableDataObject2);
        tableDataObjects.add(prepaidBillTableDataObject3);
        tableDataObjects.add(prepaidBillTableDataObject4);
        tableDataObjects.add(prepaidBillTableDataObject5);

        prepaidBillPreviewResponse.setResponse(baseResponse);

        PrepaidBillPayload payload = new PrepaidBillPayload();

        payload.setKey1("33210115");
        payload.setFee("0.0");
        payload.setAccountNumber("01833184019");
        payload.setNotificationNumber("");
        payload.setBillerCode("AKASH");
        payload.setKey4("40fdb27a7ae0eca4017aee52d5550101");// Transaction Id
        prepaidBillPreviewResponse.setTableData(tableDataObjects);
        prepaidBillPreviewResponse.setPayload(payload);
        return prepaidBillPreviewResponse;
    }

    @Override
    public PostpaidBillFetchResponse preparePostPaidBillMockUp() {
        PostpaidBillFetchResponse postpaidBillFetchResponse = new PostpaidBillFetchResponse();
        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setResponseCode("000");
        baseResponse.setResponseMessage("Paid");

        postpaidBillFetchResponse.setResponse(baseResponse);
        postpaidBillFetchResponse.setBillAmount("6700");
        PostpaidBillPayload payload = new PostpaidBillPayload();
        payload.setKey5("40fdb27a7ae0eca4017aee52d5550101");// Transaction ID

        postpaidBillFetchResponse.setPayload(payload);


        PostpaidBillTableDataObject object = new PostpaidBillTableDataObject();
        object.setKey("customerName");
        object.setValue("8801833184033");

        PostpaidBillTableDataObject object1 = new PostpaidBillTableDataObject();
        object1.setKey("dueDate");
        object1.setValue("2021-01-01");

        List<Object> tableData = new ArrayList<>();
        tableData.add(object);
        tableData.add(object1);

        postpaidBillFetchResponse.setTableData(tableData);
        return postpaidBillFetchResponse;
    }
}
