package com.example.dummy.service;

import com.example.dummy.pojo.BillingRequestInput;
import com.example.dummy.pojo.PostpaidBillFetchResponse;
import com.example.dummy.pojo.PrepaidBillPreviewResponse;
import org.springframework.stereotype.Service;

@Service
public interface IvacService {
    PostpaidBillFetchResponse loadMockFetchResponseForIVAC(BillingRequestInput billingRequestInput);
    PostpaidBillFetchResponse loadMockPaymentResponceForIVAC(BillingRequestInput billingRequestInput);
}
