package com.example.dummy.service;

import com.example.dummy.pojo.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
@Slf4j
public class InsuranceServiceImpl implements InsuranceService {
    @Override
    public PILBillFetchResponse previewResponse(BillingRequestInput input) {
        PILBillFetchResponse response = new PILBillFetchResponse();
        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setResponseCode("000");
        baseResponse.setResponseType("Success");
        response.setResponse(baseResponse);
        response.setBillAmount("2000");
        List<Object> objectList = new ArrayList<Object>();

        TableDataObject object = new TableDataObject();
        object.setKey("customerNumber");
        object.setValue("D0400643005");

        TableDataObject object1 = new TableDataObject();
        object1.setKey("customerName");
        object1.setValue("AMIR FAISAL MOHAMMAD ZAKARIA");

        TableDataObject object2 = new TableDataObject();
        object2.setKey("nextPremiumDueDate");
        object2.setValue("2021-11-11");


        TableDataObject object3 = new TableDataObject();
        object3.setKey("policyMaturityDate");
        object3.setValue("2025-11-11");

        TableDataObject object4 = new TableDataObject();
        object4.setKey("mobileNumber");
        object4.setValue("01712098523");

        TableDataObject object5 = new TableDataObject();
        object5.setKey("billAmount");
        object5.setValue("2000");


        objectList.add(object);
        objectList.add(object1);
        objectList.add(object2);
        objectList.add(object3);
        objectList.add(object4);
        objectList.add(object5);

        response.setTableData(objectList);

        PostpaidBillPayload payload = new PostpaidBillPayload();
        payload.setKey3(UUID.randomUUID().toString());
        payload.setBillerCode("PIL");
        response.setPayload(payload);
        return response;
    }
}
