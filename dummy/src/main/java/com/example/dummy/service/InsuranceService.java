package com.example.dummy.service;

import com.example.dummy.pojo.BillingRequestInput;
import com.example.dummy.pojo.PILBillFetchResponse;
import org.springframework.stereotype.Service;

@Service

public interface InsuranceService {
    public PILBillFetchResponse previewResponse(BillingRequestInput input);
}
