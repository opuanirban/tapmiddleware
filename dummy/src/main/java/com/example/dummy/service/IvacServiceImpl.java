package com.example.dummy.service;

import com.example.dummy.pojo.BaseResponse;
import com.example.dummy.pojo.BillingRequestInput;
import com.example.dummy.pojo.PostpaidBillFetchResponse;
import com.example.dummy.pojo.PrepaidBillPreviewResponse;
import lombok.Setter;
import org.springframework.stereotype.Service;

@Service
public class IvacServiceImpl implements IvacService {
    @Override
    public PostpaidBillFetchResponse loadMockFetchResponseForIVAC(BillingRequestInput billingRequestInput) {
        PostpaidBillFetchResponse ivacBillResponse = new PostpaidBillFetchResponse();
        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setResponseCode("000");
        baseResponse.setTransactionId("fe43e29d3f724ba49531867ebefa0f3a");
        ivacBillResponse.setResponse(baseResponse);
        ivacBillResponse.setBillAmount("824");

        return ivacBillResponse;
    }

    @Override
    public PostpaidBillFetchResponse loadMockPaymentResponceForIVAC(BillingRequestInput billingRequestInput) {
        PostpaidBillFetchResponse billPayResponse = new PostpaidBillFetchResponse();
        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setResponseCode("000");
        baseResponse.setResponseMessage("Payment is successful");
        billPayResponse.setResponse(baseResponse);
        return billPayResponse;
    }
}
