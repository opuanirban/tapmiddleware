package com.example.dummy.service;

import com.example.dummy.pojo.*;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AkashServiceImpl implements AkashService {
    @Override
    public AkashBillPreviewResponse loadMockAkashBillPreviewOutput(AkashBillPreviewInput akashBillPreviewInput) {
        AkashBillPreviewResponse output = new AkashBillPreviewResponse();
        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setResponseCode("000");

        output.setResponse(baseResponse);

        AkashBillTableDataObject akashBillTableDataObject = new AkashBillTableDataObject();
        akashBillTableDataObject.setKey("customerName");
        akashBillTableDataObject.setValue("Dummy Name");

        List<Object> list = new ArrayList<>();
        list.add(akashBillTableDataObject);

        output.setTableData(list);

        output.setPayload(null);

        return output;
    }

    @Override
    public BillPaymentResponse loadMockAkashBillPaymentOutput(BillPaymentInput billPaymentInput) {
        BillPaymentResponse billPaymentResponse = new BillPaymentResponse();
        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setResponseCode("000");
        baseResponse.setResponseMessage("Success");

        billPaymentResponse.setResponse(baseResponse);


//        akashBillPaymentResponse.s
        return billPaymentResponse;
    }
}
