package com.example.dummy.service;

import com.example.dummy.pojo.BillPaymentInput;
import com.example.dummy.pojo.BillPaymentResponse;
import com.example.dummy.pojo.AkashBillPreviewInput;
import com.example.dummy.pojo.AkashBillPreviewResponse;
import org.springframework.stereotype.Service;

@Service
public interface AkashService {
    AkashBillPreviewResponse loadMockAkashBillPreviewOutput(AkashBillPreviewInput akashBillPreviewInput);
    BillPaymentResponse loadMockAkashBillPaymentOutput(BillPaymentInput billPaymentInput);
}
