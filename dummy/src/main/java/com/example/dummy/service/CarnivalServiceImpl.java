package com.example.dummy.service;

import com.example.dummy.pojo.*;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

@Service
public class CarnivalServiceImpl implements CarnivalService {
    @Override
    public CarvivalBillPreviewResponse loadMockCarnivalBillPreviewOutput(BillingRequestInput input) {
        CarvivalBillPreviewResponse prepaidBillPreviewResponse = new CarvivalBillPreviewResponse();
        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setResponseCode("000");

        baseResponse.setTransactionId(UUID.randomUUID().toString());
        prepaidBillPreviewResponse.setResponse(baseResponse);

        List<Object> objectList = new ArrayList<>();

        TableDataObject tableDataObject = new TableDataObject();

        tableDataObject.setKey("customerName");
        tableDataObject.setValue("Md.Moshiur Rahman");
        objectList.add(tableDataObject);
        TableDataObject tableDataObject1 = new TableDataObject();
        tableDataObject1.setKey("customerNumber");
        tableDataObject1.setValue("8801833184019");

        objectList.add(tableDataObject1);

        prepaidBillPreviewResponse.setTableData(objectList);

        PrepaidBillPayload payload = new PrepaidBillPayload();
        payload.setKey3(UUID.randomUUID().toString());
        prepaidBillPreviewResponse.setPayload(payload);

        prepaidBillPreviewResponse.setBillAmount("111");

        return prepaidBillPreviewResponse;
    }

    @Override
    public BillPaymentResponse payCarnivalBill(BillPaymentInput billPaymentInput) {
        BillPaymentResponse response = new BillPaymentResponse();
        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setResponseCode("000");
        baseResponse.setResponseMessage("Success!");
        response.setResponse(baseResponse);
        return response;
    }
}
