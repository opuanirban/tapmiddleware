package com.example.dummy.service;

import com.example.dummy.pojo.PostpaidBillFetchResponse;
import com.example.dummy.pojo.PrepaidBillPreviewResponse;
import org.springframework.stereotype.Service;

@Service
public interface NescoService {
    PrepaidBillPreviewResponse preparePrePaidBillMockUp();
    PostpaidBillFetchResponse preparePostPaidBillMockUp();

}
