package com.example.dummy.service;

import org.springframework.stereotype.Service;

@Service
public interface BalanceService {
    public String getFormatedBalance(Double balance);
    public void creditAccount(String msisdn, Double amount);
}
