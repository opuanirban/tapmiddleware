package com.example.dummy.service;

import com.example.dummy.pojo.*;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class JGTSDLServiceImpl implements JGTSDLService {
    @Override
    public JGTSDLBillFetchResponse loadMockResponseForBillFetch(JGTSDLBillFetchInput jgtsdlBillFetchInput) {
        JGTSDLBillFetchResponse response_ = new JGTSDLBillFetchResponse();

        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setResponseCode("000");
        response_.setBillAmount("100");

        response_.setResponse(baseResponse);

        PrepaidBillPayload payload = new PrepaidBillPayload();
        payload.setKey3("40fdb27a7ae0eca4017aee52d5550101");

        response_.setPayload(payload);

        JGTSDLTableDataObject jgtsdlTableDataObject = new JGTSDLTableDataObject();
        jgtsdlTableDataObject.setKey("customerName");
        jgtsdlTableDataObject.setValue("Dummy Customer");

        JGTSDLTableDataObject jgtsdlTableDataObject_ = new JGTSDLTableDataObject();
        jgtsdlTableDataObject_.setKey("monthYear");
        jgtsdlTableDataObject_.setValue("202109");

        List<Object> list = new ArrayList<>();
        list.add(jgtsdlTableDataObject);
        list.add(jgtsdlTableDataObject_);

        response_.setTableData(list);

//        response.s
        return response_;
    }

    @Override
    public JGTSDLBillPaymentResponse loadMockResponseForBillPayment(JGTSDLBillPaymentInput jgtsdlBillPaymentInput) {
        JGTSDLBillPaymentResponse jgtsdlBillPaymentResponse = new JGTSDLBillPaymentResponse();

        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setResponseCode("000");
        baseResponse.setResponseMessage("Success");
        jgtsdlBillPaymentResponse.setResponse(baseResponse);

        return jgtsdlBillPaymentResponse;
    }
}
