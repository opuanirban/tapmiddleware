package com.example.dummy.service;

import com.example.dummy.pojo.*;
import org.springframework.stereotype.Service;

@Service
public interface CarnivalService {
    public CarvivalBillPreviewResponse loadMockCarnivalBillPreviewOutput(BillingRequestInput input);
    public BillPaymentResponse payCarnivalBill(BillPaymentInput billPaymentInput);
}

