package com.example.dummy;

import lombok.Data;

@Data
public class TblRegistrationStatus {
    private String status;
    private RegistrationParams detail;
}
