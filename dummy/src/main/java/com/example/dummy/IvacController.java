package com.example.dummy;

import com.example.dummy.pojo.BillingRequestInput;
import com.example.dummy.pojo.JGTSDLBillFetchInput;
import com.example.dummy.pojo.JGTSDLBillPaymentInput;
import com.example.dummy.service.IvacService;
import com.example.dummy.service.JGTSDLService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class IvacController {
    @Autowired
    IvacService ivacService;
    @PostMapping("/billing/ssl/ivac/preview")
    @ResponseBody
    public ResponseEntity<?> fetchIVacBill(@RequestBody BillingRequestInput billingRequestInput){
        return ResponseEntity.ok(ivacService.loadMockFetchResponseForIVAC(billingRequestInput));
    }

    @PostMapping("/billing/ssl/ivac/payment")
    @ResponseBody
    public ResponseEntity<?> payJGTDSLBill(@RequestBody BillingRequestInput billingRequestInput){
        return ResponseEntity.ok(ivacService.loadMockPaymentResponceForIVAC(billingRequestInput));
    }

}
