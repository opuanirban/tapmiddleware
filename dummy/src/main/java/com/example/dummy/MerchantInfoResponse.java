package com.example.dummy;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class MerchantInfoResponse {
    @JsonProperty("AccountId")
    private long accountId;

    @JsonProperty("AccountNumber")
    private long accountNumber;

    @JsonProperty("AccountName")
    private String accountName;

    @JsonProperty("AccountType")
    private String accountType;
}
