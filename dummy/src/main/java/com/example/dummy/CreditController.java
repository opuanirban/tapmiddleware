package com.example.dummy;

import com.example.dummy.pojo.CreditBalanceRequest;
import com.example.dummy.service.BalanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CreditController {

    @Autowired
    BalanceService balanceService;

    @PostMapping("/credit-balance")
    @ResponseBody
    public ResponseEntity<?> creditBalance(@RequestBody CreditBalanceRequest creditBalanceRequest){
//        balanceService.creditAccount(creditBalanceRequest.getMsisdn(),creditBalanceRequest.getAmount());
        return ResponseEntity.ok("Successful");
    }
}
