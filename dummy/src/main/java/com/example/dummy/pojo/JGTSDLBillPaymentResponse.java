package com.example.dummy.pojo;

import lombok.Data;

import java.util.List;

@Data
public class JGTSDLBillPaymentResponse {
    private BaseResponse response;
    private String dateTime;
    private String billAmount;
    private String billFee;
    private List<Object> additionalInfo;

}
