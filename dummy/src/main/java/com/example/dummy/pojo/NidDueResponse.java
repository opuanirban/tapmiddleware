package com.example.dummy.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class NidDueResponse {
    @JsonProperty("Amount")
    private String Amount;
}
