package com.example.dummy.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class AgentDocumentItem {
    @JsonProperty("FileTitle")
    private String documentTitle;

    @JsonProperty("FileType")
    private String documentType;

    @JsonProperty("FileExtension")
    private String documentExtension;

    @JsonProperty("ContentType")
    private String documentContentType;

    @JsonProperty("FullPath")
    private String documentPath;
}
