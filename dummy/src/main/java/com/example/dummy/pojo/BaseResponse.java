package com.example.dummy.pojo;

import lombok.Data;

@Data
public class BaseResponse {
    private String responseCode;
    private String responseType;
    private String responseMessage;
    private String transactionId;
}
