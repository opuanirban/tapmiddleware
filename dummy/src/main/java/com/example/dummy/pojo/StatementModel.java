package com.example.dummy.pojo;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class StatementModel {
    private String TxCode;
    private String TransactionDate;
    private String TransactionType;
    private Long SenderNumber;
    private Long ReceiverNumber;
    private String SenderName;
    private String ReceiverName;
    private String ActionType;
    private BigDecimal Amount;
}
