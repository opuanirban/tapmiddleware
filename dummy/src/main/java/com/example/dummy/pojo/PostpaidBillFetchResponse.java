package com.example.dummy.pojo;

import lombok.Data;

import java.util.List;

@Data
public class PostpaidBillFetchResponse {
    private BaseResponse response;
    private String billAmount;
    private List<Object> tableData;
    private PostpaidBillPayload payload;
//    private HashMap<String, Object> payload;
}
