package com.example.dummy.pojo;

import lombok.Data;

@Data
public class JGTSDLBillFetchResponse extends PrepaidBillPreviewResponse {
    private String billAmount;
}
