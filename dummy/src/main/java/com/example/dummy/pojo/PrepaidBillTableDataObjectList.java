package com.example.dummy.pojo;

import lombok.Data;

import java.util.List;

@Data
public class PrepaidBillTableDataObjectList {
    List<PrepaidBillTableDataObject> prepaidBillTableDataObjectList;
}
