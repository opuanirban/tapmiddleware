package com.example.dummy.pojo;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.List;

@Data
@Slf4j
public class CarvivalBillPreviewResponse {
    private BaseResponse response;
    private List<Object> tableData;
    private PrepaidBillPayload payload;
    private String billAmount;
}
