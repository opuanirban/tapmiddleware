package com.example.dummy.pojo;

import lombok.Data;

import java.util.List;

@Data
public class BillPaymentResponse {
    private BaseResponse response;
    private String dateTime;
    private String billAmount;
    private String billFee;
    private List<Object> additionalInfo;

}
