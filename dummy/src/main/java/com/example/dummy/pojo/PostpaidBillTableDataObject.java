package com.example.dummy.pojo;

import lombok.Data;

@Data
public class PostpaidBillTableDataObject {
    private String key;
    private String value;
}
