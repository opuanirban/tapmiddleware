package com.example.dummy.pojo;

import lombok.Data;

@Data
public class PrepaidBillPayload {
    private String key1;
    private String key2;
    private String key5;
    private String pin;
    private String key6;
    private String key3;
    private String key4;
    private String fee;
    private String key7;
    private String accountNumber;
    private String notificationNumber;
    private String billerCode;

}
