package com.example.dummy.pojo;

import lombok.Data;

import java.util.List;

@Data
public class AkashBillTableDataObjectList {
    List<AkashBillTableDataObject> akashBillTableDataObjectList;
}
