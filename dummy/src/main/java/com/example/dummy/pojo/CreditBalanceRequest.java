package com.example.dummy.pojo;

import lombok.Data;

@Data
public class CreditBalanceRequest {
    private String msisdn;
    private Double amount;
}
