package com.example.dummy;

import com.example.dummy.pojo.JGTSDLBillFetchInput;
import com.example.dummy.pojo.JGTSDLBillPaymentInput;
import com.example.dummy.service.JGTSDLService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class JGTDSLController {

    @Autowired
    JGTSDLService jgtsdlService;
    @PostMapping("/billing/jgtdsl/fetch")
    @ResponseBody
    public ResponseEntity<?> fetchJGTDSLBill(@RequestBody JGTSDLBillFetchInput jgtsdlBillFetchInput){
        return ResponseEntity.ok(jgtsdlService.loadMockResponseForBillFetch(jgtsdlBillFetchInput));
    }

    @PostMapping("/billing/jgtdsl/payment")
    @ResponseBody
    public ResponseEntity<?> payJGTDSLBill(@RequestBody JGTSDLBillPaymentInput jgtsdlBillPaymentInput){
        return ResponseEntity.ok(jgtsdlService.loadMockResponseForBillPayment(jgtsdlBillPaymentInput));
    }

}
