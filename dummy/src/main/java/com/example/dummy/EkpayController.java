package com.example.dummy;

import com.example.dummy.pojo.*;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;
import java.util.regex.PatternSyntaxException;
import java.util.zip.DataFormatException;

@RestController
public class EkpayController {
    @PostMapping("/billing/preview")
    @ResponseBody
    public PostpaidBillFetchResponse getEkPayReview(@RequestBody BillingRequestInput billingRequestInput){
        PostpaidBillFetchResponse response = new PostpaidBillFetchResponse();
        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setResponseCode("000");
        response.setResponse(baseResponse);
        response.setBillAmount("300");
        PostpaidBillPayload payload = new PostpaidBillPayload();
        payload.setKey3(UUID.randomUUID().toString());
        payload.setKey6("2021-11-28");

        response.setPayload(payload);
        return response;
    }

    @PostMapping("/billing/payment")
    public BillPaymentResponse getEkpayPayment(@RequestBody BillPaymentInput input){
        BillPaymentResponse response = new BillPaymentResponse();
        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setResponseCode("000");
        baseResponse.setResponseMessage("Bill has been paid successfully!");
        response.setResponse(baseResponse);
        return response;
    }

//    @PostMapping("/")
}
