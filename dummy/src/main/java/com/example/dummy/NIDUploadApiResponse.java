package com.example.dummy;

import lombok.Data;

@Data
public class NIDUploadApiResponse {
    private String status;
    private NIDData data;
}
