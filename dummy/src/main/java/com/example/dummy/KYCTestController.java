package com.example.dummy;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;

@RestController
public class KYCTestController {

    @PostMapping(path = "/nid-upload", consumes = {"multipart/form-data"}, produces = "application/json")
    public NIDUploadApiResponse ocr(@RequestParam("step") String step, @RequestParam("crop") String corp, @RequestParam("id_front") MultipartFile id_front, @RequestParam("id_back") MultipartFile id_back, HttpServletRequest httpServletRequest) throws Exception {
        final String authorizationHeader = httpServletRequest.getHeader("Authorization");
        if (authorizationHeader != null && authorizationHeader.startsWith("Token ")) {
            NIDUploadApiResponse nidUploadApiResponse = new NIDUploadApiResponse();
            NIDData nidData = new NIDData();
            nidData.setNid_no("5558284157487");
            nidData.setDob("1980/10/1");
            nidData.setCustomer_name_ben("নাজমা আক্তার");
            nidData.setCustomer_name_eng("Nazma Akhter");
            nidData.setFather_name("none");
            nidData.setMother_name("মুনিরা আক্তার");
            nidData.setSpouse_name("আসাদুজ্জামান খান");
            nidData.setAddress("22/1, ইসলামপুর রাড, খুলনা সিটি কর্পোরেশন");
            nidData.setId_front_image("data:image/png;base64,/9j/4AAQSkZJRgABAQEAeAB4AAD/4RUyRXhpZgAATU0AKgAAAAgABgEPAAIAAAALAAAAVgEQAAIAAAAJAA AAYgESAAMAAAABAAEAAAEyAAIAAAAUAAAAbIdpAAQAAAABAAAAgIglAAQAAAABAAAB1AAAAnhITUQgR2xvYmFsAABOb2tpYSBYNgAAMIA AAAENjg5AJKRAAIAAAAENjg5AJKSAAIAAAAENjg5AKQDAAMAAAABAAAAAOodAAkAAAABAAAATgAAAAAAAAAnAAAD6AAAAMgAAABkMjAxO DoxMjoyNSAyMD");
            nidData.setId_back_image("data:image/png;base64,/9j/4AAQSkZJRgABAQEAeAB4AAD/4RUyRXhpZgAATU0AKgAAAAgABgEPAAIAAAALAAAAVgEQAAIAAAAJAA AAYgESAAMAAAABAAEAAAEyAAIAAAAUAAAAbIdpAAQAAAABAAAAgIglAAQAAAABAAAB1AAAAnhITUQgR2xvYmFsAABOb2tpYSBYNgAAMIA AAAENjg5AJKRAAIAAAAENjg5AJKSAAIAAAAENjg5AKQDAAMAAAABAAAAAOodAAkAAAABAAAATgAAAAAAAAAnAAAD6AAAAMgAAABkMjAxO DoxMjoyNSAyMD");
            nidData.setId_front_name("idNameSomething.jpg");
            nidData.setId_back_name("idBackNameSomething.jpg");
            nidUploadApiResponse.setData(nidData);
            nidUploadApiResponse.setStatus("success");

            return nidUploadApiResponse;
        } else {
            throw new Exception("invalid header/form data");
        }
    }

    @PostMapping(path = "/customer-registration/", produces = "application/json")
    public KYCRegistrationResponse kycRegister(HttpServletRequest httpServletRequest, @RequestParam("step") String step, @RequestParam("nid_no") String nid_no, @RequestParam("dob") String dob, @RequestParam("applicant_name_ben") String applicant_name_ben, @RequestParam("applicant_name_eng") String applicant_name_eng, @RequestParam("father_name") String father_name, @RequestParam("mother_name") String mother_name, @RequestParam("spouse_name") String spouse_name, @RequestParam("pres_address") String pres_address, @RequestParam("perm_address") String perm_address, @RequestParam("id_front_name") String id_front_name, @RequestParam("id_back_name") String id_back_name, @RequestParam("gender") String gender, @RequestParam("profession") String profession, @RequestParam("nominee") String nominee, @RequestParam("nominee_relation") String nominee_relation, @RequestParam("mobile_number") String mobile_number,@RequestParam("applicant_photo") MultipartFile applicant_photo) throws Exception {
        final String authorizationHeader = httpServletRequest.getHeader("Authorization");
        if (authorizationHeader != null && authorizationHeader.startsWith("Token ")) {

            KYCRegistrationResponse kycRegistrationResponse = new KYCRegistrationResponse();
            kycRegistrationResponse.setStatus("success");
            kycRegistrationResponse.setDetails("Application successfully submitted, awaiting identity verification.");
            return kycRegistrationResponse;
        } else {
            throw new Exception("invalid header/form data");
        }
    }

    @PostMapping(path = "/get-verification-status/", produces = "application/json")
    public NIDVerificationResponse kycRegister(HttpServletRequest httpServletRequest, @RequestParam("mobile_number") String mobile_number) throws Exception {
        final String authorizationHeader = httpServletRequest.getHeader("Authorization");
        if (authorizationHeader != null && authorizationHeader.startsWith("Token ")) {

            NIDVerifyData nidVerifyData = new NIDVerifyData();
            NIDVerificationStatus nidData = new NIDVerificationStatus();
            nidData.setNid_no("5558284157487");
            nidData.setDob("1980/10/1");
            nidData.setMobile_number(mobile_number);
            nidData.setApplicant_name_ben("নাজমা আক্তার");
            nidData.setApplicant_name_ben_score(85);
            nidData.setApplicant_name_eng("Nazma Akhter");
            nidData.setApplicant_name_eng_score(85);
            nidData.setFather_name("none");
            nidData.setFather_name_score(0);
            nidData.setMother_name("মুনিরা আক্তার");
            nidData.setMother_name_score(86);
            nidData.setSpouse_name("আসাদুজ্জামান খান");
            nidData.setSpouse_name_score(85);
            nidData.setPres_address("22/1, ইসলামপুর রাড, খুলনা সিটি কর্পোরেশন");
            nidData.setPres_address_score(85);
            nidData.setTextual_info_match(true);
            nidData.setApplicant_photo_from_card("data:image/png;base64,/9j/4AAQSkZJRgABAQEAeAB4AAD/4RUyRXhpZgAA TU0AKgAAAAgABgEPAAIAAAALAAAAVgEQAAIAAAAJAAAAYgESAAMAAAABAAEAAAEyAAIAAAAUAAAAbIdpAAQA");
            nidData.setApplicant_photo_from_app("data:image/png;base64,/9j/4AAQSkZJRgABAQEAeAB4AAD/4RUyRXhpZgAAT U0AKgAAAAgABgEPAAIAAAALAAAAVgEQAAIAAAAJAAAAYgESAAMAAAABAAEAAAEyAAIAAAAUAAAAbIdpAAQA");
            nidData.setApplicant_photo_from_app_ec_match(true);
            nidData.setApplicant_photo_from_card_ec_match(true);
            nidVerifyData.setStatus("passed");
            nidVerifyData.setDetail(nidData);
            NIDVerificationResponse nidVerificationResponse = new NIDVerificationResponse();
            nidVerificationResponse.setStatus("success");
            nidVerificationResponse.setData(nidVerifyData);
            nidVerificationResponse.setStatus_code(200);
            nidVerificationResponse.setMessage("success");
            return nidVerificationResponse;
        } else {
            throw new Exception("invalid header/form data");
        }
    }
}
