package com.example.dummy;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class RegistrationParams {

    @JsonProperty("UserKey")
    private long userKey;

    @JsonProperty("PermanentAddress")
    private String permanentAddress;

    @JsonProperty("UserNumber")
    private String userNumber;

    @JsonProperty("FirstName")
    private String firstName;

    @JsonProperty("LastName")
    private String lastName;

    @JsonProperty("NationalIdNumber")
    private String nationalIdNumber;

    @JsonProperty("Sex")
    private String sex;

    @JsonProperty("UserGroupKey")
    private String userGroupKey;

    @JsonProperty("DoB")
    private String doB;

    @JsonProperty("Occupation")
    private String occupation;

    @JsonProperty("OperatorName")
    private String operatorName;

    @JsonProperty("AccountPin")
    private String accountPin;

    @JsonProperty("BanglaName")
    private String banglaName;

    @JsonProperty("EnglishName")
    private String englishName;

    @JsonProperty("Nominee")
    private String nominee;

    @JsonProperty("NomineeRelation")
    private String nomineeRelation;

    @JsonProperty("FatherName")
    private String fatherName;

    @JsonProperty("MotherName")
    private String motherName;

    @JsonProperty("SpouseName")
    private String spouseName;

    @JsonProperty("PresentAddress")
    private String presentAddress;

    @JsonProperty("PhotoString")
    private String photoString;

    @JsonProperty("IslimitedKYC")
    private Boolean isLimitedKYC;
    
}