package com.example.dummy;

import com.example.dummy.pojo.BillingRequestInput;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@Component
public class TopUpController {
    @PostMapping("/payment")
    @ResponseBody
    public ResponseEntity<?> topUpPayment(@RequestBody TopUpRequest topUpRequest){
        TopUpBillResponse response = new TopUpBillResponse();
        response.setStatusCode("200");
        response.setMessage("BDT "+topUpRequest.getAmount()+" amount recharged successfully");
        return ResponseEntity.ok(response);
    }
}
