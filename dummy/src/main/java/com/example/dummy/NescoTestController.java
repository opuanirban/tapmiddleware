package com.example.dummy;

import com.example.dummy.pojo.BaseResponse;
import com.example.dummy.pojo.BillPaymentInput;
import com.example.dummy.pojo.BillPaymentResponse;
import com.example.dummy.pojo.BillingRequestInput;
import com.example.dummy.service.NescoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class NescoTestController {

    @Autowired
    NescoService nescoService;

    @PostMapping("/billing/nesco/prepaid/preview")
    @ResponseBody
    public ResponseEntity<?> nescoBilligPrepaidReview(@RequestBody BillingRequestInput billingRequestInput){
        return ResponseEntity.ok(nescoService.preparePrePaidBillMockUp());
    }

    @PostMapping("/billing/nesco/postpaid/fetch")
    @ResponseBody
    public ResponseEntity<?> nescoPostPaidBillDetails(@RequestBody BillingRequestInput billingRequestInput){

        return ResponseEntity.ok(nescoService.preparePostPaidBillMockUp());
    }

    @GetMapping("/billing/nesco/prepaid/search")
    public String searchTransactionId(@RequestParam("transactionId") String transactionId){
        return "a";
    }

    @PostMapping("/billing/nesco/prepaid/payment")
    public BillPaymentResponse paymentResponse(@RequestBody BillPaymentInput billPaymentInput){
        BillPaymentResponse response = new BillPaymentResponse();
        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setResponseCode("000");
        baseResponse.setResponseMessage("An amount of Tk"+billPaymentInput.getFee()+" has been paid to NESCO for " +
                "prepaid Customer Number "+billPaymentInput.getAccountNumber()+". Your current Balance is 5000.0 Tk." +
                " TxID:" +billPaymentInput.getKey4()+".");
        response.setResponse(baseResponse);
        return response;
    }
}
