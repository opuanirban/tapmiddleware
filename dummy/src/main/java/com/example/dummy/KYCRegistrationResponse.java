package com.example.dummy;

import lombok.Data;

@Data
public class KYCRegistrationResponse {
    private String status;
    private String details;
}
