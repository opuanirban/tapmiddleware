package com.example.dummy;

import lombok.Data;

@Data
public class NIDVerificationStatus {
    private String nid_no;
    private String dob;
    private String mobile_number;
    private String applicant_name_ben;
    private int applicant_name_ben_score;
    private String applicant_name_eng;
    private int applicant_name_eng_score;
    private String father_name;
    private int father_name_score;
    private String mother_name;
    private int mother_name_score;
    private String spouse_name;
    private int spouse_name_score;
    private String pres_address;
    private int pres_address_score;
    private Boolean textual_info_match;
    private String applicant_photo_from_card;
    private String applicant_photo_from_app;
    private Boolean applicant_photo_from_card_ec_match;
    private Boolean applicant_photo_from_app_ec_match;

}
