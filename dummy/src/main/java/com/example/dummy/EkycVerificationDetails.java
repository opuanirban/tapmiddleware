package com.example.dummy;

import lombok.Data;

@Data
public class EkycVerificationDetails {
    private String nid_no;
    private String dob;
    private String gender;
    private String profession;
    private String applicant_name_ben;
    private int applicant_name_ben_score;
    private String applicant_name_eng;
    private int applicant_name_eng_score;
    private String father_name;
    private int father_name_score;
    private String mother_name;
    private int mother_name_score;
    private String spouse_name;
    private int spouse_name_score;
    private String pres_address;
    private int pres_address_score;
    private String prem_address;
    private int prem_address_score;
    private String applicant_photo;
    private int applicant_photo_score;
    private String mobile_number;
    private String nominee;
    private String nominee_relation;
}
