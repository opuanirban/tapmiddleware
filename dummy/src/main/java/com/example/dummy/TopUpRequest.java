package com.example.dummy;

import lombok.Data;

import java.util.Locale;

@Data
public class TopUpRequest {
    private String recipientNumber;
    private int amount;
    private String connectionType;
    private String operator;
    private String pin;
    private String accountNumber;
    private Locale requestTime;

}
