package com.example.dummy;

import com.example.dummy.pojo.BillPaymentInput;
import com.example.dummy.pojo.BillingRequestInput;
import com.example.dummy.service.CarnivalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CarnivalController {
    @Autowired
    CarnivalService carnivalService;

//    /billing/internet/carnival/fetch
    @PostMapping("/billing/internet/carnival/fetch")
    @ResponseBody
    public ResponseEntity<?> carnivalBillFetch(@RequestBody BillingRequestInput billingRequestInput){

        return ResponseEntity.ok(carnivalService.loadMockCarnivalBillPreviewOutput(billingRequestInput));
    }

    @PostMapping("/billing/internet/carnival/payment")
    @ResponseBody
    public ResponseEntity<?> carnivalBillPayment(@RequestBody BillPaymentInput billPaymentInput){
        return ResponseEntity.ok(carnivalService.payCarnivalBill(billPaymentInput));
    }

}
