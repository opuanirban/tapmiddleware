package com.example.dummy;

import com.example.dummy.pojo.BillPaymentInput;
import com.example.dummy.pojo.AkashBillPreviewInput;
import com.example.dummy.service.AkashService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AkashController {

    @Autowired
    AkashService akashService;

    @PostMapping("/billing/tv/akash/preview")
    @ResponseBody
    public ResponseEntity<?> akashBillPreview(@RequestBody AkashBillPreviewInput akashBillPreviewInput){
        return ResponseEntity.ok(akashService.loadMockAkashBillPreviewOutput(akashBillPreviewInput));
    }

    @PostMapping("/billing/tv/akash/payment")
    @ResponseBody
    public ResponseEntity<?> akashBillPayment(@RequestBody BillPaymentInput billPaymentInput){
        return ResponseEntity.ok(akashService.loadMockAkashBillPaymentOutput(billPaymentInput));
    }

}
