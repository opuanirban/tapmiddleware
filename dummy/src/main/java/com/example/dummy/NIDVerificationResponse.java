package com.example.dummy;

import lombok.Data;

@Data
public class NIDVerificationResponse {
    private String status;
    private Integer status_code;
    private NIDVerifyData data;
    private String message;
}
