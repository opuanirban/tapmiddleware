package com.example.dummy;

import com.example.dummy.pojo.*;
import com.example.dummy.service.InsuranceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PrimeLifeInsuraceController {

    @Autowired
    InsuranceService insuranceService;
    @PostMapping("/billing/insurance/pil/fetch")
    @ResponseBody
    public PILBillFetchResponse pil_fecth(@RequestBody BillingRequestInput input){
        return insuranceService.previewResponse(input);
    }

    @PostMapping("/billing/insurance/pil/payment")
    @ResponseBody
    public BillPaymentResponse pil_pay_bill(@RequestBody BillPaymentInput input){
        BillPaymentResponse response = new BillPaymentResponse();
        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setResponseCode("000");
        baseResponse.setResponseMessage("Bill has been paid successfully!");
        response.setResponse(baseResponse);
        return response;
    }
}
