package com.example.dummy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class DummyApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(DummyApplication.class, args);
    }

}
