package com.example.dummy;

import com.example.dummy.dao.RedisDao;
import com.example.dummy.pojo.AgentRegistrationParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.text.DecimalFormat;

@RestController
public class AgentRegistrationController {
    @Autowired
    RedisDao redisDao;

    public static final String DATA_SEPARATOR = "###";
    public static final String BALANCE_FORMAT = "###.##";


    public String getFormatedBalance(Double balance) {
        DecimalFormat df2 = new DecimalFormat("###.##");
        return String.valueOf(Double.valueOf(df2.format(balance)));
    }

    @PostMapping("/AgentRegistration")
    @ResponseBody
    public String agentRegistration(@RequestBody AgentRegistrationParams registrationParams) {
//        redisDao.setValue();
        String userNumber = registrationParams.getUserNumber().replace("\\xac\\xed\\x00\\x05t\\x00\\r", "");
        if (redisDao.getValue(userNumber) != null) {
            return "User already exists";
        }
        else
        {
            String value = registrationParams.getAccountPin() + DATA_SEPARATOR + getFormatedBalance((double) 0);
//            String  value="";
            redisDao.setValue(userNumber, value);
            redisDao.setValue(userNumber+"ekyc","0");

            //tblStatus
            if (redisDao.getValue(userNumber) != null) {
                return "\"Thank you. Account created successfully\"";
            } else {
                return "Registration data insert fail";
            }
        }
    }


}
