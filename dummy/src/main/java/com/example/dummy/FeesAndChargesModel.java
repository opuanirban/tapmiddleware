package com.example.dummy;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class FeesAndChargesModel {

    @JsonProperty("FromUserNumber")
    private String FromUserNumber;
    @JsonProperty("ToUserNumber")
    private String ToUserNumber;
    @JsonProperty("TransactionType")
    private String TransactionType;
    @JsonProperty("Amount")
    private BigDecimal Amount;
}
