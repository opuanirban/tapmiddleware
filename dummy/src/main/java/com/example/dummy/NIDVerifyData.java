package com.example.dummy;

import lombok.Data;

@Data
public class NIDVerifyData {
    private String status;
    private NIDVerificationStatus detail;
}
