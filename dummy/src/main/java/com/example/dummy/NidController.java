package com.example.dummy;

import com.example.dummy.pojo.NidDueResponse;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class NidController {
//
    @PostMapping("/GetNIDFeesAmount")
    @ResponseBody
    public NidDueResponse getNidFeesAmount(@RequestParam("nidNo") String nidNo,
                                           @RequestParam("correctionType")String correctionType,
                                           @RequestParam("serviceType") String serviceType){
        NidDueResponse response = new NidDueResponse();
        response.setAmount("500");
        return response;
    }
}
