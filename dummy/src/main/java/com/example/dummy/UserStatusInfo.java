package com.example.dummy;

import lombok.Data;

@Data
public class UserStatusInfo {
    private boolean userStatus;
}
