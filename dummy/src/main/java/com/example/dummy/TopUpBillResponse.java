package com.example.dummy;

import lombok.Data;

@Data
public class TopUpBillResponse {
    private String statusCode;
    private String message;
}
