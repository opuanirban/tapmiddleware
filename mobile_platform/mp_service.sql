-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 17, 2020 at 02:24 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mp_service`
--

-- --------------------------------------------------------

--
-- Table structure for table `device`
--

CREATE TABLE `device` (
  `id` int(11) NOT NULL,
  `device_uid` varchar(255) NOT NULL,
  `os_name` varchar(100) NOT NULL,
  `os_version` varchar(100) NOT NULL,
  `manufacturer` varchar(50) NOT NULL,
  `os_firm_wire_build` varchar(50) NOT NULL,
  `model_name` varchar(30) NOT NULL,
  `mac_address` varchar(17) NOT NULL,
  `imei_list` varchar(255) NOT NULL,
  `root_device` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `device`
--

INSERT INTO `device` (`id`, `device_uid`, `os_name`, `os_version`, `manufacturer`, `os_firm_wire_build`, `model_name`, `mac_address`, `imei_list`, `root_device`, `created`) VALUES
(1, 'C8PVCK9RJC69', 'iOS', '13.6', 'apple', '3.05.00', 'iPhone 8', '40:98:AD:0A:DF:B1', '35 67 6308 106763 2', 1, '2020-08-17 11:49:48'),
(2, 'C8PVCK9RJC69', 'iOS', '13.6', 'apple', '3.05.00', 'iPhone 8', '40:98:AD:0A:DF:B1', '35 67 6308 106763 2', 1, '2020-08-17 11:53:31');

-- --------------------------------------------------------

--
-- Table structure for table `wallet`
--

CREATE TABLE `wallet` (
  `id` int(11) NOT NULL,
  `wallet_uid` varchar(255) NOT NULL,
  `device_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` varchar(10) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wallet`
--

INSERT INTO `wallet` (`id`, `wallet_uid`, `device_id`, `user_id`, `status`, `created`) VALUES
(1, '69a26bc7-1009-4715-8ccf-8db14f9069c3', 1, 1, '1', '2020-08-17 11:49:52'),
(2, 'c3dcf648-ffc6-4342-817e-0f7811d521df', 2, 1, '0', '2020-08-17 11:53:34');

-- --------------------------------------------------------

--
-- Table structure for table `wallet_user`
--

CREATE TABLE `wallet_user` (
  `id` int(11) NOT NULL,
  `msisdn` varchar(15) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` varchar(10) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wallet_user`
--

INSERT INTO `wallet_user` (`id`, `msisdn`, `name`, `status`, `created`) VALUES
(1, '01833184008', '01833184008', '0', '2020-08-17 11:49:45');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `device`
--
ALTER TABLE `device`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wallet`
--
ALTER TABLE `wallet`
  ADD PRIMARY KEY (`id`),
  ADD KEY `device_relation` (`device_id`),
  ADD KEY `user_relation` (`user_id`);

--
-- Indexes for table `wallet_user`
--
ALTER TABLE `wallet_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `device`
--
ALTER TABLE `device`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `wallet`
--
ALTER TABLE `wallet`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `wallet_user`
--
ALTER TABLE `wallet_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `wallet`
--
ALTER TABLE `wallet`
  ADD CONSTRAINT `device_relation` FOREIGN KEY (`device_id`) REFERENCES `device` (`id`),
  ADD CONSTRAINT `user_relation` FOREIGN KEY (`user_id`) REFERENCES `wallet_user` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
