package com.mfs.mobile_platform.enumeration;

public enum WalletStatus {
    ACTIVE,
    INACTIVE;
}
