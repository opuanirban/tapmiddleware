package com.mfs.mobile_platform.enumeration;

public enum RequestType {
    NEW_REGISTRATION,
    DEVICE_CHANGE,
}
