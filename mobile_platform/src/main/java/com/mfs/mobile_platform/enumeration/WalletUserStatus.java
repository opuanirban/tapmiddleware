package com.mfs.mobile_platform.enumeration;

public enum WalletUserStatus {
    ACTIVE,
    INACTIVE,
    SUSPENDED;
}
