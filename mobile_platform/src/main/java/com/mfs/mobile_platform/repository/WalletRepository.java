package com.mfs.mobile_platform.repository;

import com.mfs.mobile_platform.entity.Device;
import com.mfs.mobile_platform.entity.Wallet;
import com.mfs.mobile_platform.entity.WalletUser;
import org.hibernate.sql.Update;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface WalletRepository extends JpaRepository<Wallet, Integer> {
    Optional<List<Wallet>> findByWalletUser(WalletUser walletUser);
    Optional<Wallet> findByDeviceAccess(Device device);
}
