package com.mfs.mobile_platform.repository;

import com.mfs.mobile_platform.entity.WalletUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface WalletUserRepository extends JpaRepository<WalletUser, Integer> {
    Optional<WalletUser> findByMsisdn (String msisdn);
}
