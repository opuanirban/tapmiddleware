package com.mfs.mobile_platform.exception;

public class WalletExistException extends RuntimeException {

    public WalletExistException(String message) {
        super(message);
    }

    public WalletExistException(String message, Throwable cause) {
        super(message, cause);
    }
}
