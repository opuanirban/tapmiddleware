package com.mfs.mobile_platform.exception;

public class WalletCreateException extends RuntimeException {

    public WalletCreateException(String message) {
        super(message);
    }

    public WalletCreateException(String message, Throwable cause) {
        super(message, cause);
    }
}
