package com.mfs.mobile_platform.exception;

import com.mfs.mobile_platform.Util.CommonConstant;
import com.mfs.mobile_platform.dto.WalletCreateResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class CustomExceptionHandler {

    @ExceptionHandler(value = WalletExistException.class)
    public ResponseEntity<Object> handleApiRequestException(WalletExistException e) {
        CustomException customException = new CustomException(
                CommonConstant.WALLET_ERROR,
                e.getMessage()
        );
        return new ResponseEntity<>(customException, org.springframework.http.HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = WalletNotFoundException.class)
    public ResponseEntity<Object> handleApiRequestException(WalletNotFoundException e) {
        CustomException customException = new CustomException(
                CommonConstant.WALLET_NOT_FOUND,
                e.getMessage()
        );
        return new ResponseEntity<>(customException, org.springframework.http.HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = WalletInactiveException.class)
    public ResponseEntity<Object> handleApiRequestException(WalletInactiveException e) {
        CustomException customException = new CustomException(
                CommonConstant.WALLET_NOT_ACTIVE,
                e.getMessage()
        );
        return new ResponseEntity<>(customException, org.springframework.http.HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = WalletCreateException.class)
    public ResponseEntity<Object> handleApiRequestException(WalletCreateException e) {
        CustomException customException = new CustomException(
                CommonConstant.WALLET_FAILED,
                e.getMessage()
        );
        return new ResponseEntity<>(customException, org.springframework.http.HttpStatus.BAD_REQUEST);
    }
}
