package com.mfs.mobile_platform.exception;

public class WalletInactiveException extends RuntimeException {

    public WalletInactiveException(String message) {
        super(message);
    }

    public WalletInactiveException(String message, Throwable cause) {
        super(message, cause);
    }
}
