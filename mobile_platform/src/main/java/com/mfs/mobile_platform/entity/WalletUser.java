package com.mfs.mobile_platform.entity;

import com.mfs.mobile_platform.enumeration.WalletUserStatus;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.Date;

@Data
@Entity
@Table(name = "wallet_user")
public class WalletUser {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotNull
    private String msisdn;

    @NotNull
    private String name;

    @NotNull
    private WalletUserStatus status;

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    @OneToMany(mappedBy = "walletUser")
    private Collection<Wallet> wallets;

}
