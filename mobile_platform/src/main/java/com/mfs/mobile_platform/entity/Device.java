package com.mfs.mobile_platform.entity;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@Entity
@Table(name = "device")
public class Device {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotNull
    private String device_uid;


    @NotNull
    private String os_name;

    @NotNull
    private String os_version;

    @NotNull
    private String manufacturer;

    @NotNull
    private String os_firm_wire_build;

    @Column
    @NotNull
    private String model_name;

    @NotNull
    private String mac_address;

    @NotNull
    private String imei_list;


    @NotNull
    private int root_device;

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    @OneToOne(mappedBy = "deviceAccess")
    private Wallet walletAccess;
}
