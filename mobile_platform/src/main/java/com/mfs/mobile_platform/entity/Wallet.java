package com.mfs.mobile_platform.entity;

import com.mfs.mobile_platform.enumeration.WalletStatus;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@Entity
@Table(name = "wallet")
public class Wallet {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotNull
    private String wallet_uid;

    @NotNull
    private WalletStatus status;

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    @ManyToOne
    @JoinColumn(name = "user_id")
    WalletUser walletUser;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "device_id", referencedColumnName = "id", unique = true)
    private Device deviceAccess;


}
