package com.mfs.mobile_platform.controller;

import com.mfs.mobile_platform.dto.WalletCreateRequest;
import com.mfs.mobile_platform.dto.WalletCreateResponse;
import com.mfs.mobile_platform.service.WalletService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class WalletController {

    @Autowired
    WalletService walletService;

    @ResponseBody
    @PostMapping(path = "/create_wallet",produces = "application/json",consumes = "application/json")
    public WalletCreateResponse createWallet(@RequestBody WalletCreateRequest walletCreatRequest){
        return walletService.createWallet(walletCreatRequest);
    }

}
