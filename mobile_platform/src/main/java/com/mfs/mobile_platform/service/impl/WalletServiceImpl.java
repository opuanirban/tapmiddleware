package com.mfs.mobile_platform.service.impl;

import com.mfs.mobile_platform.Util.CommonConstant;
import com.mfs.mobile_platform.dto.WalletCreateRequest;
import com.mfs.mobile_platform.dto.WalletCreateResponse;
import com.mfs.mobile_platform.entity.Device;
import com.mfs.mobile_platform.entity.Wallet;
import com.mfs.mobile_platform.entity.WalletUser;
import com.mfs.mobile_platform.enumeration.RequestType;
import com.mfs.mobile_platform.enumeration.WalletStatus;
import com.mfs.mobile_platform.enumeration.WalletUserStatus;
import com.mfs.mobile_platform.exception.WalletCreateException;
import com.mfs.mobile_platform.exception.WalletExistException;
import com.mfs.mobile_platform.exception.WalletInactiveException;
import com.mfs.mobile_platform.exception.WalletNotFoundException;
import com.mfs.mobile_platform.repository.DeviceRepository;
import com.mfs.mobile_platform.repository.WalletRepository;
import com.mfs.mobile_platform.repository.WalletUserRepository;
import com.mfs.mobile_platform.service.WalletService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@Slf4j
public class WalletServiceImpl implements WalletService {

    @Autowired
    DeviceRepository deviceRepository;

    @Autowired
    WalletRepository walletRepository;

    @Autowired
    WalletUserRepository walletUserRepository;


    @Override
    public WalletCreateResponse createWallet(WalletCreateRequest walletCreateRequest) {

        WalletCreateResponse walletCreateResponse = new WalletCreateResponse();
        if (walletCreateRequest.getRequestType().equals(RequestType.NEW_REGISTRATION)) {
            Optional<WalletUser> walletUser = walletUserRepository.findByMsisdn(walletCreateRequest.getUserDto().getMsisdn());
            if (walletUser.isPresent()) {
                Optional<List<Wallet>> wallet = walletRepository.findByWalletUser(walletUser.get());
                if (wallet.isPresent()) {
                    throw new WalletExistException("Wallet already exists for this number");
                }
            } else {

                try {
                    WalletUser newUser = new WalletUser();
                    newUser.setMsisdn(walletCreateRequest.getUserDto().getMsisdn());
                    newUser.setName(walletCreateRequest.getUserDto().getMsisdn());
                    newUser.setStatus(WalletUserStatus.ACTIVE);
                    newUser.setCreated(new Date());
                    newUser = walletUserRepository.save(newUser);
                    Device device = addDevice(walletCreateRequest);
                    Wallet wallet = new Wallet();
                    wallet.setWallet_uid(String.valueOf(UUID.randomUUID()));
                    wallet.setDeviceAccess(device);
                    wallet.setWalletUser(newUser);
                    wallet.setStatus(WalletStatus.ACTIVE);
                    wallet.setCreated(new Date());
                    wallet = walletRepository.save(wallet);
                    walletCreateResponse.setCode(CommonConstant.SUCCESS_CODE);
                    walletCreateResponse.setWalletId(wallet.getWallet_uid());
                    log.info("Wallet & Device added successfully for new user" + walletCreateRequest.getUserDto().getMsisdn());
                    return walletCreateResponse;
                } catch (Exception ex) {
                    log.error(ex.getMessage());
                    ex.printStackTrace();
                    throw new WalletCreateException(ex.getMessage());
                }


            }
        } else {
            Optional<WalletUser> walletUser = walletUserRepository.findByMsisdn(walletCreateRequest.getUserDto().getMsisdn());
            if (walletUser.isPresent()) {
                if (walletUser.get().getStatus().equals(WalletUserStatus.ACTIVE)) {
                    Optional<List<Wallet>> wallet = walletRepository.findByWalletUser(walletUser.get());
                    if (wallet.isPresent()) {
                        for (Wallet w : wallet.get()) {
                            if (w.getStatus().equals(WalletStatus.ACTIVE)) {
                                w.setStatus(WalletStatus.INACTIVE);
                                walletRepository.save(w);
                            }
                        }
                        try {
                            Wallet newWallet = new Wallet();
                            Device newDevice = addDevice(walletCreateRequest);
                            newWallet.setWallet_uid(String.valueOf(UUID.randomUUID()));
                            newWallet.setDeviceAccess(newDevice);
                            newWallet.setWalletUser(walletUser.get());
                            newWallet.setStatus(WalletStatus.ACTIVE);
                            newWallet.setCreated(new Date());
                            newWallet = walletRepository.save(newWallet);
                            walletCreateResponse.setCode(CommonConstant.SUCCESS_CODE);
                            walletCreateResponse.setWalletId(newWallet.getWallet_uid());
                            log.info("Wallet & Device added successfully for existing user" + walletCreateRequest.getUserDto().getMsisdn());
                            return walletCreateResponse;
                        } catch (Exception ex) {
                            log.error(ex.getMessage());
                            ex.printStackTrace();
                            throw new WalletCreateException(ex.getMessage());
                        }

                    }
                } else {
                    throw new WalletInactiveException("Wallet user is not active.Current Status:" + walletUser.get().getStatus());
                }
            } else {
                throw new WalletNotFoundException("No existing Wallet found for given msisdn");
            }


        }

        return null;
    }

    private Device addDevice(WalletCreateRequest walletCreateRequest) {
        Device device = new Device();
        device.setDevice_uid(walletCreateRequest.getDeviceDto().getDeviceUnqId());
        device.setOs_name(walletCreateRequest.getDeviceDto().getOsName());
        device.setOs_version(walletCreateRequest.getDeviceDto().getOsVersion());
        device.setManufacturer(walletCreateRequest.getDeviceDto().getManufacturer());
        device.setOs_firm_wire_build(walletCreateRequest.getDeviceDto().getOsFirmWireBuild());
        device.setModel_name(walletCreateRequest.getDeviceDto().getModelName());
        device.setMac_address(walletCreateRequest.getDeviceDto().getMacAddress());
        device.setImei_list(walletCreateRequest.getDeviceDto().getImeiList());
        device.setRoot_device(walletCreateRequest.getDeviceDto().getRootDevice());
        device.setCreated(new Date());
        device = deviceRepository.save(device);
        return device;
    }
}
