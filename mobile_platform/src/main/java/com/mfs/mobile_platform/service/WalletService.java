package com.mfs.mobile_platform.service;

import com.mfs.mobile_platform.dto.WalletCreateRequest;
import com.mfs.mobile_platform.dto.WalletCreateResponse;

public interface WalletService {
    public WalletCreateResponse createWallet(WalletCreateRequest walletCreateRequest);
}
