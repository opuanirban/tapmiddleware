package com.mfs.mobile_platform.dto;

import lombok.Data;

@Data
public class DeviceDto {
    private String deviceUnqId;
    private String osName;
    private String osVersion;
    private String manufacturer;
    private String osFirmWireBuild;
    private String modelName;
    private String macAddress;
    private String imeiList;
    private Integer rootDevice;
}
