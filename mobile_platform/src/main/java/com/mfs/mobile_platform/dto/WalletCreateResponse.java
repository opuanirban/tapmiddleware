package com.mfs.mobile_platform.dto;

import lombok.Data;

@Data
public class WalletCreateResponse {
    private String code;
    private String walletId;
}
