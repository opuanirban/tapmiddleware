package com.mfs.mobile_platform.dto;

import lombok.Data;

@Data
public class UserDto {
    private String msisdn;
    private String userName;
}
