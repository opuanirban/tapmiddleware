package com.mfs.mobile_platform.dto;

import com.mfs.mobile_platform.enumeration.RequestType;
import lombok.Data;

@Data
public class WalletCreateRequest {
    private UserDto userDto;
    private DeviceDto deviceDto;
    private RequestType requestType;

}
