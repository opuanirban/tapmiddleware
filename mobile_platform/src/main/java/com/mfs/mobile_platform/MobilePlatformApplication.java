package com.mfs.mobile_platform;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MobilePlatformApplication {

	public static void main(String[] args) {
		SpringApplication.run(MobilePlatformApplication.class, args);
	}

}
