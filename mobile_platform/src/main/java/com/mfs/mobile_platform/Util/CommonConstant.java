package com.mfs.mobile_platform.Util;

public class CommonConstant {
    public static final String SUCCESS_CODE = "success";
    public static final String WALLET_ERROR = "333";
    public static final String WALLET_NOT_ACTIVE = "334";
    public static final String WALLET_NOT_FOUND = "335";
    public static final String WALLET_FAILED = "336";
}
