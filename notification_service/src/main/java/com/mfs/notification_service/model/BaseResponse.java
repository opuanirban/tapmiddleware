package com.mfs.notification_service.model;

import lombok.Data;

@Data
public class BaseResponse {
    private String code;
    private String message;

}
