package com.mfs.notification_service.model;

import lombok.Data;

@Data
public class TapSMSResponse {
    private Integer statusCode;
    private String message;
    private ResponseData data;
}
