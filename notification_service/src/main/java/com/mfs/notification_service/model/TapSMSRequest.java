package com.mfs.notification_service.model;

import lombok.Data;

@Data
public class TapSMSRequest {
    private String referenceId;
    private String channel;
    private String phoneNumber;
    private String message;
    private Integer telcoId;
}
