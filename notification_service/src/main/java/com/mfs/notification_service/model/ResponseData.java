package com.mfs.notification_service.model;

import lombok.Data;

@Data
public class ResponseData {
    private String referenceId;
}
