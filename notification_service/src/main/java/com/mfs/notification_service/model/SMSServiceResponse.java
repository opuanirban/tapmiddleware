package com.mfs.notification_service.model;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Data
@XmlRootElement(name = "ArrayOfServiceClass")
@XmlAccessorType(XmlAccessType.FIELD)
public class SMSServiceResponse{
    @XmlElement(name = "ServiceClass")
    private List<SMSServiceClass> smsServiceClasses = null;
}
