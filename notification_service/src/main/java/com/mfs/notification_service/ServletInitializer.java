package com.mfs.notification_service;

import com.mfs.notification_service.util.CommonConstant;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

public class ServletInitializer extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
//		System.out.println(CommonConstant.telcoCodeParser("88017"));
		return application.sources(NotificationServiceApplication.class);
	}

}
