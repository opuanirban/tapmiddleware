package com.mfs.notification_service.exception;


import lombok.Data;

import java.time.ZonedDateTime;

@Data
public class ApiException {
    private final String code;
    private final String message;
}
