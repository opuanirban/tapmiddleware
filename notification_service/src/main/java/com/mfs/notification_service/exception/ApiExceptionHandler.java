package com.mfs.notification_service.exception;

import com.mfs.notification_service.util.CommonConstant;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ApiExceptionHandler {
    @ExceptionHandler(value = SMSSendingException.class)
    public ResponseEntity<Object> handleSMSSendingException(SMSSendingException e) {
        //1.Create payload containing exception details
        ApiException exception = new ApiException(
                CommonConstant.SMS_SENDING_FAILED,
                e.getMessage());
        return new ResponseEntity<>(exception, org.springframework.http.HttpStatus.BAD_REQUEST);
    }
}
