package com.mfs.notification_service.enumeration;

public enum ContentType {
    PLAIN_TEXT,
    HTML_TEXT
}
