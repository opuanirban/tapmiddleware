package com.mfs.notification_service.enumeration;

public enum MessageType {
    DEVICE_INACTIVE,
    EMERGENCY_PUSH,
    MARKETING_PUSH
}
