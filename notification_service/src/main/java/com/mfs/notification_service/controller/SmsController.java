package com.mfs.notification_service.controller;

import com.mfs.notification_service.gateway.ISmsGatewayService;
import com.mfs.notification_service.model.BaseResponse;
import com.mfs.notification_service.service.SMSSendingHelper;
import com.mfs.notification_service.util.CommonConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
public class SmsController {

    @Autowired
    ISmsGatewayService iSmsGatewayService;

    @Autowired
    SMSSendingHelper messagingFormatService;

//    @PostMapping(path = "/send_sms", produces = "application/json")
//    public BaseResponse userRefresh(@RequestParam("msisdn") String msisdn, @RequestParam("text") String text, HttpServletRequest httpServletRequest) {
//        if(httpServletRequest.getHeader(CommonConstant.AUTH_CODE)!=null) {
//            text = text.replace("%2B", "+");
//            return iSmsGatewayService.sendSms(msisdn, text, httpServletRequest);
//        }
//        else {
//            BaseResponse baseResponse=new BaseResponse();
//            baseResponse.setCode(CommonConstant.MISSING_CODE);
//            baseResponse.setMessage("Auth code missing");
//            return baseResponse;
//        }
//    }
//
//    @GetMapping(path = "/send_sms", produces = "application/json")
//    public BaseResponse sendSMS(@RequestParam("msisdn") String msisdn, @RequestParam("text") String text, HttpServletRequest httpServletRequest) {
//        if(httpServletRequest.getHeader(CommonConstant.AUTH_CODE)!=null) {
//            return iSmsGatewayService.sendSms(msisdn, text, httpServletRequest);
//        }
//        else {
//            BaseResponse baseResponse=new BaseResponse();
//            baseResponse.setCode(CommonConstant.MISSING_CODE);
//            baseResponse.setMessage("Auth code missing");
//            return baseResponse;
//        }
//    }

    @GetMapping(path = "/send_sms",produces = "application/json")
    public BaseResponse sendSMSToTap(@RequestParam(value = "msisdn",required = true)String msisdn,
                                     @RequestParam(value = "text",required = true)String text,
                                     @RequestParam(value = "channel",required = false,defaultValue = "undefined")String channel,
                                     @RequestParam(value = "telcoId",required = false,defaultValue = "-1")String telcoId,
                 HttpServletRequest httpServletRequest){

        if(httpServletRequest.getHeader(CommonConstant.AUTH_CODE) != null){
            return iSmsGatewayService.sendSmsToTap(msisdn,text,channel,telcoId,httpServletRequest);
        }

        return messagingFormatService.generateAuthCodeMissingResponse();
    }

    @PostMapping(path = "/send_sms",produces = "application/json")
    public BaseResponse sendSMSToTapPost(@RequestParam(value = "msisdn",required = true)String msisdn,
                                     @RequestParam(value = "text",required = true)String text,
                                     @RequestParam(value = "channel",required = false,defaultValue = "undefined")String channel,
                                     @RequestParam(value = "telcoId",required = false,defaultValue = "-1")String telcoId,
                                     HttpServletRequest httpServletRequest){

        if(httpServletRequest.getHeader(CommonConstant.AUTH_CODE) != null){
            return iSmsGatewayService.sendSmsToTap(msisdn,text,channel,telcoId,httpServletRequest);
        }

        return messagingFormatService.generateAuthCodeMissingResponse();
    }

}
