package com.mfs.notification_service.controller;

import com.google.firebase.messaging.FirebaseMessagingException;
import com.mfs.notification_service.model.Note;
import com.mfs.notification_service.service.FirebaseMessagingService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class NotificationController {

    private final FirebaseMessagingService firebaseService;

    public NotificationController(FirebaseMessagingService firebaseService) {
        this.firebaseService = firebaseService;
    }

    @RequestMapping("/send-notification-wallet")
    @ResponseBody
    public String sendNotification(@RequestBody Note note,
                                   @RequestParam String fcmReference, @RequestParam String devicePlatform) throws FirebaseMessagingException {
        return firebaseService.sendNotificationtoken(note, fcmReference,devicePlatform,false);
    }


    @RequestMapping("/send-broadcast-notification")
    @ResponseBody
    public String sendNotification(@RequestBody Note note,@RequestParam String devicePlatform ) throws FirebaseMessagingException {
        return firebaseService.sendNotificationtopic(note, null,devicePlatform,true);
    }

}