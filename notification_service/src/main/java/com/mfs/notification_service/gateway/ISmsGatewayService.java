package com.mfs.notification_service.gateway;

import com.mfs.notification_service.model.BaseResponse;
import com.mfs.notification_service.model.TapSMSRequest;

import javax.servlet.http.HttpServletRequest;

public interface ISmsGatewayService {
    BaseResponse sendSms(String receiverInfo, String text, HttpServletRequest httpServletRequest);
    BaseResponse sendSmsToTap(String receiverInfo, String text,
                              String channel,String telcoId,
                              HttpServletRequest httpServletRequest);
}
