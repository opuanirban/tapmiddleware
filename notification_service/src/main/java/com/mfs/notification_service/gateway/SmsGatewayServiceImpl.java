package com.mfs.notification_service.gateway;

import com.google.gson.Gson;
import com.mfs.notification_service.config.ApplicationProperties;
import com.mfs.notification_service.exception.SMSSendingException;
import com.mfs.notification_service.model.*;
import com.mfs.notification_service.util.CommonConstant;
import lombok.Generated;
import lombok.extern.slf4j.Slf4j;
import okhttp3.*;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.ssl.TrustStrategy;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Service
@Slf4j
public class SmsGatewayServiceImpl implements ISmsGatewayService {

    private static final Logger logger = LoggerFactory.getLogger(SmsGatewayServiceImpl.class);

    @Autowired
    ApplicationProperties applicationProperties;

    @Autowired
    CloseableHttpClient httpClient;

    @Autowired
    OkHttpClient okHttpClient;

    @Value("#{'${whitelist.test.msisdn}'.split(',')}")
    private List<String> allowedMSISDN;

    @Value("${test.call.allowed}")
    private boolean testAllowed;

    @Value("${notification_sms_auth_code}")
    private String authCode;

    @Value("${tap-sms-gateway.url}")
    private String tapSmsGatewayUrl;

    @Autowired
    RestTemplate restTemplate;

    @Override
    public BaseResponse sendSms(String receiverInfo, String text, HttpServletRequest httpServletRequest) {
        // TODO applied for test only
        BaseResponse baseResponse=new BaseResponse();
        if (httpServletRequest.getHeader(CommonConstant.AUTH_CODE).equalsIgnoreCase(authCode)) {
            if (testAllowed && !allowedMSISDN.contains(receiverInfo)) {
              //  return "";
                 baseResponse.setCode(CommonConstant.SUCCESS_CODE);
                 baseResponse.setMessage("");
                 return baseResponse;
            }

            try {
                String uri = new StringBuffer(applicationProperties.getSmsGatewayUrl())
                        .append("?Username=").append(applicationProperties.getSmsGatewayUsername())
                        .append("&Password=").append(applicationProperties.getSmsGatewayPassword())
                        .append("&From=").append(applicationProperties.getSmsGatewaySender())
                        .append("&To=").append("88" + receiverInfo)
                        .append("&Message=").append(URLEncoder.encode(text, "UTF-8"))
                        .toString();

                SSLContextBuilder builder = new SSLContextBuilder();
                builder.loadTrustMaterial(null, (TrustStrategy) (chain, authType) -> true);

                SSLConnectionSocketFactory sslSF = new SSLConnectionSocketFactory(builder.build(),
                        SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

                HttpClient httpClient = HttpClients.custom().setSSLSocketFactory(sslSF).build();

                HttpGet request = new HttpGet(uri);
                HttpResponse response = httpClient.execute(request);
                String responseStr = EntityUtils.toString(response.getEntity());

                logger.debug("uri: {}, sms response: {}", uri, response);
                JAXBContext jaxbContext = JAXBContext.newInstance(SMSServiceResponse.class);
                Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

                StringReader reader = new StringReader(responseStr);
                SMSServiceResponse smsServiceResponseList = (SMSServiceResponse) unmarshaller.unmarshal(reader);
                SMSServiceClass smsServiceResponse = smsServiceResponseList.getSmsServiceClasses().get(0);
                if (smsServiceResponse.getStatus() == -1) {
                    throw new SMSSendingException("SMS Sending failed for reason: " + smsServiceResponse.getErrorText());
                }

                baseResponse.setCode(CommonConstant.SUCCESS_CODE);
                baseResponse.setMessage("SMS Sent Successfully");
                return baseResponse;

            } catch (Exception e) {
                logger.error(e.getMessage());
                throw new SMSSendingException("SMS Sending failed");
            }
        }
        else{
            baseResponse.setCode(CommonConstant.MISMATCH_CODE);
            baseResponse.setMessage("Api auth code mismatch");
            return baseResponse;
        }
    }

    @Override
    public BaseResponse sendSmsToTap(String receiverInfo, String text,
                                     String channel,String telcoId, HttpServletRequest httpServletRequest) {
        BaseResponse baseResponse = new BaseResponse();
        if(!httpServletRequest.getHeader(CommonConstant.AUTH_CODE).equalsIgnoreCase(authCode)){
            baseResponse.setCode(CommonConstant.MISMATCH_CODE);
            baseResponse.setMessage("Api auth code mismatch");
            return baseResponse;
        }
        if(testAllowed && !allowedMSISDN.contains(receiverInfo))
        {
            baseResponse.setCode(CommonConstant.SUCCESS_CODE);
            baseResponse.setMessage("");
            return baseResponse;
        }

        TapSMSResponse tapSMSResponse = new TapSMSResponse();
        TapSMSRequest tapSMSRequest = new TapSMSRequest();

        tapSMSRequest.setReferenceId(UUID.randomUUID().toString());
        tapSMSRequest.setChannel(channel);
        tapSMSRequest.setTelcoId(telcoId.equals("-1")? CommonConstant.telcoCodeParser(receiverInfo):Integer.parseInt(telcoId));
//

        tapSMSRequest.setPhoneNumber(
                tapSMSRequest.getTelcoId().equals(1) ?
                        CommonConstant.checkNumber(receiverInfo).substring(2):
                CommonConstant.checkNumber(receiverInfo)
        );

        tapSMSRequest.setMessage(text);
        logger.debug("{}",tapSMSRequest);
        Gson gson = new Gson();
//        ResponseBody responseBody = new ResponseBody() {
//        }
        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(mediaType,gson.toJson(tapSMSRequest));
        try {
            Request request = new Request.Builder()
                    .url(tapSmsGatewayUrl)
                    .method("POST",body)
                    .addHeader("Content-Type", "application/json")
                    .build();
            ResponseBody responseBody = okHttpClient.newCall(request).execute().body();
            tapSMSResponse = gson.fromJson(responseBody.string(),TapSMSResponse.class);
        } catch (Exception e) {
            logger.error(e.getMessage());
//            logger.error(res);
            throw new SMSSendingException("SMS Sending failed");
        }
        logger.debug("{}",tapSMSResponse);
        if(tapSMSResponse.getStatusCode().equals(200)){
            baseResponse.setCode(CommonConstant.SUCCESS_CODE);
            baseResponse.setMessage("SMS Sent Successfully");
            return baseResponse;
        }
        logger.error("Failed "+tapSMSRequest.getPhoneNumber()+" "+tapSMSRequest.getReferenceId());
        baseResponse.setCode(CommonConstant.SMS_SENDING_FAILED);
        baseResponse.setMessage("SMS Sending failed");
        return baseResponse;
    }
}
