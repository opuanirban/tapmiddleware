package com.mfs.notification_service.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Data
public class ApplicationProperties {

    @Value("${http.url.sms-gateway}")
    private String smsGatewayUrl;

    @Value("${sms-gateway.username}")
    private String smsGatewayUsername;

    @Value("${sms-gateway.password}")
    private String smsGatewayPassword;

    @Value("${sms-gateway.sender}")
    private String smsGatewaySender;

    @Value("60")
    private String ttl;

}
