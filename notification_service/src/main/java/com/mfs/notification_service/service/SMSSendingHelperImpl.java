package com.mfs.notification_service.service;

import com.mfs.notification_service.model.BaseResponse;
import com.mfs.notification_service.util.CommonConstant;
import org.springframework.stereotype.Service;

@Service
public class SMSSendingHelperImpl implements SMSSendingHelper {
    public BaseResponse generateAuthCodeMissingResponse(){
        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setCode(CommonConstant.MISSING_CODE);
        baseResponse.setMessage("Auth code missing");
        return baseResponse;
    }
}
