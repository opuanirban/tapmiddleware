package com.mfs.notification_service.service;

import com.mfs.notification_service.model.BaseResponse;
import org.springframework.stereotype.Service;

@Service
public interface SMSSendingHelper {
    public BaseResponse generateAuthCodeMissingResponse();
}
