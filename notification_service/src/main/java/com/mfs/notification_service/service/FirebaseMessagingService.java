package com.mfs.notification_service.service;

import com.google.firebase.messaging.*;
import com.google.gson.Gson;
import com.mfs.notification_service.config.ApplicationProperties;
import com.mfs.notification_service.model.Note;
import com.mfs.notification_service.util.CommonConstant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;

@Service
@Slf4j
public class FirebaseMessagingService {

    @Autowired
    ApplicationProperties applicationProperties;


    private final FirebaseMessaging firebaseMessaging;

    public FirebaseMessagingService(FirebaseMessaging firebaseMessaging) {
        this.firebaseMessaging = firebaseMessaging;
    }


    private AndroidConfig getAndroidConfig() {
        return AndroidConfig.builder()
                .setTtl(Duration.ofMinutes(Long.valueOf(applicationProperties.getTtl())).toMillis())
                .setPriority(AndroidConfig.Priority.HIGH)
//                .setNotification(null)
//                .setNotification(AndroidNotification.builder().setSound("default").setColor("#FFFF00").build())
                .build();
    }

    private ApnsConfig getApnsConfig() {
        Map<String, Object> customData = new HashMap<>();
        customData.put("content-available", 1);
        customData.put("mutable-content",1);


        return ApnsConfig.builder().setAps(Aps.builder().putAllCustomData(customData).setSound("default").build()).build();


    }

    public String sendNotificationtopic(Note note, String fcmReference,String devicePlatform, boolean isBroadcast) throws FirebaseMessagingException {
        AndroidConfig androidConfig = getAndroidConfig();
        ApsAlert apsAlert = ApsAlert.builder().setBody(new Gson().toJson(note.getData())).build();

        ApnsConfig apnsConfig = ApnsConfig
                .builder()
                .setAps(Aps
                        .builder()
                        .setAlert(apsAlert)
                        .setContentAvailable(true)
                        .setMutableContent(true)
                        .setSound("default")
                        .build()
                ).build();

        if(devicePlatform.equalsIgnoreCase("iOS")){
//            log.info("device platform: "+devicePlatform);
            Notification notification = Notification
                    .builder()
//                    .setTitle(note.getData().get("TITLE"))
//                    .setBody(note.getData().get("Content"))
//                    .setTitle(note.getSubject())
//                    .setBody(note.getContent())
                    .build();

            Message.Builder messageBuilder = Message
                    .builder()
                    .setNotification(notification)
                    .putAllData(note.getData())
                    .setAndroidConfig(androidConfig)
                    .setApnsConfig(apnsConfig);

            if(isBroadcast)
                messageBuilder.setTopic(CommonConstant.NOTIFACTION_BROADCAST_TOPIC_TEST);
            else
                messageBuilder.setToken(fcmReference);


            return firebaseMessaging.send(messageBuilder.build());
        }
        else {
//            log.info("device platform: "+devicePlatform);
            Notification notification = Notification
                    .builder()
//                    .setTitle(note.getSubject())
//                    .setBody(note.getContent())
                    .build();

            Message.Builder messageBuilder = Message
                    .builder()
                    .setNotification(notification)
                    .putAllData(note.getData())
                    .setAndroidConfig(androidConfig)
                    .setApnsConfig(apnsConfig);

            if (isBroadcast)
                messageBuilder.setTopic(CommonConstant.NOTIFACTION_BROADCAST_TOPIC_TEST);
            else
                messageBuilder.setToken(fcmReference);


            return firebaseMessaging.send(messageBuilder.build());
        }
    }

    public String sendNotificationtoken(Note note, String fcmReference,String devicePlatform, boolean isBroadcast) throws FirebaseMessagingException {
        AndroidConfig androidConfig = getAndroidConfig();
        ApnsConfig apnsConfig = getApnsConfig();

        if(devicePlatform.equalsIgnoreCase("iOS")){
            log.info("expecting ios");
            log.info("device platform: "+devicePlatform);
            Notification notification = Notification
                    .builder()
//                    .setTitle(note.getData().get("TITLE"))
//                    .setBody(note.getData().get("Content"))
                    .setTitle(note.getSubject())
                    .setBody(note.getContent())
                    .build();

            Message.Builder messageBuilder = Message
                    .builder()
                    .setNotification(notification)
                    .putAllData(note.getData())
                    .setAndroidConfig(androidConfig)
                    .setApnsConfig(apnsConfig);

            if(isBroadcast)
                messageBuilder.setTopic(CommonConstant.NOTIFACTION_BROADCAST_TOPIC_TEST);
            else
                messageBuilder.setToken(fcmReference);


            return firebaseMessaging.send(messageBuilder.build());
        }
        else {
            log.info("expecting android");
            log.info("device platform: "+devicePlatform);
            Notification notification = Notification
                    .builder()
//                    .setTitle(note.getSubject())
//                    .setBody(note.getContent())
                    .build();

            Message.Builder messageBuilder = Message
                    .builder()
                    .setNotification(notification)
                    .putAllData(note.getData())
                    .setAndroidConfig(androidConfig)
                    .setApnsConfig(apnsConfig);

            if (isBroadcast)
                messageBuilder.setTopic(CommonConstant.NOTIFACTION_BROADCAST_TOPIC_TEST);
            else
                messageBuilder.setToken(fcmReference);


            return firebaseMessaging.send(messageBuilder.build());
        }
    }


}
