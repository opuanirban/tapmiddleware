package com.mfs.notification_service.util;

import java.util.HashMap;

public class CommonConstant {

    public static final String SUCCESS_CODE = "000";
    public static final String MISMATCH_CODE = "001";
    public static final String MISSING_CODE = "003";
    public static final String SMS_SENDING_FAILED = "011";
    public static final String NOTIFACTION_BROADCAST_TOPIC_DEV="UPDATES";
    public static final String NOTIFACTION_BROADCAST_TOPIC_TEST="ADVERTISING";
    public final static String AUTH_CODE="API_AUTH_CODE";

    public static String checkNumber(String accountNo) {
        if (accountNo.startsWith("88")) {

        } else if (accountNo.startsWith("+88")) {
            accountNo = accountNo.substring(1);
        } else {
            StringBuilder sb = new StringBuilder(accountNo);
            sb.insert(0, "88");
            accountNo = sb.toString();
        }
        return accountNo;
    }

    public static Integer telcoCodeParser(String accountNo){
        accountNo = checkNumber(accountNo);
        accountNo = accountNo.substring(0,5);
        System.out.println(accountNo);
        HashMap<String,Integer> telcoCodeMapper = new HashMap<>();
//        GrameenPhone 1
//        Banglalink 3
//        Robi 4
//        Teletalk 5
//        Airtel 6
        telcoCodeMapper.put("88013",1);
        telcoCodeMapper.put("88017",1);
        telcoCodeMapper.put("88014",3);
        telcoCodeMapper.put("88019",3);
        telcoCodeMapper.put("88018",4);
        telcoCodeMapper.put("88015",5);
        telcoCodeMapper.put("88016",6);
//        Character num = accountNo.charAt(4);
//        if(num.equals('3')  || num.equals('7')) return new Integer(1);
//        if(num == '4' || num == '9') return new Integer(3);
//        if(num == '8') return new Integer(4);
//        if(num == '5') return new Integer(5);
//        if(num == '6') return new Integer(6);

        return telcoCodeMapper.get(accountNo);
    }
}
