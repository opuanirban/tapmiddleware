/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package bulk_qrcode_generator;

/**
 *
 * @author Mashrur.Sadat
 */
public class QRParent {
    
    public QRItem qrCode;
    
    public NumberTextItem agentNumber;
    
    public OtherTextItem agentName, region, district, address;

    public QRItem getQrCode() {
        return qrCode;
    }

    public void setQrCode(QRItem qrCode) {
        this.qrCode = qrCode;
    }

    public NumberTextItem getAgentNumber() {
        return agentNumber;
    }

    public void setAgentNumber(NumberTextItem agentNumber) {
        this.agentNumber = agentNumber;
    }

    public OtherTextItem getAgentName() {
        return agentName;
    }

    public void setAgentName(OtherTextItem agentName) {
        this.agentName = agentName;
    }

    public OtherTextItem getRegion() {
        return region;
    }

    public void setRegion(OtherTextItem region) {
        this.region = region;
    }

    public OtherTextItem getDistrict() {
        return district;
    }

    public void setDistrict(OtherTextItem district) {
        this.district = district;
    }

    public OtherTextItem getAddress() {
        return address;
    }

    public void setAddress(OtherTextItem address) {
        this.address = address;
    }
   
}
