/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package bulk_qrcode_generator;

import java.awt.Color;



/**
 *
 * @author Mashrur.Sadat
 */
public class OtherTextItem {
    
    
    public int textMaxLength;
    public int textX;
    public int textY1;
    public int textY2;
    public int textFontSize;
    public Color color;

    
    public int getTextMaxLength() {
        return textMaxLength;
    }

    public void setTextMaxLength(int textMaxLength) {
        this.textMaxLength = textMaxLength;
    }

    public int getTextX() {
        return textX;
    }

    public void setTextX(int textX) {
        this.textX = textX;
    }

    public int getTextY1() {
        return textY1;
    }

    public void setTextY1(int textY1) {
        this.textY1 = textY1;
    }

    public int getTextY2() {
        return textY2;
    }

    public void setTextY2(int textY2) {
        this.textY2 = textY2;
    }

    

    public int getTextFontSize() {
        return textFontSize;
    }

    public void setTextFontSize(int textFontSize) {
        this.textFontSize = textFontSize;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    
    
}
