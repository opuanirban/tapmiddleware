/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMain.java to edit this template
 */
package bulk_qrcode_generator;

import com.google.zxing.WriterException;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
//import javafx.scene.paint.Color;
import static java.awt.Color.white;
//import java.awt.Label;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ProgressIndicator;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.scene.control.Label;
import javafx.scene.text.Font;

/**
 *
 * @author Mashrur.Sadat
 */
public class BulkQrCodeGeneratorApplication extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {

        //String backgroundImage;        
        QRParent obj1 = new QRParent(); //This obj1 is for (2000 x 1338) size black and white background image

        //QR Code
        QRItem qRItem = new QRItem();

        qRItem.setQrSize(450);
        qRItem.setQrX(1415);
        qRItem.setQrY(370);
        obj1.setQrCode(qRItem);

        //Agent Number
        NumberTextItem numberItem = new NumberTextItem();

        numberItem.setNumberDistance(34);
        numberItem.setNumberFontSize(30);
        numberItem.setNumberX(1459);
        numberItem.setNumberY(910);
        obj1.setAgentNumber(numberItem);

        //Agent Name
        OtherTextItem agentItem = new OtherTextItem();

        agentItem.setColor(white);
        agentItem.setTextFontSize(17);
        agentItem.setTextMaxLength(32);
        agentItem.setTextX(1520);
        agentItem.setTextY1(995);
        obj1.setAgentName(agentItem);

        //Region
        OtherTextItem regionItem = new OtherTextItem();

        regionItem.setColor(white);
        regionItem.setTextFontSize(17);
        regionItem.setTextMaxLength(32);
        regionItem.setTextX(1520);
        regionItem.setTextY1(1045);
        obj1.setRegion(regionItem);

        //District
        OtherTextItem districtItem = new OtherTextItem();

        districtItem.setColor(white);
        districtItem.setTextFontSize(17);
        districtItem.setTextMaxLength(32);
        districtItem.setTextX(1520);
        districtItem.setTextY1(1095);//1095
        //districtItem.setTextY2(1185);
        obj1.setDistrict(districtItem);

        //Address
        OtherTextItem addressItem = new OtherTextItem();

        addressItem.setColor(white);
        addressItem.setTextFontSize(17);
        addressItem.setTextMaxLength(35);
        addressItem.setTextX(1520);
        addressItem.setTextY1(1165);//1165
        addressItem.setTextY2(1185);//1185
        obj1.setAddress(addressItem);

        QRParent obj2 = new QRParent(); //This obj2 is for (3265 x 2185) size black background image

        //QR Code
        QRItem qRItem2 = new QRItem();

        qRItem2.setQrSize(700);
        qRItem2.setQrX(2330);
        qRItem2.setQrY(600);
        obj2.setQrCode(qRItem2);

        //Agent Number
        NumberTextItem numberItem2 = new NumberTextItem();

        numberItem2.setNumberDistance(56);
        numberItem2.setNumberFontSize(50);
        numberItem2.setNumberX(2380);
        numberItem2.setNumberY(1485);
        obj2.setAgentNumber(numberItem2);

        //Agent Name
        OtherTextItem agentItem2 = new OtherTextItem();

        agentItem2.setColor(white);
        agentItem2.setTextFontSize(28);
        agentItem2.setTextMaxLength(32);
        agentItem2.setTextX(2500);
        agentItem2.setTextY1(1625);
        obj2.setAgentName(agentItem2);

        //Region
        OtherTextItem regionItem2 = new OtherTextItem();

        regionItem2.setColor(white);
        regionItem2.setTextFontSize(28);
        regionItem2.setTextMaxLength(32);
        regionItem2.setTextX(2500);
        regionItem2.setTextY1(1706);
        obj2.setRegion(regionItem2);

        //District
        OtherTextItem districtItem2 = new OtherTextItem();

        districtItem2.setColor(white);
        districtItem2.setTextFontSize(28);
        districtItem2.setTextMaxLength(32);
        districtItem2.setTextX(2500);
        districtItem2.setTextY1(1794);//1095
        //districtItem.setTextY2(1185);
        obj2.setDistrict(districtItem2);

        //Address
        OtherTextItem addressItem2 = new OtherTextItem();

        addressItem2.setColor(white);
        addressItem2.setTextFontSize(28);
        addressItem2.setTextMaxLength(35);
        addressItem2.setTextX(2500);
        addressItem2.setTextY1(1900);//1165
        addressItem2.setTextY2(1940);//1185
        obj2.setAddress(addressItem2);

        QRParent obj3 = new QRParent(); //This obj3 is for (2721 x 1820) size white background image

        //QR Code
        QRItem qRItem3 = new QRItem();

        qRItem3.setQrSize(600);
        qRItem3.setQrX(1930);
        qRItem3.setQrY(490);
        obj3.setQrCode(qRItem3);

        //Agent Number
        NumberTextItem numberItem3 = new NumberTextItem();

        numberItem3.setNumberDistance(45);
        numberItem3.setNumberFontSize(40);
        numberItem3.setNumberX(1990);
        numberItem3.setNumberY(1240);
        obj3.setAgentNumber(numberItem3);

        //Agent Name
        OtherTextItem agentItem3 = new OtherTextItem();

        agentItem3.setColor(white);
        agentItem3.setTextFontSize(24);
        agentItem3.setTextMaxLength(32);
        agentItem3.setTextX(2070);
        agentItem3.setTextY1(1354);
        obj3.setAgentName(agentItem3);

        //Region
        OtherTextItem regionItem3 = new OtherTextItem();

        regionItem3.setColor(white);
        regionItem3.setTextFontSize(24);
        regionItem3.setTextMaxLength(32);
        regionItem3.setTextX(2070);
        regionItem3.setTextY1(1423);
        obj3.setRegion(regionItem3);

        //District
        OtherTextItem districtItem3 = new OtherTextItem();

        districtItem3.setColor(white);
        districtItem3.setTextFontSize(24);
        districtItem3.setTextMaxLength(32);
        districtItem3.setTextX(2070);
        districtItem3.setTextY1(1486);//1095
        //districtItem.setTextY2(1185);
        obj3.setDistrict(districtItem3);

        //Address
        OtherTextItem addressItem3 = new OtherTextItem();

        addressItem3.setColor(white);
        addressItem3.setTextFontSize(24);
        addressItem3.setTextMaxLength(35);
        addressItem3.setTextX(2070);
        addressItem3.setTextY1(1580);//1165
        addressItem3.setTextY2(1610);//1185
        obj3.setAddress(addressItem3);
        
        //String backgroundImage;        
        QRParent obj4 = new QRParent(); //This obj4 is for (1490 x 2105) size merchant rickshaw puller background image
        
        //QR Code
        QRItem qRItem4 = new QRItem();
        
        qRItem4.setQrSize(750);
        qRItem4.setQrX(375);
        qRItem4.setQrY(750);
        obj4.setQrCode(qRItem4);
        
        //Agent Number
        NumberTextItem numberItem4 = new NumberTextItem();
        
        numberItem4.setNumberDistance(82);
        numberItem4.setNumberFontSize(75);
        numberItem4.setNumberX(310);
        numberItem4.setNumberY(1750);
        obj4.setAgentNumber(numberItem4);
        
        QRParent obj5 = new QRParent(); //This obj5 is for (3060 x 1980) size white background image (Final image)

        //QR Code
        QRItem qRItem5 = new QRItem();

        qRItem5.setQrSize(700);
        qRItem5.setQrX(2215);
        qRItem5.setQrY(500);
        obj5.setQrCode(qRItem5);

        //Agent Number
        NumberTextItem numberItem5 = new NumberTextItem();

        numberItem5.setNumberDistance(55);
        numberItem5.setNumberFontSize(50);
        numberItem5.setNumberX(2273);
        numberItem5.setNumberY(1385);
        obj5.setAgentNumber(numberItem5);

        //Agent Name
        OtherTextItem agentItem5 = new OtherTextItem();

        agentItem5.setColor(white);
        agentItem5.setTextFontSize(28);
        agentItem5.setTextMaxLength(32);
        agentItem5.setTextX(2400);
        agentItem5.setTextY1(1515);
        obj5.setAgentName(agentItem5);

        //Region
        OtherTextItem regionItem5 = new OtherTextItem();

        regionItem5.setColor(white);
        regionItem5.setTextFontSize(28);
        regionItem5.setTextMaxLength(32);
        regionItem5.setTextX(2400);
        regionItem5.setTextY1(1605);
        obj5.setRegion(regionItem5);

        //District
        OtherTextItem districtItem5 = new OtherTextItem();

        districtItem5.setColor(white);
        districtItem5.setTextFontSize(28);
        districtItem5.setTextMaxLength(32);
        districtItem5.setTextX(2400);
        districtItem5.setTextY1(1690);//1095
        //districtItem.setTextY2(1185);
        obj5.setDistrict(districtItem5);

        //Address
        OtherTextItem addressItem5 = new OtherTextItem();

        addressItem5.setColor(white);
        addressItem5.setTextFontSize(28);
        addressItem5.setTextMaxLength(35);
        addressItem5.setTextX(2400);
        addressItem5.setTextY1(1795);//1165
        addressItem5.setTextY2(1835);//1185
        obj5.setAddress(addressItem5);
        
        
        
   

        HashMap<String, QRParent> hashmap = new HashMap<String, QRParent>();
        hashmap.put("Black Agent Sticker 2000 x 1338", obj1);
        hashmap.put("White Agent Sticker 2000 x 1338", obj1);
        hashmap.put("Black Agent Sticker 3265 x 2185", obj2);
        hashmap.put("White Agent Sticker 2721 x 1820", obj3);
        hashmap.put("Rickshaw puller Sticker 1490 x 2105", obj4);
        hashmap.put("White Background Sticker 2400 x 2368", obj4);
        hashmap.put("White Agent Sticker 3060 x 1980", obj5);
        

        primaryStage.setTitle("Bulk QR Code Generator App");

        FileChooser fileChooser = new FileChooser();
        //fileChooser.setInitialDirectory(new File("src"));
//        fileChooser.setInitialFileName("myfile.txt");

        DirectoryChooser directoryChooser = new DirectoryChooser();
//        directoryChooser.setInitialDirectory(new File("src"));

        TextField textField1 = new TextField();

        TextField textField2 = new TextField();

        //TextField textField3 = new TextField();
        ProgressBar pb = new ProgressBar(1.0);
        pb.setPrefSize(500, 30);

        ProgressIndicator indicator = new ProgressIndicator(1.0);

        Label label1 = new Label("Select QR type");
        label1.setFont(new Font("Arial", 16));

        ComboBox comboBox = new ComboBox();
        comboBox.getItems().addAll("QR for Agent", "QR for Merchant");
        //comboBox.setPromptText("Select Background Image");
        comboBox.setValue("QR for Agent");

        Label label2 = new Label("Select a background image");
        label2.setFont(new Font("Arial", 16));

        ComboBox comboBox2 = new ComboBox();
        comboBox2.setValue("Black Agent Sticker 2000 x 1338");
        comboBox2.getItems().addAll("Black Agent Sticker 2000 x 1338", "White Agent Sticker 2000 x 1338", "Black Agent Sticker 3265 x 2185", "White Agent Sticker 2721 x 1820", "White Agent Sticker 3060 x 1980");
        //comboBox2.getItems().addAll("Black Agent Sticker 2000 x 1338","White Agent Sticker 2000 x 1338","Black Agent Sticker 3265 x 2185","White Agent Sticker 2721 x 1820");
        //comboBox.setPromptText("Select Background Image");
        // comboBox2.setValue("Black Agent Sticker 2000 x 1338");
        //choiceBox.getItems().add("text3");

        //Creating a dialog
        //Alert alert = new Alert(Alert.AlertType.WARNING);
        //Setting the title
        //alert.setTitle("Alert");
        //Setting the content of the dialog
        //alert.setContentText("Please Select a Background Image");
        //Adding buttons to the dialog pane
        //alert.getDialogPane().getButtonTypes().
        //String outputDir = null;
        comboBox.setOnAction(e -> {

            //comboBox2.getItems().clear();
            if (comboBox.getValue().toString() == "QR for Merchant") {
                comboBox2.getItems().clear();
                comboBox2.setValue("Rickshaw puller Sticker 1490 x 2105");
                comboBox2.getItems().addAll("Rickshaw puller Sticker 1490 x 2105", "White Background Sticker 2400 x 2368");
            }
            if (comboBox.getValue().toString() == "QR for Agent") {
                comboBox2.getItems().clear();
                comboBox2.setValue("Black Agent Sticker 2000 x 1338");
                comboBox2.getItems().addAll("Black Agent Sticker 2000 x 1338", "White Agent Sticker 2000 x 1338", "Black Agent Sticker 3265 x 2185", "White Agent Sticker 2721 x 1820", "White Agent Sticker 3060 x 1980");

            }

        });

        Button button1 = new Button("Select Excel File");
        button1.setStyle("-fx-background-color: #5DADE2");

        button1.setOnAction(e -> {

            File selectedFile = fileChooser.showOpenDialog(primaryStage);
            textField1.setText(selectedFile.getAbsolutePath());

        });

        Button button2 = new Button("Select Folder To Save QR codes");
        button2.setStyle("-fx-background-color: #5DADE2");

        button2.setOnAction(e -> {

            File selectedDirectory = directoryChooser.showDialog(primaryStage);
            textField2.setText(selectedDirectory.getAbsolutePath());

        });

        Button button3 = new Button("Generate QR Code");
        button3.setStyle("-fx-background-color: #82E0AA");

        Alert a = new Alert(AlertType.ERROR, "Error occured!! Please select Background Image/ Excel File/ Destination Folder correctly and Restart the application.");

        VBox vBox = new VBox(label1, comboBox, label2, comboBox2, button1, textField1, button2, textField2, button3);
        //vBox.getChildren().add(label);
        //vBox.getChildren().add(pb);
        vBox.setAlignment(Pos.CENTER_LEFT);
        vBox.setStyle("-fx-padding: 50px");
        vBox.setSpacing(10);

        Scene scene = new Scene(vBox, 600, 500);
        //scene.setFill(Color.BROWN);
        primaryStage.setScene(scene);
        primaryStage.show();

        button3.setOnAction(e -> {

            if (textField1.getText() == null || textField1.getText().isEmpty()) {

                textField1.setText("File is not selected");

            } else if (textField2.getText() == null || textField2.getText().isEmpty()) {

                textField2.setText("Directory is not selected");

            } else {
                try {
                    vBox.getChildren().add(pb);

                    System.out.println(comboBox.getValue().toString());
                    //textField3.setText("QR codes are being created on your selected folder...");
                    QrCodeGenerator codeGenerator = new QrCodeGenerator();
                    codeGenerator.readExcelFile(textField1.getText(), textField2.getText(), hashmap.get(comboBox2.getValue().toString()), comboBox2.getValue().toString(), comboBox.getValue().toString());
                    //textField3.setText("QR codes created successfully");
                    vBox.getChildren().add(indicator);

                } catch (Exception ex) {
                    Logger.getLogger(BulkQrCodeGeneratorApplication.class.getName()).log(Level.SEVERE, null, ex);
                    System.out.println("Error occured in third catch");
                    a.show();
                }

            }
        });

    }

    /**
     * @param args the command line arguments
     */
}
