/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package bulk_qrcode_generator;

/**
 *
 * @author Mashrur.Sadat
 */
public class NumberTextItem {
    
    public int numberX;
    public int numberY;
    public int numberDistance;
    public int numberFontSize;

    public int getNumberX() {
        return numberX;
    }

    public void setNumberX(int numberX) {
        this.numberX = numberX;
    }

    public int getNumberY() {
        return numberY;
    }

    public void setNumberY(int numberY) {
        this.numberY = numberY;
    }

    public int getNumberDistance() {
        return numberDistance;
    }

    public void setNumberDistance(int numberDistance) {
        this.numberDistance = numberDistance;
    }

    public int getNumberFontSize() {
        return numberFontSize;
    }

    public void setNumberFontSize(int numberFontSize) {
        this.numberFontSize = numberFontSize;
    }
    
}
