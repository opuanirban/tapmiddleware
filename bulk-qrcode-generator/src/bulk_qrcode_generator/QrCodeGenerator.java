/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package bulk_qrcode_generator;

//imports for reading excel file
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import org.apache.poi.xssf.usermodel.*;
import java.util.EnumMap;
import java.util.Map;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

//imports for merging an image over another
import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 *
 * @author Mashrur.Sadat
 */
public class QrCodeGenerator {

    public void readExcelFile(String inputFileName, String outputDir, QRParent qRParent, String backgroundImage, String QRType) {

        System.out.println(inputFileName + " -- " + outputDir);

        //Properties prop=new Properties();
        //FileReader reader=new FileReader("config.properties");
        //prop.load(reader);
        //System.out.println(prop.getProperty("test"));
        //System.out.println(prop.getProperty("imageFilePath"));
        String imageFilePath = outputDir + "\\";

        String excelFilePath = inputFileName;

        FileInputStream inputStream;
        System.out.println("line 60");
        try {
            inputStream = new FileInputStream(excelFilePath);
            XSSFWorkbook workbook = new XSSFWorkbook(inputStream);
            XSSFSheet sheet = workbook.getSheet("Sheet1");
            int rows = sheet.getLastRowNum();
            int cols = sheet.getRow(1).getLastCellNum();
            for (int r = 1; r <= rows; r++) {

                XSSFRow row1 = sheet.getRow(r);

                for (int c = 0; c < cols; c++) {

                    XSSFCell cell = row1.getCell(c);

                    if (QRType.equals("QR for Agent")) {
                        if (cell.getColumnIndex() == 0) {
                            System.out.println(cell.getColumnIndex());

                            //System.out.println(cell.getNumericCellValue());
                            long num = (long) cell.getNumericCellValue();
                            System.out.println(num);

                            //data that we want to store in the QR code in string format 
                            String str = Long.toString(num);
                            System.out.println("line 86");
                            QRcode(str, r, imageFilePath, qRParent.getQrCode(), backgroundImage, QRType);
                            numberImage(str, r, imageFilePath, qRParent.getAgentNumber());
                        }
                        if (cell.getColumnIndex() == 1) {

                            String name = cell.getStringCellValue();
                            System.out.println(name);
                            System.out.println("line 93");
                            agentNameImage(name, r, imageFilePath, qRParent.getAgentName());
                        }
                        if (cell.getColumnIndex() == 2) {

                            String name = cell.getStringCellValue();
                            System.out.println(name);
                            System.out.println("line 100");
                            agentRegionImage(name, r, imageFilePath, qRParent.getRegion());
                        }

                        if (cell.getColumnIndex() == 3) {

                            String name = cell.getStringCellValue();
                            System.out.println(name);
                            System.out.println("line 108");
                            agentDistrictImage(name, r, imageFilePath, qRParent.getDistrict());
                        }

                        if (cell.getColumnIndex() == 4) {

                            String name = cell.getStringCellValue();
                            System.out.println(name);
                            System.out.println("line 116");
                            agentAddressImage(name, r, imageFilePath, qRParent.getAddress());
                        }
                        
                    } else if (QRType.equals("QR for Merchant")) {
                        if (cell.getColumnIndex() == 0) {
                            System.out.println(cell.getColumnIndex());

                            //System.out.println(cell.getNumericCellValue());
                            long num = (long) cell.getNumericCellValue();
                            System.out.println(num);

                            //data that we want to store in the QR code in string format  
                            String str = Long.toString(num);

                            System.out.println("######### " + cell.getColumnIndex());
                            System.out.println("line 132");
                            System.out.println(QRType);
                            QRcode(str, r, imageFilePath, qRParent.getQrCode(), backgroundImage, QRType);
                        }

                        if (cell.getColumnIndex() == 1) {

                            long number = (long) cell.getNumericCellValue();
                            System.out.println(number);
                            System.out.println("line 140");
                            numberImage(String.valueOf(number), r, imageFilePath, qRParent.getAgentNumber());
                        }

                    }

                }

            }
            System.out.println("images created successfully");
        } catch (FileNotFoundException ex) {
            Logger.getLogger(QrCodeGenerator.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        } catch (IOException ex) {
            Logger.getLogger(QrCodeGenerator.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
        
        //JFrame jFrame = new JFrame();
        //jFrame.setSize(300, 300);
        //JOptionPane.showMessageDialog(jFrame, "QR Code Images created successfully");
    }

    public static void QRcode(String number, int row_num, String imageFilePath, QRItem qRItem, String backgroundImage, String QRType) {
        System.out.println(QRType);
        System.out.println("###### QRcode()");
        String filePath = imageFilePath + row_num + ".jpg";
        int size = qRItem.getQrSize();
        String crunchifyFileType = "jpg";
        File crunchifyFile = new File(filePath);
        try {

            Map<EncodeHintType, Object> crunchifyHintType = new EnumMap<EncodeHintType, Object>(EncodeHintType.class);
            crunchifyHintType.put(EncodeHintType.CHARACTER_SET, "UTF-8");

            // Now with version 3.4.1 you could change margin (white border size)
            crunchifyHintType.put(EncodeHintType.MARGIN, 1);
            /* default = 4 */
            Object put = crunchifyHintType.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);

            QRCodeWriter mYQRCodeWriter = new QRCodeWriter(); // throws com.google.zxing.WriterException

            String merchantQRData;
            BitMatrix crunchifyBitMatrix = new BitMatrix(1);
            if (QRType.equals("QR for Merchant")) {
                System.err.println("######## QR for Merchant");
                merchantQRData = generateQRCodeData("https://api.bdkepler.com/api_middleware-0.0.1-RELEASE", number, "merchant-qr-tap");
                crunchifyBitMatrix = mYQRCodeWriter.encode(merchantQRData, BarcodeFormat.QR_CODE, size,
                        size, crunchifyHintType);

            } else if (QRType.equals("QR for Agent")) {
                System.err.println("######## QR for Agent");
                crunchifyBitMatrix = mYQRCodeWriter.encode(number, BarcodeFormat.QR_CODE, size,
                        size, crunchifyHintType);
            }
            int CrunchifyWidth = crunchifyBitMatrix.getWidth();

            // The BufferedImage subclass describes an Image with an accessible buffer of crunchifyImage data.
            BufferedImage crunchifyImage = new BufferedImage(CrunchifyWidth, CrunchifyWidth,
                    BufferedImage.TYPE_INT_RGB);

            // Creates a Graphics2D, which can be used to draw into this BufferedImage.
            crunchifyImage.createGraphics();

            // This Graphics2D class extends the Graphics class to provide more sophisticated control over geometry, coordinate transformations, color management, and text layout.
            // This is the fundamental class for rendering 2-dimensional shapes, text and images on the Java(tm) platform.
            Graphics2D crunchifyGraphics = (Graphics2D) crunchifyImage.getGraphics();

            // setColor() sets this graphics context's current color to the specified color.
            // All subsequent graphics operations using this graphics context use this specified color.
            crunchifyGraphics.setColor(Color.white);

            // fillRect() fills the specified rectangle. The left and right edges of the rectangle are at x and x + width - 1.
            crunchifyGraphics.fillRect(0, 0, CrunchifyWidth, CrunchifyWidth);

            // TODO: Please change this color as per your need
            crunchifyGraphics.setColor(Color.BLACK);

            for (int i = 0; i < CrunchifyWidth; i++) {
                for (int j = 0; j < CrunchifyWidth; j++) {
                    if (crunchifyBitMatrix.get(i, j)) {
                        crunchifyGraphics.fillRect(i, j, 1, 1);
                    }
                }
            }

            // A class containing static convenience methods for locating
            // ImageReaders and ImageWriters, and performing simple encoding and decoding.
            ImageIO.write(crunchifyImage, crunchifyFileType, crunchifyFile);

            System.out.println("\nCongratulation.. You have successfully created QR Code.. \n"
                    + "Check your code here: " + filePath);
        } catch (WriterException | IOException e) {
            System.out.println("\nSorry.. Something went wrong...\n");
            e.printStackTrace();
        }

        BufferedImage im;
        BufferedImage im2;
        try {
            im = ImageIO.read(new File(backgroundImage + ".jpg"));
            im2 = ImageIO.read(new File(imageFilePath + row_num + ".jpg"));
            Graphics2D g = im.createGraphics();
            g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC, 0.4f));
            g.drawImage(im2, qRItem.getQrX(),qRItem.getQrY(), null);
            g.dispose();
            ImageIO.write(im, "jpg", new File(imageFilePath + row_num + ".jpg"));
        } catch (IOException ex) {
            Logger.getLogger(QrCodeGenerator.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    static String generateQRCodeData(String url, String merchantId, String salt) {
        // url: https://api.bdkepler.com/api_middleware-0.0.1-RELEASE
        // salt: merchant-qr-tap
        String urlToHash = url + "/merchant-qr?id=" + merchantId;
        String urlSecret = generateSHA512Hash(urlToHash, salt);
        return (urlToHash + "&secret=" + urlSecret);
    }

    static String generateSHA512Hash(String input, String salt) {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-512");
            md.update(salt.getBytes(StandardCharsets.UTF_8));
            byte[] bytes = md.digest(input.getBytes(StandardCharsets.UTF_8));
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.length; i++) {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            // todo: log error
            return null;
        }
    }

    public static void display(BufferedImage image) {
        JFrame f = new JFrame();
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.getContentPane().add(new JLabel(new ImageIcon(image)));
        f.pack();
        f.setVisible(true);
    }

    public static void numberImage(String agent_num, int x, String imageFilePath, NumberTextItem numberItem) throws IOException {
        String substr = agent_num.substring(agent_num.length() - 11, agent_num.length());
        System.out.println(substr);

        final BufferedImage image = ImageIO.read(new File(imageFilePath + x + ".jpg"));

        Graphics g = image.getGraphics();
        //g.setFont(g.getFont().deriveFont(30f));
        g.setFont(new Font(null, Font.BOLD, numberItem.getNumberFontSize()));
        g.setColor(Color.black);

        int distance = 0;
        for (int i = 0; i < 11; i++) {
            g.drawString(String.valueOf(substr.charAt(i)), numberItem.getNumberX() + distance, numberItem.getNumberY());
            distance = distance + numberItem.getNumberDistance();
        }
        g.dispose();

        ImageIO.write(image, "jpg", new File(imageFilePath + x + ".jpg"));

    }

    public static void agentNameImage(String name, int r, String imageFilePath, OtherTextItem agentItem) throws IOException {

        final BufferedImage image = ImageIO.read(new File(imageFilePath + r + ".jpg"));

        Graphics g = image.getGraphics();
        g.setFont(new Font(null, Font.BOLD, agentItem.getTextFontSize()));
        //String textColor = agentItem.getColor();
        g.setColor(agentItem.getColor());

        if (name.length() > 32) {

            String substr = name.substring(0, 31);

            g.drawString(substr, agentItem.getTextX(), agentItem.getTextY1());

        } else {

            g.drawString(name, agentItem.getTextX(), agentItem.getTextY1());
        }

        g.dispose();

        ImageIO.write(image, "jpg", new File(imageFilePath + r + ".jpg"));

    }

    public static void agentRegionImage(String name, int r, String imageFilePath, OtherTextItem regionItem) throws IOException {

        final BufferedImage image = ImageIO.read(new File(imageFilePath + r + ".jpg"));

        Graphics g = image.getGraphics();
        g.setFont(new Font(null, Font.BOLD, regionItem.getTextFontSize()));
        g.setColor(regionItem.getColor());

        if (name.length() > 32) {

            String substr = name.substring(0, 31);
            g.drawString(substr, regionItem.getTextX(), regionItem.getTextY1());

        } else {

            g.drawString(name, regionItem.getTextX(), regionItem.getTextY1());
        }

        g.dispose();

        ImageIO.write(image, "jpg", new File(imageFilePath + r + ".jpg"));

    }

    public static void agentDistrictImage(String name, int r, String imageFilePath, OtherTextItem districtItem) throws IOException {

        final BufferedImage image = ImageIO.read(new File(imageFilePath + r + ".jpg"));

        Graphics g = image.getGraphics();
        g.setFont(new Font(null, Font.BOLD, districtItem.getTextFontSize()));
        g.setColor(districtItem.getColor());

        if (name.length() > 32) {

            String substr = name.substring(0, 31);
            g.drawString(substr, districtItem.getTextX(), districtItem.getTextY1());

        } else {
            g.drawString(name, districtItem.getTextX(), districtItem.getTextY1());
        }

        g.dispose();

        ImageIO.write(image, "jpg", new File(imageFilePath + r + ".jpg"));

    }

    public static void agentAddressImage(String name, int r, String imageFilePath, OtherTextItem addressItem) throws IOException {

        final BufferedImage image = ImageIO.read(new File(imageFilePath + r + ".jpg"));

        Graphics g = image.getGraphics();
        g.setFont(new Font(null, Font.BOLD, addressItem.getTextFontSize()));
        g.setColor(addressItem.getColor());

        if (name.length() > addressItem.getTextMaxLength()) {

            String substr = name.substring(0, addressItem.getTextMaxLength());
            g.drawString(substr, addressItem.getTextX(), addressItem.getTextY1());

            String substr1 = name.substring(addressItem.getTextMaxLength(), name.length());
            g.drawString(substr1, addressItem.getTextX(), addressItem.getTextY2());

        } else {

            g.drawString(name, addressItem.getTextX(), addressItem.getTextY1());
        }

        g.dispose();

        //display(image);
        ImageIO.write(image, "jpg", new File(imageFilePath + r + ".jpg"));

    }
}
