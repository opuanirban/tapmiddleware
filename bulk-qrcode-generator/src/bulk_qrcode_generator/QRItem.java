/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package bulk_qrcode_generator;

/**
 *
 * @author Mashrur.Sadat
 */
public class QRItem {
    
    public int qrX;
    public int qrY;
    public int qrSize;

    public int getQrX() {
        return qrX;
    }

    public void setQrX(int qrX) {
        this.qrX = qrX;
    }

    public int getQrY() {
        return qrY;
    }

    public void setQrY(int qrY) {
        this.qrY = qrY;
    }

    public int getQrSize() {
        return qrSize;
    }

    public void setQrSize(int qrSize) {
        this.qrSize = qrSize;
    }
    
    
}
