package com.reddotmfs.scheduler_service.enumerations;

public enum GenderEnum {
    M("Male"),
    F("Female"),
    O("Others");

    private String genderGroup;

    GenderEnum(String genderGroup) {
        this.genderGroup = genderGroup;
    }

    public String getGenderGroup() {
        return genderGroup;
    }
}

