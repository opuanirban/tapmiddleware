package com.reddotmfs.scheduler_service.enumerations;


public enum ContextUrl {

    KYC_INFO_UPDATE("/EkycUpdate"),

    KYC_VERIFICATION("/get-verification-status/"),

    GET_KYC_TBL("/UserEkyc");
    private String url;

    ContextUrl(String envUrl) {
        this.url = envUrl;
    }

    public String getUrl() {
        return url;
    }
}