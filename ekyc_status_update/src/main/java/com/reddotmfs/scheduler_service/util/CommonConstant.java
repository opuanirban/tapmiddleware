package com.reddotmfs.scheduler_service.util;


public class CommonConstant {


    public static final String TBL_API_AUTH_HEADER = "Authorization";


    public static final String TARGET_TYPE = "TargetType";

    public static final String SUCCESS = "success";
    public static final String PASSED = "passed";
    public static final String FAILED = "failed";
    public static final String PENDING = "pending";
    public static final String INVALID = "invalid";
    public static final String EC_REQUESTED = "ec_requested";
    public static final String ERROR = "error";
    public static final String NOT_FOUND = "not_found";


//    public static final String KYC_FAIL = ",KYC verification submission failed";
//    public static final String KYC_EXISTS = ",However number/nid has been used for another kyc verification";

    public static String checkNumber(String accountNo) {
        if (accountNo.startsWith("88")) {

        } else if (accountNo.startsWith("+88")) {
            accountNo = accountNo.substring(1);
        } else {
            StringBuilder sb = new StringBuilder(accountNo);
            sb.insert(0, "88");
            accountNo = sb.toString();
        }
        return accountNo;
    }


}
