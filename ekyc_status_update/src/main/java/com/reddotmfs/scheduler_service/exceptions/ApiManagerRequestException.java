package com.reddotmfs.scheduler_service.exceptions;

public class ApiManagerRequestException extends RuntimeException {

    public ApiManagerRequestException(String message) {
        super(message);
    }

    public ApiManagerRequestException(String message, Throwable cause) {
        super(message, cause);
    }
}
