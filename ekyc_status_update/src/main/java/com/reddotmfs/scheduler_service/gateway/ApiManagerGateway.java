package com.reddotmfs.scheduler_service.gateway;

import com.google.gson.Gson;

import com.reddotmfs.scheduler_service.config.ApplicationProperties;
import com.reddotmfs.scheduler_service.dto.RegistrationParams;
import com.reddotmfs.scheduler_service.dto.TblRegistrationStatus;
import com.reddotmfs.scheduler_service.enumerations.ContextUrl;
import com.reddotmfs.scheduler_service.exceptions.ApiManagerRequestException;
import com.reddotmfs.scheduler_service.util.CommonConstant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import java.util.Locale;

@Slf4j
@Service
public class ApiManagerGateway {

    @Autowired
    RestTemplate restTemplate;
    @Value("${api_manager_app.url}")
    private String apiManagerUrl;

    @Value("${api_encKey}")
    private String encKey;
    @Value("${api.auth_header}")
    private String authHeader;

    public static final String SHORT_CODE = "app";

    @Autowired
    ApplicationProperties applicationProperties;


    private String getResponseString(String url, Locale locale) {
        log.debug("url requested: " + url);

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(CommonConstant.TBL_API_AUTH_HEADER, authHeader);
        final HttpEntity<Object> entity = new HttpEntity<>(httpHeaders);

        ResponseEntity<String> response = restTemplate.exchange(url,
                HttpMethod.POST,
                entity,
                String.class);

        if (response.getStatusCode() == HttpStatus.OK) {
            log.debug("response: " + response.getBody());
            return response.getBody();
        } else if (response.getStatusCode() == HttpStatus.BAD_REQUEST) {
            throw new ApiManagerRequestException("Invalid Input");
            //throw new ApiManagerRequestException("Invalid Input");
        } else {
            throw new ApiManagerRequestException("Failed to process request through t-cash API");
            //throw new ApiManagerRequestException("Failed to process request through t-cash API");
        }
    }


    public String registrationStatusUpdate(RegistrationParams tblRegistrationStatus) {

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(CommonConstant.TBL_API_AUTH_HEADER, authHeader);

        HttpEntity<RegistrationParams> entity = new HttpEntity<RegistrationParams>(tblRegistrationStatus, httpHeaders);
        log.debug(entity.toString());

        log.info("Calling t-cash api for updating kyc info of registration");
        try {
            ResponseEntity<String> response = restTemplate.exchange(apiManagerUrl + ContextUrl.KYC_INFO_UPDATE.getUrl(), HttpMethod.POST, entity, String.class);
            log.debug(response.getBody());
            if (response.getStatusCode() == HttpStatus.OK) {
                log.debug(response.getBody());
                if (response.getBody().startsWith("\"") & response.getBody().endsWith("\"")) {
                    return response.getBody().substring(1, response.getBody().length() - 1);
                } else {
                    return response.getBody();
                }
            } else if (response.getStatusCode() == HttpStatus.BAD_REQUEST) {
                throw new ApiManagerRequestException(response.getBody());
            } else {
                throw new ApiManagerRequestException(response.getBody());
                // throw new ApiManagerRequestException(messageRetrieverService.getMessage(locale, ErrorKeyConstants.API_MANAGER_FAIL_KEY));
            }
        } catch (Exception e) {
            log.error("error : ", e);
            throw new ApiManagerRequestException(e.getMessage());
        }
    }


    public TblRegistrationStatus getKyc(String msisdn, Locale locale) {

        String url = new StringBuilder(apiManagerUrl)
                .append(ContextUrl.GET_KYC_TBL.getUrl())
                .append("?userNumber=").append(CommonConstant.checkNumber(msisdn))
                .append("&encKey=").append(encKey).toString();

        try {
            String response = getResponseString(url, locale);
            TblRegistrationStatus kycDetails = new TblRegistrationStatus();
            if (response.startsWith("\"") & response.endsWith("\"")) {
                kycDetails = new Gson().fromJson(response.substring(1, response.length() - 1), TblRegistrationStatus.class);

            } else {
                kycDetails = new Gson().fromJson(response, TblRegistrationStatus.class);
            }
            return kycDetails;

        } catch (Exception e) {
            if(e instanceof ResourceAccessException){
                throw new ApiManagerRequestException("Unable to process at this moment. Please try again later.");
            } else {
                throw new ApiManagerRequestException(e.getMessage());
            }
        }
    }


}
