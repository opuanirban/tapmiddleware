package com.reddotmfs.scheduler_service.gateway;

import com.google.gson.Gson;


import com.reddotmfs.scheduler_service.dto.NIDVerificationResponse;
import com.reddotmfs.scheduler_service.enumerations.ContextUrl;
import com.reddotmfs.scheduler_service.exceptions.ApiManagerRequestException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

@Slf4j
@Service
public class KYCGateway {

    @Autowired
    RestTemplate restTemplate;
    @Value("${api_manager_app.url}")
    private String apiManagerUrl;
    @Value("${ekyc.url}")
    private String ekyUrl;

    @Value("${ekyc.token}")
    private String token;

    private static Map<Integer, String> gigatechResponseMessage;
    static
    {
        gigatechResponseMessage = new HashMap<>();
        gigatechResponseMessage.put(4002, "Customer is successfully re-enrolled, waiting verification");
        gigatechResponseMessage.put(4003, "Customer is successfully enrolled, waiting verification");
        gigatechResponseMessage.put(5001, "Please check your image fields");
        gigatechResponseMessage.put(5002, "Mandatory requested data not present");
        gigatechResponseMessage.put(5003, "id_front_name or id_back_name field can not be more than 100 characters");
        gigatechResponseMessage.put(5004, "Please provide a valid Bangladeshi mobile number without country code");
        gigatechResponseMessage.put(5005, "Please upload a valid photo");
        gigatechResponseMessage.put(6001, "Nid or Date of Birth not found");
        gigatechResponseMessage.put(6002, "Nid or Date of Birth not found");
        gigatechResponseMessage.put(6003, "Applicant with this NID or mobile number already exists");
        gigatechResponseMessage.put(6005, "No applicants record found");

    }

    public NIDVerificationResponse nidVerifyStatus(String mobile_number) {

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Authorization", "Token " + token);

        try {
            MultiValueMap<String, Object> requestParameterMap = new LinkedMultiValueMap<>();
            requestParameterMap.add("mobile_number", mobile_number);


            HttpEntity<MultiValueMap<String, Object>> entity = new HttpEntity<>(requestParameterMap, httpHeaders);

            String url = new StringBuilder(ekyUrl)
                    .append(ContextUrl.KYC_VERIFICATION.getUrl()).toString();

            log.info("Calling eKYC api for nid verification status");

            ResponseEntity<String> response = restTemplate
                    .exchange(url,
                            HttpMethod.POST,
                            entity,
                            String.class);

            if (response.getStatusCode() == HttpStatus.OK) {
                log.debug("response from gt: {}", response.getBody());
                NIDVerificationResponse nidResponseSuccess = new Gson().fromJson(response.getBody(), NIDVerificationResponse.class);
                log.debug("response from gigatech: " + nidResponseSuccess.getData().getStatus());
                return nidResponseSuccess;
            } else if (response.getStatusCode() == HttpStatus.BAD_REQUEST) {
                throw new ApiManagerRequestException("Invalid Input");
            } else {
                throw new ApiManagerRequestException("Failed to process request through eKyc API");
            }
        } catch (Exception e) {
            log.error("error : ", e);
            throw new ApiManagerRequestException("Unable to connect with eKyc API");
        }
    }

//    private Resource createTempFileResource(MultipartFile content) throws IOException {
//        Path tempFile = Files.createTempFile(content.getName(), "." + FilenameUtils.getExtension(content.getOriginalFilename()));
//        Files.write(tempFile, content.getBytes());
//        return new FileSystemResource(tempFile.toFile());
//    }
}
