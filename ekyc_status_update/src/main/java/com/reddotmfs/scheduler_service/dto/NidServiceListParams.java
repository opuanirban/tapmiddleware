package com.reddotmfs.scheduler_service.dto;

import lombok.Data;

@Data
public class NidServiceListParams {
    private Integer Id;
    private String Description;
    private String Amount;
}
