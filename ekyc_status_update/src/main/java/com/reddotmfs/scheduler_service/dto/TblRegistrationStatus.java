package com.reddotmfs.scheduler_service.dto;

import lombok.Data;

@Data
public class TblRegistrationStatus {
    private String status;
    private RegistrationParams detail;
}
