package com.reddotmfs.scheduler_service.dto;

import lombok.Data;

@Data
public class RefreshEKYCResponse {
    private String gigatechStatus;
    private String tblStatus;
}
