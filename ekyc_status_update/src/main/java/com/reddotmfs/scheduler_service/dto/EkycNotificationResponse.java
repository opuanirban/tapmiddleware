package com.reddotmfs.scheduler_service.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class EkycNotificationResponse {
    @NotNull
    @NotEmpty
    private String status;
    @NotNull
    private EkycVerificationDetails detail;
    @NotNull
    private Boolean textual_info_match;
    @NotNull
    private Boolean applicant_photo_card_ec_match;
    @NotNull
    private Boolean applicant_photo_app_ec_match;


}
