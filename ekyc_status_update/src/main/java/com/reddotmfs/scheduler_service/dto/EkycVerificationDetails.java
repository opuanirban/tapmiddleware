package com.reddotmfs.scheduler_service.dto;


import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;


@Data
public class EkycVerificationDetails {
    @NotNull
    @NotEmpty
    private String nid_no;
    @NotNull
    @NotEmpty
    private String dob;
    private String gender;
    private String profession;
    private String applicant_name_ben;
    private int applicant_name_ben_score;
    private String applicant_name_eng;
    private int applicant_name_eng_score;
    private String father_name;
    private int father_name_score;
    private String mother_name;
    private int mother_name_score;
    private String spouse_name;
    private int spouse_name_score;
    private String pres_address;
    private int pres_address_score;
    private String prem_address;
    @NotNull
    @NotEmpty
    private String applicant_photo;
    private int applicant_photo_score;
    @NotNull
    @NotEmpty
    private String mobile_number;
    private String nominee;
    private String nominee_relation;
}
