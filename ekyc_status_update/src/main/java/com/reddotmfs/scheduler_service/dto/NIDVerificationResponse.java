package com.reddotmfs.scheduler_service.dto;

import lombok.Data;

@Data
public class NIDVerificationResponse {
    private String status;
    private Integer status_code;
    private NIDVerifyData data;
    private String message;
}
