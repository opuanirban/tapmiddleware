package com.reddotmfs.scheduler_service.dto;

//import lombok.Data;
import lombok.*;

import java.util.List;
//import org.springframework.web.bind.annotation.GetMapping;


@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class EkycApiResponse {
    private Integer missingInTBL;
    private Integer containsInTBL;
    private Integer missingInGT;
    private String message;
}
