package com.reddotmfs.scheduler_service.services;


import com.reddotmfs.scheduler_service.dto.NIDVerificationResponse;
import org.springframework.web.multipart.MultipartFile;

import java.util.Locale;

public interface KYCService {
//    public NIDUploadApiResponse uploadNID(MultipartFile id_front, MultipartFile id_back, Locale locale);
//    public KYCRegistrationResponse kycRegister(String nid_no, String dob, String applicant_name_ben, String applicant_name_eng, String father_name, String mother_name, String spouse_name, String pres_address, String perm_address, String id_front_name, String id_back_name, String gender, String profession, String nominee, String nominee_relation, String mobile_number, MultipartFile applicant_photo, Locale locale);
    public NIDVerificationResponse verifyNidStatus(String mobile_number);
}

