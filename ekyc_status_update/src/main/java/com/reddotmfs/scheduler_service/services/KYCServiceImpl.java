package com.reddotmfs.scheduler_service.services;


import com.reddotmfs.scheduler_service.dto.NIDVerificationResponse;
import com.reddotmfs.scheduler_service.gateway.KYCGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.Locale;

@Service
public class KYCServiceImpl implements KYCService {

    @Autowired
    KYCGateway kycGateway;

//    @Override
//    public NIDUploadApiResponse uploadNID(MultipartFile id_front, MultipartFile id_back, Locale locale) {
//        return kycGateway.nidUpload(id_front, id_back,locale);
//    }
//
//    @Override
//    public KYCRegistrationResponse kycRegister(String nid_no, String dob, String applicant_name_ben, String applicant_name_eng, String father_name, String mother_name, String spouse_name, String pres_address, String perm_address, String id_front_name, String id_back_name, String gender, String profession, String nominee, String nominee_relation, String mobile_number, MultipartFile applicant_photo, Locale locale){
//        return kycGateway.customerRegistration(nid_no, dob, applicant_name_ben, applicant_name_eng, father_name, mother_name, spouse_name, pres_address, perm_address, id_front_name, id_back_name, gender, profession, nominee, nominee_relation, mobile_number, applicant_photo,locale);
//    }

    @Override
    public NIDVerificationResponse verifyNidStatus(String mobile_number) {
        return kycGateway.nidVerifyStatus(mobile_number);
    }
}
