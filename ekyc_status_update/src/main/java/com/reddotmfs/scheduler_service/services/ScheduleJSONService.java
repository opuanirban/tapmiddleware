package com.reddotmfs.scheduler_service.services;

import com.reddotmfs.scheduler_service.dto.EkycApiResponse;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public interface ScheduleJSONService {
    public void processSchedule(MultipartFile file);
}
