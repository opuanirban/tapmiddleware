package com.reddotmfs.scheduler_service.services;



import com.reddotmfs.scheduler_service.dto.RefreshEKYCResponse;

import java.util.Locale;

public interface RefreshEKYCService {
    public RefreshEKYCResponse refreshStatus(String msisdn);

}
