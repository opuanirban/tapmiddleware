package com.reddotmfs.scheduler_service.services;

import com.reddotmfs.scheduler_service.dto.RefreshEKYCResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.iterators.ArrayIterator;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Date;
import java.util.Iterator;


//@Service
@Component
@Slf4j
public class SchedulingService {

    @Autowired
    RefreshEKYCService refreshEKYCService;

    @Value("${input.file}")
    private String Unprocessed;

    @Value("${output.file}")
    private String Processed;

    //  private String FILE_NAME = "src\\main\\resources\\msisdn_list.xlsx";

    @Scheduled(cron = "0 0 0 * * *", zone = "Asia/Dhaka")
    public void callGTandTbl() {

        //System.out.println("Schedule working");
        log.debug("Schedule working");

        try {
            FileInputStream excelFile = new FileInputStream(new File(Unprocessed));
            Workbook workbook = new XSSFWorkbook(excelFile);
            Sheet datatypeSheet = workbook.getSheetAt(0);
            Row header = datatypeSheet.getRow(0);
            Cell headerCell = header.createCell(4);
            headerCell.setCellValue("TBL_Status");
            Iterator<Row> iterator = datatypeSheet.iterator();
            int rowCount = 0;
            String cell = "";
            while (iterator.hasNext()) {
                Row currentRow = iterator.next();

//                if (currentRow.getCell(2).getStringCellValue().equalsIgnoreCase("verification_status")) {
//                    currentRow.createCell(4).setCellValue("TBL_Status");
//
//                }
//                if(rowCount==0){
//                    currentRow.createCell(4).setCellValue("TBL_Status");
//                }

                if(rowCount == 0) {
                    currentRow.createCell(4).setCellValue("TBL_Status");
                    rowCount++;
                    continue;
                }else{
                    currentRow.createCell(4).setCellValue("");
                }

                //else if(rowCount==1)
                String final_number = "";
                Iterator<Cell> cellIterator = currentRow.iterator();
                int msisdn;
                while (cellIterator.hasNext()) {
                    Cell currentCell = cellIterator.next();

                    if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {

                        msisdn = (int) currentCell.getNumericCellValue();
                        String mobile_number = String.valueOf(msisdn);

                        if (mobile_number.length() > 10) {
                            final_number = mobile_number.substring(mobile_number.length() - 10);
                        } else {
                            final_number = mobile_number;
                        }

//                        RefreshEKYCResponse refreshEKYCResponse = refreshEKYCService.refreshStatus(final_number);
//                        currentRow.createCell(6).setCellValue(refreshEKYCResponse.getTblStatus());

                    } else if (currentCell.getCellTypeEnum() == CellType.STRING) {
//                        if(currentCell.getStringCellValue().equalsIgnoreCase("mobile_number")){
//                            currentRow.createCell(6).setCellValue("TBL_Status");
//                        }
                        // rowCount++;
                        if (currentCell.getStringCellValue().equalsIgnoreCase("passed")) {
                            RefreshEKYCResponse refreshEKYCResponse = refreshEKYCService.refreshStatus("0" + final_number);
                            currentRow.createCell(4).setCellValue(refreshEKYCResponse.getTblStatus());
                        } else if (currentCell.getStringCellValue().equalsIgnoreCase("failed") || currentCell.getStringCellValue().equalsIgnoreCase("invalid") || currentCell.getStringCellValue().equalsIgnoreCase("ec_requested")) {
                            currentRow.createCell(4).setCellValue("null");
                        }

                    }

                }
                rowCount++;
            }
            for (int i = 0; i < 7; i++) {
                datatypeSheet.autoSizeColumn(i);
            }
            FileOutputStream fileOut = new FileOutputStream(Unprocessed);
            workbook.write(fileOut);
            fileOut.close();

            workbook.close();

            workbook.close();


        } catch (
                FileNotFoundException e) {
            e.printStackTrace();
        } catch (
                IOException e) {
            e.printStackTrace();
        }


        try {
            Files.move(Paths.get(Unprocessed), Paths.get(Processed), StandardCopyOption.ATOMIC_MOVE);
        } catch (
                IOException e) {
            e.printStackTrace();
        }

    }
}

