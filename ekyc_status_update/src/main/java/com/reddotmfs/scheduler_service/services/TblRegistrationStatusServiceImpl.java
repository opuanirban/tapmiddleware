package com.reddotmfs.scheduler_service.services;


import com.reddotmfs.scheduler_service.config.ApplicationProperties;
import com.reddotmfs.scheduler_service.dto.CommonResponse;
import com.reddotmfs.scheduler_service.dto.EkycNotificationResponse;
import com.reddotmfs.scheduler_service.dto.RegistrationParams;
import com.reddotmfs.scheduler_service.dto.TblRegistrationStatus;
import com.reddotmfs.scheduler_service.enumerations.GenderEnum;
import com.reddotmfs.scheduler_service.gateway.ApiManagerGateway;
import com.reddotmfs.scheduler_service.util.CommonConstant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

@Service
@Slf4j
public class TblRegistrationStatusServiceImpl implements TblRegistrationStatusService {

    @Autowired
    ApiManagerGateway apiManagerGateway;

    @Autowired
    ApplicationProperties applicationProperties;

    @Override
    public CommonResponse passDataToTbl(EkycNotificationResponse data) {
//        TblRegistrationStatus registrationStatus = new TblRegistrationStatus();
        RegistrationParams details = new RegistrationParams();
        CommonResponse commonResponse = new CommonResponse();
        if (data.getStatus().equalsIgnoreCase(CommonConstant.PASSED)) {
//            registrationStatus.setStatus(CommonConstant.PASSED);
//            registrationStatus.setDetail(data.getDetail());
            details.setBanglaName(data.getDetail().getApplicant_name_ben());
            details.setEnglishName(data.getDetail().getApplicant_name_eng());
            if (data.getDetail().getFather_name()==null) {
                details.setFatherName("");
            } else {
                details.setFatherName(data.getDetail().getFather_name());
            }
            if (data.getDetail().getMother_name()==null) {
                details.setMotherName("");
            } else {
                details.setMotherName(data.getDetail().getMother_name());
            }
            if (data.getDetail().getSpouse_name()==null) {
                details.setSpouseName("");
            } else {
                details.setSpouseName(data.getDetail().getSpouse_name());
            }
            if (data.getDetail().getDob() != null || !data.getDetail().getDob().isEmpty()) {
                try {
                    Date date = new SimpleDateFormat("yyyy/MM/dd").parse(data.getDetail().getDob());
                    details.setDoB(new SimpleDateFormat("yyyy-MM-dd").format(date));
                } catch (ParseException e) {
                    log.error(e.getMessage());
                }
            }
            details.setUserKey(Long.valueOf(0));
            if(data.getDetail().getGender() != null) {
                if (data.getDetail().getGender().equalsIgnoreCase(GenderEnum.F.getGenderGroup())) {
                    details.setSex(GenderEnum.F.name());
                } else if (data.getDetail().getGender().equalsIgnoreCase(GenderEnum.M.getGenderGroup())) {
                    details.setSex(GenderEnum.M.name());
                } else if (data.getDetail().getGender().equalsIgnoreCase(GenderEnum.O.getGenderGroup())) {
                    details.setSex(GenderEnum.O.name());
                }
            }
            details.setOccupation(data.getDetail().getProfession());
            details.setUserNumber(CommonConstant.checkNumber(data.getDetail().getMobile_number()));
            details.setNationalIdNumber(data.getDetail().getNid_no());
            details.setNominee(data.getDetail().getNominee());
            details.setNomineeRelation(data.getDetail().getNominee_relation());
            details.setPresentAddress(data.getDetail().getPres_address());
            details.setPermanentAddress(data.getDetail().getPrem_address());
//            details.setPhotoString(data.getDetail().getApplicant_photo());
            details.setIsLimitedKYC(false);
            String response = apiManagerGateway.registrationStatusUpdate(details);
            commonResponse.setMessage(response);
        }
        return commonResponse;
    }

//    @Override
//    public TblRegistrationStatus getKycFromTbl(String msisdn, Locale locale) {
//        return apiManagerGateway.getKyc(msisdn, locale);
//    }
}
