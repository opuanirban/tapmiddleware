package com.reddotmfs.scheduler_service.services;

import com.reddotmfs.scheduler_service.dto.EkycApiResponse;
import com.reddotmfs.scheduler_service.dto.RefreshEKYCResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Date;
import java.util.Iterator;

@Service
@Slf4j
public class ScheduleJSONServiceImpl implements ScheduleJSONService{
    @Autowired
    RefreshEKYCService refreshEKYCService;

    @Value("${input.file}")
    private String Unprocessed;

    @Value("${output.file}")
    private String Processed;

    @Override
    @Async
    public void processSchedule(MultipartFile file ) {
        log.debug("Schedule working");

        Integer missingInTBL = 0;
        Integer containsInTBL = 0;
        Integer missingInGT = 0;

        long time = new Date().getTime();

        try {
//            FileInputStream excelFile = new FileInputStream((File) file);
            InputStream excelFile = new BufferedInputStream(file.getInputStream());
            Workbook workbook = new XSSFWorkbook(excelFile);
            Sheet datatypeSheet = workbook.getSheetAt(0);
            Row header = datatypeSheet.getRow(0);
            Cell headerCell = header.createCell(4);
            headerCell.setCellValue("TBL_Status");
            Iterator<Row> iterator = datatypeSheet.iterator();
            int rowCount = 0;
            String cell = "";
            while (iterator.hasNext()) {
                Row currentRow = iterator.next();
                log.debug("processing row number: " +  currentRow.getRowNum());

                if(rowCount == 0) {
                    currentRow.createCell(4).setCellValue("TBL_Status");
                    rowCount++;
                    continue;
                }else{
                    currentRow.createCell(4).setCellValue("");
                }

                String final_number = "";
                Iterator<Cell> cellIterator = currentRow.iterator();
                int msisdn;
                while (cellIterator.hasNext()) {
                    Cell currentCell = cellIterator.next();

                    if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {

                        msisdn = (int) currentCell.getNumericCellValue();
                        String mobile_number = String.valueOf(msisdn);

                        if (mobile_number.length() > 10) {
                            final_number = mobile_number.substring(mobile_number.length() - 10);
                        } else {
                            final_number = mobile_number;
                        }

                    } else if (currentCell.getCellTypeEnum() == CellType.STRING) {

                        if (currentCell.getStringCellValue().equalsIgnoreCase("passed")) {
                            try {
                                RefreshEKYCResponse refreshEKYCResponse = refreshEKYCService.refreshStatus("0" + final_number);
                                if (refreshEKYCResponse.getTblStatus() == null) {
                                    missingInGT++;
                                    currentRow.createCell(4).setCellValue(refreshEKYCResponse.getGigatechStatus());
                                } else {
                                    if (refreshEKYCResponse.getTblStatus().contains("User number missing in TBL DB") == true) {
                                        missingInTBL++;
                                        currentRow.createCell(4).setCellValue(refreshEKYCResponse.getTblStatus());
                                    } else {
                                        if (refreshEKYCResponse.getTblStatus().contains("E-KYC has been updated successfully")) {
                                            currentRow.createCell(4).setCellValue("completed");
                                        } else {
                                            currentRow.createCell(4).setCellValue(refreshEKYCResponse.getTblStatus());
                                        }
                                        containsInTBL++;
                                    }
                                }
                            } catch (Exception e) {
                                log.error("error for number " + final_number + " with error: " + e.getMessage());
                            }

                        } else if (currentCell.getStringCellValue().equalsIgnoreCase("failed") || currentCell.getStringCellValue().equalsIgnoreCase("invalid") || currentCell.getStringCellValue().equalsIgnoreCase("ec_requested")) {
                            missingInGT++;
                        }
                    }
                }
                rowCount++;
            }
            for (int i = 0; i < 7; i++) {
                datatypeSheet.autoSizeColumn(i);
            }

            FileOutputStream fileOut = new FileOutputStream(Processed + "/outfile_" + time + ".xlsx");
            workbook.write(fileOut);
            fileOut.close();
            workbook.close();
            workbook.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            log.error("error in file operation: {}", e);
        } catch (IOException e) {
            e.printStackTrace();
            log.error("error: {}", e);
        }

        EkycApiResponse ekycApiResponse = new EkycApiResponse();
        ekycApiResponse.setContainsInTBL(containsInTBL);
        ekycApiResponse.setMissingInTBL(missingInTBL);
        ekycApiResponse.setMissingInGT(missingInGT);

        ekycApiResponse.setMessage("Operation Completed");

        /*try {
            Files.move(Paths.get(Unprocessed), Paths.get(Processed), StandardCopyOption.ATOMIC_MOVE);
        } catch (
                IOException e) {
            e.printStackTrace();
            ekycApiResponse.setMessage("Operation Failed");
        }*/

        log.info("process completed");
        log.info("response: {}", ekycApiResponse);
    }
}
