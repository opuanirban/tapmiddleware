package com.reddotmfs.scheduler_service.services;


import com.reddotmfs.scheduler_service.dto.CommonResponse;
import com.reddotmfs.scheduler_service.dto.EkycNotificationResponse;
import com.reddotmfs.scheduler_service.dto.TblRegistrationStatus;

import java.util.Locale;

public interface TblRegistrationStatusService {
    public CommonResponse passDataToTbl(EkycNotificationResponse data);

//    public TblRegistrationStatus getKycFromTbl(String msisdn, Locale locale);
}
