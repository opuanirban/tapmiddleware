package com.reddotmfs.scheduler_service.controller;

import com.reddotmfs.scheduler_service.dto.CommonResponse;
import com.reddotmfs.scheduler_service.dto.EkycApiResponse;
import com.reddotmfs.scheduler_service.services.ScheduleJSONService;
import com.reddotmfs.scheduler_service.services.SchedulingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController

public class ProcessGTandTblNID {

    @Autowired
    ScheduleJSONService scheduleJSONService;

    @PostMapping("/processNIDFromTBL")
    public String processNIDFromTBL(@RequestParam("file")MultipartFile file){
        scheduleJSONService.processSchedule(file);
        return "Request received and in processing";
    }


}
