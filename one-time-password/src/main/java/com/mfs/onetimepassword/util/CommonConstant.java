package com.mfs.onetimepassword.util;

public class CommonConstant {

    public static final String SUCCESS_CODE = "000";
    public static final String MISMATCH_CODE = "001";
    public static final String EXPIRATION_CODE = "002";
    public static final String INVALID_REFERENCE = "003";
    public static final String INVALID_CLIENT = "004";
    public static final String INVALID_SERVICE = "005";
    public static final String INVALID_SERVICE_ACCESS = "006";
    public static final String NO_TEMPLATE = "007";
    public static final String OTP_FAIL = "008";
    public static final String OTP_NOT_FOUND = "011";
    public static final String SMS_SENDING_FAILED = "011";

    public static final String CLIENT_IP_HEADER = "X-FORWARDED-FOR";
    public static final String CLIENT_USERNAME_HEADER = "X-CLIENT-USERNAME";
    public static final String CLIENT_PASSWORD_HEADER = "X-CLIENT-PASSWORD";
    public final static String ACCOUNT_SID = "AC0355ccfbf9813323d3e60fe03a343500";
    public final static String AUTH_ID = "9f0186fa69f8367f8ca8aa5c450562cc";

    public static final String SMS_SUCCESS = "SMS Sent Successfully";
    public static final String OTP_EVENT = "OTP_EVENT";


//    public final static String SMS_GATEWAY_URL = "https://<smsc-ip>:<http-port>/sms";
//    public final static String SMS_SENDER = "";
//    public final static String SMS_GATEWAY_USER = "user";
//    public final static String SMS_GATEWAY_PASSWORD = "password";


}
