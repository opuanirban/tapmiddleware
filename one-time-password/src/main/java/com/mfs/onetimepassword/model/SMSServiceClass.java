package com.mfs.onetimepassword.model;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@XmlRootElement(name = "ServiceClass")
@XmlAccessorType(XmlAccessType.FIELD)
public class SMSServiceClass {
    private Integer MessageId;
    private Integer Status;
    private String StatusText;
    private Integer ErrorCode;
    private String ErrorText;
    private Integer SMSCount;
    private Integer CurrentCredit;
}
