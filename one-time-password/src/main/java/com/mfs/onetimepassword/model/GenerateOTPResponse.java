package com.mfs.onetimepassword.model;

import lombok.Data;

@Data
public class GenerateOTPResponse extends BaseResponse {
    private String otpReferenceId;
}
