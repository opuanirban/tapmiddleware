package com.mfs.onetimepassword.model;

import lombok.Data;

@Data
public class OTPFetch {
    Integer otp;
}
