package com.mfs.onetimepassword.model;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class GenerateOTPRequest {
    @NotNull
    @NotEmpty
    private String serviceName;
    @NotNull
    @NotEmpty
    private String receiverInfo;
    @NotNull
    @NotEmpty
    private String deviceType;
}
