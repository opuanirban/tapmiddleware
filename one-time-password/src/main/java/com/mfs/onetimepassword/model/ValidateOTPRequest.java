package com.mfs.onetimepassword.model;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class ValidateOTPRequest {
    @NotNull
    @NotEmpty
    private Integer otp;
    @NotNull
    @NotEmpty
    private String otpReferenceId;
    @NotNull
    @NotEmpty
    private String receiverInfo;

}
