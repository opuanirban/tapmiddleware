package com.mfs.onetimepassword.model;

import lombok.Data;

@Data
public class BaseResponse {
    private String code;
    private String message;

}
