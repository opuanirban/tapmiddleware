package com.mfs.onetimepassword.config;

import com.mfs.onetimepassword.exception.ApiRequestException;
import com.mfs.onetimepassword.util.CommonConstant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.DatatypeConverter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@Component
public class ServiceRequestFilter implements Filter {

    private static final Logger logger = LoggerFactory.getLogger(ServiceRequestFilter.class);

    @Autowired
    ApplicationProperties applicationProperties;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    @Transactional
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;

        if (!httpServletRequest.getServletPath().startsWith("/actuator")) {
            String userName = httpServletRequest.getHeader(CommonConstant.CLIENT_USERNAME_HEADER);
            String password = httpServletRequest.getHeader(CommonConstant.CLIENT_PASSWORD_HEADER);
            String passwordHash;
            try {
                MessageDigest md = MessageDigest.getInstance("SHA-512");
                passwordHash = DatatypeConverter.printHexBinary(md.digest(password.getBytes(StandardCharsets.UTF_8)));
            } catch (NoSuchAlgorithmException e) {
                throw new ApiRequestException("Hash algorithm not initialized properly");
            }
            String clientIpAddress = httpServletRequest.getHeader(CommonConstant.CLIENT_IP_HEADER);
            if (clientIpAddress == null) {
                clientIpAddress = httpServletRequest.getRemoteAddr();
            }

            if (userName.equals(applicationProperties.getClientId()) && passwordHash.equals(applicationProperties.getClientPassword())
                    && (applicationProperties.getClientAllowedIpList().contains(clientIpAddress) || clientIpAddress.startsWith("10.0"))) {
                filterChain.doFilter(servletRequest, servletResponse);
            } else {
                throw new ApiRequestException("Sorry Client not found");
            }
        } else {
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }

    @Override
    public void destroy() {
    }

}
