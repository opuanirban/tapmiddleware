package com.mfs.onetimepassword.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Data
public class ApplicationProperties {

    @Value("${http.url.sms-gateway}")
    private String smsGatewayUrl;

    @Value("${sms-gateway.username}")
    private String smsGatewayUsername;

    @Value("${sms-gateway.password}")
    private String smsGatewayPassword;

    @Value("${sms-gateway.sender}")
    private String smsGatewaySender;

    @Value("${client.id}")
    private String clientId;

    @Value("${client.password}")
    private String clientPassword;

    @Value("${client.allowed.iplist}")
    private List<String> clientAllowedIpList;

    @Value("${receiver.type}")
    private String receiverType;

    @Value("${otp.length}")
    private int otpLength;

    @Value("${otp.validation.period}")
    private int otpValidationPeriod;

    @Value("${otp.text.android}")
    private String otpTextAndroid;

    @Value("${otp.text.ios}")
    private String otpTextIOS;

    @Value("${otp.text.uid}")
    private String otpUID;

}
