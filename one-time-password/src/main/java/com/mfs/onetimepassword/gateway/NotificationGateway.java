package com.mfs.onetimepassword.gateway;

import com.mfs.onetimepassword.exception.SMSSendingException;
import com.mfs.onetimepassword.util.CommonConstant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Locale;

@Service
@Slf4j
public class NotificationGateway {

    @Autowired
    RestTemplate restTemplate;

    @Value("${notification_service.url}")
    private String notificationServiceUrl;


    public void sendText(String msisdn,String text) {

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("API_AUTH_CODE", "THI0QHRVU6YTZUY94DWC");

        final HttpEntity<Object> entity = new HttpEntity<>(httpHeaders);

        String url = new StringBuilder(notificationServiceUrl)
                .append("?msisdn=").append(msisdn)
                .append("&text=").append(text).toString();
        log.info(url);

        log.info("Calling notification service for sending sms");
        try {
            ResponseEntity<String> response = restTemplate
                    .exchange(url,
                            HttpMethod.POST,
                            entity,
                            String.class);

            if (response.getStatusCode() == HttpStatus.OK) {
                log.info(response.getBody());
                if (response.getBody().contains(CommonConstant.SMS_SUCCESS)) {
                    return;
                } else {
                    throw new SMSSendingException("Sms Sending Failed");
                }
            } else if (response.getStatusCode() == HttpStatus.BAD_REQUEST) {
                throw new SMSSendingException("Sms Sending Failed");
            } else {
                throw new SMSSendingException("Sms Sending Failed");
            }
        } catch (Exception e) {
            log.error("error : ", e);
            throw new SMSSendingException("Sms Sending Failed");
        }
    }
}
