package com.mfs.onetimepassword.exception;

public class InvalidServcieAccessException extends RuntimeException {

    public InvalidServcieAccessException(String message) {
        super(message);
    }

    public InvalidServcieAccessException(String message, Throwable cause) {
        super(message, cause);
    }
}
