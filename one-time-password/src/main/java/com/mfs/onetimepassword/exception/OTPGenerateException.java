package com.mfs.onetimepassword.exception;

import lombok.Data;

@Data
public class OTPGenerateException {
    private final String error_code;
    private final String error_description;
}
