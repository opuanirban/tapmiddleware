package com.mfs.onetimepassword.exception;

public class OTPFailException extends RuntimeException {

    public OTPFailException(String message) {
        super(message);
    }

    public OTPFailException(String message, Throwable cause) {
        super(message, cause);
    }
}
