package com.mfs.onetimepassword.exception;


import lombok.Data;

import java.time.ZonedDateTime;

@Data
public class ApiException {
    private final String message;
    private final ZonedDateTime timestamp;
}
