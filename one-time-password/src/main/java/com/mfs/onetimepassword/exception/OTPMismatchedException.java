package com.mfs.onetimepassword.exception;

public class OTPMismatchedException extends RuntimeException {

    public OTPMismatchedException(String message) {
        super(message);
    }

    public OTPMismatchedException(String message, Throwable cause) {
        super(message, cause);
    }
}
