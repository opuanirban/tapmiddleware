package com.mfs.onetimepassword.exception;

public class OTPNotFoundException extends RuntimeException {

    public OTPNotFoundException(String message) {
        super(message);
    }

    public OTPNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
