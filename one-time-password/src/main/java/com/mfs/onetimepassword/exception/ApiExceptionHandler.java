package com.mfs.onetimepassword.exception;

import com.mfs.onetimepassword.util.CommonConstant;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.ZoneId;
import java.time.ZonedDateTime;

@ControllerAdvice
public class ApiExceptionHandler {

    @ExceptionHandler(value = ApiRequestException.class)
    public ResponseEntity<Object> handleApiRequestException(ApiRequestException e) {
        //1.Create payload containing exception details
        ApiException apiException = new ApiException(
                e.getMessage(),
                ZonedDateTime.now(ZoneId.of("Z"))
        );
        //2. return response entity
        return new ResponseEntity<>(apiException, org.springframework.http.HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = UserNotFoundException.class)
    public ResponseEntity<OTPGenerateException> handleClinetRequestException(UserNotFoundException e) {
        //1.Create payload containing exception details
        OTPGenerateException exception = new OTPGenerateException(
                CommonConstant.INVALID_CLIENT,
                e.getMessage());
        return new ResponseEntity<>(exception, org.springframework.http.HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = InvalidServcieException.class)
    public ResponseEntity<OTPGenerateException> handleServiceRequestException(InvalidServcieException e) {
        //1.Create payload containing exception details
        OTPGenerateException exception = new OTPGenerateException(
                CommonConstant.INVALID_SERVICE,
                e.getMessage());
        return new ResponseEntity<>(exception, org.springframework.http.HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = InvalidServcieAccessException.class)
    public ResponseEntity<OTPGenerateException> handleServiceRequestException(InvalidServcieAccessException e) {
        //1.Create payload containing exception details
        OTPGenerateException exception = new OTPGenerateException(
                CommonConstant.INVALID_SERVICE_ACCESS,
                e.getMessage());
        return new ResponseEntity<>(exception, org.springframework.http.HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = TemplateNotFoundException.class)
    public ResponseEntity<OTPGenerateException> handleServiceRequestException(TemplateNotFoundException e) {
        //1.Create payload containing exception details
        OTPGenerateException exception = new OTPGenerateException(
                CommonConstant.NO_TEMPLATE,
                e.getMessage());
        return new ResponseEntity<>(exception, org.springframework.http.HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = OTPFailException.class)
    public ResponseEntity<OTPGenerateException> handleServiceRequestException(OTPFailException e) {
        //1.Create payload containing exception details
        OTPGenerateException exception = new OTPGenerateException(
                CommonConstant.OTP_FAIL,
                e.getMessage());
        return new ResponseEntity<>(exception, org.springframework.http.HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = OTPExpiredException.class)
    public ResponseEntity<OTPGenerateException> handleServiceRequestException(OTPExpiredException e) {
        //1.Create payload containing exception details
        OTPGenerateException exception = new OTPGenerateException(
                CommonConstant.EXPIRATION_CODE,
                e.getMessage());
        return new ResponseEntity<>(exception, org.springframework.http.HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = OTPMismatchedException.class)
    public ResponseEntity<OTPGenerateException> handleServiceRequestException(OTPMismatchedException e) {
        //1.Create payload containing exception details
        OTPGenerateException exception = new OTPGenerateException(
                CommonConstant.MISMATCH_CODE,
                e.getMessage());
        return new ResponseEntity<>(exception, org.springframework.http.HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = InvalidReferenceException.class)
    public ResponseEntity<OTPGenerateException> handleServiceRequestException(InvalidReferenceException e) {
        //1.Create payload containing exception details
        OTPGenerateException exception = new OTPGenerateException(
                CommonConstant.INVALID_REFERENCE,
                e.getMessage());
        return new ResponseEntity<>(exception, org.springframework.http.HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = OTPNotFoundException.class)
    public ResponseEntity<OTPGenerateException> handleServiceRequestException(OTPNotFoundException e) {
        //1.Create payload containing exception details
        OTPGenerateException exception = new OTPGenerateException(
                CommonConstant.OTP_NOT_FOUND,
                e.getMessage());
        return new ResponseEntity<>(exception, org.springframework.http.HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = SMSSendingException.class)
    public ResponseEntity<OTPGenerateException> handleSMSSendingException(SMSSendingException e) {
        //1.Create payload containing exception details
        OTPGenerateException exception = new OTPGenerateException(
                CommonConstant.SMS_SENDING_FAILED,
                e.getMessage());
        return new ResponseEntity<>(exception, org.springframework.http.HttpStatus.BAD_REQUEST);
    }
}
