package com.mfs.onetimepassword.exception;

public class SMSSendingException extends RuntimeException {

    public SMSSendingException(String message) {
        super(message);
    }

    public SMSSendingException(String message, Throwable cause) {
        super(message, cause);
    }
}
