package com.mfs.onetimepassword.exception;

public class InvalidServcieException extends RuntimeException {

    public InvalidServcieException(String message) {
        super(message);
    }

    public InvalidServcieException(String message, Throwable cause) {
        super(message, cause);
    }
}
