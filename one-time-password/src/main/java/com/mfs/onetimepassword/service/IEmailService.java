package com.mfs.onetimepassword.service;

import org.springframework.mail.MailException;

public interface IEmailService {
    public void sendOtpInMail(String mailTo, String subject, String text) throws MailException;
}
