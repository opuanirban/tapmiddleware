package com.mfs.onetimepassword.service;

import com.mfs.onetimepassword.dao.RedisDao;
import com.mfs.onetimepassword.util.CommonConstant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class TempStoreTransactionServiceImpl implements ITempStoreTransactionService {

    @Autowired
    RedisDao redisDao;

    public static final String DATA_SEPARATOR = "#";

    @Value("${msisdn.timeout.in.seconds}")
    private Integer timeout;

    @Override
    public void storeProcess(String msisdn) {
        String key = msisdn + DATA_SEPARATOR + CommonConstant.OTP_EVENT;
        if (redisDao.getValue(key) != null) {
            Integer count = Integer.parseInt(redisDao.getValue(key));
            count++;
            redisDao.setValueWithTimeout(key, count.toString(), timeout);

        } else {
            redisDao.setValueWithTimeout(key, "1", timeout);
        }

    }
}
