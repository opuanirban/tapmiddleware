package com.mfs.onetimepassword.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class EmailServiceImpl implements IEmailService {

    private static final Logger logger = LoggerFactory.getLogger(EmailServiceImpl.class);
    @Autowired
    private JavaMailSender mailSender;

    @Override
    public void sendOtpInMail(String mailTo, String subject, String text) throws MailException {
        SimpleMailMessage msg = new SimpleMailMessage();
        msg.setTo(mailTo);

        msg.setSubject(subject);
        msg.setText(text);
        mailSender.send(msg);
        logger.info("OTP successfully sent to mail " + mailTo);

    }
}
