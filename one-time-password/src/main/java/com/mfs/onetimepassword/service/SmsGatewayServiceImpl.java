package com.mfs.onetimepassword.service;

import com.mfs.onetimepassword.config.ApplicationProperties;
import com.mfs.onetimepassword.exception.SMSSendingException;
import com.mfs.onetimepassword.model.SMSServiceClass;
import com.mfs.onetimepassword.model.SMSServiceResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.ssl.TrustStrategy;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;
import java.net.URLEncoder;
import java.util.List;

@Slf4j
@Service
@Profile("prod")
public class SmsGatewayServiceImpl implements ISmsGatewayService {

    private static final Logger logger = LoggerFactory.getLogger(SmsGatewayServiceImpl.class);

    @Autowired
    ApplicationProperties applicationProperties;

    @Autowired
    CloseableHttpClient httpClient;

    @Value("#{'${whitelist.test.msisdn}'.split(',')}")
    private List<String> allowedMSISDN;

    @Value("${test.call.allowed}")
    private boolean testAllowed;

    @Autowired
    RestTemplate restTemplate;

    @Override
    public void sendSms(String receiverInfo, String text) {
        try {
            String uri = new StringBuffer(applicationProperties.getSmsGatewayUrl())
                    .append("?Username=").append(applicationProperties.getSmsGatewayUsername())
                    .append("&Password=").append(applicationProperties.getSmsGatewayPassword())
                    .append("&From=").append(applicationProperties.getSmsGatewaySender())
                    .append("&To=").append("88" + receiverInfo)
                    .append("&Message=").append(URLEncoder.encode(text, "UTF-8"))
                    .toString();

            SSLContextBuilder builder = new SSLContextBuilder();
            builder.loadTrustMaterial(null, (TrustStrategy) (chain, authType) -> true);

            SSLConnectionSocketFactory sslSF = new SSLConnectionSocketFactory(builder.build(),
                    SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

            HttpClient httpClient = HttpClients.custom().setSSLSocketFactory(sslSF).build();

            HttpGet request = new HttpGet(uri);
            HttpResponse response = httpClient.execute(request);
            String responseStr = EntityUtils.toString(response.getEntity());

            logger.debug("uri: {}, sms response: {}", uri, response);
            JAXBContext jaxbContext = JAXBContext.newInstance(SMSServiceResponse.class);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

            StringReader reader = new StringReader(responseStr);
            SMSServiceResponse smsServiceResponseList = (SMSServiceResponse) unmarshaller.unmarshal(reader);
            for (int i = 0; i < smsServiceResponseList.getSmsServiceClasses().size() ; i++) {
                log.debug("response item: {}, data: {}",i,smsServiceResponseList.getSmsServiceClasses().get(i));
            }
            SMSServiceClass smsServiceResponse = smsServiceResponseList.getSmsServiceClasses().get(0);
            if(smsServiceResponse.getStatus() == -1){
                throw new SMSSendingException("SMS Sending failed for reason: " + smsServiceResponse.getErrorText());
            }

            return;

        } catch (Exception e) {
            logger.error(e.getMessage());
            throw new SMSSendingException("SMS Sending failed");
        }
    }
}
