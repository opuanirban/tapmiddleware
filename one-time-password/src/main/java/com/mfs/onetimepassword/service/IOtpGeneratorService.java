package com.mfs.onetimepassword.service;

import com.mfs.onetimepassword.model.GenerateOTPRequest;
import com.mfs.onetimepassword.model.GenerateOTPResponse;

public interface IOtpGeneratorService {
    public Integer generateOTP(int n);

    public GenerateOTPResponse generateOTP(GenerateOTPRequest generateOTPRequest, String userName);
}
