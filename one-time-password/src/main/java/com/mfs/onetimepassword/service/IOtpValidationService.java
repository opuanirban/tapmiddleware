package com.mfs.onetimepassword.service;

import com.mfs.onetimepassword.model.BaseResponse;
import com.mfs.onetimepassword.model.OTPFetch;
import com.mfs.onetimepassword.model.ValidateOTPRequest;

public interface IOtpValidationService {
    public BaseResponse verifyOTP(ValidateOTPRequest validateOTPRequest);

    public OTPFetch getOTPByReference(String reference_id);
}
