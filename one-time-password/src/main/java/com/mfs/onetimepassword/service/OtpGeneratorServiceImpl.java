package com.mfs.onetimepassword.service;

import com.mfs.onetimepassword.config.ApplicationProperties;
import com.mfs.onetimepassword.dao.RedisDao;
import com.mfs.onetimepassword.exception.OTPFailException;
import com.mfs.onetimepassword.gateway.NotificationGateway;
import com.mfs.onetimepassword.model.GenerateOTPRequest;
import com.mfs.onetimepassword.model.GenerateOTPResponse;
import com.mfs.onetimepassword.util.CommonConstant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailException;
import org.springframework.stereotype.Service;

import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.security.SecureRandom;
import java.util.List;
import java.util.UUID;

@Slf4j
@Service
public class OtpGeneratorServiceImpl implements IOtpGeneratorService {

    private static final String OTP_CODE_IDENTIFIER = "$otp_data$";
    private static final String OTP_EXPIRY_IDENTIFIER = "$otp_expiry$";
//    @Autowired
//    ISmsGatewayService smsGatewayService;

    @Autowired
    NotificationGateway notificationGateway;
    @Autowired
    IEmailService emailService;
    @Autowired
    ApplicationProperties applicationProperties;
    @Autowired
    ISmsGatewayService smsGatewayService;
    @Autowired
    RedisDao redisDao;
    @Value("#{'${whitelist.test.msisdn}'.split(',')}")
    private List<String> allowedMSISDN;
    @Value("${whitelist.test.msisdn.prefix}")
    private String allowedMSISDNPrefix;
    @Value("${test.call.allowed}")
    private boolean testAllowed;

    @Override
    public Integer generateOTP(int n) {
        SecureRandom random = new SecureRandom();

        int m = (int) Math.pow(10, n - 1);
        log.info("Generating OTP of length " + n);
        return m + random.nextInt(9 * m);
    }

    @Override
    public GenerateOTPResponse generateOTP(GenerateOTPRequest generateOTPRequest, String userName) {

        GenerateOTPResponse generateOTPResponse = new GenerateOTPResponse();
        int otpVal = 0;
        String otpStr = null;
        String otpRefId = String.valueOf(UUID.randomUUID());

        try {
            MessageDigest md = MessageDigest.getInstance("SHA-512");
            if(testAllowed && generateOTPRequest.getReceiverInfo().startsWith(allowedMSISDNPrefix)){
                otpVal = 123123;
            } else if(generateOTPRequest.getReceiverInfo().contains("01875317710")) {    // iOS test number
                otpVal = 123654;
            } else if(generateOTPRequest.getReceiverInfo().contains("01550703243")) {    // agent test number
                otpVal = 123654;
            } else{
                otpVal = generateOTP(applicationProperties.getOtpLength());
            }

            log.debug("otp: " + otpVal);
            String otpHash = DatatypeConverter.printHexBinary(md.digest(String.valueOf(otpVal).getBytes("UTF-8")));
            otpStr = otpHash + "#" + generateOTPRequest.getReceiverInfo();
            redisDao.setValueWithTimeout(otpRefId, otpStr, applicationProperties.getOtpValidationPeriod());

        } catch (Exception e) {
            log.error(e.getMessage());
            throw new OTPFailException(e.getMessage());
        }

        String otpTemplate;
        if (generateOTPRequest.getDeviceType().equalsIgnoreCase("ANDROID")) {
            otpTemplate = applicationProperties.getOtpTextAndroid() + " UID:" + applicationProperties.getOtpUID();
        } else {
            otpTemplate = applicationProperties.getOtpTextIOS();
        }


        if (applicationProperties.getReceiverType().equals("SMS")) {
            try {
                if (generateOTPRequest.getReceiverInfo().length() == 11 && generateOTPRequest.getReceiverInfo().startsWith("0")) {
                    if(testAllowed && generateOTPRequest.getReceiverInfo().startsWith(allowedMSISDNPrefix)){
                        log.debug("no sms send as considering it as testing :{} on number: {}", testAllowed, generateOTPRequest.getReceiverInfo());
                    }else {
                        String smsText = otpTemplate.replace(OTP_CODE_IDENTIFIER, String.valueOf(otpVal))
                                .replace(OTP_EXPIRY_IDENTIFIER, String.valueOf(applicationProperties.getOtpValidationPeriod()));
//                        smsGatewayService.sendSms(generateOTPRequest.getReceiverInfo(), smsText);
                    notificationGateway.sendText(generateOTPRequest.getReceiverInfo(), smsText);
                        log.info("OTP sent to " + generateOTPRequest.getReceiverInfo());
                    }
                    generateOTPResponse.setOtpReferenceId(otpRefId);
                    generateOTPResponse.setCode(CommonConstant.SUCCESS_CODE);
                    generateOTPResponse.setMessage("Success!");
                } else {
                    log.error("Invalid receiver info");
                    throw new OTPFailException("Invalid receiver info");
                }
            } catch (Exception ex) {
                log.error(ex.getMessage());
                throw new OTPFailException(ex.getMessage());
            }
        } else if (applicationProperties.getReceiverType().equals("EMAIL")) {
            try {
                String mailText = otpTemplate.replaceAll(OTP_CODE_IDENTIFIER, String.valueOf(otpVal))
                        .replaceAll(OTP_EXPIRY_IDENTIFIER, String.valueOf(applicationProperties.getOtpValidationPeriod()));

                emailService.sendOtpInMail(generateOTPRequest.getReceiverInfo(), "OTP Verification", mailText);

                generateOTPResponse.setOtpReferenceId(otpRefId);
                generateOTPResponse.setCode(CommonConstant.SUCCESS_CODE);
                generateOTPResponse.setMessage("Success!");
            } catch (MailException ex) {
                log.error(ex.getMessage());
                throw new OTPFailException(ex.getMessage());
            }
        }

        return generateOTPResponse;
    }


}
