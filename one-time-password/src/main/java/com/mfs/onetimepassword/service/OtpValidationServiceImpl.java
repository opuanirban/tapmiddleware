package com.mfs.onetimepassword.service;

import com.mfs.onetimepassword.dao.RedisDao;
import com.mfs.onetimepassword.exception.OTPExpiredException;
import com.mfs.onetimepassword.exception.OTPFailException;
import com.mfs.onetimepassword.exception.OTPMismatchedException;
import com.mfs.onetimepassword.model.BaseResponse;
import com.mfs.onetimepassword.model.OTPFetch;
import com.mfs.onetimepassword.model.ValidateOTPRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;

import static com.mfs.onetimepassword.util.CommonConstant.SUCCESS_CODE;

@Service
public class OtpValidationServiceImpl implements IOtpValidationService {

    private static final Logger logger = LoggerFactory.getLogger(OtpValidationServiceImpl.class);

    @Autowired
    RedisDao redisDao;

    @Autowired
    ITempStoreTransactionService tempStoreTransactionService;

    @Transactional
    public BaseResponse verifyOTP(ValidateOTPRequest validateOTPRequest) {

        String otpData;
        try {
            tempStoreTransactionService.storeProcess(validateOTPRequest.getReceiverInfo());
            otpData = redisDao.getValue(validateOTPRequest.getOtpReferenceId());
        } catch (Exception e) {
            logger.error("redis error: " + e.getMessage());
            throw new OTPExpiredException("internal server error");
        }

        if(otpData == null || otpData.isEmpty()){
            throw new OTPExpiredException("OTP expired or Invalid reference number");
        }

        String otpHash = otpData.split("#")[0];
        String msisdn = otpData.split("#")[1];

        String otpHashReq;
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-512");
            otpHashReq = DatatypeConverter.printHexBinary(md.digest(String.valueOf(validateOTPRequest.getOtp()).getBytes("UTF-8")));
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw new OTPFailException(e.getMessage());
        }

        BaseResponse validationResponse = new BaseResponse();
        if (otpHashReq.equals(otpHash) && msisdn.equals(validateOTPRequest.getReceiverInfo())) {
            validationResponse.setCode(SUCCESS_CODE);
            validationResponse.setMessage("Success!");
            logger.info("OTP successfully verified for " + validateOTPRequest.getReceiverInfo());
            redisDao.deleteValue(validateOTPRequest.getOtpReferenceId());
        } else {
            logger.info("OTP does not match for " + validateOTPRequest.getReceiverInfo());
            throw new OTPMismatchedException("OTP does not match");
        }

        return validationResponse;
    }

    @Transactional
    public OTPFetch getOTPByReference(String reference_id) {
        if (!redisDao.getValue(reference_id).isEmpty()) {
            OTPFetch otp = new OTPFetch();
            otp.setOtp(111111);
            return otp;
        }
        return null;
    }
}
