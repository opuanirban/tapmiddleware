package com.mfs.onetimepassword.service;

import com.mfs.onetimepassword.util.CommonConstant;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Service
@Profile("test")
public class TwilioServiceImpl implements ISmsGatewayService {

    private static final Logger logger = LoggerFactory.getLogger(TwilioServiceImpl.class);

    static {
        Twilio.init(CommonConstant.ACCOUNT_SID, CommonConstant.AUTH_ID);
    }

    @Override
    public void sendSms(String receiverInfo, String text) throws Exception {
        Message.creator(new PhoneNumber(receiverInfo), new PhoneNumber("+15072054105"), text).create();
    }
}
