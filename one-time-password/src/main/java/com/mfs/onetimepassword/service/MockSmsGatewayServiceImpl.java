package com.mfs.onetimepassword.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Service
@Profile("dev")
public class MockSmsGatewayServiceImpl implements ISmsGatewayService {

    private static final Logger logger = LoggerFactory.getLogger(MockSmsGatewayServiceImpl.class);

    @Override
    public void sendSms(String receiverInfo, String text) {
    }
}
