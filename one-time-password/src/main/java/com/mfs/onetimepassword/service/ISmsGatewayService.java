package com.mfs.onetimepassword.service;

public interface ISmsGatewayService {
    void sendSms(String receiverInfo, String text) throws Exception;
}
