package com.mfs.onetimepassword.controller;

import com.mfs.onetimepassword.model.*;
import com.mfs.onetimepassword.service.IOtpGeneratorService;
import com.mfs.onetimepassword.service.IOtpValidationService;
import com.mfs.onetimepassword.util.CommonConstant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/mfs")
public class OtpController {

    private static final Logger logger = LoggerFactory.getLogger(OtpController.class);
    @Autowired
    private IOtpGeneratorService otpGeneratorService;
    @Autowired
    private IOtpValidationService otpValidationService;

    @ResponseBody
    @PostMapping(path = "/sendOtp", consumes = "application/json", produces = "application/json")
    public GenerateOTPResponse sendOTP(@RequestBody GenerateOTPRequest generateOTPRequest,
                                       HttpServletRequest httpServletRequest) {
        String userName = httpServletRequest.getHeader(CommonConstant.CLIENT_USERNAME_HEADER);
        return otpGeneratorService.generateOTP(generateOTPRequest, userName);
    }

    @ResponseBody
    @PostMapping(path = "/validateOtp", consumes = "application/json", produces = "application/json")
    public BaseResponse validateOTP(@RequestBody ValidateOTPRequest validateOTPRequest,
                                    HttpServletRequest httpServletRequest) {
        return otpValidationService.verifyOTP(validateOTPRequest);
    }

}
