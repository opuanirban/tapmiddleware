package com.mfs.agent_middleware.controller;

import java.util.Locale;

import com.mfs.agent_middleware.dto.FeatureConfigList;
import com.mfs.agent_middleware.service.FetchTransactionTypeService;
import com.mfs.agent_middleware.service.ServiceConfigService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DataConfigController {

    @Autowired
    FetchTransactionTypeService transactionTypeService;
    @Autowired
    ServiceConfigService serviceConfigService;

    @ResponseBody
    @GetMapping(path = "/config-data", produces = "application/json")
    public FeatureConfigList feeCharge(Locale locale) {
        return transactionTypeService.fetchTypes(locale);
    }

}
