package com.mfs.agent_middleware.controller;


import java.util.List;
import java.util.Locale;

import com.mfs.agent_middleware.dto.DataJson;
import com.mfs.agent_middleware.dto.DataListResponse;
import com.mfs.agent_middleware.dto.DonationMerchantListRequest;
import com.mfs.agent_middleware.service.DataFetchService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DataFetchController {

    @Autowired
    DataFetchService dataFetchService;

    @GetMapping(path = "/getList")
    public List<DataListResponse> getList(Locale locale) {
        return dataFetchService.getBillerList(locale);
    }

    @GetMapping(path = "/getInsurance")
    public DataJson[] getInsurance(Locale locale) {
        return dataFetchService.getInsurance(locale);
    }

    @GetMapping(path = "/getInstitution")
    public DataJson[] getInstitution(Locale locale) {
        return dataFetchService.getInstitution(locale);
    }

    @GetMapping(path = "/getUtility")
    public List<DataListResponse> getUtility(Locale locale) {
        return dataFetchService.getUtility(locale);
    }

    @GetMapping(path = "/getOthers")
    public DataJson[] getOthers(Locale locale) {
        return dataFetchService.getOthers(locale);
    }

    @GetMapping(path = "/getDonations")
    public DataJson[] getDonations(Locale locale) {
        return dataFetchService.getDonations(locale);
    }

    @PostMapping(path = "/modify-donation-merchant")
    public void modifyDonationMerchantList(@RequestBody DonationMerchantListRequest donationMerchantList){
        dataFetchService.modifyDonationMerchantList(donationMerchantList.getDonationMerchantList());
    }

}
