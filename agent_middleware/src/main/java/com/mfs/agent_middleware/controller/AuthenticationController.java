package com.mfs.agent_middleware.controller;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import com.mfs.agent_middleware.config.ApplicationProperties;
import com.mfs.agent_middleware.dto.CommonResponse;
import com.mfs.agent_middleware.dto.TokenValidationResponse;
import com.mfs.agent_middleware.dto.UserLoginInfo;
import com.mfs.agent_middleware.dto.UserStatusInfo;
import com.mfs.agent_middleware.security.AppUserDetailsService;
import com.mfs.agent_middleware.security.JwtService;
import com.mfs.agent_middleware.security.bean.AuthenticationRequest;
import com.mfs.agent_middleware.security.bean.AuthenticationResponse;
import com.mfs.agent_middleware.service.LogoutService;
import com.mfs.agent_middleware.service.UserRegistrationService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.web.bind.annotation.*;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
public class AuthenticationController {

    private static final Logger logger = LoggerFactory.getLogger(AuthenticationController.class);

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    AppUserDetailsService userDetailsService;

    @Autowired
    JwtService jwtService;

    @Autowired
    UserRegistrationService userRegistrationService;

    @Autowired
    LogoutService logoutService;

    @Autowired
    ApplicationProperties applicationProperties;


    @ResponseBody
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public AuthenticationResponse createAuthenticationToken(@RequestBody AuthenticationRequest authenticationRequest) {

        long requestTime = System.currentTimeMillis();
        UserLoginInfo userLoginInfo = userRegistrationService.fetchUserLoginInfo(authenticationRequest.getMsisdn(),
                authenticationRequest.getPin());
        final String jwt = jwtService.generateToken(authenticationRequest.getMsisdn());

        AuthenticationResponse authenticationResponse = new AuthenticationResponse();
        authenticationResponse.setJwt(jwt);
        authenticationResponse.setTimeout(applicationProperties.getJwtExpireTime()/1000);
        authenticationResponse.setUserLoginInfo(userLoginInfo);

        long elapsedTime = System.currentTimeMillis() - requestTime;
        log.info("Login token generated for user: " + authenticationRequest.getMsisdn() + " Elapsed time: " +
                elapsedTime);
        return authenticationResponse;

    }

    @ResponseBody
    @RequestMapping(value = "/refresh-token", method = RequestMethod.GET)
    public AuthenticationResponse refreshWalletToken(HttpServletRequest httpServletRequest, Locale locale) {
        final String jwt = jwtService.generateToken(httpServletRequest.getHeader("msisdn"));

        AuthenticationResponse authenticationResponse = new AuthenticationResponse();
        authenticationResponse.setJwt(jwt);
        authenticationResponse.setTimeout(applicationProperties.getJwtExpireTime()/1000);

        String authorizationHeader = httpServletRequest.getHeader("Authorization");
        logoutService.addTokenToblacklist(authorizationHeader.substring(7));

        return authenticationResponse;
    }

    @ResponseBody
    @RequestMapping(value = "/validate-token", method = RequestMethod.GET) 
    public TokenValidationResponse validateWalletToken(@RequestParam("jwtToken") String jwtToken, @RequestParam("msisdn") String msisdn) {
        TokenValidationResponse response = new TokenValidationResponse();
        response.setIsValidated(jwtService.validateToken(jwtToken, msisdn));
        return response;
    }

    @ResponseBody
    @RequestMapping(value = "/checkUser", method = RequestMethod.POST)
    public UserStatusInfo checkForUser(@RequestParam("msisdn") String msisdn, Locale locale) {
        return userRegistrationService.checkForUser(msisdn,locale);
    }


    @ResponseBody
    @PostMapping(path = "/signOut")
    public CommonResponse logout(HttpServletRequest httpServletRequest) {
        String authorizationHeader = httpServletRequest.getHeader("Authorization");
        return logoutService.addTokenToblacklist(authorizationHeader.substring(7));
    }

}
