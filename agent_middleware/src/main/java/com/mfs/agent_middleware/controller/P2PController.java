package com.mfs.agent_middleware.controller;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.mfs.agent_middleware.dto.CommonResponse;
import com.mfs.agent_middleware.dto.P2PRequest;
import com.mfs.agent_middleware.service.P2PService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class P2PController {

    @Autowired
    P2PService p2PService;

    @ResponseBody
    @PostMapping(path = "/p2p", produces = "application/json")
    public CommonResponse p2p(@Valid @RequestBody P2PRequest p2PRequest, HttpServletRequest httpServletRequest, Locale locale) throws Exception {
        return p2PService.p2p(p2PRequest,httpServletRequest.getHeader("msisdn"),locale);
    }
}
