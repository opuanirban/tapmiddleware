package com.mfs.agent_middleware.controller;

import java.util.Locale;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.mfs.agent_middleware.dto.CommonResponse;
import com.mfs.agent_middleware.dto.ForceUpdateRequest;
import com.mfs.agent_middleware.dto.ForceUpdateResponse;
import com.mfs.agent_middleware.service.ForceUpdateService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ForceUpdateController {
    @Autowired
    ForceUpdateService forceUpdateService;

    @PostMapping(path = "/force_update_blacklist", produces = "application/json")
    public CommonResponse setForceUpdateVersion(@RequestBody ForceUpdateRequest forceUpdateRequest, Locale locale){
        return forceUpdateService.setForceUpdateVersion(forceUpdateRequest,locale);
    }

    @ResponseBody
    @GetMapping(path = "/check_forceupdate_version", produces = "application/json")
    public ForceUpdateResponse checkForceUpdateVersion(@Valid @NotNull @RequestParam("platform") String platform, Locale locale) {
        return forceUpdateService.checkForceUpdateVersion(platform,locale);
    }
}
