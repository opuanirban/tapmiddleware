package com.mfs.agent_middleware.controller;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.mfs.agent_middleware.dto.BnrfParam;
import com.mfs.agent_middleware.dto.CommonResponse;
import com.mfs.agent_middleware.service.BnrfService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BnrfController {

    @Autowired
    BnrfService bnrfService;

    @PostMapping(path = "/bnrf", produces = "application/json")
    public CommonResponse payDipFee(@Valid @RequestBody BnrfParam bnrfParam, HttpServletRequest httpServletRequest, Locale locale) {
        return bnrfService.bnrfRequest(bnrfParam, httpServletRequest.getHeader("msisdn"),locale);
    }
}
