package com.mfs.agent_middleware.controller;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.mfs.agent_middleware.dto.CommonResponse;
import com.mfs.agent_middleware.dto.DonationPayment;
import com.mfs.agent_middleware.dto.MerchantPayment;
import com.mfs.agent_middleware.service.MerchantPaymentService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MerchantPaymentController {

    @Autowired
    MerchantPaymentService merchantPaymentService;

    @PostMapping(path = "/merchant_payment",produces = "application/json")
    public CommonResponse merchantPayment(@Valid @RequestBody MerchantPayment merchantPayment, HttpServletRequest httpServletRequest, Locale locale){
        return merchantPaymentService.payMerchant(merchantPayment,httpServletRequest.getHeader("msisdn"),locale);
    }

    @PostMapping(path = "/merchant_payment/donation",produces = "application/json")
    public CommonResponse donationPayment(@Valid @RequestBody DonationPayment donationPaymentPayment, HttpServletRequest httpServletRequest, Locale locale){
        return merchantPaymentService.payDonation(donationPaymentPayment, httpServletRequest.getHeader("msisdn"),locale);
    }
}
