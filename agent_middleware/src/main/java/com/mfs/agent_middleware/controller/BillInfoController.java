package com.mfs.agent_middleware.controller;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.mfs.agent_middleware.dto.BillInfoResponse;
import com.mfs.agent_middleware.dto.CommonResponse;
import com.mfs.agent_middleware.dto.DPDCBillInfo;
import com.mfs.agent_middleware.dto.NescoBillInfo;
import com.mfs.agent_middleware.dto.NescoBillInfoResponse;
import com.mfs.agent_middleware.dto.PrepaidBillDetailsResponse;
import com.mfs.agent_middleware.service.BillInfoService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BillInfoController {

    @Autowired
    BillInfoService billInfoService;

    @ResponseBody
    @PostMapping(path = "/dpdc_bill", produces = "application/json")
    public BillInfoResponse dpdcBill(@Valid @RequestBody DPDCBillInfo dpdcBillInfo, Locale locale){
        return billInfoService.getDpdcBill(dpdcBillInfo,locale);
    }

    @ResponseBody
    @PostMapping(path = "/desco_bill", produces = "application/json")
    public BillInfoResponse descoBill(@Valid @RequestParam("billNo") String billNo, Locale locale){
        return billInfoService.getDescoBill(billNo,locale);
    }

    @ResponseBody
    @PostMapping(path = "/nesco_bill", produces = "application/json")
    public NescoBillInfoResponse nescoBill(@Valid @RequestBody NescoBillInfo nescoBillInfo, Locale locale, HttpServletRequest httpServletRequest){
        final String customerAccountNumber = httpServletRequest.getHeader("msisdn");
        return billInfoService.getNescoBill(customerAccountNumber, nescoBillInfo, locale);
    }

    @ResponseBody
    @PostMapping(path = "/desco_prepaid_bill", produces = "application/json")
    public CommonResponse descoBillPrepaid(@Valid @RequestParam("meterNo") String meterNo, @Valid @RequestParam("amount") String amount, Locale locale){
        return billInfoService.getDescoPrepaidInfo(meterNo,amount,locale);
    }

    @ResponseBody
    @PostMapping(path = "/nesco_prepaid_bill", produces = "application/json")
    public PrepaidBillDetailsResponse nescoBillPrepaid(@RequestParam(value = "customerNo", required = false) String customerNo, 
    @RequestParam(value = "meterNo", required = false) String meterNo, @RequestParam("amount") String amount, HttpServletRequest httpServletRequest, 
    Locale locale){
        String customerAccountNumber = httpServletRequest.getHeader("msisdn");
        return billInfoService.getNescoPrepaidInfo(customerAccountNumber, customerNo, meterNo, amount, locale);
    }

    @ResponseBody
    @PostMapping(path = "/reb_prepaid_bill", produces = "application/json")
    public PrepaidBillDetailsResponse rebBillPrepaid(@RequestParam(value = "meterNo", required = false) String meterNo, @RequestParam("amount") String amount, HttpServletRequest httpServletRequest, 
    Locale locale){
        String customerAccountNumber = httpServletRequest.getHeader("msisdn");
        return billInfoService.getREBPrepaidInfo(customerAccountNumber, meterNo, amount, locale);
    }
}
