package com.mfs.agent_middleware.controller;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.mfs.agent_middleware.dto.CommonResponse;
import com.mfs.agent_middleware.dto.InsurancePaymentRequest;
import com.mfs.agent_middleware.service.InsuranceService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class InsuranceController {

    @Autowired
    InsuranceService insuranceService;

    @PostMapping(path = "/insurancePayment", produces = "application/json")
    public CommonResponse insurancePayment(@Valid @RequestBody InsurancePaymentRequest insurancePaymentRequest, HttpServletRequest httpServletRequest, Locale locale) {
        return insuranceService.insurancePayment(insurancePaymentRequest, httpServletRequest.getHeader("msisdn"),locale);
    }
}
