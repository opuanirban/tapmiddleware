package com.mfs.agent_middleware.controller;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.mfs.agent_middleware.dto.CommonResponse;
import com.mfs.agent_middleware.dto.DPDCBillPayRequest;
import com.mfs.agent_middleware.dto.DescoPostPaidRequest;
import com.mfs.agent_middleware.dto.DescoPrepaidRequest;
import com.mfs.agent_middleware.dto.NescoPostpaidRequest;
import com.mfs.agent_middleware.dto.NescoPrepaidRequest;
import com.mfs.agent_middleware.dto.REBPrepaidRequest;
import com.mfs.agent_middleware.service.DescoService;
import com.mfs.agent_middleware.service.DpdcService;
import com.mfs.agent_middleware.service.NescoService;
import com.mfs.agent_middleware.service.REBService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ElectricityBillController {

    @Autowired
    DescoService descoService;
    @Autowired
    DpdcService dpdcService;
    @Autowired
    NescoService nescoService;
    @Autowired
    REBService rebService;

    @ResponseBody
    @PostMapping(path = "/desco_prepaid", produces = "application/json")
    public CommonResponse descoPrepaid(@Valid @RequestBody DescoPrepaidRequest prepaidRequest, HttpServletRequest httpServletRequest, Locale locale) throws Exception {
        return descoService.payDescoBill(prepaidRequest,httpServletRequest.getHeader("msisdn"),locale);
    }

    @ResponseBody
    @PostMapping(path = "/desco_postpaid", produces = "application/json")
    public CommonResponse descoPostpaid(@Valid @RequestBody DescoPostPaidRequest postPaidRequest,HttpServletRequest httpServletRequest,Locale locale) throws Exception {
        return descoService.payDescoBill(postPaidRequest,httpServletRequest.getHeader("msisdn"),locale);
    }

    @ResponseBody
    @PostMapping(path = "/dpdc_pay", produces = "application/json")
    public CommonResponse descoPrepaid(@Valid @RequestBody DPDCBillPayRequest dpdcBillPayRequestt, HttpServletRequest httpServletRequest, Locale locale){
        return dpdcService.payDpdcBill(dpdcBillPayRequestt,httpServletRequest.getHeader("msisdn"),locale);
    }

    @ResponseBody
    @PostMapping(path = "/nesco_prepaid", produces = "application/json")
    public CommonResponse nescoPrepaid(@Valid @RequestBody NescoPrepaidRequest prepaidRequest, HttpServletRequest httpServletRequest, Locale locale) {
        return nescoService.payNescoBill(prepaidRequest,httpServletRequest.getHeader("msisdn"),locale);
    }

    @ResponseBody
    @PostMapping(path = "/nesco_postpaid", produces = "application/json")
    public CommonResponse nescoPostpaid(@Valid @RequestBody NescoPostpaidRequest postPaidRequest,HttpServletRequest httpServletRequest,Locale locale) throws Exception {
        return nescoService.payNescoPostpaidBill(postPaidRequest,httpServletRequest.getHeader("msisdn"),locale);
    }

    @ResponseBody
    @PostMapping(path = "/reb_prepaid", produces = "application/json")
    public CommonResponse rebPrepaid(@Valid @RequestBody REBPrepaidRequest prepaidRequest, HttpServletRequest httpServletRequest, Locale locale) {
        return rebService.payREBPrepaidBill(prepaidRequest,httpServletRequest.getHeader("msisdn"),locale);
    }
}
