package com.mfs.agent_middleware.controller;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.mfs.agent_middleware.dto.CommonResponse;
import com.mfs.agent_middleware.dto.TuitionFeePaymentRequest;
import com.mfs.agent_middleware.service.TuitionPaymentService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TuitionFeeController {

    @Autowired
    TuitionPaymentService tuitionPaymentService;

    @PostMapping(path = "/tuitionPayment",produces = "application/json")
    public CommonResponse payTuition(@Valid @RequestBody TuitionFeePaymentRequest tuitionFeePaymentRequest, HttpServletRequest httpServletRequest, Locale locale){
        return tuitionPaymentService.payTuition(tuitionFeePaymentRequest,httpServletRequest.getHeader("msisdn"),locale);
    }
}
