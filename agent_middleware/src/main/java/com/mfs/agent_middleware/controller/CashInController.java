package com.mfs.agent_middleware.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.mfs.agent_middleware.dto.CashInRequest;
import com.mfs.agent_middleware.dto.CommonResponse;
import com.mfs.agent_middleware.service.CashInService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Locale;

@RestController
public class CashInController {
    @Autowired
    CashInService cashinService;

    @ResponseBody
    @PostMapping(path = "/cash-in", produces = "application/json")
    public CommonResponse cashin(@Valid @RequestBody CashInRequest cashInRequest, HttpServletRequest httpServletRequest, Locale locale) throws Exception {
        return cashinService.cashIn(cashInRequest,httpServletRequest.getHeader("msisdn"),locale);
    }
}
