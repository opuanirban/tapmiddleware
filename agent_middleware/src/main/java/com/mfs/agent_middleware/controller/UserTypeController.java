package com.mfs.agent_middleware.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.mfs.agent_middleware.dto.UserPresenceResponse;
import com.mfs.agent_middleware.dto.UserTypeResponseWallet;
import com.mfs.agent_middleware.service.UserTypeService;

import java.util.Locale;

@RestController
public class UserTypeController {

    @Autowired
    UserTypeService userTypeService;

    @ResponseBody
    @GetMapping(path = "/user_type", produces = "application/json")
    public UserTypeResponseWallet fetchUserType(@Valid @NotNull @RequestParam("userNumber") String userNumber, Locale locale) {
        return userTypeService.fetchUserType(userNumber, locale);
    }

    @ResponseBody
    @GetMapping(path = "/user_presence", produces = "application/json")
    public UserPresenceResponse fetchUserPresence(@Valid @NotNull @RequestParam("userNumber") String userNumber, Locale locale) {
        return userTypeService.fecthUserPresence(userNumber, locale);
    }
}
