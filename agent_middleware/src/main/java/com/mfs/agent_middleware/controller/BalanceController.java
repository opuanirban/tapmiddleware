package com.mfs.agent_middleware.controller;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import com.mfs.agent_middleware.dto.BalanceInquiry;
import com.mfs.agent_middleware.service.BalanceService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
public class BalanceController {

    @Autowired
    BalanceService balanceService;

    @ResponseBody
    @PostMapping(path = "/check_balance", produces = "application/json")
    public BalanceInquiry checkBalance(HttpServletRequest httpServletRequest, Locale locale) {
        long requestTime = System.currentTimeMillis();
        BalanceInquiry balanceInquiry = balanceService.checkBalance(httpServletRequest.getHeader("msisdn"),locale);
        long elapsedTime = System.currentTimeMillis() - requestTime;
        log.info("Balance inquiry Elapsed time: " + elapsedTime);
        return balanceInquiry;
    }
}
