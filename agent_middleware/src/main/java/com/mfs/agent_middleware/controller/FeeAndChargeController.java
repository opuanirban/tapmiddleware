package com.mfs.agent_middleware.controller;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.mfs.agent_middleware.dto.FeesAndChargesModelWallet;
import com.mfs.agent_middleware.dto.FeesChargesResponse;
import com.mfs.agent_middleware.service.FeesAndChargesService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FeeAndChargeController {

    @Autowired
    FeesAndChargesService feesAndChargesService;

    @ResponseBody
    @PostMapping(path = "/fee_charge", produces = "application/json")
    public FeesChargesResponse feeCharge(@Valid @RequestBody FeesAndChargesModelWallet feesAndChargesModel, HttpServletRequest httpServletRequest, Locale locale) {
        return feesAndChargesService.feeAndCharge(feesAndChargesModel, locale);
    }
}
