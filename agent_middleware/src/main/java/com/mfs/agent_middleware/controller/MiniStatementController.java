package com.mfs.agent_middleware.controller;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import com.mfs.agent_middleware.dto.MiniStatementResponse;
import com.mfs.agent_middleware.service.MinistatementService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MiniStatementController {

    @Autowired
    MinistatementService ministatementService;

    @ResponseBody
    @PostMapping(path = "/mini_statement", produces = "application/json")
    public MiniStatementResponse checkBalance(HttpServletRequest httpServletRequest, Locale locale) throws Exception {
        return ministatementService.fetchMiniStatement(httpServletRequest.getHeader("msisdn"),locale);
    }
}
