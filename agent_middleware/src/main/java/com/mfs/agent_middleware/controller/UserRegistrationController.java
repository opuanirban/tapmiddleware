package com.mfs.agent_middleware.controller;


import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.mfs.agent_middleware.dto.*;
import com.mfs.agent_middleware.enumeration.GenderEnum;
import com.mfs.agent_middleware.enumeration.MobileOperatorEnum;
import com.mfs.agent_middleware.enumeration.UserGroupEnum;
import com.mfs.agent_middleware.gateway.model.AgentInfoDto;
import com.mfs.agent_middleware.gateway.model.AgentNidInfoDto;
import com.mfs.agent_middleware.gateway.model.AgentShopInfoDto;
import com.mfs.agent_middleware.service.KYCService;
import com.mfs.agent_middleware.service.UserRegistrationService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
public class UserRegistrationController {

    @Autowired
    UserRegistrationService userRegistrationService;
    @Autowired
    KYCService kycService;

    @PostMapping(path = "/agent_registration_kyc")
    public BaseResponse AgentkycRegister(@RequestParam(value = "agent_number") String agentNumber,
                                    @RequestParam(value = "nid_no") String nidNo,
                                    @RequestParam(value = "nid_dob") String nidDateOfBirth,
                                    @RequestParam(value = "nid_name_en") String nidNameEnglish,
                                    @RequestParam(value = "nid_name_bn") String nidNameBangla,
                                    @RequestParam(value = "nid_father_name") String nidFatherName,
                                    @RequestParam(value = "nid_mother_name") String nidMotherName,
                                    @RequestParam(value = "nid_spouse_name") String nidSpouseName,
                                    @RequestParam(value = "nid_present_address") String nidPresentAddress,
                                    @RequestParam(value = "nid_front_image_name") String nidFrontImageName,
                                    @RequestParam(value = "nid_back_image_name") String nidBackImageName,
                                    @RequestParam(value = "agent_selfie") MultipartFile agentSelfie,
                                    @RequestParam(value = "agent_first_name") String agentFirstName,
                                    @RequestParam(value = "agent_last_name") String agentLastName,
                                    @RequestParam(value = "agent_wallet_pin") String agentWalletPin,
                                    @RequestParam(value = "agent_occupation") String agentOccupation,
                                    @RequestParam(value = "agent_operator_name") String agentOperatorName,
                                    @RequestParam(value = "trade_license") MultipartFile tradeLicense,
                                    @RequestParam(value = "tin_certificate") MultipartFile tinCertificate,
                                    @RequestParam(value = "agent_shop_image") MultipartFile agentShopImage,
                                    @RequestParam(value = "agent_gender") String agentGender,
                                    @RequestParam(value = "agent_nominee") String agentNominee,
                                    @RequestParam(value = "agent_nominee_relation") String agentNomineeRelation,
                                    @RequestParam(value = "shop_name") String shopName,
                                    @RequestParam(value = "shop_type") String shopType,
                                    @RequestParam(value = "division") String division,
                                    @RequestParam(value = "district") String district,
                                    @RequestParam(value = "upazila") String upazila,
                                    @RequestParam(value = "agent_trade_license_number") String agentTradeLicenseNumber,
                                    @RequestParam(value = "agent_tin_number") String agentTinNumber,
                                    @RequestParam(value = "union") String union) {

        AgentInfoDto agentInfoDto = new AgentInfoDto();
        agentInfoDto.setAgentNumber(agentNumber);
        agentInfoDto.setTradeLicense(tradeLicense);
        agentInfoDto.setTinCertificate(tinCertificate);
        agentInfoDto.setAgentGender(agentGender);
        agentInfoDto.setAgentNominee(agentNominee);
        agentInfoDto.setAgentNomineeRelation(agentNomineeRelation);
        agentInfoDto.setAgentSelfie(agentSelfie);
        agentInfoDto.setAgentFirstName(agentFirstName);
        agentInfoDto.setAgentLastName(agentLastName);
        agentInfoDto.setAgentWalletPin(agentWalletPin);
        agentInfoDto.setAgentOccupation(agentOccupation);
        agentInfoDto.setAgentOperatorName(agentOperatorName);
        agentInfoDto.setAgentTradeLicenseNumber(agentTradeLicenseNumber);
        agentInfoDto.setAgentTinNumber(agentTinNumber);

        // populate NID dto from request
        AgentNidInfoDto agentNidInfoDto = new AgentNidInfoDto();
        agentNidInfoDto.setAgentNidDob(nidDateOfBirth);
        agentNidInfoDto.setAgentNidNameEn(nidNameEnglish);
        agentNidInfoDto.setAgentNidNameBn(nidNameBangla);
        agentNidInfoDto.setAgentNidNumber(nidNo);
        agentNidInfoDto.setAgentNidFatherName(nidFatherName);
        agentNidInfoDto.setAgentNidBackImageName(nidBackImageName);
        agentNidInfoDto.setAgentNidFrontImageName(nidFrontImageName);
        agentNidInfoDto.setAgentNidMotherName(nidMotherName);
        agentNidInfoDto.setAgentNidSpouseName(nidSpouseName);
        agentNidInfoDto.setAgentNidPresentAddress(nidPresentAddress);
        agentInfoDto.setAgentNidInfoDto(agentNidInfoDto);
        // populate shop dto from request
        AgentShopInfoDto agentShopInfoDto = new AgentShopInfoDto();
        agentShopInfoDto.setShopName(shopName);
        agentShopInfoDto.setDistrict(district);
        agentShopInfoDto.setDivision(division);
        agentShopInfoDto.setUpazila(upazila);
        agentShopInfoDto.setUnion(union);
        agentShopInfoDto.setShopType(shopType);
        agentShopInfoDto.setAgentShopImage(agentShopImage);
        agentInfoDto.setAgentShopInfo(agentShopInfoDto);

        return userRegistrationService.registerAgent(agentInfoDto, null);
    }

    @ResponseBody
    @PostMapping(path = "/user_basic_registration", produces = "application/json")
    public CommonResponse userBasicRegistration(@Valid @RequestBody UserRegistrationRequest userRegistrationRequest, HttpServletRequest httpServletRequest, Locale locale) {
        return userRegistrationService.userBasicRegister(userRegistrationRequest,httpServletRequest.getHeader("msisdn"), locale);
    }

    @PostMapping(path = "/ekyc_verify")
    public KYCRegistrationResponse ekycVerify(@RequestParam("nid_no") String nid_no, @RequestParam("dob") String dob, @RequestParam("applicant_name_ben") String applicant_name_ben, @RequestParam("applicant_name_eng") String applicant_name_eng, @RequestParam("father_name") String father_name, @RequestParam("mother_name") String mother_name, @RequestParam("spouse_name") String spouse_name, @RequestParam("pres_address") String pres_address, @RequestParam("perm_address") String perm_address, @RequestParam("id_front_name") String id_front_name, @RequestParam("id_back_name") String id_back_name, @RequestParam("gender") String gender, @RequestParam("profession") String profession, @RequestParam("nominee") String nominee, @RequestParam("nominee_relation") String nominee_relation, @RequestParam("mobile_number") String mobile_number, @RequestParam("applicant_photo") MultipartFile applicant_photo, Locale locale) {
        return kycService.kycRegister(nid_no, dob, applicant_name_ben, applicant_name_eng, father_name, mother_name, spouse_name, pres_address, perm_address, id_front_name, id_back_name, gender, profession, nominee, nominee_relation, mobile_number, applicant_photo, locale);
    }


}
