package com.mfs.agent_middleware.controller;

import javax.validation.Valid;

import com.mfs.agent_middleware.dto.CommonResponse;
import com.mfs.agent_middleware.exception.ExcelUploadException;
import com.mfs.agent_middleware.service.ErrorMessageUploader;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
public class MessageUploadController {

    @Autowired
    ErrorMessageUploader errorMessageUploader;

    @PostMapping(path = "/uploadErrorMessage")
    public CommonResponse uploadFile(@RequestParam("file") @Valid MultipartFile file){
        if(!file.isEmpty()){
            return errorMessageUploader.uploadFile(file);
        }else {
            throw new ExcelUploadException("Please upload a valid file");
        }

    }
}
