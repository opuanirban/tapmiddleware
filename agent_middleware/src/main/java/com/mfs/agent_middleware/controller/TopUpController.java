package com.mfs.agent_middleware.controller;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.mfs.agent_middleware.dto.CommonResponse;
import com.mfs.agent_middleware.dto.TopUpRequest;
import com.mfs.agent_middleware.service.TopUpService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
public class TopUpController {

    @Autowired
    TopUpService topUpService;

    @ResponseBody
    @PostMapping(path = "/top_up", produces = "application/json")
    public CommonResponse topup(@Valid @RequestBody TopUpRequest topUpRequest, HttpServletRequest httpServletRequest, Locale locale) throws Exception {
        long requestTime = System.currentTimeMillis();
        log.info("Recharge request received: {}", topUpRequest);
        CommonResponse commonResponse = topUpService.sendTopUp(topUpRequest,httpServletRequest.getHeader("msisdn"),locale);
        long elapsedTime = System.currentTimeMillis() - requestTime;
        log.info("Recharge response: {} with elapsed time: {}", commonResponse, elapsedTime);
        return commonResponse;
    }
}
