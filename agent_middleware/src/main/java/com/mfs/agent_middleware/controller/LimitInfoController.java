package com.mfs.agent_middleware.controller;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import com.mfs.agent_middleware.dto.LimitInfoResponse;
import com.mfs.agent_middleware.service.LimitInfoService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LimitInfoController {

    @Autowired
    LimitInfoService limitInfoService;

    @ResponseBody
    @PostMapping(path = "/limit_info", produces = "application/json")
    public List<LimitInfoResponse> limitInfo(HttpServletRequest httpServletRequest, Locale locale) {
        return limitInfoService.fetchLimit(httpServletRequest.getHeader("msisdn"), locale);
    }
}
