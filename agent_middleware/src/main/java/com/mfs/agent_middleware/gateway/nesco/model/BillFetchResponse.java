package com.mfs.agent_middleware.gateway.nesco.model;

import java.util.HashMap;
import java.util.List;

import com.mfs.agent_middleware.gateway.model.BaseResponse;

import lombok.Data;

@Data
public class BillFetchResponse {
    private BaseResponse response;
    private String billAmount;
    private List<HashMap<String, Object>> tableData;
    private HashMap<String, Object> payload;
}
