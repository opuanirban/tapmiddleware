package com.mfs.agent_middleware.gateway.model;

import org.springframework.web.multipart.MultipartFile;

import lombok.Data;

@Data
public class AgentShopInfoDto {
    private String shopName;
    private String shopType;
    private String division;
    private String district;
    private String upazila;
    private String union;
    private MultipartFile agentShopImage;
}
