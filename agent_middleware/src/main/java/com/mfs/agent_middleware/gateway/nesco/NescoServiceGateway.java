package com.mfs.agent_middleware.gateway.nesco;

import java.time.Duration;

import com.mfs.agent_middleware.exception.ApiRequestException;
import com.mfs.agent_middleware.gateway.nesco.model.BillFetchResponse;
import com.mfs.agent_middleware.gateway.nesco.model.BillPreviewOutput;
import com.mfs.agent_middleware.gateway.nesco.model.BillPreviewRestInput;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class NescoServiceGateway {
    @Value("${biller.nesco.url}")
    private String nescoUrl;

    private final WebClient webClient;

    public NescoServiceGateway(WebClient.Builder webClientBuilder) {
        this.webClient = webClientBuilder.build();
    }

    public BillPreviewOutput summaryPrepaidBillPreview(BillPreviewRestInput input){
        log.debug("request: {}", input);
        return webClient.post()
                .uri(nescoUrl + "/billing/nesco/prepaid/preview")
                .body(BodyInserters.fromValue(input))
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, clientResponse -> {
                    log.error("error in response: {}", clientResponse);
                    throw new ApiRequestException("Bill details request failed");
                })
                .onStatus(HttpStatus::is5xxServerError, clientResponse -> {
                    log.error("error in response: {}", clientResponse);
                    throw new ApiRequestException("Bill details request failed");
                })
                .bodyToMono(BillPreviewOutput.class)
                .timeout(Duration.ofMillis(20000))
                .onTerminateDetach()
                .block();       
    }

    public BillFetchResponse fetchPostpaidBillDetails(BillPreviewRestInput input){
        log.debug("request: {}", input);
        return webClient.post()
                .uri(nescoUrl + "/billing/nesco/postpaid/fetch")
                .body(BodyInserters.fromValue(input))
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, clientResponse -> {
                    log.error("error in response: {}", clientResponse);
                    throw new ApiRequestException("Bill details request failed");
                })
                .onStatus(HttpStatus::is5xxServerError, clientResponse -> {
                    log.error("error in response: {}", clientResponse);
                    throw new ApiRequestException("Bill details request failed");
                })
                .bodyToMono(BillFetchResponse.class)
                .timeout(Duration.ofMillis(20000))
                .onTerminateDetach()
                .block();       
    }

}
