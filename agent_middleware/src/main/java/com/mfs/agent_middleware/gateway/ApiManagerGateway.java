package com.mfs.agent_middleware.gateway;

import java.lang.reflect.Type;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeoutException;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.mfs.agent_middleware.config.ApplicationProperties;
import com.mfs.agent_middleware.dao.RedisDao;
import com.mfs.agent_middleware.dto.*;
import com.mfs.agent_middleware.enumeration.ContextUrl;
import com.mfs.agent_middleware.exception.ApiManagerRequestException;
import com.mfs.agent_middleware.exception.ApiRequestException;
import com.mfs.agent_middleware.service.MessageRetrieverService;
import com.mfs.agent_middleware.util.CommonConstant;
import com.mfs.agent_middleware.util.TBLResponsePlaceHolder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ApiManagerGateway {

    @Autowired
    RestTemplate restTemplate;
    @Value("${api_manager_app.url}")
    private String apiManagerUrl;

    @Value("${api_manager_transaction.url}")
    private String apiManagerTransactionUrl;

    @Value("${api_encKey}")
    private String encKey;
    @Value("${api.auth_header}")
    private String authHeader;
    @Value("${api_manager_auth_code}")
    private String apiManagerAuthCode;

    public static final String SHORT_CODE = "app";
    public static final String TOPUP_URL_CONTEXT = "/RechargeGateway";

    @Autowired
    RedisDao redisDao;

    @Autowired
    ApplicationProperties applicationProperties;

    @Autowired
    MessageRetrieverService messageRetrieverService;

    private final WebClient webClient;

    public ApiManagerGateway(WebClient.Builder webClientBuilder) {
        this.webClient = webClientBuilder.build();
    }

    public String apiManagerRequestMethod(ApiManagerRequest apiManagerRequest, Locale locale, Boolean isTransaction) {

        String url = new StringBuilder(isTransaction ? apiManagerTransactionUrl: apiManagerUrl)
                .append(apiManagerRequest.getContext())
                .append("?msgId=").append(apiManagerRequest.getMsgId())
                .append("&userNumber=").append(apiManagerRequest.getUserNumber())
                .append("&smsText=").append(apiManagerRequest.getSmsText())
                .append("&telcoId=").append(apiManagerRequest.getTelcoId())
                .append("&shortCode=").append(apiManagerRequest.getShortCode())
                .append("&encKey=").append(encKey)
                .toString();

        log.debug("calling url: " + url);
        log.info("Calling t-cash api for " + apiManagerRequest.getUserNumber());
        String response = getResponseString(url, locale);
        log.debug("response from api manager: {}", response);

        if (response.startsWith("\"") & response.endsWith("\"")) {
            return response.substring(1, response.length() - 1);
        } else {
            return response;
        }
    }

    public Boolean validateNID(NIDValidationRequest nidValidationRequest, String msisdn, Boolean isNewRegistration, Locale locale){
        String url = new StringBuilder(apiManagerUrl).append(ContextUrl.NID_VALIDATION_KYC_NEW.getUrl()).toString();

        return webClient.post()
                .uri(url)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(nidValidationRequest))
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, clientResponse -> {
                    throw new ApiManagerRequestException(CommonConstant.HTTP_4XX_ERROR);
                })
                .onStatus(HttpStatus::is5xxServerError, clientResponse -> {
                    throw new ApiManagerRequestException(CommonConstant.HTTP_5XX_ERROR);
                })
                .bodyToMono(Boolean.class)
                .timeout(Duration.ofMillis(20000))
                .onTerminateDetach()
                .onErrorMap(throwable -> {
                    log.error("error: {}", throwable);
                    if(throwable instanceof ApiManagerRequestException) {
                        throw new ApiRequestException(throwable.getMessage());
                    } else if (throwable instanceof TimeoutException) {
                        throw new ApiRequestException(CommonConstant.TIMEOUT_ERROR);
                    } else {
                        throw new ApiRequestException(CommonConstant.COMMON_ERROR);
                    }
                })
                .block();
    }

    public String userRefresh(String msisdn) {

        String url = new StringBuilder(apiManagerUrl)
                .append(ContextUrl.USER_REFRESH.getUrl())
                .append("?userNumber=").append(CommonConstant.checkNumber(msisdn))
                .append("&encKey=").append(encKey).toString();

        String response = getResponseString(url, Locale.getDefault());
        if (response.startsWith("\"") & response.endsWith("\"")) {
            response = response.substring(1, response.length() - 1);
        }

        if (response.length() == 8)
            return response;
        else
            throw new ApiManagerRequestException(response);

    }
    
    public MerchantInfoResponse fetchMerchantInfoByAccountId(String merchantAccountId){
        String url = new StringBuilder(apiManagerUrl)
                .append(ContextUrl.MERCHANT_INFO_FROM_ACC_ID.getUrl()).toString();

        MerchantInfoRequest merchantInfoRequest = new MerchantInfoRequest();
        merchantInfoRequest.setAccountId(Long.parseLong(merchantAccountId));

        String merchantResponse = webClient.post()
                .uri(url)
                .header("Authorization", apiManagerAuthCode)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(merchantInfoRequest))
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, clientResponse -> {
                    throw new ApiManagerRequestException(CommonConstant.HTTP_4XX_ERROR);
                })
                .onStatus(HttpStatus::is5xxServerError, clientResponse -> {
                    throw new ApiManagerRequestException(CommonConstant.HTTP_5XX_ERROR);
                })
                .bodyToMono(String.class)
                .timeout(Duration.ofMillis(20000))
                .onTerminateDetach()
                .onErrorMap(throwable -> {
                    log.error("error: {}", throwable);
                    if(throwable instanceof ApiManagerRequestException) {
                        throw new ApiRequestException(throwable.getMessage());
                    } else if (throwable instanceof TimeoutException) {
                        throw new ApiRequestException(CommonConstant.TIMEOUT_ERROR);
                    } else {
                        throw new ApiRequestException(CommonConstant.COMMON_ERROR);
                    }
                })
                .block();

        String response = merchantResponse.replace("\\", "");
        log.debug("merchant raw response: {}", response);
        MerchantInfoResponse merchantInfoResponse;
        if (response.startsWith("\"") & response.endsWith("\"")) {
            merchantInfoResponse = new Gson().fromJson(response.substring(1, response.length() - 1), MerchantInfoResponse.class);
        } else {
            merchantInfoResponse = new Gson().fromJson(response, MerchantInfoResponse.class);
        }
        log.debug("merchant response: {}", merchantInfoResponse);
        return  merchantInfoResponse;
    }
    
    public String userLoginInfo(String msisdn, String pin) {

        String url = new StringBuilder(apiManagerUrl)
                .append(ContextUrl.USER_LOGIN.getUrl())
                .append("?userNumber=").append(CommonConstant.checkNumber(msisdn))
                .append("&pin=").append(pin).toString();

        String response = getResponseString(url, Locale.getDefault());
        if (response.startsWith("\"") & response.endsWith("\"")) {
            response = response.substring(1, response.length() - 1);
        }

        log.debug("response from api manager: " + response);
        return response;
    }

    public UserStatusInfo checkUSer(String msisdn, Locale locale) {
        String url = new StringBuilder(apiManagerUrl)
                .append(ContextUrl.USER_STATUS.getUrl())
                .append("?userNumber=").append(CommonConstant.checkNumber(msisdn))
                .append("&shortCode=").append(SHORT_CODE).toString();

        String response = getResponseString(url, locale);
        if (response.startsWith("\"") & response.endsWith("\"")) {
            response = response.substring(1, response.length() - 1);
        }

        UserStatusInfo userStatusInfo = new UserStatusInfo();
        if (response.toLowerCase().contains(TBLResponsePlaceHolder.CHECK_USER_STATUS_SUCCESS))
            userStatusInfo.setUserStatus(true);
        else
            userStatusInfo.setUserStatus(false);

        return userStatusInfo;
    }

    private String getResponseString(String url, Locale locale) {
        log.debug("url requested: " + url);

        return webClient.post()
                .uri(url)
                .header(CommonConstant.TBL_API_AUTH_HEADER, authHeader)
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, clientResponse -> {
                    throw new ApiManagerRequestException(CommonConstant.HTTP_4XX_ERROR);
                })
                .onStatus(HttpStatus::is5xxServerError, clientResponse -> {
                    throw new ApiManagerRequestException(CommonConstant.HTTP_5XX_ERROR);
                })
                .bodyToMono(String.class)
                .timeout(Duration.ofMillis(30000))
                .onTerminateDetach()
                .onErrorMap(throwable -> {
                    log.error("error: {}", throwable);
                    if(throwable instanceof ApiManagerRequestException) {
                        throw new ApiRequestException(throwable.getMessage());
                    } else if (throwable instanceof TimeoutException) {
                        throw new ApiRequestException(CommonConstant.TIMEOUT_ERROR);
                    } else {
                        throw new ApiRequestException(CommonConstant.COMMON_ERROR);
                    }
                })
                .block();

    }

    public String getList(String fileName, Locale locale) {

        String url = new StringBuilder(apiManagerUrl)
                .append(ContextUrl.DATA_LIST_URL.getUrl())
                .append("?fileName=").append(fileName).toString();
        log.debug("request url: " + url);

        return getBill(locale, url);
    }

    public String getNidServiceList(Locale locale) {

        String url = new StringBuilder(apiManagerUrl)
                .append(ContextUrl.NID_SERVICE_LIST.getUrl()).toString();
        log.debug("request url: " + url);

        return getBill(locale, url);
    }

    private String getBill(Locale locale, String url) {
        return webClient.get()
                .uri(url)
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, clientResponse -> {
                    throw new ApiManagerRequestException(CommonConstant.HTTP_4XX_ERROR);
                })
                .onStatus(HttpStatus::is5xxServerError, clientResponse -> {
                    throw new ApiManagerRequestException(CommonConstant.HTTP_5XX_ERROR);
                })
                .bodyToMono(String.class)
                .timeout(Duration.ofMillis(30000))
                .onTerminateDetach()
                .onErrorMap(throwable -> {
                    log.error("error: {}", throwable);
                    if(throwable instanceof ApiManagerRequestException) {
                        throw new ApiRequestException(throwable.getMessage());
                    } else if (throwable instanceof TimeoutException) {
                        throw new ApiRequestException(CommonConstant.TIMEOUT_ERROR);
                    } else {
                        throw new ApiRequestException(CommonConstant.COMMON_ERROR);
                    }
                })
                .block();
    }

    public String getUtilityDetails(String fileName, String code, Locale locale) {

        String url = new StringBuilder(apiManagerUrl)
                .append(ContextUrl.DATA_LIST_URL.getUrl())
                .append("?fileName=").append(fileName)
                .append("&code=").append(code).toString();
        System.out.println(url);
        log.info("Calling t-cash api for Utility Details");
        return getBill(locale, url);
    }

    public String getUtility(String fileName, Locale locale) {

        String url = new StringBuilder(apiManagerUrl)
                .append(ContextUrl.BILL_LIST.getUrl())
                .append("?fileName=").append(fileName).toString();
        System.out.println(url);

        log.info("Calling t-cash api for utility list");
        return getBill(locale, url);
    }

    public String getBillAmount(String billNo, Locale locale) {
        String url = new StringBuilder(apiManagerUrl)
                .append(ContextUrl.DESCO_BILL.getUrl())
                .append("?BillNo=").append(billNo).toString();

        return webClient.post()
                .uri(url)
                .header(CommonConstant.TBL_API_AUTH_HEADER, authHeader)
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, clientResponse -> {
                    throw new ApiManagerRequestException(CommonConstant.HTTP_4XX_ERROR);
                })
                .onStatus(HttpStatus::is5xxServerError, clientResponse -> {
                    throw new ApiManagerRequestException(CommonConstant.HTTP_5XX_ERROR);
                })
                .bodyToMono(String.class)
                .timeout(Duration.ofMillis(30000))
                .onTerminateDetach()
                .onErrorMap(throwable -> {
                    log.error("error: {}", throwable);
                    if(throwable instanceof ApiManagerRequestException) {
                        throw new ApiRequestException(throwable.getMessage());
                    } else if (throwable instanceof TimeoutException) {
                        throw new ApiRequestException(CommonConstant.TIMEOUT_ERROR);
                    } else {
                        throw new ApiRequestException(CommonConstant.COMMON_ERROR);
                    }
                })
                .block();
    }

    public String getDescoBillPrepaid(String meterNo, String amount, Locale locale) {
        String url = new StringBuilder(apiManagerUrl)
                .append(ContextUrl.DESCO_BILL_PREPAID.getUrl())
                .append("?meterNo=").append(meterNo)
                .append("&amount=").append(amount).toString();

        String response = webClient.post()
                .uri(url)
                .header(CommonConstant.TBL_API_AUTH_HEADER, authHeader)
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, clientResponse -> {
                    throw new ApiManagerRequestException(CommonConstant.HTTP_4XX_ERROR);
                })
                .onStatus(HttpStatus::is5xxServerError, clientResponse -> {
                    throw new ApiManagerRequestException(CommonConstant.HTTP_5XX_ERROR);
                })
                .bodyToMono(String.class)
                .timeout(Duration.ofMillis(30000))
                .onTerminateDetach()
                .onErrorMap(throwable -> {
                    log.error("error: {}", throwable);
                    if(throwable instanceof ApiManagerRequestException) {
                        throw new ApiRequestException(throwable.getMessage());
                    } else if (throwable instanceof TimeoutException) {
                        throw new ApiRequestException(CommonConstant.TIMEOUT_ERROR);
                    } else {
                        throw new ApiRequestException(CommonConstant.COMMON_ERROR);
                    }
                })
                .block();

        if (response.startsWith("\"") & response.endsWith("\"")) {
            return response.substring(1, response.length() - 1);
        } else {
            return response;
        }
    }

    public String getDPDCBill(DPDCBillInfo dpdcBillInfo, Locale locale) {
        String url = new StringBuilder(apiManagerUrl)
                .append(ContextUrl.DPDC_BILL.getUrl())
                .append("?LocationCode=").append(dpdcBillInfo.getLocationCode())
                .append("&BillMonth=").append(dpdcBillInfo.getBillMonth())
                .append("&accountNumber=").append(dpdcBillInfo.getAccountNumber()).toString();

        return webClient.post()
                .uri(url)
                .header(CommonConstant.TBL_API_AUTH_HEADER, authHeader)
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, clientResponse -> {
                    throw new ApiManagerRequestException(CommonConstant.HTTP_4XX_ERROR);
                })
                .onStatus(HttpStatus::is5xxServerError, clientResponse -> {
                    throw new ApiManagerRequestException(CommonConstant.HTTP_5XX_ERROR);
                })
                .bodyToMono(String.class)
                .timeout(Duration.ofMillis(30000))
                .onTerminateDetach()
                .onErrorMap(throwable -> {
                    log.error("error: {}", throwable);
                    if(throwable instanceof ApiManagerRequestException) {
                        throw new ApiRequestException(throwable.getMessage());
                    } else if (throwable instanceof TimeoutException) {
                        throw new ApiRequestException(CommonConstant.TIMEOUT_ERROR);
                    } else {
                        throw new ApiRequestException(CommonConstant.COMMON_ERROR);
                    }
                })
                .block();
    }

    public String userRegistration(RegistrationParams registrationParams, Locale locale) {
        String response =  webClient.post()
                .uri(apiManagerUrl + ContextUrl.KYC_USER_REGISTRATION.getUrl())
                .body(BodyInserters.fromValue(registrationParams))
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, clientResponse -> {
                    throw new ApiManagerRequestException(CommonConstant.HTTP_4XX_ERROR);
                })
                .onStatus(HttpStatus::is5xxServerError, clientResponse -> {
                    throw new ApiManagerRequestException(CommonConstant.HTTP_5XX_ERROR);
                })
                .bodyToMono(String.class)
                .timeout(Duration.ofMillis(30000))
                .onTerminateDetach()
                .onErrorMap(throwable -> {
                    log.error("error: {}", throwable);
                    if(throwable instanceof ApiManagerRequestException) {
                        throw new ApiRequestException(throwable.getMessage());
                    } else if (throwable instanceof TimeoutException) {
                        throw new ApiRequestException(CommonConstant.TIMEOUT_ERROR);
                    } else {
                        throw new ApiRequestException(CommonConstant.COMMON_ERROR);
                    }
                })
                .block();

        if (response.startsWith("\"") & response.endsWith("\"")) {
            return response.substring(1, response.length() - 1);
        } else {
            return response;
        }
    }

    public String feeAndCharges(FeesAndChargesModel feesAndChargesModel, Locale locale) {
        String response =  webClient.post()
                .uri(apiManagerUrl + ContextUrl.FEE_CHARGES.getUrl())
                .body(BodyInserters.fromValue(feesAndChargesModel))
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, clientResponse -> {
                    throw new ApiManagerRequestException(CommonConstant.HTTP_4XX_ERROR);
                })
                .onStatus(HttpStatus::is5xxServerError, clientResponse -> {
                    throw new ApiManagerRequestException(CommonConstant.HTTP_5XX_ERROR);
                })
                .bodyToMono(String.class)
                .timeout(Duration.ofMillis(30000))
                .onTerminateDetach()
                .onErrorMap(throwable -> {
                    log.error("error: {}", throwable);
                    if(throwable instanceof ApiManagerRequestException) {
                        throw new ApiRequestException(throwable.getMessage());
                    } else if (throwable instanceof TimeoutException) {
                        throw new ApiRequestException(CommonConstant.TIMEOUT_ERROR);
                    } else {
                        throw new ApiRequestException(CommonConstant.COMMON_ERROR);
                    }
                })
                .block();

        if (response.startsWith("\"") & response.endsWith("\"")) {
            return response.substring(1, response.length() - 1);
        } else {
            return response;
        }
    }

    public String registrationStatusUpdate(RegistrationParams tblRegistrationStatus, Locale locale) {
        String response =  webClient.post()
                .uri(apiManagerUrl + ContextUrl.KYC_INFO_UPDATE.getUrl())
                .header(CommonConstant.TBL_API_AUTH_HEADER, authHeader)
                .body(BodyInserters.fromValue(tblRegistrationStatus))
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, clientResponse -> {
                    throw new ApiManagerRequestException(CommonConstant.HTTP_4XX_ERROR);
                })
                .onStatus(HttpStatus::is5xxServerError, clientResponse -> {
                    throw new ApiManagerRequestException(CommonConstant.HTTP_5XX_ERROR);
                })
                .bodyToMono(String.class)
                .timeout(Duration.ofMillis(30000))
                .onTerminateDetach()
                .onErrorMap(throwable -> {
                    log.error("error: {}", throwable);
                    if(throwable instanceof ApiManagerRequestException) {
                        throw new ApiRequestException(throwable.getMessage());
                    } else if (throwable instanceof TimeoutException) {
                        throw new ApiRequestException(CommonConstant.TIMEOUT_ERROR);
                    } else {
                        throw new ApiRequestException(CommonConstant.COMMON_ERROR);
                    }
                })
                .block();

        if (response.startsWith("\"") & response.endsWith("\"")) {
            return response.substring(1, response.length() - 1);
        } else {
            return response;
        }
    }

    public List<MiniStatementResponseEntity> miniStatement(String msisdn, Locale locale) {

        String url = new StringBuilder(apiManagerUrl)
                .append(ContextUrl.USER_STATEMENT.getUrl())
                .append("?userNumber=").append(CommonConstant.checkNumber(msisdn))
                .append("&encKey=").append(encKey).toString();

        String response = getResponseString(url, locale).replace("\\", "");
        log.debug(response);
        response = response.replace("\\", "");
        Type statementList = new TypeToken<ArrayList<MiniStatementResponseEntity>>() {
        }.getType();
        List<MiniStatementResponseEntity> statementModels = new ArrayList<MiniStatementResponseEntity>();
        if (response.startsWith("\"") & response.endsWith("\"")) {
            statementModels = new Gson().fromJson(response.substring(1, response.length() - 1), statementList);

        } else {
            statementModels = new Gson().fromJson(response, statementList);

        }
        return statementModels;
    }

    public List<TransactionTypeParams> getTransactionType(Locale locale) {

        String url = new StringBuilder(apiManagerUrl)
                .append(ContextUrl.TRANSACTION_TYPES.getUrl()).toString();

        String response = getResponseString(url, locale).replace("\\", "");
        log.debug(response);
        response = response.replace("\\", "");
        Type transactionTypes = new TypeToken<ArrayList<TransactionTypeParams>>() {
        }.getType();
        List<TransactionTypeParams> typeParams = new ArrayList<TransactionTypeParams>();
        if (response.startsWith("\"") & response.endsWith("\"")) {
            typeParams = new Gson().fromJson(response.substring(1, response.length() - 1), transactionTypes);

        } else {
            typeParams = new Gson().fromJson(response, transactionTypes);

        }
        return typeParams;
    }

    public List<LimitInfoResponse> limitInfo(String msisdn, Locale locale) {

        String url = new StringBuilder(apiManagerUrl)
                .append(ContextUrl.LIMIT_INFO.getUrl())
                .append("?userNumber=").append(CommonConstant.checkNumber(msisdn)).toString();

        String response = getResponseString(url, locale).replace("\\", "").replaceAll("u000du000a", "");
        log.debug(response);
        Type infoList = new TypeToken<ArrayList<LimitInfoResponse>>() {
        }.getType();
        List<LimitInfoResponse> infoResponses = new ArrayList<LimitInfoResponse>();
        if (response.startsWith("\"") & response.endsWith("\"")) {
            infoResponses = new Gson().fromJson(response.substring(1, response.length() - 1), infoList);

        } else {
            infoResponses = new Gson().fromJson(response, infoList);

        }
        return infoResponses;
    }

    public UserTypeResponse userType(String msisdn, Locale locale) {

        String url = new StringBuilder(apiManagerUrl)
                .append(ContextUrl.USER_TYPE.getUrl())
                .append("?userNumber=").append(CommonConstant.checkNumber(msisdn)).toString();

        String response = getBill(locale, url);
        log.debug(response);

        UserTypeResponse userTypeResponse = new UserTypeResponse();
        if (response.startsWith("\"") & response.endsWith("\"")) {
            userTypeResponse = new Gson().fromJson(response.substring(1, response.length() - 1), UserTypeResponse.class);
        } else {
            userTypeResponse = new Gson().fromJson(response, UserTypeResponse.class);
        }
        return userTypeResponse;
    }

    public String balanceCheck(String msisdn, Locale locale) {

        String url = new StringBuilder(apiManagerUrl)
                .append(ContextUrl.BALANCE_CHECK.getUrl())
                .append("?userNumber=").append(CommonConstant.checkNumber(msisdn))
                .append("&encKey=").append(encKey).toString();

        String response = getResponseString(url, locale);
        if (response.startsWith("\"") & response.endsWith("\"")) {
            return response.substring(1, response.length() - 1);
        } else {
            return response;
        }
    }

    public String updatePin(String msisdn, String oldPin, String newPin, Locale locale) {
        String url = new StringBuilder(apiManagerUrl)
                .append(ContextUrl.UPDATE_PIN.getUrl())
                .append("?userNumber=").append(CommonConstant.checkNumber(msisdn))
                .append("&oldPin=").append(oldPin)
                .append("&newPin=").append(newPin)
                .append("&encKey=").append(encKey).toString();

        String response = getResponseString(url, locale);
        if (response.startsWith("\"") & response.endsWith("\"")) {
            return response.substring(1, response.length() - 1);
        } else {
            return response;
        }
    }

    public TopUpResponse requestTopUp(TopUpRequest2 topUpRequest, Locale locale) {
        String responseStr = webClient.post()
                .uri(apiManagerUrl + TOPUP_URL_CONTEXT)
                .header(CommonConstant.TBL_API_AUTH_HEADER, "MWU0YmUyM2MtMTVjNi00MDg1LWIwYzAtMDFjZjczYzhmNGZi")
                .body(BodyInserters.fromValue(topUpRequest))
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, clientResponse -> {
                    log.error("error response: {}", clientResponse);
                    throw new ApiManagerRequestException(CommonConstant.HTTP_4XX_ERROR);
                })
                .onStatus(HttpStatus::is5xxServerError, clientResponse -> {
                    log.error("error response: {}", clientResponse);
                    throw new ApiManagerRequestException(CommonConstant.HTTP_5XX_ERROR);
                })
                .bodyToMono(String.class)
                .timeout(Duration.ofMillis(30000))
                .onTerminateDetach()
                .onErrorMap(throwable -> {
                    if(throwable instanceof ApiManagerRequestException) {
                        throw new ApiRequestException(throwable.getMessage());
                    } else if (throwable instanceof TimeoutException) {
                        throw new ApiRequestException(CommonConstant.TIMEOUT_ERROR);
                    } else {
                        throw new ApiRequestException(CommonConstant.COMMON_ERROR);
                    }
                })
                .block();

        responseStr = responseStr.replace("\\", "");
        if (responseStr.startsWith("\"") & responseStr.endsWith("\"")) {
            responseStr = responseStr.substring(1, responseStr.length() - 1);
        }

        log.info("Recharge core response: {}",responseStr);
        return new Gson().fromJson(responseStr, TopUpResponse.class);
    }

    public String getUserRegistrationStatus(String mobile_number,Locale locale){

        mobile_number = CommonConstant.checkNumber(mobile_number);
        String responseStr = webClient.
                get().
                uri(apiManagerUrl+"/GetUserRegistrationStatus?userNumber="+CommonConstant.checkNumber(mobile_number)).
                header(CommonConstant.TBL_API_AUTH_HEADER,"MWU0YmUxM2MtMTVjNi00MDk1LWIwYzAtMDFjZjczYzhmNGZh")
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, clientResponse -> {
                    log.error("error response: {}", clientResponse);
                    throw new ApiManagerRequestException(CommonConstant.HTTP_4XX_ERROR);
                })
                .onStatus(HttpStatus::is5xxServerError, clientResponse -> {
                    log.error("error response: {}", clientResponse);
                    throw new ApiManagerRequestException(CommonConstant.HTTP_5XX_ERROR);
                })
                .bodyToMono(String.class)
                .timeout(Duration.ofMillis(30000))
                .onTerminateDetach()
                .onErrorMap(throwable -> {
                    if(throwable instanceof ApiManagerRequestException) {
                        throw new ApiRequestException(throwable.getMessage());
                    } else if (throwable instanceof TimeoutException) {
                        throw new ApiRequestException(CommonConstant.TIMEOUT_ERROR);
                    } else {

                        throw new ApiRequestException(CommonConstant.COMMON_ERROR);
//                        throw new ApiRequestException(throwable.getMessage());
                    }
                })
                .block();

        responseStr = responseStr.replace("\\", "");
        if (responseStr.startsWith("\"") & responseStr.endsWith("\"")) {
            responseStr = responseStr.substring(1, responseStr.length() - 1);
        }

        log.info(responseStr);
        return  responseStr;
    }

}
