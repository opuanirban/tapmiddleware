package com.mfs.agent_middleware.gateway.nesco.model;

import lombok.Data;

@Data
public class BillPaymentInput {
    private String accountNumber;
    private String billerCode;
    private String paymentChannel;
    private String pin;
    private String fee;
    private String notificationNumber;
    private String key1;
    private String key2;
    private String key3;
    private String key4;
    private String key5;
    private String key6;
    private String key7;
}
