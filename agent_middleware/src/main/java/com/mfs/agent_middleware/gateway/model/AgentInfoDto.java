package com.mfs.agent_middleware.gateway.model;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
public class AgentInfoDto {
    private String agentNumber;
    private String agentGender;
    private String agentNominee;
    private String agentNomineeRelation;
    private MultipartFile tradeLicense;
    private MultipartFile tinCertificate;
    private AgentShopInfoDto agentShopInfo;
    private AgentNidInfoDto agentNidInfoDto;
    private MultipartFile agentSelfie;
    private String agentFirstName;
    private String agentLastName;
    private String agentWalletPin;
    private String agentOccupation;
    private String agentOperatorName;
    private String agentTradeLicenseNumber;
    private String agentTinNumber;
}