package com.mfs.agent_middleware.gateway;

import java.time.Duration;
import java.util.Locale;
import java.util.concurrent.TimeoutException;

import com.google.gson.Gson;
import com.mfs.agent_middleware.config.ApplicationProperties;
import com.mfs.agent_middleware.config.RequestCorrelation;
import com.mfs.agent_middleware.dto.BaseResponse;
import com.mfs.agent_middleware.dto.GenerateOTPRequest;
import com.mfs.agent_middleware.dto.GenerateOTPResponse;
import com.mfs.agent_middleware.dto.ValidateOTPRequest;
import com.mfs.agent_middleware.exception.ApiRequestException;
import com.mfs.agent_middleware.exception.OTPValidationRequestException;
import com.mfs.agent_middleware.service.MessageRetrieverService;
import com.mfs.agent_middleware.service.OtpBlacklistService;
import com.mfs.agent_middleware.util.CommonConstant;
import com.mfs.agent_middleware.util.ErrorKeyConstants;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class OTPserviceGateway {

    @Autowired
    RestTemplate restTemplate;
    @Autowired
    ApplicationProperties applicationProperties;
    @Value("${otp-service.username}")
    private String otpServiceUserName;
    @Value("${otp-service.password}")
    private String otpServicePassword;

    @Autowired
    MessageRetrieverService messageRetrieverService;

    @Autowired
    OtpBlacklistService otpBlacklistService;

    private final WebClient webClient;

    public OTPserviceGateway(WebClient.Builder webClientBuilder) {
        this.webClient = webClientBuilder.build();
    }

    public GenerateOTPResponse getOtp(GenerateOTPRequest generateOTPRequest, Locale locale) {
        final String OTPUrl = applicationProperties.getOtpServiceUrl() + CommonConstant.OTP_GENERATION_CONTEXT;
        return webClient.post()
                .uri(OTPUrl)
                .header(RequestCorrelation.CORRELATION_ID_HEADER_NAME, RequestCorrelation.getId())
                .header(CommonConstant.CLIENT_USERNAME_HEADER, otpServiceUserName)
                .header(CommonConstant.CLIENT_PASSWORD_HEADER, otpServicePassword)
                .body(BodyInserters.fromValue(generateOTPRequest))
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, clientResponse -> {
                    throw new OTPValidationRequestException(CommonConstant.HTTP_4XX_ERROR);
                })
                .onStatus(HttpStatus::is5xxServerError, clientResponse -> {
                    throw new OTPValidationRequestException(CommonConstant.HTTP_5XX_ERROR);
                })
                .bodyToMono(GenerateOTPResponse.class)
                .timeout(Duration.ofMillis(20000))
                .onTerminateDetach()
                .onErrorMap(throwable -> {
                    log.error("error: {}", throwable);
                    if(throwable instanceof OTPValidationRequestException) {
                        throw new ApiRequestException(throwable.getMessage());
                    } else if (throwable instanceof TimeoutException) {
                        throw new ApiRequestException(CommonConstant.TIMEOUT_ERROR);
                    } else {
                        throw new ApiRequestException(CommonConstant.COMMON_ERROR);
                    }
                })
                .block();
    }

    public BaseResponse validateOTP(ValidateOTPRequest validateOTPRequest, Locale locale) {
        if (otpBlacklistService.checkMsisdn(validateOTPRequest.getReceiverInfo())) {
            throw new OTPValidationRequestException("You have reached your maximum retry limit, please try after sometime.");
        } else {
            final String OTPUrl = applicationProperties.getOtpServiceUrl() + CommonConstant.OTP_VALIDATION_CONTEXT;

            String response = webClient.post()
                    .uri(OTPUrl)
                    .header(RequestCorrelation.CORRELATION_ID_HEADER_NAME, RequestCorrelation.getId())
                    .header(CommonConstant.CLIENT_USERNAME_HEADER, otpServiceUserName)
                    .header(CommonConstant.CLIENT_PASSWORD_HEADER, otpServicePassword)
                    .body(BodyInserters.fromValue(validateOTPRequest))
                    .retrieve()
                    .onStatus(HttpStatus::is4xxClientError, clientResponse -> {
                        throw new OTPValidationRequestException(messageRetrieverService.getMessage(locale, ErrorKeyConstants.OTP_EXPIRED_KEY));
                    })
                    .onStatus(HttpStatus::is5xxServerError, clientResponse -> {
                        throw new OTPValidationRequestException(CommonConstant.HTTP_5XX_ERROR);
                    })
                    .bodyToMono(String.class)
                    .timeout(Duration.ofMillis(20000))
                    .onTerminateDetach()
                    .onErrorMap(throwable -> {
                        log.error("error: {}", throwable);
                        if(throwable instanceof OTPValidationRequestException) {
                            throw new ApiRequestException(throwable.getMessage());
                        } else if (throwable instanceof TimeoutException) {
                            throw new ApiRequestException(CommonConstant.TIMEOUT_ERROR);
                        } else {
                            throw new ApiRequestException(CommonConstant.COMMON_ERROR);
                        }
                    })
                    .block();

            return new Gson().fromJson(response, BaseResponse.class);
        }
    }

}


