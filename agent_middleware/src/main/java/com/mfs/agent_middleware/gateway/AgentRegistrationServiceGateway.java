package com.mfs.agent_middleware.gateway;

import com.google.gson.Gson;
import com.mfs.agent_middleware.dto.BaseResponse;
import com.mfs.agent_middleware.dto.KYCRegistrationResponse;
import com.mfs.agent_middleware.exception.GigatechException;
import com.mfs.agent_middleware.gateway.model.AgentInfoDto;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.client.MultipartBodyBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.http.HttpStatus;

import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Duration;
import java.util.concurrent.TimeoutException;

import com.mfs.agent_middleware.exception.ApiRequestException;
import com.mfs.agent_middleware.gateway.model.AgentStatusResponse;
import com.mfs.agent_middleware.util.CommonConstant;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

@Service
@Slf4j
public class AgentRegistrationServiceGateway {
    @Value("${http.url.agent-registration-service}")
    private String agentRegServiceUrl;

    private final WebClient webClient;

    public AgentRegistrationServiceGateway(WebClient.Builder webClientBuilder) {
        this.webClient = webClientBuilder.build();
    }

    public AgentStatusResponse fetchRegistrationServiceStatus(String agentNumber){
        log.debug("request: {}", agentNumber);
        return webClient.get()
                .uri(agentRegServiceUrl + "/agent/fetch-verification-status?agent_number=" + agentNumber)
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, clientResponse -> {
                    log.error("error in response: {}", clientResponse);
                    throw new ApiRequestException(CommonConstant.HTTP_4XX_ERROR);
                })
                .onStatus(HttpStatus::is5xxServerError, clientResponse -> {
                    log.error("error in response: {}", clientResponse);
                    throw new ApiRequestException(CommonConstant.HTTP_4XX_ERROR);
                })
                .bodyToMono(AgentStatusResponse.class)
                .timeout(Duration.ofMillis(20000))
                .onTerminateDetach()
                .block();       
    }

    public void agentRegistration(AgentInfoDto agentInfoDto) {
        MultipartBodyBuilder bodyBuilder = new MultipartBodyBuilder();
        bodyBuilder.part("agent_number", agentInfoDto.getAgentNumber());
        bodyBuilder.part("nid_no", agentInfoDto.getAgentNidInfoDto().getAgentNidNumber());
        bodyBuilder.part("nid_dob", agentInfoDto.getAgentNidInfoDto().getAgentNidDob());
        bodyBuilder.part("nid_name_en", agentInfoDto.getAgentNidInfoDto().getAgentNidNameEn());
        bodyBuilder.part("nid_name_bn", agentInfoDto.getAgentNidInfoDto().getAgentNidNameBn());
        bodyBuilder.part("nid_father_name", agentInfoDto.getAgentNidInfoDto().getAgentNidFatherName());
        bodyBuilder.part("nid_mother_name", agentInfoDto.getAgentNidInfoDto().getAgentNidMotherName());
        bodyBuilder.part("nid_present_address", agentInfoDto.getAgentNidInfoDto().getAgentNidPresentAddress());
        bodyBuilder.part("nid_front_image_name", agentInfoDto.getAgentNidInfoDto().getAgentNidFrontImageName());
        bodyBuilder.part("nid_back_image_name", agentInfoDto.getAgentNidInfoDto().getAgentNidBackImageName());
        bodyBuilder.part("trade_license", createTempFileResource(agentInfoDto.getTradeLicense()));
        bodyBuilder.part("tin_certificate", createTempFileResource(agentInfoDto.getTinCertificate()));
        bodyBuilder.part("agent_shop_image", createTempFileResource(agentInfoDto.getAgentShopInfo().getAgentShopImage()));
        bodyBuilder.part("agent_gender", agentInfoDto.getAgentGender());
        bodyBuilder.part("agent_nominee", agentInfoDto.getAgentNominee());
        bodyBuilder.part("agent_nominee_relation", agentInfoDto.getAgentNomineeRelation());
        bodyBuilder.part("shop_name", agentInfoDto.getAgentShopInfo().getShopName());
        bodyBuilder.part("shop_type", agentInfoDto.getAgentShopInfo().getShopType());
        bodyBuilder.part("division", agentInfoDto.getAgentShopInfo().getDivision());
        bodyBuilder.part("district", agentInfoDto.getAgentShopInfo().getDistrict());
        bodyBuilder.part("upazila", agentInfoDto.getAgentShopInfo().getUpazila());
        bodyBuilder.part("union", agentInfoDto.getAgentShopInfo().getUnion());
        bodyBuilder.part("nid_spouse_name", agentInfoDto.getAgentNidInfoDto().getAgentNidSpouseName());
        bodyBuilder.part("agent_selfie", createTempFileResource(agentInfoDto.getAgentSelfie()));
        bodyBuilder.part("agent_first_name", agentInfoDto.getAgentFirstName());
        bodyBuilder.part("agent_last_name", agentInfoDto.getAgentLastName());
        bodyBuilder.part("agent_wallet_pin", agentInfoDto.getAgentWalletPin());
        bodyBuilder.part("agent_occupation", agentInfoDto.getAgentOccupation());
        bodyBuilder.part("agent_operator_name", agentInfoDto.getAgentOperatorName());
        bodyBuilder.part("agent_trade_license_number", agentInfoDto.getAgentTradeLicenseNumber());
        bodyBuilder.part("agent_tin_number", agentInfoDto.getAgentTinNumber());

        webClient.post()
                .uri(agentRegServiceUrl + "/agent/registration")
                .body(BodyInserters.fromMultipartData(bodyBuilder.build()))
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, clientResponse ->
                        clientResponse.bodyToMono(String.class) // error body as String or other class
                            .flatMap(error -> Mono.error(new ApiRequestException(new Gson().fromJson(error, BaseResponse.class).getMessage())))
                )
                .onStatus(HttpStatus::is5xxServerError, clientResponse -> {
                    throw new GigatechException(CommonConstant.HTTP_5XX_ERROR);
                })
                .bodyToMono(KYCRegistrationResponse.class)
                .timeout(Duration.ofMillis(30000))
                .onTerminateDetach()
                .onErrorMap(throwable -> {
                    log.error("error: {}", throwable);
                    if(throwable instanceof ApiRequestException) {
                        throw new ApiRequestException(throwable.getMessage());
                    } else if (throwable instanceof TimeoutException) {
                        throw new ApiRequestException(CommonConstant.TIMEOUT_ERROR);
                    } else {
                        throw new ApiRequestException(CommonConstant.COMMON_ERROR);
                    }
                })
                .block();
    }

    private Resource createTempFileResource(MultipartFile content) {
        try {
            Path tempFile = Files.createTempFile(content.getName(), "." + FilenameUtils.getExtension(content.getOriginalFilename()));
            Files.write(tempFile, content.getBytes());
            return new FileSystemResource(tempFile.toFile());
        } catch (Exception e) {
            log.error("error: {}", e);
            throw new ApiRequestException(CommonConstant.COMMON_ERROR);
        }
    }
}
