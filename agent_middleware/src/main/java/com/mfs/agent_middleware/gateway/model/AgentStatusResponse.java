package com.mfs.agent_middleware.gateway.model;

import lombok.Data;

@Data
public class AgentStatusResponse {
    private String agentVerificationStatus;
}
