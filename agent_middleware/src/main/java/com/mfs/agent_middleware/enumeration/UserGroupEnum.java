package com.mfs.agent_middleware.enumeration;

public enum UserGroupEnum {
    GENERAL("78"),
    STUDENT("72"),
    SALARY("84");

    private String userGroup;

    UserGroupEnum(String userGroup){
        this.userGroup = userGroup;
    }

    public String getUserGroup() {
        return userGroup;
    }
}
