package com.mfs.agent_middleware.enumeration;

public enum ConnectionTypeEnum {
    Prepaid("PR"),
    Postpaid("PO");

    private String connection;

    ConnectionTypeEnum(String con) {
        this.connection = con;
    }

    public String getConnection() {
        return connection;
    }
}
