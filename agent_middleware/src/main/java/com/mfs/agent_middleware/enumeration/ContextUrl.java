package com.mfs.agent_middleware.enumeration;


public enum ContextUrl {
    USER_REGISTRATION("/UserRegistration"),
    KYC_USER_REGISTRATION("/UserPOST"),
    KYC_INFO_UPDATE("/EkycUpdate"),
    BALANCE_CHECK("/GetUserBalance"),
    UPDATE_PIN("/UpdatePin"),
    USER_STATEMENT("/UserStatement"),
    API_MANAGER_CONTEXT("/ProcessRequest"),
    USER_REFRESH("/UserRefresh"),
    USER_LOGIN("/UserLogin"),
    USER_CBS_AC_INFO("/GetCoreBankAccountInfo"),
    DATA_LIST_URL("/FileLoadRequest"),
    BILL_LIST("/GetBillTypes"),
    USER_STATUS("/GetUserStatus"),
    NID_VALIDATION_KYC_UPDATE("/ValidateNIDIneKYCUpdate"),
    NID_VALIDATION_KYC_NEW("/ValidateNID"),
    NID_UPLOAD("/nid-upload"),
    KYC_REGISTRATION("/agent/customer-registration/"),
    KYC_VERIFICATION("/agent/get-verification-status/"),
    DPDC_BILL("/GetDPDCBillInfo"),
    DESCO_BILL("/GetDescoBillInfo"),
    DESCO_BILL_PREPAID("/GetDescoPrepaidBreakdownDetail"),
    LIMIT_INFO("/GetLimitInfo"),
    FEE_CHARGES("/GetFeesAndCharges"),
    USER_TYPE("/GetUserType"),
    NID_SERVICE_LIST("/GetNIDServiceList"),
    TRANSACTION_TYPES("/GetAllTransactionTypes"),
    MERCHANT_INFO_FROM_ACC_ID("/ReadQR"),
    GET_USER_REGISTRATION_STATUS("/GetUserRegistrationStatus"),
    GET_KYC_TBL("/UserEkyc");
    private String url;

    ContextUrl(String envUrl) {
        this.url = envUrl;
    }

    public String getUrl() {
        return url;
    }
}