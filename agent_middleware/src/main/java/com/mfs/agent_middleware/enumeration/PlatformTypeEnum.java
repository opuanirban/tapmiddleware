package com.mfs.agent_middleware.enumeration;

public enum PlatformTypeEnum {
    IOS,
    ANDROID,
    HUAWEI
}
