package com.mfs.agent_middleware.enumeration;

public enum GenderEnum {
    M("Male"),
    F("Female"),
    O("Others");

    private String genderGroup;

    GenderEnum(String genderGroup) {
        this.genderGroup = genderGroup;
    }

    public String getGenderGroup() {
        return genderGroup;
    }
}

