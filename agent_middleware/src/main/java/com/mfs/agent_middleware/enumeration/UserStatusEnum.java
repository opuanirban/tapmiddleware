package com.mfs.agent_middleware.enumeration;

public enum UserStatusEnum { 
    PRESENT, PENDING, SUCCESS, FAILED, NOTFOUND
}
