package com.mfs.agent_middleware.enumeration;

public enum MobileOperatorEnum {
    Airtel, Robi, GrameenPhone, Teletalk, Banglalink
}
