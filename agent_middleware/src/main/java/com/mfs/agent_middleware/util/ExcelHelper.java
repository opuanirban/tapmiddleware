package com.mfs.agent_middleware.util;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.mfs.agent_middleware.dto.ExcelDto;
import com.mfs.agent_middleware.exception.ExcelUploadException;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.multipart.MultipartFile;


public class ExcelHelper {
    public static String TYPE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

    public static boolean hasExcelFormat(MultipartFile file) {

        if (!TYPE.equals(file.getContentType())) {
            return false;
        }

        return true;
    }

    public static List<ExcelDto> excelToTutorials(InputStream is) {
        try {
            Workbook workbook = new XSSFWorkbook(is);

//            Sheet sheet = workbook.getSheet(SHEET);
            Sheet sheet = workbook.getSheetAt(0);
            Iterator<Row> rows = sheet.iterator();

            List<ExcelDto> readList = new ArrayList<ExcelDto>();

            int rowNumber = 0;
            while (rows.hasNext()) {
                Row currentRow = rows.next();

                // skip header
                if (rowNumber == 0) {
                    rowNumber++;
                    continue;
                }

                Iterator<Cell> cellsInRow = currentRow.iterator();

                ExcelDto excelDto = new ExcelDto();

                int cellIdx = 0;
                while (cellsInRow.hasNext()) {
                    Cell currentCell = cellsInRow.next();

                    switch (cellIdx) {
                        case 0:
                            excelDto.setKey(currentCell.getStringCellValue());
                            break;

                        case 1:
                            excelDto.setValue(currentCell.getStringCellValue());
                            break;

                        default:
                            break;
                    }

                    cellIdx++;
                }

                readList.add(excelDto);
            }

            workbook.close();

            return readList;
        } catch (Exception e) {
            throw new ExcelUploadException("fail to parse Excel file: " + e.getMessage());
        }
    }
}