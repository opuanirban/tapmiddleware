package com.mfs.agent_middleware.util;

import java.util.HashMap;
import java.util.Map;

import com.google.gson.Gson;
import com.mfs.agent_middleware.dto.BillJson;
import com.mfs.agent_middleware.dto.DataJson;
import com.mfs.agent_middleware.dto.NidServiceListParams;

public class CommonConstant {

    public static final String SUCCESS_CODE = "000";
    public static final String API_MANGER_REQUEST_FAIL = "103";
    public static final String OTP_GENERATION_FAILED = "108";
    public static final String OTP_VALIDATION_FAILED = "109";
    public static final String CRYPTOGRAPHY_OPERATION_FAILED = "110";
    public static final String USER_REGISTRATION_FAILED = "111";
    public static final String TRANSACTION_ERROR = "777";
    public static final String INVALID_TRANSACTION = "778";
    public static final String HTTP_4XX_ERROR = "Invalid request, Unable to process at this moment. Please try after sometime";
    public static final String HTTP_5XX_ERROR = "Internal processing error, Unable to process at this moment. Please try after sometime.";
    public static final String TIMEOUT_ERROR = "Request processing failed due to system delay. Please try after sometime.";
    public static final String EXCEL_ERROR = "444";
    public static final String COMMON_ERROR = "Request processing failed. Please try again";
    public static final String COMMON_ERROR_BN = "সিস্টেমের অভ্যন্তরীণ ত্রুটি ঘটেছে। অনুগ্রহপূর্বক আবার চেষ্টা করুন";
    public static final String COMMON_ERROR_CODE = "433";
    public static final String EMPTY_INPUT = "455";
    public static final String GIGATECH_EXCEPTION = "477";
    public static final String PIN_DECODE_EXCEPTION = "466";
    public static final String PIN_UPDATE_EXCEPTION = "488";
    public static final String FORCE_UPDATE_RESPONSE_MESSAGE = "A new version is currently available. Please update the application.";
    public static final String SERVICE_UNAVAILABLE_MESSAGE_EN = "System is under maintenance.";
    public static final String SERVICE_UNAVAILABLE_MESSAGE_BN = "সিস্টেম এর রক্ষণাবেক্ষণের কাজ চলছে.";

    public static final String NOT_ENOUGH_BITS = "Last unit does not have enough valid bits";
    public static final String DECODE_EXCEPTION_MESSAGE = "PIN not encoded";

    public static final String CLIENT_USERNAME_HEADER = "X-CLIENT-USERNAME";
    public static final String CLIENT_PASSWORD_HEADER = "X-CLIENT-PASSWORD";

    public static final String TBL_API_AUTH_HEADER = "Authorization";

    public static final String OTP_GENERATION_CONTEXT = "/sendOtp";
    public static final String OTP_VALIDATION_CONTEXT = "/validateOtp";
    public static final String API_MANAGER_CONTEXT = "/ProcessRequest";

    public static final String TARGET_TYPE = "TargetType";
    public static final String PROD_PROFILE = "prod";

    public static final String LOGOUT_JWT = "session#blacklist#";
    public static final String SUCCESS = "success";
    public static final String PASSED = "passed";
    public static final String FAILED = "failed";
    public static final String PENDING = "pending";
    public static final String INVALID = "invalid";
    public static final String EC_REQUESTED = "ec_requested";
    public static final String ERROR = "error";
    public static final String NOT_FOUND = "not_found";

    public static final String FORCE_UPDATE_VERSION = "agent#force#update#version";
    public static final String FORCE_UPDATE_MESSAGE="agent#force#update#message";
    public static final String FORCE_UPDATE_LINK="agent#force#update#link";
    public static final String FORCE_UPDATE_DEFAULT_LINK="";
    public static final String BLACKLIST_VERSION = "agent#blacklist#version";
    public static final String PLATFORM = "agent#platform";
    public static final String HIDE_FEATURE_VERSION = "agent#feature#hide#version";
    public static final String HIDE_FEATURE_STATUS = "agent#feature#hide#status";
    public static final String SERVICE_AVAILABLE_STATUS = "agent#service#available#status";
    public static final String SERVICE_UNAVAILABLE_KEY_EN = "agent#service#available#message_en";
    public static final String SERVICE_UNAVAILABLE_KEY_BN = "agent#service#available#message_bn";

    public static final String IMAGE_EXT_LEFT = "data:image/";
    public static final String IMAGE_EXT_RIGHT = ";base64,";
    public static final String KYC_MESSAGE = "Applicant with this NID or mobile number already exists.";

    public static final String BILLER_IMAGE_URL = "https://api.bdkepler.com/images/biller-icon/";

    public static final String GIGATECH_TOKEN = "USER-TOKEN";

    public static final String PENDING_MSG = "User E-KYC is Pending.";
    public static final String SUCCESS_MSG = "User E-KYC is Successfully Updated";
    public static final String FAIL_MSG = "User E-KYC has Failed.Please retry.";
    public static final String KYC_GATEWAY_FAIL = "No valid crumb was included in the request";

    public static final String CONFIG_UPDATE_AUTH_KEY = "zSKAGWGTkQ";
    public static final String DONATION_MERCHANT_KEY = "DONATION_MERCHANT";

//    public static final String KYC_FAIL = ",KYC verification submission failed";
//    public static final String KYC_EXISTS = ",However number/nid has been used for another kyc verification";

    public static String checkNumber(String accountNo) {
        if (accountNo.startsWith("88")) {

        } else if (accountNo.startsWith("+88")) {
            accountNo = accountNo.substring(1);
        } else {
            StringBuilder sb = new StringBuilder(accountNo);
            sb.insert(0, "88");
            accountNo = sb.toString();
        }
        return accountNo;
    }

    public static DataJson[] formatResponse(String response) {
        if (response.contains(TARGET_TYPE)) {
            response = (((((response.replace("\"[", "[")).replace("]\"", "]")).replace("   \\u000d\\u000a\\u0009", "")).replace("\\u000d\\u000a\\u0009", "")).replace("\\u000d\\u000a", "")).replace("\\", "").replace("u0009", "");
        } else {
            response = (((((response.replace("\"[", "[")).replace("]\"", "]")).replace(",   \\u000d\\u000a\\u0009", "")).replace(",\\u000d\\u000a\\u0009", "")).replace("\\u000d\\u000a", "")).replace("\\", "").replace("u0009", "");
        }
        System.out.println(response);
        DataJson[] insuranceJson = new Gson().fromJson(response, DataJson[].class);
        return insuranceJson;
    }

    public static BillJson[] formatUtilityResponse(String response) {
        response = (((((response.replace("\"[", "[")).replace("]\"", "]")).replace(",   \\u000d\\u000a\\u0009", "")).replace(",\\u000d\\u000a\\u0009", "")).replace("\\u000d\\u000a", "")).replace("\\", "").replace("u0009", "");
        System.out.println(response);
        BillJson[] billJsons = new Gson().fromJson(response, BillJson[].class);
        return billJsons;
    }

    public static NidServiceListParams[] formatNidServices(String response) {
        response = (((((response.replace("\"[", "[")).replace("]\"", "]")).replace(",   \\u000d\\u000a\\u0009", "")).replace(",\\u000d\\u000a\\u0009", "")).replace("\\u000d\\u000a", "")).replace("\\", "").replace("u0009", "");
        System.out.println(response);
        NidServiceListParams[] nidServiceListParams = new Gson().fromJson(response, NidServiceListParams[].class);
        for (NidServiceListParams n : nidServiceListParams) {
            if (n.getAmount().equalsIgnoreCase("0")) {
                n.setAmount("0.0");
            }
        }
        return nidServiceListParams;
    }

    public static Map<String, Integer> TOPUP_OPERATOR;
    static {
        TOPUP_OPERATOR = new HashMap<>();
        TOPUP_OPERATOR.put("GrameenPhone", 1);
        TOPUP_OPERATOR.put("Banglalink", 2);
        TOPUP_OPERATOR.put("Robi", 3);
        TOPUP_OPERATOR.put("Teletalk", 5);
        TOPUP_OPERATOR.put("Airtel", 6);
    }

    public static String REQUEST_CHANNEL_APP = "APP";
}
