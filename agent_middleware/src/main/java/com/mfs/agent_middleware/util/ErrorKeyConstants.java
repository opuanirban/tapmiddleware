package com.mfs.agent_middleware.util;

public class ErrorKeyConstants {

    public static final String OTP_MISMATCH_KEY = "otp_mismatch";
    public static final String OTP_EXPIRED_KEY = "otp_expired";
    public static final String REGISTRATION_KYC_FAIL_KEY = "registration_kyc_fail";
    public static final String REGISTRATION_NO_NID_KEY = "registration_no_nid";
    public static final String REGISTRATION_KYC_PENDING_KEY = "registration_kyc_pending";
    public static final String TEXT_EC_PHOTO_MISMATCH_KEY = "text_ec_photo_mismatch";
    public static final String TEXT_GIVEN_PHOTO_MISMATCH_KEY = "text_given_photo_mismatch";
    public static final String TEXT_NID_PHOTO_MISMATCH_KEY = "text_nid_photo_mismatch";
    public static final String EC_PHOTO_MISMATCH_KEY = "ec_photo_mismatch";
    public static final String GIVEN_PHOTO_MISMATCH_KEY = "given_photo_mismatch";
    public static final String NID_PHOTO_MISMATCH_KEY = "nid_photo_mismatch";
    public static final String NID_TEXT_MISMATCH_KEY = "nid_text_mismatch";
    public static final String API_MANAGER_INVALID_KEY = "api_manager_invalid";
    public static final String API_MANAGER_FAIL_KEY = "api_manager_fail";
    public static final String API_MANAGER_CONNECTION_KEY = "api_manager_connection_fail";
    public static final String KYC_FAIL_KEY = "kyc_fail";
    public static final String KYC_INVALID_KEY = "kyc_invalid";
    public static final String KYC_CONNECTION_KEY = "kyc_connection_fail";
    public static final String OTP_REQUEST_ERROR_KEY = "otp_request_error_fail";
    public static final String OTP_UNSUCCESS_KEY = "otp_unsuccess_fail";
    public static final String OTP_ERROR_KEY = "otp_error";
    public static final String KYC_SERVER_ERROR_KEY = "kyc_server_error";
    public static final String API_MANAGER_STATEMENT_FAIL_KEY = "api_manager_statement_fail";
    public static final String API_MANAGER_REGISTRATION_FAIL_KEY = "api_manager_registration_fail";
    public static final String API_MANAGER_REGISTRATION_SUCCESS_KEY = "api_manager_registration_success";
    public static final String GENERAL_SUCCESS_KEY = "general_success";
    public static final String API_MANAGER_USER_EXISTS = "api_manager_user_exists";
    public static final String API_MANAGER_DATA_ENTRY_FAIL = "api_manager_data_fail";


}
