package com.mfs.agent_middleware.config;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import lombok.Data;

@Component
@Data
public class ApplicationProperties {

    @Value("${http.url.otp-service}")
    private String otpServiceUrl;

    @Value("${middleware.allowed.tps}")
    private double allowedTps;

    @Value("${jwt.token.expiration.in.mills}")
    private Integer jwtExpireTime;

    @Value("${jwt.token.secret}")
    private String jwtTokenSecret;

    @Value("${encryption.enabled}")
    private Boolean encryptionEnabled;

    @Value("${process.check.enabled}")
    private Boolean checkEnabled;

    @Value("${process.timeout.in.mills}")
    private int duplicateReqTimeOutInMills;

    @Value("#{'${success.placeholder}'.split(',')}")
    private List<String> successPlaceholderList;

    @Value("${api_middleware_url}")
    private String apiMiddlewareUrl;
}
