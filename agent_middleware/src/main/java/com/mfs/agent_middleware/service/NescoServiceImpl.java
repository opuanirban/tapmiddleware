package com.mfs.agent_middleware.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Pattern;

import com.mfs.agent_middleware.dto.ApiManagerRequest;
import com.mfs.agent_middleware.dto.CommonResponse;
import com.mfs.agent_middleware.dto.NescoPostpaidRequest;
import com.mfs.agent_middleware.dto.NescoPrepaidRequest;
import com.mfs.agent_middleware.enumeration.ContextUrl;
import com.mfs.agent_middleware.enumeration.TransactionEvents;
import com.mfs.agent_middleware.exception.DecodePinException;
import com.mfs.agent_middleware.exception.TransactionProcessException;
import com.mfs.agent_middleware.gateway.ApiManagerGateway;
import com.mfs.agent_middleware.gateway.nesco.NescoServiceGateway;
import com.mfs.agent_middleware.service.security.ISecurityService;
import com.mfs.agent_middleware.util.CommonConstant;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class NescoServiceImpl extends ProcessRequestService implements NescoService{
    @Autowired
    ApiManagerGateway apiManagerGateway;

    @Autowired
    ISecurityService iSecurityService;

    @Autowired
    TransactionProcessService transactionProcessService;
    
    @Autowired
    NescoServiceGateway nescoServiceGateway;

    @Override
    public CommonResponse payNescoBill(NescoPrepaidRequest prepaidRequest, String accountNo, Locale locale) {
        if (applicationProperties.getCheckEnabled()) {
            if (transactionProcessService.findProcess(accountNo, prepaidRequest.getBillAmount(),prepaidRequest.getMeterNumber(), TransactionEvents.NESCO_PREPAID.toString())) {
                log.error("Same transaction within 10 minutes");
                throw new TransactionProcessException("User not allowed to do the same transaction within " +
                        applicationProperties.getDuplicateReqTimeOutInMills() / 60000 + " minutes of last transaction");
            } else {
                return doPayNescoBill(prepaidRequest, accountNo, locale);
            }
        } else {
            return doPayNescoBill(prepaidRequest, accountNo, locale);
        }
    }

    public CommonResponse doPayNescoBill(NescoPrepaidRequest prepaidRequest, String accountNo, Locale locale) {
        // TrustMM NESPR CustomerNo Amount Pin NotificiationMobileNo
        if (Pattern.matches("......==", prepaidRequest.getPin())) {
            String formattedText = "TRUSTMM NESPR " + prepaidRequest.getCustomerNumber() + " " + prepaidRequest.getBillAmount() + " " + new String(Base64.getDecoder().decode(prepaidRequest.getPin())) + " " + CommonConstant.checkNumber(prepaidRequest.getNotificationNumber());
            ApiManagerRequest apiManagerRequest = makeProcessedRequest(applicationProperties.getEncryptionEnabled() ?
                    iSecurityService.encrypt(formattedText, "", "") : formattedText, ContextUrl.API_MANAGER_CONTEXT.getUrl(), accountNo);

            CommonResponse response = getTransactionFormattedResponse(apiManagerGateway, apiManagerRequest, locale, true);

            try {
                transactionProcessService.storeProcess(accountNo, prepaidRequest.getBillAmount(),prepaidRequest.getMeterNumber(), TransactionEvents.NESCO_PREPAID.toString());
            } catch (Exception e) {
                log.error(e.getMessage());
            }
            return response;

        } else {
            throw new DecodePinException(CommonConstant.DECODE_EXCEPTION_MESSAGE);
        }
    }

    @Override
    public CommonResponse payNescoPostpaidBill(NescoPostpaidRequest postpaidRequest, String accountNo, Locale locale) {
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime (date); // set to the current time
        calendar.set (Calendar.MONTH, calendar.get (Calendar.MONTH)-1); // set to the previous month
        date = calendar.getTime();
        if(postpaidRequest.getBillMonth() == null) {
            DateFormat dateFormat = new SimpleDateFormat("MM");
            postpaidRequest.setBillMonth(postpaidRequest.getBillMonth() != null? postpaidRequest.getBillMonth() : Integer.parseInt(dateFormat.format(date)));
        }
        if(postpaidRequest.getBillYear() == null) {
            DateFormat dateFormat = new SimpleDateFormat("yyyy");
            postpaidRequest.setBillYear(postpaidRequest.getBillYear() != null ? postpaidRequest.getBillYear() : Integer.parseInt(dateFormat.format(date)));
        }

        if (applicationProperties.getCheckEnabled()) {
            if (transactionProcessService.findProcess(accountNo, postpaidRequest.getBillAmount(),postpaidRequest.getAccountNo(), TransactionEvents.NESCO_POSTPAID.toString())) {
                log.error("Same transaction within 10 minutes");
                throw new TransactionProcessException("User not allowed to do the same transaction within " +
                        applicationProperties.getDuplicateReqTimeOutInMills() / 60000 + " minutes of last transaction");
            } else {
                return doPayNescoBill(postpaidRequest, accountNo, locale);
                // return doPayNescoBillMS(postpaidRequest, accountNo, locale);
            }
        } else {
            return doPayNescoBill(postpaidRequest, accountNo, locale);
        // return doPayNescoBillMS(postpaidRequest, accountNo, locale);
        }    
    }

    public CommonResponse doPayNescoBill(NescoPostpaidRequest postpaidRequest, String accountNo, Locale locale) {
        if (Pattern.matches("......==", postpaidRequest.getPin())) {
            String formattedText = "TRUSTMM NESCO " + postpaidRequest.getAccountNo() + " " + postpaidRequest.getBillMonth() + " " + postpaidRequest.getBillYear() + " " + new String(Base64.getDecoder().decode(postpaidRequest.getPin())) + " " + CommonConstant.checkNumber(postpaidRequest.getNotificationNumber());
            ApiManagerRequest apiManagerRequest = makeProcessedRequest(applicationProperties.getEncryptionEnabled() ?
                    iSecurityService.encrypt(formattedText, "", "") : formattedText, ContextUrl.API_MANAGER_CONTEXT.getUrl(), accountNo);

            CommonResponse response = getTransactionFormattedResponse(apiManagerGateway, apiManagerRequest, locale, true);

            try {
                transactionProcessService.storeProcess(accountNo, postpaidRequest.getBillAmount(),postpaidRequest.getAccountNo(), TransactionEvents.NESCO_POSTPAID.toString());
            } catch (Exception e) {
                log.error(e.getMessage());
            }
            return response;

        } else {
            throw new DecodePinException(CommonConstant.DECODE_EXCEPTION_MESSAGE);
        }
    }

    // public CommonResponse doPayNescoBillMS(NescoPostpaidRequest postpaidRequest, String accountNo, Locale locale) {
    //     CommonResponse commonResponse = new CommonResponse();
    //     // populate bill payment request
    //     BillPaymentInput billPaymentInput = new BillPaymentInput();
    //     billPaymentInput.setAccountNumber(accountNo);
    //     billPaymentInput.setBillerCode("NESCO");
    //     billPaymentInput.setKey1(postpaidRequest.getAccountNo());
    //     billPaymentInput.setKey2(String.valueOf(postpaidRequest.getBillMonth()));
    //     billPaymentInput.setKey3(String.valueOf(postpaidRequest.getBillYear()));
    //     billPaymentInput.setKey4(String.valueOf(postpaidRequest.getBillAmount()));
    //     billPaymentInput.setKey5(postpaidRequest.getTransactionId());
    //     billPaymentInput.setNotificationNumber(postpaidRequest.getNotificationNumber());
    //     billPaymentInput.setPin(postpaidRequest.getPin());
    //     billPaymentInput.setPaymentChannel(CommonConstant.REQUEST_CHANNEL_APP);
        
    //     BillPaymentResponse billPaymentResponse = nescoServiceGateway.paymentPostpaidBill(billPaymentInput);
    //     if(!billPaymentResponse.getResponse().getResponseCode().equals(CommonConstant.SUCCESS_CODE)) {
    //         throw new ApiRequestException(billPaymentResponse.getResponse().getResponseMessage());
    //     }

    //     commonResponse.setMessage(billPaymentResponse.getResponse().getResponseMessage() + "; Transaction Id: " + billPaymentResponse.getResponse().getResponseMessage());
    //     return commonResponse;
    // }

}
