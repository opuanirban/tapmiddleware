package com.mfs.agent_middleware.service;

import java.util.Locale;

import com.mfs.agent_middleware.dto.CommonResponse;
import com.mfs.agent_middleware.dto.REBPrepaidRequest;

public interface REBService {
    public CommonResponse payREBPrepaidBill(REBPrepaidRequest prepaidRequest, String accountNo, Locale locale);
}
