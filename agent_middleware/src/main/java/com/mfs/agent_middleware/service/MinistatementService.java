package com.mfs.agent_middleware.service;

import java.util.Locale;

import com.mfs.agent_middleware.dto.MiniStatementResponse;

public interface MinistatementService {
    MiniStatementResponse fetchMiniStatement(String userAccount, Locale locale);
}
