package com.mfs.agent_middleware.service;

import java.util.List;
import java.util.Locale;

import com.mfs.agent_middleware.dto.DataJson;
import com.mfs.agent_middleware.dto.DataListResponse;

public interface DataFetchService {
    public List<DataListResponse> getBillerList(Locale locale);

    public DataJson[] getInsurance(Locale locale);

    public DataJson[] getInstitution(Locale locale);

    public DataJson[] getOthers(Locale locale);

    public DataJson[] getDonations(Locale locale);

    public List<DataListResponse> getUtility(Locale locale);

    public void modifyDonationMerchantList(String donationMerchantList);
}
