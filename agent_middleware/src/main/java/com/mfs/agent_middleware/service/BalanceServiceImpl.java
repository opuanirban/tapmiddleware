package com.mfs.agent_middleware.service;

import java.util.Locale;

import com.mfs.agent_middleware.config.ApplicationProperties;
import com.mfs.agent_middleware.dto.BalanceInquiry;
import com.mfs.agent_middleware.exception.ApiManagerRequestException;
import com.mfs.agent_middleware.gateway.ApiManagerGateway;
import com.mfs.agent_middleware.service.security.ISecurityService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BalanceServiceImpl extends ProcessRequestService implements BalanceService {
    @Autowired
    ApiManagerGateway apiManagerGateway;

    @Autowired
    ISecurityService securityService;

    @Autowired
    ApplicationProperties applicationProperties;

    @Override
    public BalanceInquiry checkBalance(String userAccount, Locale locale) {
        BalanceInquiry balanceInquiry = new BalanceInquiry();

        String response = apiManagerGateway.balanceCheck(userAccount, locale);
        if (!response.isEmpty() && response != null) {
            balanceInquiry.setCurrentBalance(response);
            return balanceInquiry;
        } else {
            throw new ApiManagerRequestException("Could not fetch balance.Server Side error");
        }
    }
}
