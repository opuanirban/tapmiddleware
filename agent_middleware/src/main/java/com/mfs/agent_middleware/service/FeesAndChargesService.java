package com.mfs.agent_middleware.service;

import java.util.Locale;

import com.mfs.agent_middleware.dto.FeesAndChargesModelWallet;
import com.mfs.agent_middleware.dto.FeesChargesResponse;

public interface FeesAndChargesService {
    public FeesChargesResponse feeAndCharge(FeesAndChargesModelWallet feesAndChargesModel, Locale locale);
}
