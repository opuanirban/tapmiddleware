package com.mfs.agent_middleware.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class OtpBlacklistServiceImpl implements OtpBlacklistService {

    @Autowired
    TransactionProcessService transactionProcessService;

    @Value("${maximum.otp.request}")
    private Integer maxRequest;

    @Override
    public boolean checkMsisdn(String msisdn) {
        if (transactionProcessService.findInBlackList(msisdn)) {
            return true;
        } else {
            Integer count = transactionProcessService.findOtpCount(msisdn);
            if (count == maxRequest) {
                transactionProcessService.addToBlackList(msisdn);
                return true;
            } else {
                return false;
            }
        }
    }
}
