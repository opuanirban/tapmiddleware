package com.mfs.agent_middleware.service;

import java.util.Locale;

import com.mfs.agent_middleware.dto.UserPresenceResponse;
import com.mfs.agent_middleware.dto.UserTypeResponseWallet;

public interface UserTypeService {
   UserTypeResponseWallet fetchUserType(String userAccount, Locale locale);
   UserPresenceResponse fecthUserPresence(String userAccount, Locale locale);
}
