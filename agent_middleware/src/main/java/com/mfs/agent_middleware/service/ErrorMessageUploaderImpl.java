package com.mfs.agent_middleware.service;

import java.io.IOException;
import java.util.List;

import com.mfs.agent_middleware.dao.RedisDao;
import com.mfs.agent_middleware.dto.CommonResponse;
import com.mfs.agent_middleware.dto.ExcelDto;
import com.mfs.agent_middleware.exception.ExcelUploadException;
import com.mfs.agent_middleware.util.ExcelHelper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ErrorMessageUploaderImpl implements ErrorMessageUploader {

    @Autowired
    RedisDao redisDao;

    @Override
    public CommonResponse uploadFile(MultipartFile file) {
        try {
            if (ExcelHelper.hasExcelFormat(file)) {
                List<ExcelDto> excelDtos = ExcelHelper.excelToTutorials(file.getInputStream());
                for (ExcelDto e : excelDtos) {
                    if(e.getKey()!=null && e.getValue()!=null) {
                        redisDao.setValue(e.getKey(), e.getValue());
                    }else{
                        throw new ExcelUploadException("Key or Value can not be empty");
                    }
                }
                log.info("Error Message file content stored in database");
                CommonResponse commonResponse = new CommonResponse();
                commonResponse.setMessage("Error Messages uploaded Successfully");
                return commonResponse;
            }else {
                throw new ExcelUploadException("Not valid file format. Upload xls/xlsx file");
            }
        } catch (IOException e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new ExcelUploadException("Fail to store excel data: " + e.getMessage());
        }

    }
}
