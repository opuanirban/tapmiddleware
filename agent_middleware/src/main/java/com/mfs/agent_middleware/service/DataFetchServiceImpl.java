package com.mfs.agent_middleware.service;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import com.mfs.agent_middleware.dao.RedisDao;
import com.mfs.agent_middleware.dto.BillJson;
import com.mfs.agent_middleware.dto.DataJson;
import com.mfs.agent_middleware.dto.DataListResponse;
import com.mfs.agent_middleware.enumeration.BillTypeEnum;
import com.mfs.agent_middleware.gateway.ApiManagerGateway;
import com.mfs.agent_middleware.util.CommonConstant;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class DataFetchServiceImpl implements DataFetchService {

    @Autowired
    ApiManagerGateway apiManagerGateway;

    @Autowired
    RedisDao redisDao;

    @Override
    public List<DataListResponse> getBillerList(Locale locale) {
        String insuranceResponse = apiManagerGateway.getList(BillTypeEnum.INSURANCE.getBill(), locale);
        System.out.println(insuranceResponse);
        String utilityResponse = apiManagerGateway.getList(BillTypeEnum.UTILITY.getBill(), locale);
        System.out.println(utilityResponse);
        String institutionResponse = apiManagerGateway.getList(BillTypeEnum.INSTITUTION.getBill(), locale);
        System.out.println(institutionResponse);
        List<DataListResponse> dataListResponse = new ArrayList<DataListResponse>();
        int id = 1;
        DataJson[] insuranceJson = CommonConstant.formatResponse(insuranceResponse);
        dataListResponse.add(prepareListObject(id, BillTypeEnum.INSURANCE.name(), insuranceJson, null));
        id = id + 1;
        DataJson[] utilityJsons = CommonConstant.formatResponse(utilityResponse);
        dataListResponse.add(prepareListObject(id, BillTypeEnum.UTILITY.name(), utilityJsons, null));
        id = id + 1;
        DataJson[] institutionJsons = CommonConstant.formatResponse(institutionResponse);
        dataListResponse.add(prepareListObject(id, BillTypeEnum.INSTITUTION.name(), institutionJsons, null));
        return dataListResponse;

    }

    @Override
    public List<DataListResponse> getUtility(Locale locale) {
        String utilityResponse = apiManagerGateway.getUtility(BillTypeEnum.BILLTYPE.getBill(), locale);
        BillJson[] utilityJsons = CommonConstant.formatUtilityResponse(utilityResponse);
        DataJson[] detailsJson;
        List<DataListResponse> dataListResponse = new ArrayList<DataListResponse>();
        int id = 1;
        for (BillJson bill : utilityJsons) {
            String result = apiManagerGateway.getUtilityDetails(BillTypeEnum.UTILITY.getBill(), bill.getCode(), locale);
            if (result != null) {
                detailsJson = CommonConstant.formatResponse(result);
                Arrays.stream(detailsJson).forEach((e) -> {
                    try {
                        if(e.getIconSource() != null && !e.getIconSource().isEmpty()) {
                            e.setIconSource(CommonConstant.BILLER_IMAGE_URL + URLEncoder.encode(e.getIconSource(), "UTF-8"));
                        }
                    } catch (UnsupportedEncodingException unsupportedEncodingException) {
                        unsupportedEncodingException.printStackTrace();
                    }
                });
                dataListResponse.add(prepareListObject(id, bill.getDescription(), detailsJson, bill.getIconSource()));
                id = id + 1;
            }

        }
        System.out.println(utilityJsons);
        return dataListResponse;
    }

    @Override
    public void modifyDonationMerchantList(String donationMerchantList) {
        redisDao.setValue(CommonConstant.DONATION_MERCHANT_KEY, donationMerchantList);
    }

    @Override
    public DataJson[] getInsurance(Locale locale) {
        String insuranceResponse = apiManagerGateway.getList(BillTypeEnum.INSURANCE.getBill(), locale);
        return getDataJsons(insuranceResponse);
    }

    @Override
    public DataJson[] getInstitution(Locale locale) {
        String institutionResponse = apiManagerGateway.getList(BillTypeEnum.INSTITUTION.getBill(), locale);
        return getDataJsons(institutionResponse);
    }

    @Override
    public DataJson[] getOthers(Locale locale) {
        String insuranceResponse = apiManagerGateway.getList(BillTypeEnum.OTHERS.getBill(), locale);
        return getDataJsons(insuranceResponse);
    }

    @Override
    public DataJson[] getDonations(Locale locale) {
        String donationBillerList = null;
        try {
            donationBillerList = redisDao.getValue(CommonConstant.DONATION_MERCHANT_KEY);
        }catch (Exception e){
            log.error("data retrieval error from redis: " + e.getMessage());
        }

        if(donationBillerList == null || donationBillerList.isEmpty()){
            donationBillerList = "\"[\\u000d\\u000a   {\\u000d\\u000a      \\\"Id\\\":\\\"2024dc48-6b09-4631-921d-b8b2267c8000\\\",\\u000d\\u000a      \\\"Code\\\":\\\"01766685686\\\",\\u000d\\u000a      \\\"Description\\\":\\\"Bidyanondo Foundation\\\",\\u000d\\u000a      \\\"IconSource\\\":\\\"Bidyanondo.png\\\",   \\u000d\\u000a      \\\"LabelView\\\":\\\"\\\",\\u000d\\u000a      \\\"DefaultNoOfMonth\\\":\\\"false\\\",\\u000d\\u000a      \\\"TermsAndConditions\\\":\\\"\\\",\\u000d\\u000a      \\\"TargetType\\\":\\\"\\\"\\u000d\\u000a\\u0009},\\u000d\\u000a   {\\u000d\\u000a      \\\"Id\\\":\\\"2024dc48-6b09-4631-921d-b8b2267c8001\\\",\\u000d\\u000a      \\\"Code\\\":\\\"01730482278\\\",\\u000d\\u000a      \\\"Description\\\":\\\"MASTUL Foundation\\\",\\u000d\\u000a      \\\"IconSource\\\":\\\"MASTUL-Foundation.png\\\",   \\u000d\\u000a      \\\"LabelView\\\":\\\"\\\",\\u000d\\u000a      \\\"DefaultNoOfMonth\\\":\\\"false\\\",\\u000d\\u000a      \\\"TermsAndConditions\\\":\\\"\\\",\\u000d\\u000a      \\\"TargetType\\\":\\\"\\\"\\u000d\\u000a\\u0009},\\u000d\\u000a   {\\u000d\\u000a      \\\"Id\\\":\\\"2024dc48-6b09-4631-921d-b8b2267c8001\\\",\\u000d\\u000a      \\\"Code\\\":\\\"01937791254\\\",\\u000d\\u000a      \\\"Description\\\":\\\"Embassy of the State of Palestine\\\",\\u000d\\u000a      \\\"IconSource\\\":\\\"Palestine.jpg\\\",   \\u000d\\u000a      \\\"LabelView\\\":\\\"\\\",\\u000d\\u000a      \\\"DefaultNoOfMonth\\\":\\\"false\\\",\\u000d\\u000a      \\\"TermsAndConditions\\\":\\\"\\\",\\u000d\\u000a      \\\"TargetType\\\":\\\"\\\"\\u000d\\u000a\\u0009}\\u000d\\u000a]\"";
        }
        return getDataJsons(donationBillerList);
    }

    private DataJson[] getDataJsons(String response) {
        DataJson[] result = CommonConstant.formatResponse(response);
        Arrays.stream(result).forEach((e) -> {
            try {
                if(e.getIconSource() != null && !e.getIconSource().isEmpty()) {
                    e.setIconSource(CommonConstant.BILLER_IMAGE_URL + URLEncoder.encode(e.getIconSource(), "UTF-8"));
                }else{
                    e.setIconSource(null);
                }
            } catch (UnsupportedEncodingException unsupportedEncodingException) {
                unsupportedEncodingException.printStackTrace();
            }
        });
        return result;
    }

    public DataListResponse prepareListObject(Integer i, String title, DataJson[] list, String iconSource) {
        DataListResponse dataListResponse = new DataListResponse();
        dataListResponse.setCategoryId(i);
        dataListResponse.setCategoryTitle(title);
        try {
            if(iconSource != null && !iconSource.isEmpty()) {
                dataListResponse.setCategoryIconSource(iconSource != null ? CommonConstant.BILLER_IMAGE_URL + URLEncoder.encode(iconSource, "UTF-8") : null);
            }else{
                dataListResponse.setCategoryIconSource(null);
            }
        } catch (UnsupportedEncodingException e) {
            dataListResponse.setCategoryIconSource(null);
            log.error("url encoding exception: " + e.getMessage());
        }
        dataListResponse.setItemList(list);
        return dataListResponse;
    }
}


