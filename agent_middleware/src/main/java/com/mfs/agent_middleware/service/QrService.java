package com.mfs.agent_middleware.service;

import com.mfs.agent_middleware.dto.MerchantInfoResponse;

public interface QrService {
    public MerchantInfoResponse getMerchantInfo(String id,String secret);
}
