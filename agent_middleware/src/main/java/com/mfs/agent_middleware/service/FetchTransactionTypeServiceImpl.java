package com.mfs.agent_middleware.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.mfs.agent_middleware.dto.FeatureConfigItem;
import com.mfs.agent_middleware.dto.FeatureConfigList;
import com.mfs.agent_middleware.enumeration.TransactionTypes;
import com.mfs.agent_middleware.gateway.ApiManagerGateway;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class FetchTransactionTypeServiceImpl implements FetchTransactionTypeService {

    @Autowired
    ApiManagerGateway apiManagerGateway;

    @Override
    public FeatureConfigList fetchTypes(Locale locale) {

        FeatureConfigList featureConfigList = new FeatureConfigList();
        List<FeatureConfigItem> featureConfigItems = new ArrayList<>();

        for (TransactionTypes transactionTypes:TransactionTypes.values()) {
            FeatureConfigItem featureConfigItem = new FeatureConfigItem();
            featureConfigItem.setFeatureName(transactionTypes.name());
            featureConfigItem.setCode(transactionTypes.getTransactionType().split("#")[0]);
            featureConfigItem.setMinAmount(new BigDecimal(transactionTypes.getTransactionType().split("#")[1]));
            featureConfigItem.setMaxAmount(new BigDecimal(transactionTypes.getTransactionType().split("#")[2]));

            featureConfigItems.add(featureConfigItem);
        }

        featureConfigList.setFeaturesConfigList(featureConfigItems);
        return featureConfigList;

    }
}
