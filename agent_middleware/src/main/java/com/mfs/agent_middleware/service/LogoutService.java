package com.mfs.agent_middleware.service;

import com.mfs.agent_middleware.dto.CommonResponse;

public interface LogoutService {
    public CommonResponse addTokenToblacklist(String jwt);
}
