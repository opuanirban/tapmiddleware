package com.mfs.agent_middleware.service;

import java.util.Locale;

import com.mfs.agent_middleware.dto.CommonResponse;
import com.mfs.agent_middleware.dto.PinInfo;

public interface PinUpdateService {
    public CommonResponse update_pin(String msisdn, PinInfo pinInfo, Locale locale);
}
