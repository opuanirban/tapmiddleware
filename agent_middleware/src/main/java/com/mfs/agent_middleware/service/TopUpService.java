package com.mfs.agent_middleware.service;

import java.util.Locale;

import com.mfs.agent_middleware.dto.CommonResponse;
import com.mfs.agent_middleware.dto.TopUpRequest;

public interface TopUpService {

    public CommonResponse sendTopUp(TopUpRequest topUpRequest, String accountNo, Locale locale);
}
