package com.mfs.agent_middleware.service;

import java.util.Locale;

import com.mfs.agent_middleware.dao.RedisDao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class MessageRetrieverServiceImpl implements MessageRetrieverService{

    private static final String BANGLA_EXTENSION = "_bn";

    @Autowired
    RedisDao redisDao;

    @Override
    public String getMessage(Locale locale, String keyName) {
        if (locale.getLanguage().equalsIgnoreCase("bn")) {
            return redisDao.getValue(keyName+BANGLA_EXTENSION);
        }else{
            return redisDao.getValue(keyName);
        }
    }
}
