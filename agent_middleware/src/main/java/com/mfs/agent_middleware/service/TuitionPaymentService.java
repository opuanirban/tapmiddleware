package com.mfs.agent_middleware.service;

import java.util.Locale;

import com.mfs.agent_middleware.dto.CommonResponse;
import com.mfs.agent_middleware.dto.TuitionFeePaymentRequest;

public interface TuitionPaymentService {
    public CommonResponse payTuition(TuitionFeePaymentRequest tuitionFeePaymentRequest, String accountNo, Locale locale);
}
