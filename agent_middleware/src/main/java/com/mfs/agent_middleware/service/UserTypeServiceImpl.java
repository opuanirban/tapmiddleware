package com.mfs.agent_middleware.service;

import java.util.Locale;

import com.mfs.agent_middleware.dto.UserPresenceResponse;
import com.mfs.agent_middleware.dto.UserTypeResponse;
import com.mfs.agent_middleware.dto.UserTypeResponseWallet;
import com.mfs.agent_middleware.enumeration.UserStatusEnum;
import com.mfs.agent_middleware.exception.ApiRequestException;
import com.mfs.agent_middleware.gateway.AgentRegistrationServiceGateway;
import com.mfs.agent_middleware.gateway.ApiManagerGateway;
import com.mfs.agent_middleware.gateway.model.AgentStatusResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserTypeServiceImpl implements UserTypeService {

    @Autowired
    ApiManagerGateway apiManagerGateway;

    @Autowired
    AgentRegistrationServiceGateway agentRegServiceGateway;

    @Override
    public UserTypeResponseWallet fetchUserType(String userAccount, Locale locale) {
        UserTypeResponse response = apiManagerGateway.userType(userAccount, locale);
        UserTypeResponseWallet userTypeResponseWallet = new UserTypeResponseWallet();
        userTypeResponseWallet.setUserType(response.getKey());
        userTypeResponseWallet.setUserName(response.getValue());
        return userTypeResponseWallet;
    }

    @Override
    public UserPresenceResponse fecthUserPresence(String userAccount, Locale locale) {
        UserPresenceResponse userStatusResponse = new UserPresenceResponse();

        UserTypeResponse response = apiManagerGateway.userType(userAccount, locale);
        if(response.getKey().equals("Validation")) {
            userStatusResponse.setUserStatus(UserStatusEnum.NOTFOUND.name());
            AgentStatusResponse agentStatusResponse = agentRegServiceGateway.fetchRegistrationServiceStatus(userAccount);
            userStatusResponse.setVerificationStatus(agentStatusResponse.getAgentVerificationStatus());
        } else if(response.getKey().equals("User")) {
            throw new ApiRequestException("Customer is not allowed to use agent app.");
        } else if (response.getKey().equals("Merchant")) {
            throw new ApiRequestException("Merchant is not allowed to use agent app.");
        } else if (response.getKey().equals("Paypoint")) {
            userStatusResponse.setUserStatus(UserStatusEnum.PRESENT.name());
        }
        return userStatusResponse;
    }
}
