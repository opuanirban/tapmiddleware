package com.mfs.agent_middleware.service;

import java.util.Locale;

import com.mfs.agent_middleware.dto.CommonResponse;
import com.mfs.agent_middleware.dto.InsurancePaymentRequest;

public interface InsuranceService {
    public CommonResponse insurancePayment(InsurancePaymentRequest insurancePaymentRequest, String accountNo, Locale locale);
}
