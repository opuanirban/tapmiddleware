package com.mfs.agent_middleware.service;

import java.util.Locale;

import com.mfs.agent_middleware.dto.BalanceInquiry;

public interface BalanceService {
    public BalanceInquiry checkBalance(String userAccount, Locale locale);
}
