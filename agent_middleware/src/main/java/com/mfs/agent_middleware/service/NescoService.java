package com.mfs.agent_middleware.service;

import java.util.Locale;

import com.mfs.agent_middleware.dto.CommonResponse;
import com.mfs.agent_middleware.dto.NescoPostpaidRequest;
import com.mfs.agent_middleware.dto.NescoPrepaidRequest;

public interface NescoService {
    public CommonResponse payNescoBill(NescoPrepaidRequest prepaidRequest, String accountNo, Locale locale);
    public CommonResponse payNescoPostpaidBill(NescoPostpaidRequest postpaidRequest, String accountNo, Locale locale);
}
