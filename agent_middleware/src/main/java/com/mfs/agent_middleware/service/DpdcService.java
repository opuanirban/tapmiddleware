package com.mfs.agent_middleware.service;

import java.util.Locale;

import com.mfs.agent_middleware.dto.CommonResponse;
import com.mfs.agent_middleware.dto.DPDCBillPayRequest;

public interface DpdcService {
    public CommonResponse payDpdcBill(DPDCBillPayRequest dpdcBillPayRequest,String accountNo, Locale locale);
}
