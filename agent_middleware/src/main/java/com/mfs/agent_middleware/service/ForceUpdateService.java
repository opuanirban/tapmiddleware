package com.mfs.agent_middleware.service;

import java.util.Locale;

import com.mfs.agent_middleware.dto.CommonResponse;
import com.mfs.agent_middleware.dto.ForceUpdateRequest;
import com.mfs.agent_middleware.dto.ForceUpdateResponse;

public interface ForceUpdateService {
    public CommonResponse setForceUpdateVersion(ForceUpdateRequest forceUpdateRequest, Locale locale);
    public ForceUpdateResponse checkForceUpdateVersion(String platform, Locale locale);
}
