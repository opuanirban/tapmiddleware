package com.mfs.agent_middleware.service;

import java.util.Base64;
import java.util.Locale;
import java.util.regex.Pattern;

import com.mfs.agent_middleware.config.ApplicationProperties;
import com.mfs.agent_middleware.dto.ApiManagerRequest;
import com.mfs.agent_middleware.dto.CashInRequest;
import com.mfs.agent_middleware.dto.CommonResponse;
import com.mfs.agent_middleware.enumeration.ContextUrl;
import com.mfs.agent_middleware.enumeration.TransactionEvents;
import com.mfs.agent_middleware.exception.DecodePinException;
import com.mfs.agent_middleware.exception.TransactionProcessException;
import com.mfs.agent_middleware.gateway.ApiManagerGateway;
import com.mfs.agent_middleware.service.security.ISecurityService;
import com.mfs.agent_middleware.util.CommonConstant;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class CashInServiceImpl extends ProcessRequestService implements CashInService {
    @Autowired
    ApiManagerGateway apiManagerGateway;

    @Autowired
    ISecurityService iSecurityService;

    @Autowired
    ApplicationProperties applicationProperties;

    @Autowired
    TransactionProcessService transactionProcessService;
    
    @Override
    public CommonResponse cashIn(CashInRequest cashInRequest, String accountNo, Locale locale) {
        if (applicationProperties.getCheckEnabled()) {
            if (transactionProcessService.findProcess(accountNo, cashInRequest.getAmount(),cashInRequest.getRecipientNumber(), TransactionEvents.CASH_IN.toString())) {
                log.error("Same transaction within 10 minutes");
                throw new TransactionProcessException("User not allowed to do the same transaction within " +
                        applicationProperties.getDuplicateReqTimeOutInMills() / 60000 + " minutes of last transaction");
            } else {
                return doCashIn(cashInRequest, accountNo, locale);
            }
        } else {
            return doCashIn(cashInRequest, accountNo, locale);
        }

    }

    private CommonResponse doCashIn(CashInRequest cashInRequest, String accountNo, Locale locale) {
        if (Pattern.matches("......==", cashInRequest.getPin())) {
            // Trustmm DP ToUserNumber Amount PIN
            String formattedText = "TRUSTMM DP " + CommonConstant.checkNumber(cashInRequest.getRecipientNumber()) + " " + cashInRequest.getAmount() + " " + new String(Base64.getDecoder().decode(cashInRequest.getPin()));
            ApiManagerRequest apiManagerRequest = makeProcessedRequest(applicationProperties.getEncryptionEnabled() ?
                    iSecurityService.encrypt(formattedText, "", "") : formattedText, ContextUrl.API_MANAGER_CONTEXT.getUrl(), accountNo);

            CommonResponse response = getTransactionFormattedResponse(apiManagerGateway, apiManagerRequest, locale, true);

            try {
                transactionProcessService.storeProcess(accountNo, cashInRequest.getAmount(),cashInRequest.getRecipientNumber(), TransactionEvents.CASH_IN.toString());
            } catch (Exception e) {
                log.error(e.getMessage());
            }

            return response;

        } else {
            throw new DecodePinException(CommonConstant.DECODE_EXCEPTION_MESSAGE);
        }

    }
    
}
