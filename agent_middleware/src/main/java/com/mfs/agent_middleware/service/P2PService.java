package com.mfs.agent_middleware.service;

import java.util.Locale;

import com.mfs.agent_middleware.dto.CommonResponse;
import com.mfs.agent_middleware.dto.P2PRequest;

public interface P2PService {

    public CommonResponse p2p(P2PRequest p2PRequest, String accountNo, Locale locale);
}
