package com.mfs.agent_middleware.service;

import java.util.Locale;

import com.mfs.agent_middleware.dto.BnrfParam;
import com.mfs.agent_middleware.dto.CommonResponse;

public interface BnrfService {
    public CommonResponse bnrfRequest(BnrfParam bnrfParam, String accountNo, Locale locale);
}
