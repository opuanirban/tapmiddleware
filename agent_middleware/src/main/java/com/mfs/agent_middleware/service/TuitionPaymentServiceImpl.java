package com.mfs.agent_middleware.service;

import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

import com.mfs.agent_middleware.config.ApplicationProperties;
import com.mfs.agent_middleware.dto.ApiManagerRequest;
import com.mfs.agent_middleware.dto.CommonResponse;
import com.mfs.agent_middleware.dto.TuitionFeePaymentRequest;
import com.mfs.agent_middleware.enumeration.ContextUrl;
import com.mfs.agent_middleware.exception.DecodePinException;
import com.mfs.agent_middleware.gateway.ApiManagerGateway;
import com.mfs.agent_middleware.service.security.ISecurityService;
import com.mfs.agent_middleware.util.CommonConstant;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TuitionPaymentServiceImpl extends ProcessRequestService implements TuitionPaymentService {

    @Autowired
    ApiManagerGateway apiManagerGateway;

    @Autowired
    ISecurityService iSecurityService;

    @Autowired
    ApplicationProperties applicationProperties;

    /*
        Special Tuition Fee Payment Message Format:
        BISC: TRUSTMM BISC ReqNo NoOfMonth PIN StudentName NotificationNumber
        BUP: TRUSTMM BUP ReferenceNumber PaymentToMonth ShortName/InvoiceNumber  NotificiationMobileNo Pin

    */

    @Override
    public CommonResponse payTuition(TuitionFeePaymentRequest tuitionFeePaymentRequest, String accountNo, Locale locale) {
        if (Pattern.matches("......==", tuitionFeePaymentRequest.getPin())) {
            String formattedText;

            List<String> specialFormattedList = Arrays.asList("MIS","BUP","RCPSC","CESC","DPSC","DCGPC","LPSC",
                    "NCPSC","PISE","BGBPS","CPSCR","TMSR","BISC");
            if(specialFormattedList.contains(tuitionFeePaymentRequest.getInstitutionCode())) {
                formattedText = "TRUSTMM " + tuitionFeePaymentRequest.getInstitutionCode() + " " + tuitionFeePaymentRequest.getRegNo() + " " + tuitionFeePaymentRequest.getNumberOfMonth() + " " + new String(Base64.getDecoder().decode(tuitionFeePaymentRequest.getPin())) + " " + tuitionFeePaymentRequest.getShortName();
            } else {
                formattedText = "TRUSTMM " + tuitionFeePaymentRequest.getInstitutionCode() + " " + tuitionFeePaymentRequest.getRegNo() + " " + tuitionFeePaymentRequest.getNumberOfMonth() + " " + tuitionFeePaymentRequest.getShortName() + " " + new String(Base64.getDecoder().decode(tuitionFeePaymentRequest.getPin()));
            }

            ApiManagerRequest apiManagerRequest = makeProcessedRequest(applicationProperties.getEncryptionEnabled() ?
                    iSecurityService.encrypt(formattedText, "", "") : formattedText, ContextUrl.API_MANAGER_CONTEXT.getUrl(), accountNo);
            CommonResponse response = getTransactionFormattedResponse(apiManagerGateway, apiManagerRequest, locale, true);
            return response;

        } else {
            throw new DecodePinException(CommonConstant.DECODE_EXCEPTION_MESSAGE);
        }

    }
}
