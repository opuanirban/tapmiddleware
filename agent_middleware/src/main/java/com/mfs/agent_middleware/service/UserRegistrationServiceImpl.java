package com.mfs.agent_middleware.service;

import java.util.Base64;
import java.util.Locale;
import java.util.regex.Pattern;

import com.mfs.agent_middleware.dto.BaseResponse;
import com.mfs.agent_middleware.dto.CommonResponse;
import com.mfs.agent_middleware.dto.KYCRegistrationResponse;
import com.mfs.agent_middleware.dto.RegistrationParams;
import com.mfs.agent_middleware.dto.UserLoginInfo;
import com.mfs.agent_middleware.dto.UserRegistrationRequest;
import com.mfs.agent_middleware.dto.UserStatusInfo;
import com.mfs.agent_middleware.exception.ApiManagerRequestException;
import com.mfs.agent_middleware.exception.DecodePinException;
import com.mfs.agent_middleware.exception.GigatechException;
import com.mfs.agent_middleware.gateway.AgentRegistrationServiceGateway;
import com.mfs.agent_middleware.gateway.ApiManagerGateway;
import com.mfs.agent_middleware.gateway.KYCGateway;
import com.mfs.agent_middleware.gateway.model.AgentInfoDto;
import com.mfs.agent_middleware.gateway.model.AgentNidInfoDto;
import com.mfs.agent_middleware.service.security.ISecurityService;
import com.mfs.agent_middleware.util.CommonConstant;
import com.mfs.agent_middleware.util.ErrorKeyConstants;
import com.mfs.agent_middleware.util.TBLResponsePlaceHolder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class UserRegistrationServiceImpl extends ProcessRequestService implements UserRegistrationService {

    @Autowired
    ApiManagerGateway apiManagerGateway;

    @Autowired
    KYCGateway kycGateway;

    @Autowired
    AgentRegistrationServiceGateway agentRegistrationServiceGateway;

    @Autowired
    ISecurityService iSecurityService;

    @Autowired
    KYCService kycService;

    @Value("${registration.msisdn}")
    private String userNumber;

    @Value("${registration.pin}")
    private String userPin;

    @Autowired
    MessageRetrieverService messageRetrieverService;

    @Override
    public UserStatusInfo checkForUser(String msisdn, Locale locale) {
        return apiManagerGateway.checkUSer(msisdn, locale);
    }

    @Override
    public UserLoginInfo fetchUserLoginInfo(String msisdn, String pin) {
        String loginResponse = apiManagerGateway.userLoginInfo(msisdn, pin);

        if (loginResponse.contains("|")) { // success response has | as data separator
            String[] infos = loginResponse.replace("|", "###")
                    .split("###");
            UserLoginInfo userLoginInfo = new UserLoginInfo();
            userLoginInfo.setUserName(infos[0]);
            userLoginInfo.setBalance(infos[1]);
            userLoginInfo.setUserStatus(infos[3]);

            return userLoginInfo;
        } else if (loginResponse.isEmpty()) {
            throw new ApiManagerRequestException(CommonConstant.COMMON_ERROR);
        } else {
            throw new ApiManagerRequestException(loginResponse);
        }
    }

    @Override
    public BaseResponse registerAgent(AgentInfoDto agentInfo, Locale locale) {
        BaseResponse baseResponse = new BaseResponse();
        // request for ekyc register
        AgentNidInfoDto agentNidInfoDto = agentInfo.getAgentNidInfoDto();
        KYCRegistrationResponse kycRegistrationResponse = kycGateway.customerRegistration(agentNidInfoDto.getAgentNidNumber(), agentNidInfoDto.getAgentNidDob(),
                agentNidInfoDto.getAgentNidNameBn(), agentNidInfoDto.getAgentNidNameEn(), agentNidInfoDto.getAgentNidFatherName(), agentNidInfoDto.getAgentNidMotherName(),
                agentNidInfoDto.getAgentNidSpouseName(), agentNidInfoDto.getAgentNidPresentAddress(), agentNidInfoDto.getAgentNidPresentAddress(), agentNidInfoDto.getAgentNidFrontImageName(),
                agentNidInfoDto.getAgentNidBackImageName(), agentInfo.getAgentGender(), agentInfo.getAgentOccupation(), agentInfo.getAgentNominee(), agentInfo.getAgentNomineeRelation(),
                agentInfo.getAgentNumber(), agentInfo.getAgentSelfie(),null);
        if(kycRegistrationResponse.getStatus_code() != 4003) {
            throw new GigatechException(kycRegistrationResponse.getMessage());
        }
        // request for agent registration
        agentRegistrationServiceGateway.agentRegistration(agentInfo);

        baseResponse.setCode(CommonConstant.SUCCESS_CODE);
        baseResponse.setMessage("Agent registration successfully completed");
        return baseResponse;
    }

    @Override
    public CommonResponse userBasicRegister(UserRegistrationRequest userRegistrationRequest,String agent_number, Locale locale) {
        log.debug("Customer Basic Registration with TBL for " + userRegistrationRequest.getMobileNumber());
        if (Pattern.matches("......==", userRegistrationRequest.getPin())) {
            RegistrationParams registrationParams = new RegistrationParams();
//            registrationParams.setAgentUserNumber();
            registrationParams.setUserNumber(CommonConstant.checkNumber(userRegistrationRequest.getMobileNumber()));
//            registrationParams.setAccountPin(new String(Base64.getDecoder().decode(userRegistrationRequest.getPin())));
            registrationParams.setAccountPin("");
            registrationParams.setFirstName(userRegistrationRequest.getFirstName());
            registrationParams.setLastName(userRegistrationRequest.getLastName()==null?"":userRegistrationRequest.getLastName());
//            registrationParams.setNationalIdNumber(userRegistrationRequest.getNid());
            registrationParams.setAgentUserNumber(agent_number);
            registrationParams.setSex(userRegistrationRequest.getGender().name());
            registrationParams.setUserGroupKey(userRegistrationRequest.getAccountType().getUserGroup());
            registrationParams.setOccupation(userRegistrationRequest.getOccupation());
            registrationParams.setOperatorName(userRegistrationRequest.getMno().name());
            registrationParams.setIsLimitedKYC(true);

            String result = apiManagerGateway.userRegistration(registrationParams, locale);
            if (result.contains(applicationProperties.getSuccessPlaceholderList().get(2))) {
                CommonResponse response = new CommonResponse();
                response.setMessage(messageRetrieverService.getMessage(locale, ErrorKeyConstants.API_MANAGER_REGISTRATION_SUCCESS_KEY));
                return response;
            } else if (result.contains(TBLResponsePlaceHolder.REGISTER_USER_EXISTS)) {
                throw new ApiManagerRequestException(messageRetrieverService.getMessage(locale, ErrorKeyConstants.API_MANAGER_USER_EXISTS));
            } else if (result.contains(TBLResponsePlaceHolder.REGISTER_DATA_FAIL)) {
                throw new ApiManagerRequestException(messageRetrieverService.getMessage(locale, ErrorKeyConstants.API_MANAGER_DATA_ENTRY_FAIL));
            } else {
                throw new ApiManagerRequestException(messageRetrieverService.getMessage(locale, ErrorKeyConstants.API_MANAGER_REGISTRATION_FAIL_KEY));
            }
        } else {
            throw new DecodePinException(CommonConstant.DECODE_EXCEPTION_MESSAGE);
        }
    }
}
