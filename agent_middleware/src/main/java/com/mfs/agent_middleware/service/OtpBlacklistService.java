package com.mfs.agent_middleware.service;

public interface OtpBlacklistService {
    boolean checkMsisdn(String msisdn);
}
