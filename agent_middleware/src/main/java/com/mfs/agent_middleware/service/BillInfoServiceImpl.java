package com.mfs.agent_middleware.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

import com.google.gson.Gson;
import com.mfs.agent_middleware.dto.BillInfoResponse;
import com.mfs.agent_middleware.dto.CommonResponse;
import com.mfs.agent_middleware.dto.DPDCBillInfo;
import com.mfs.agent_middleware.dto.NescoBillInfo;
import com.mfs.agent_middleware.dto.NescoBillInfoResponse;
import com.mfs.agent_middleware.dto.PrepaidBillDetailsResponse;
import com.mfs.agent_middleware.exception.ApiManagerRequestException;
import com.mfs.agent_middleware.exception.ApiRequestException;
import com.mfs.agent_middleware.exception.EmptyInputException;
import com.mfs.agent_middleware.gateway.ApiManagerGateway;
import com.mfs.agent_middleware.gateway.model.BaseResponse;
import com.mfs.agent_middleware.gateway.nesco.NescoServiceGateway;
import com.mfs.agent_middleware.gateway.nesco.model.BillFetchResponse;
import com.mfs.agent_middleware.gateway.nesco.model.BillPreviewOutput;
import com.mfs.agent_middleware.gateway.nesco.model.BillPreviewRestInput;
import com.mfs.agent_middleware.util.CommonConstant;
import com.mfs.agent_middleware.util.TBLResponsePlaceHolder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BillInfoServiceImpl implements BillInfoService {

    @Autowired
    ApiManagerGateway apiManagerGateway;
    @Autowired
    NescoServiceGateway nescoServiceGateway;

    private static final String EMPTY_STRING = "\"\"";

    @Override
    public BillInfoResponse getDpdcBill(DPDCBillInfo dpdcBillInfo, Locale locale) {
        String bill = apiManagerGateway.getDPDCBill(dpdcBillInfo, locale);
        return getBillInfoResponse(bill);
    }

    @Override
    public BillInfoResponse getDescoBill(String billNo, Locale locale) {
        if (!billNo.isEmpty()) {
            String bill = apiManagerGateway.getBillAmount(billNo, locale);
            return getBillInfoResponse(bill);
        } else {
            throw new EmptyInputException("Please provide a bill number");
        }
    }

    @Override
    public CommonResponse getDescoPrepaidInfo(String meterNo, String amount, Locale locale) {
        if (!meterNo.isEmpty() && !amount.isEmpty()) {
            String bill = apiManagerGateway.getDescoBillPrepaid(meterNo, amount, locale);
            bill = bill.replace("\\", "");
            CommonResponse response = new Gson().fromJson(bill, CommonResponse.class);
            if (Pattern.compile(Pattern.quote(TBLResponsePlaceHolder.DESCO_PREPAID_BREAKDOWN_SUCCESS),
                    Pattern.CASE_INSENSITIVE).matcher(response.getMessage()).find()) {
                return response;
            } else {
                throw new ApiManagerRequestException(response.getMessage());
            }
        } else {
            throw new EmptyInputException("Bill number and amount can not be empty");
        }
    }

    private BillInfoResponse getBillInfoResponse(String bill) {
        BillInfoResponse billInfoResponse = new BillInfoResponse();
        if (bill != null && !bill.equals(EMPTY_STRING)) {
            billInfoResponse.setBillAmount(bill.replace("\"", ""));
        } else {
            billInfoResponse.setBillAmount(bill);
        }
        return billInfoResponse;
    }

    @Override
    public PrepaidBillDetailsResponse getNescoPrepaidInfo(String customerAccountNumber, String customerNo, String meterNo, String amount, Locale locale) {
        PrepaidBillDetailsResponse commonResponse = new PrepaidBillDetailsResponse();
        BillPreviewRestInput input = new BillPreviewRestInput();
        input.setAccountNumber(customerAccountNumber);
        input.setBillerId("Nesco-Prepaid");
        input.setCode("NESPR");
        input.setKey1(customerNo);
        input.setKey2(meterNo);
        input.setKey3(amount);

        try {
            BillPreviewOutput billPreviewOutput = nescoServiceGateway.summaryPrepaidBillPreview(input);
            BaseResponse baseResponse = billPreviewOutput.getResponse();
            if(baseResponse != null) {
                if(baseResponse.getResponseCode().equals(CommonConstant.SUCCESS_CODE)){
                    List<HashMap<String, Object>> tableDatas = billPreviewOutput.getTableData();
                    List<String> feeDetailsPlaceholderList = Arrays.asList("Energy Cost,Rebate(1%),Last Error,VAT(5%),Paid Debt,Demand Charge(30/kW),PFC".split("\\s*,\\s*"));
                    StringBuilder feeDetails = new StringBuilder();
                    boolean firstElement = true;
                    for(int i=0; i<tableDatas.size(); i++){
                        for (String s : feeDetailsPlaceholderList) {
                            if(Pattern.compile(Pattern.quote(s), Pattern.CASE_INSENSITIVE).matcher(String.valueOf(tableDatas.get(i).get("label_en"))).find()){
                                if(!firstElement){
                                    feeDetails.append(" ; ");
                                }
                                firstElement = false;
                                feeDetails.append(String.valueOf(tableDatas.get(i).get("label_en").toString()));
                                feeDetails.append(": ");
                                feeDetails.append(String.valueOf(tableDatas.get(i).get("value").toString()));
                            }
                        }
                    }

                    commonResponse.setMessage(feeDetails.toString());
                    commonResponse.setTransactionId(String.valueOf(billPreviewOutput.getPayload().get("key4"))); // transaction id: key4
                    return commonResponse;
                } else {
                    throw new ApiRequestException(baseResponse.getResponseMessage());    
                }
            } else {
                throw new ApiRequestException(CommonConstant.COMMON_ERROR);
            }
        } catch (Exception e) {
            throw new ApiRequestException(e.getMessage());
        }
    }

    @Override
    public NescoBillInfoResponse getNescoBill(String customerAccountNumber, NescoBillInfo nescoBillInfo, Locale locale) {
        NescoBillInfoResponse billInfoResponse = new NescoBillInfoResponse();

        BillPreviewRestInput input = new BillPreviewRestInput();
        input.setAccountNumber(customerAccountNumber);
        input.setBillerId("Nesco-Postpaid");
        input.setCode("NESCO");
        input.setKey1(nescoBillInfo.getAccountNo());

        DateFormat monthFormat = new SimpleDateFormat("MM");
        DateFormat yearFormat = new SimpleDateFormat("yyyy");
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime (date); // set to the current time
        calendar.set(Calendar.MONTH, calendar.get (Calendar.MONTH)-1); // set to the previous month
        date = calendar.getTime();
            
        input.setKey2(nescoBillInfo.getBillMonth() != null ? nescoBillInfo.getBillMonth() : monthFormat.format(date));
        input.setKey3(nescoBillInfo.getBillYear() != null ? nescoBillInfo.getBillYear() : yearFormat.format(date));

        try {
            BillFetchResponse billFetchResponse = nescoServiceGateway.fetchPostpaidBillDetails(input);
            BaseResponse baseResponse = billFetchResponse.getResponse();
            if(baseResponse != null) {
                if(baseResponse.getResponseCode().equals(CommonConstant.SUCCESS_CODE)){
                    billInfoResponse.setBillAmount(billFetchResponse.getBillAmount());
                    billInfoResponse.setTransactionId(String.valueOf(billFetchResponse.getPayload().get("key5"))); // key5 for transaction id
                    
                    List<HashMap<String, Object>> tableDatas = billFetchResponse.getTableData();
                    for(int i=0; i<tableDatas.size(); i++){
                        HashMap<String, Object> item = tableDatas.get(i);
                        if(item.get("key").equals("customerName")){
                            billInfoResponse.setCustomerName(String.valueOf(item.get("value")));
                        } else if(item.get("key").equals("dueDate")) {
                            billInfoResponse.setDueDate(String.valueOf(item.get("value")));
                        }
                    }
                    return billInfoResponse;
                } else if (baseResponse.getResponseMessage().toLowerCase().contains("paid")) {
                    billInfoResponse.setBillAmount("-1");
                    return billInfoResponse;
                }else {
                    throw new ApiRequestException(baseResponse.getResponseMessage());    
                }
            } else {
                throw new ApiRequestException(CommonConstant.COMMON_ERROR);
            }
        } catch (Exception e) {
            throw new ApiRequestException(e.getMessage());
        }
    }

    @Override
    public PrepaidBillDetailsResponse getREBPrepaidInfo(String customerAccountNumber, String meterNo, String amount,
            Locale locale) {
        PrepaidBillDetailsResponse billDetailsResponse = new PrepaidBillDetailsResponse();
        // Todo: integrate with api

        billDetailsResponse.setMessage("Meter Rent 1P: 0; Demand Charge: 0; VAT: 61.9");
        billDetailsResponse.setTransactionId("123123123");
        return billDetailsResponse;
    }
}
