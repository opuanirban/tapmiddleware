package com.mfs.agent_middleware.service;

import java.util.List;
import java.util.Locale;

import com.mfs.agent_middleware.dto.LimitInfoResponse;
import com.mfs.agent_middleware.gateway.ApiManagerGateway;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LimitInfoServiceImpl implements LimitInfoService {

    @Autowired
    ApiManagerGateway apiManagerGateway;

    @Override
    public List<LimitInfoResponse> fetchLimit(String userAccount, Locale locale) {
        return apiManagerGateway.limitInfo(userAccount, locale);
    }
}
