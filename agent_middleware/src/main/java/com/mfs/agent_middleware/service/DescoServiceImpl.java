package com.mfs.agent_middleware.service;

import java.math.BigDecimal;
import java.util.Base64;
import java.util.Locale;
import java.util.regex.Pattern;

import com.mfs.agent_middleware.config.ApplicationProperties;
import com.mfs.agent_middleware.dto.ApiManagerRequest;
import com.mfs.agent_middleware.dto.CommonResponse;
import com.mfs.agent_middleware.dto.DescoPostPaidRequest;
import com.mfs.agent_middleware.dto.DescoPrepaidRequest;
import com.mfs.agent_middleware.enumeration.ContextUrl;
import com.mfs.agent_middleware.enumeration.TransactionEvents;
import com.mfs.agent_middleware.exception.DecodePinException;
import com.mfs.agent_middleware.exception.TransactionProcessException;
import com.mfs.agent_middleware.gateway.ApiManagerGateway;
import com.mfs.agent_middleware.service.security.ISecurityService;
import com.mfs.agent_middleware.util.CommonConstant;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

//import com.sun.org.apache.bcel.internal.generic.LLOAD;

@Service
@Slf4j
public class DescoServiceImpl extends ProcessRequestService implements DescoService {

    @Autowired
    ApiManagerGateway apiManagerGateway;

    @Autowired
    ISecurityService iSecurityService;

    @Autowired
    ApplicationProperties applicationProperties;

    @Autowired
    TransactionProcessService transactionProcessService;

    @Override
    public CommonResponse payDescoBill(DescoPrepaidRequest prepaidRequest, String accountNo, Locale locale) {
        if (applicationProperties.getCheckEnabled()) {
            if (transactionProcessService.findProcess(accountNo, prepaidRequest.getBillAmount(),prepaidRequest.getMeterNumber(), TransactionEvents.DESCO_PREPAID.toString())) {
                log.error("Same transaction within 10 minutes");
                throw new TransactionProcessException("User not allowed to do the same transaction within " +
                        applicationProperties.getDuplicateReqTimeOutInMills() / 60000 + " minutes of last transaction");
            } else {
                //return doP2P(p2PRequest, accountNo,locale);
                return doPayDescoBill(prepaidRequest, accountNo, locale);
            }
        } else {
            return doPayDescoBill(prepaidRequest, accountNo, locale);
        }

    }


    public CommonResponse doPayDescoBill(DescoPrepaidRequest prepaidRequest, String accountNo, Locale locale) {
        if (Pattern.matches("......==", prepaidRequest.getPin())) {
            String formattedText = "TRUSTMM DESPR " + prepaidRequest.getMeterNumber() + " " + prepaidRequest.getBillAmount() + " " + new String(Base64.getDecoder().decode(prepaidRequest.getPin())) + " " + CommonConstant.checkNumber(prepaidRequest.getNotificationNumber());
            ApiManagerRequest apiManagerRequest = makeProcessedRequest(applicationProperties.getEncryptionEnabled() ?
                    iSecurityService.encrypt(formattedText, "", "") : formattedText, ContextUrl.API_MANAGER_CONTEXT.getUrl(), accountNo);

            CommonResponse response = getTransactionFormattedResponse(apiManagerGateway, apiManagerRequest, locale, true);

            try {
                transactionProcessService.storeProcess(accountNo, prepaidRequest.getBillAmount(),prepaidRequest.getMeterNumber(), TransactionEvents.DESCO_PREPAID.toString());
            } catch (Exception e) {
                log.error(e.getMessage());
            }
            return response;

        } else {
            throw new DecodePinException(CommonConstant.DECODE_EXCEPTION_MESSAGE);
        }
    }

    @Override
    public CommonResponse payDescoBill(DescoPostPaidRequest postPaidRequest, String accountNo, Locale locale) {
        if (applicationProperties.getCheckEnabled()) {
            if (transactionProcessService.findProcess(accountNo, new BigDecimal(0),postPaidRequest.getBillNumber(), TransactionEvents.DESCO_POSTPAID.toString())) {
                log.error("Same transaction within 10 minutes");
                throw new TransactionProcessException("User not allowed to do the same transaction within " +
                        applicationProperties.getDuplicateReqTimeOutInMills() / 60000 + " minutes of last transaction");
            } else {
                return doPayDescoBill(postPaidRequest, accountNo, locale);
            }
        } else {
            return doPayDescoBill(postPaidRequest, accountNo, locale);
        }

    }

    public CommonResponse doPayDescoBill(DescoPostPaidRequest postPaidRequest, String accountNo, Locale locale) {
        if (Pattern.matches("......==", postPaidRequest.getPin())) {
            String formattedText = "TRUSTMM DESCO " + postPaidRequest.getBillNumber() + " " + new String(Base64.getDecoder().decode(postPaidRequest.getPin())) + " " + CommonConstant.checkNumber(postPaidRequest.getNotificationNumber());
            ApiManagerRequest apiManagerRequest = makeProcessedRequest(applicationProperties.getEncryptionEnabled() ?
                    iSecurityService.encrypt(formattedText, "", "") : formattedText, ContextUrl.API_MANAGER_CONTEXT.getUrl(), accountNo);

            CommonResponse response = getTransactionFormattedResponse(apiManagerGateway, apiManagerRequest, locale, true);

            try {
                transactionProcessService.storeProcess(accountNo, new BigDecimal(0),postPaidRequest.getBillNumber(), TransactionEvents.DESCO_POSTPAID.toString());
            } catch (Exception e) {
                log.error(e.getMessage());
            }
            return response;

        } else {
            throw new DecodePinException(CommonConstant.DECODE_EXCEPTION_MESSAGE);
        }


    }
}
