package com.mfs.agent_middleware.service;

import java.math.BigDecimal;
import java.util.Base64;
import java.util.Locale;
import java.util.regex.Pattern;

import com.mfs.agent_middleware.config.ApplicationProperties;
import com.mfs.agent_middleware.dto.ApiManagerRequest;
import com.mfs.agent_middleware.dto.CommonResponse;
import com.mfs.agent_middleware.dto.TopUpRequest;
import com.mfs.agent_middleware.dto.TopUpRequest2;
import com.mfs.agent_middleware.dto.TopUpResponse;
import com.mfs.agent_middleware.enumeration.ContextUrl;
import com.mfs.agent_middleware.enumeration.TransactionEvents;
import com.mfs.agent_middleware.exception.ApiManagerRequestException;
import com.mfs.agent_middleware.exception.DecodePinException;
import com.mfs.agent_middleware.exception.TransactionProcessException;
import com.mfs.agent_middleware.gateway.ApiManagerGateway;
import com.mfs.agent_middleware.service.security.ISecurityService;
import com.mfs.agent_middleware.util.CommonConstant;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class TopUpServiceImpl extends ProcessRequestService implements TopUpService {

    @Autowired
    ApiManagerGateway apiManagerGateway;

    @Autowired
    ISecurityService iSecurityService;

    @Autowired
    ApplicationProperties applicationProperties;

    @Autowired
    TransactionProcessService transactionProcessService;

    @Override
    public CommonResponse sendTopUp(TopUpRequest topUpRequest, String accountNo, Locale locale) {

        if (applicationProperties.getCheckEnabled()) {
            if (transactionProcessService.findProcess(accountNo, BigDecimal.valueOf(topUpRequest.getAmount()),topUpRequest.getRecipientNumber(), TransactionEvents.TOP_UP.toString())) {
                log.error("Same transaction within " + applicationProperties.getDuplicateReqTimeOutInMills() / 60000 + " minutes");
                throw new TransactionProcessException("User not allowed to do the same transaction within " +
                        applicationProperties.getDuplicateReqTimeOutInMills() / 60000 + " minutes of last transaction");
            } else {
//                return doTopUp(topUpRequest, accountNo, locale);
                return doTopUpSSL(topUpRequest, accountNo, locale);
            }
        } else {
//            return doTopUp(topUpRequest, accountNo, locale);
            return doTopUpSSL(topUpRequest, accountNo, locale);
        }
    }

    private CommonResponse doTopUp(TopUpRequest topUpRequest, String accountNo, Locale locale) {
        if (Pattern.matches("......==", topUpRequest.getPin())) {
            String formattedText = "Trustmm Topup " + CommonConstant.checkNumber(topUpRequest.getRecipientNumber()) + " " + topUpRequest.getAmount() + " " + new String(Base64.getDecoder().decode(topUpRequest.getPin())) + " " + topUpRequest.getConnectionType().getConnection() + " " + topUpRequest.getOperator();
            ApiManagerRequest apiManagerRequest = makeProcessedRequest(applicationProperties.getEncryptionEnabled() ?
                    iSecurityService.encrypt(formattedText, "", "") : formattedText, ContextUrl.API_MANAGER_CONTEXT.getUrl(), accountNo);

            CommonResponse response = getTransactionFormattedResponse(apiManagerGateway, apiManagerRequest, locale, true);

            try {
                transactionProcessService.storeProcess(accountNo, BigDecimal.valueOf(topUpRequest.getAmount()),topUpRequest.getRecipientNumber(), TransactionEvents.TOP_UP.toString());
            } catch (Exception e) {
                log.error(e.getMessage());
            }

            return response;

        } else {
            throw new DecodePinException(CommonConstant.DECODE_EXCEPTION_MESSAGE);
        }
    }

    private CommonResponse doTopUpSSL(TopUpRequest topUpRequest, String accountNo, Locale locale){
        CommonResponse commonResponse = new CommonResponse();

        TopUpRequest2 topUpRequest2 = new TopUpRequest2();
        topUpRequest2.setOperatorId(CommonConstant.TOPUP_OPERATOR.get(topUpRequest.getOperator()));
        topUpRequest2.setAmount(topUpRequest.getAmount());
        topUpRequest2.setConnectionType(topUpRequest.getConnectionType().name());
        topUpRequest2.setRecipientMsisdn(CommonConstant.checkNumber(topUpRequest.getRecipientNumber()));
        topUpRequest2.setFromAccount(CommonConstant.checkNumber(accountNo));
        topUpRequest2.setPin(topUpRequest.getPin());

        TopUpResponse topUpResponse = apiManagerGateway.requestTopUp(topUpRequest2, locale);
        log.debug("topup response: {}", topUpResponse);
        if(topUpResponse.getCode() == 200){
            commonResponse.setMessage(topUpResponse.getMessage());
            return  commonResponse;
        } else {
            log.error("recharge failed for msisdn:{} with error message:{}", accountNo, topUpResponse.getMessage());
            throw new ApiManagerRequestException(topUpResponse.getMessage());
        }
    }
}
