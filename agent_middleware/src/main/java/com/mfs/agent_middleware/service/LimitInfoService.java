package com.mfs.agent_middleware.service;

import java.util.List;
import java.util.Locale;

import com.mfs.agent_middleware.dto.LimitInfoResponse;

public interface LimitInfoService {
    List<LimitInfoResponse> fetchLimit(String userAccount, Locale locale);
}
