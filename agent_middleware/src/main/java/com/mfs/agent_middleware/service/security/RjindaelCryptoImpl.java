package com.mfs.agent_middleware.service.security;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.spec.SecretKeySpec;

import com.mfs.agent_middleware.exception.CryptographyRequestException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class RjindaelCryptoImpl implements ISecurityService {
    @Value("${rjindael.key}")
    public String KEY;
    @Value("${rjindael.salt}")
    public String SALT;

    //    public static final String KEY = "feraree";
//    public static final String SALT = "feraree";
    private final static Pattern CODE_PATTERN = Pattern.compile("%[0-9A-Fa-f]{2}");

    @Autowired
    AESService aesService;

    /*
        param1: input text
        param2: key
        param3: salt
    */
    @Override
    public String encrypt(String param1, String param2, String param3) {
        try {
            System.out.println(param1);
            PasswordDeriveBytes deriveBytes = new PasswordDeriveBytes(KEY.getBytes(StandardCharsets.US_ASCII),
                    SALT.getBytes(StandardCharsets.US_ASCII));
            byte[] secretKey = deriveBytes.GetBytes(32);
            byte[] iv = deriveBytes.GetBytes(16);
//            System.out.println(Arrays.toString(secretKey));
            SecretKeySpec finalKey = new SecretKeySpec(secretKey, "AES");
            String response = AESService.encrypt(param1, finalKey, iv);
            System.out.println(URLEncoder.encode(response, "UTF-8"));
            return recode(URLEncoder.encode(response, "UTF-8"));
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new CryptographyRequestException("Encrypt failed");
        }
    }

    /*
        param1: input text
        param2: key
        param3: salt
    */
    @Override
    public String decrypt(String param1, String param2, String param3) {
        try {
            PasswordDeriveBytes deriveBytes = new PasswordDeriveBytes(KEY.getBytes(StandardCharsets.US_ASCII),
                    SALT.getBytes(StandardCharsets.US_ASCII));
            byte[] secretKey = deriveBytes.GetBytes(32);
            byte[] iv = deriveBytes.GetBytes(16);
            SecretKeySpec finalKey = new SecretKeySpec(secretKey, "AES");
            return aesService.decrypt(param1, finalKey, iv);
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new CryptographyRequestException("Decrypt failed");
        }
    }

    public String recode(String urlString) {
        StringBuffer sb = new StringBuffer();
        Matcher m = CODE_PATTERN.matcher(urlString);

        while (m.find()) {
            m.appendReplacement(sb, m.group().toLowerCase());
        }
        m.appendTail(sb);

        return sb.toString();
    }
}
