package com.mfs.agent_middleware.service;

import java.util.Base64;
import java.util.Locale;
import java.util.regex.Pattern;

import com.mfs.agent_middleware.config.ApplicationProperties;
import com.mfs.agent_middleware.dto.ApiManagerRequest;
import com.mfs.agent_middleware.dto.CommonResponse;
import com.mfs.agent_middleware.dto.InsurancePaymentRequest;
import com.mfs.agent_middleware.enumeration.ContextUrl;
import com.mfs.agent_middleware.enumeration.TransactionEvents;
import com.mfs.agent_middleware.exception.DecodePinException;
import com.mfs.agent_middleware.exception.TransactionProcessException;
import com.mfs.agent_middleware.gateway.ApiManagerGateway;
import com.mfs.agent_middleware.service.security.ISecurityService;
import com.mfs.agent_middleware.util.CommonConstant;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class InsuranceServiceImpl extends ProcessRequestService implements InsuranceService {

    @Autowired
    ApiManagerGateway apiManagerGateway;

    @Autowired
    ISecurityService iSecurityService;
    @Autowired
    ApplicationProperties applicationProperties;

    @Autowired
    TransactionProcessService transactionProcessService;

    @Override
    public CommonResponse insurancePayment(InsurancePaymentRequest insurancePaymentRequest, String accountNo, Locale locale) {

        if (applicationProperties.getCheckEnabled()) {
            if (transactionProcessService.findProcess(accountNo, insurancePaymentRequest.getAmount(), insurancePaymentRequest.getPolicyNo(), TransactionEvents.INSURANCE+insurancePaymentRequest.getCode()
                    +insurancePaymentRequest.getPolicyNo())) {
                log.error("Same transaction within 10 minutes");
                throw new TransactionProcessException("User not allowed to do the same transaction within " +
                        applicationProperties.getDuplicateReqTimeOutInMills() / 60000 + " minutes of last transaction");
            } else {
                return doInsurancePayment(insurancePaymentRequest, accountNo, locale);
            }
        } else {
            return doInsurancePayment(insurancePaymentRequest, accountNo, locale);
        }

    }


    public CommonResponse doInsurancePayment(InsurancePaymentRequest insurancePaymentRequest, String accountNo, Locale locale) {

        if (Pattern.matches("......==", insurancePaymentRequest.getPin())) {
            String formattedText = "TRUSTMM " + insurancePaymentRequest.getCode() + " " + insurancePaymentRequest.getPolicyNo() + " " + insurancePaymentRequest.getAmount() + " " + new String(Base64.getDecoder().decode(insurancePaymentRequest.getPin())) + " " + CommonConstant.checkNumber(insurancePaymentRequest.getNotificationNumber());
            ApiManagerRequest apiManagerRequest = makeProcessedRequest(applicationProperties.getEncryptionEnabled() ?
                    iSecurityService.encrypt(formattedText, "", "") : formattedText, ContextUrl.API_MANAGER_CONTEXT.getUrl(), accountNo);

            CommonResponse response = getTransactionFormattedResponse(apiManagerGateway, apiManagerRequest, locale, true);
            try {
                transactionProcessService.storeProcess(accountNo, insurancePaymentRequest.getAmount(),insurancePaymentRequest.getPolicyNo(), TransactionEvents.INSURANCE+insurancePaymentRequest.getCode()
                        +insurancePaymentRequest.getPolicyNo());
            } catch (Exception e) {
                log.error(e.getMessage());
            }
            return response;

        } else {
            throw new DecodePinException(CommonConstant.DECODE_EXCEPTION_MESSAGE);
        }


    }
}
