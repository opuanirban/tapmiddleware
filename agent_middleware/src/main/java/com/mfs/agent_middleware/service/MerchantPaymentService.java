package com.mfs.agent_middleware.service;

import java.util.Locale;

import com.mfs.agent_middleware.dto.CommonResponse;
import com.mfs.agent_middleware.dto.DonationPayment;
import com.mfs.agent_middleware.dto.MerchantPayment;

public interface MerchantPaymentService {
    public CommonResponse payMerchant(MerchantPayment merchantPayment, String account, Locale locale);
    public CommonResponse payDonation(DonationPayment donationPaymentPayment, String account, Locale locale);
}
