package com.mfs.agent_middleware.service;

import java.util.Locale;

import com.mfs.agent_middleware.dto.CashInRequest;
import com.mfs.agent_middleware.dto.CommonResponse;

public interface CashInService {
    public CommonResponse cashIn(CashInRequest cashInRequest, String accountNo, Locale locale);
}
