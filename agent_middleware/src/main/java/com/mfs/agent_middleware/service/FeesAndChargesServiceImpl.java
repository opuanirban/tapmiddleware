package com.mfs.agent_middleware.service;

import java.util.Locale;

import com.google.gson.Gson;
import com.mfs.agent_middleware.dto.FeesAndChargesModel;
import com.mfs.agent_middleware.dto.FeesAndChargesModelWallet;
import com.mfs.agent_middleware.dto.FeesChargesResponse;
import com.mfs.agent_middleware.exception.InvalidTransactionException;
import com.mfs.agent_middleware.gateway.ApiManagerGateway;
import com.mfs.agent_middleware.util.CommonConstant;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class FeesAndChargesServiceImpl implements FeesAndChargesService {

    @Autowired
    ApiManagerGateway apiManagerGateway;

    @Override
    public FeesChargesResponse feeAndCharge(FeesAndChargesModelWallet feesAndChargesModelWallet, Locale locale) {

        FeesAndChargesModel feesAndChargesModel = new FeesAndChargesModel();
        feesAndChargesModel.setFromUserNumber(feesAndChargesModelWallet.getFromUserNumber());
        if(feesAndChargesModelWallet.getToUserNumber() != null && !feesAndChargesModelWallet.getToUserNumber().isEmpty()) {
            feesAndChargesModel.setToUserNumber(feesAndChargesModelWallet.getToUserNumber());
        }

        feesAndChargesModel.setAmount(feesAndChargesModelWallet.getAmount());
        feesAndChargesModel.setTransactionType(feesAndChargesModelWallet.getTransactionType());

        feesAndChargesModel.setFromUserNumber(CommonConstant.checkNumber(feesAndChargesModel.getFromUserNumber()));
        if(feesAndChargesModel.getTransactionType().equalsIgnoreCase("FCCO")) {
            feesAndChargesModel.setToUserNumber("0");
        } else if (feesAndChargesModel.getToUserNumber() != null && !feesAndChargesModel.getToUserNumber().isEmpty()) {
            feesAndChargesModel.setToUserNumber(CommonConstant.checkNumber(feesAndChargesModel.getToUserNumber()));
        }else {
            feesAndChargesModel.setToUserNumber("0");
        }

        String apiResponse = (apiManagerGateway.feeAndCharges(feesAndChargesModel, locale));
        if (apiResponse != null && !apiResponse.isEmpty()) {
            FeesChargesResponse response = new Gson().fromJson(apiResponse.replace("\\", ""), FeesChargesResponse.class);
            return response;
        } else {
            throw new InvalidTransactionException("You are not allowed to make this transaction");
        }

    }
}
