package com.mfs.agent_middleware.service;

import java.util.Locale;

import com.mfs.agent_middleware.dto.BaseResponse;
import com.mfs.agent_middleware.dto.CommonResponse;
import com.mfs.agent_middleware.dto.UserLoginInfo;
import com.mfs.agent_middleware.dto.UserRegistrationRequest;
import com.mfs.agent_middleware.dto.UserStatusInfo;
import com.mfs.agent_middleware.gateway.model.AgentInfoDto;

public interface UserRegistrationService {
    public UserStatusInfo checkForUser(String msisdn, Locale locale);
    public UserLoginInfo fetchUserLoginInfo(String msisdn, String pin);
    BaseResponse registerAgent(AgentInfoDto agentInfo, Locale locale);
    CommonResponse userBasicRegister(UserRegistrationRequest registrationParams,String agent_number, Locale locale);
}
