package com.mfs.agent_middleware.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.mfs.agent_middleware.config.ApplicationProperties;
import com.mfs.agent_middleware.dto.MiniStatementResponse;
import com.mfs.agent_middleware.dto.MiniStatementResponseEntity;
import com.mfs.agent_middleware.gateway.ApiManagerGateway;
import com.mfs.agent_middleware.service.security.ISecurityService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class MiniStatementServiceImpl extends ProcessRequestService implements MinistatementService {
    @Autowired
    ApiManagerGateway apiManagerGateway;

    @Autowired
    ISecurityService securityService;

    @Autowired
    ApplicationProperties applicationProperties;

    @Autowired
    MessageRetrieverService messageRetrieverService;


    @Override
    public MiniStatementResponse fetchMiniStatement(String userAccount,Locale locale) {
        MiniStatementResponse miniStatementResponse = new MiniStatementResponse();
        List<MiniStatementResponseEntity> miniStatementResponseEntityList = new ArrayList<>();

        miniStatementResponseEntityList = apiManagerGateway.miniStatement(userAccount, locale);
        miniStatementResponse.setStatement(miniStatementResponseEntityList);
        return miniStatementResponse;
    }
}
