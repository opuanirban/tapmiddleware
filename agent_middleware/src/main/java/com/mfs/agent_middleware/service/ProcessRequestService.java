package com.mfs.agent_middleware.service;

import java.util.Locale;
import java.util.regex.Pattern;

import com.mfs.agent_middleware.config.ApplicationProperties;
import com.mfs.agent_middleware.dto.ApiManagerRequest;
import com.mfs.agent_middleware.dto.CommonResponse;
import com.mfs.agent_middleware.exception.ApiManagerRequestException;
import com.mfs.agent_middleware.gateway.ApiManagerGateway;
import com.mfs.agent_middleware.util.CommonConstant;

import org.springframework.beans.factory.annotation.Autowired;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ProcessRequestService {
    @Autowired
    ApplicationProperties applicationProperties;

    public ApiManagerRequest makeProcessedRequest(String formattedText, String contextPath, String accountNo){
        ApiManagerRequest apiManagerRequest = new ApiManagerRequest();
        apiManagerRequest.setMsgId(1);
        apiManagerRequest.setUserNumber(CommonConstant.checkNumber(accountNo));
        apiManagerRequest.setShortCode("app");
        apiManagerRequest.setTelcoId(1);
        apiManagerRequest.setContext(contextPath);
        apiManagerRequest.setSmsText(formattedText);

        return apiManagerRequest;
    }

    public CommonResponse getTransactionFormattedResponse(ApiManagerGateway apiManagerGateway, ApiManagerRequest apiManagerRequest, Locale locale, Boolean isTransaction){
        CommonResponse response = new CommonResponse();

        String responseFromApi = apiManagerGateway.apiManagerRequestMethod(apiManagerRequest,locale, isTransaction)
                .replace("\\", "");
        if(isMatched(responseFromApi)) {
            response.setMessage(responseFromApi);
        }else {
            throw new ApiManagerRequestException(responseFromApi);
        }

        return response;
    }

    private boolean isMatched(String responseMessage){
        boolean match = false;
        for (String s : applicationProperties.getSuccessPlaceholderList()) {
            if(Pattern.compile(Pattern.quote(s), Pattern.CASE_INSENSITIVE).matcher(responseMessage).find() &&
             !responseMessage.toLowerCase().contains("insufficient balance")){
                match = true;
                break;
            }
        }
        return match;
    }
}
