package com.mfs.agent_middleware.service;

import java.util.Locale;

import com.mfs.agent_middleware.dto.CommonResponse;
import com.mfs.agent_middleware.dto.FeatureHidePermissionRequest;
import com.mfs.agent_middleware.dto.ServiceAvailibilityInfo;
import com.mfs.agent_middleware.dto.ServiceConfigResponse;

public interface ServiceConfigService {
    public ServiceConfigResponse getConfiguration();
    public CommonResponse setHiddenFeatureVersion(FeatureHidePermissionRequest featureHidePermissionRequest, Locale locale);
    public void setServiceAvailibility(ServiceAvailibilityInfo serviceAvailibilityInfo);
}
