package com.mfs.agent_middleware.service;

import java.util.Locale;

import com.mfs.agent_middleware.dto.BillInfoResponse;
import com.mfs.agent_middleware.dto.CommonResponse;
import com.mfs.agent_middleware.dto.DPDCBillInfo;
import com.mfs.agent_middleware.dto.NescoBillInfo;
import com.mfs.agent_middleware.dto.NescoBillInfoResponse;
import com.mfs.agent_middleware.dto.PrepaidBillDetailsResponse;

public interface BillInfoService {
    public BillInfoResponse getDpdcBill(DPDCBillInfo dpdcBillInfo, Locale locale);
    public NescoBillInfoResponse getNescoBill(String customerAccountNumber, NescoBillInfo nescoBillInfo, Locale locale);
    public BillInfoResponse getDescoBill(String billNo,Locale locale);
    public CommonResponse getDescoPrepaidInfo(String meterNo, String amount, Locale locale);
    public PrepaidBillDetailsResponse getNescoPrepaidInfo(String customerAccountNumber, String customerNo, String meterNo, String amount, Locale locale);
    public PrepaidBillDetailsResponse getREBPrepaidInfo(String customerAccountNumber, String meterNo, String amount, Locale locale);
}
