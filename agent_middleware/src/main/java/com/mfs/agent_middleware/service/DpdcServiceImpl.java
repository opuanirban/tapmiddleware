package com.mfs.agent_middleware.service;

import java.util.Base64;
import java.util.Locale;
import java.util.regex.Pattern;

import com.mfs.agent_middleware.dto.ApiManagerRequest;
import com.mfs.agent_middleware.dto.CommonResponse;
import com.mfs.agent_middleware.dto.DPDCBillPayRequest;
import com.mfs.agent_middleware.enumeration.ContextUrl;
import com.mfs.agent_middleware.exception.DecodePinException;
import com.mfs.agent_middleware.gateway.ApiManagerGateway;
import com.mfs.agent_middleware.service.security.ISecurityService;
import com.mfs.agent_middleware.util.CommonConstant;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class DpdcServiceImpl extends ProcessRequestService implements DpdcService {

    @Autowired
    ApiManagerGateway apiManagerGateway;

    @Autowired
    ISecurityService iSecurityService;

    @Override
    public CommonResponse payDpdcBill(DPDCBillPayRequest dpdcBillPayRequest, String accountNo, Locale locale) {
        if (Pattern.matches("......==", dpdcBillPayRequest.getPin())) {
            String formattedText = "TRUSTMM DPDC " + dpdcBillPayRequest.getAccountNumber() + " " + dpdcBillPayRequest.getLocationCode() + " " + dpdcBillPayRequest.getBillMonth() + " " + new String(Base64.getDecoder().decode(dpdcBillPayRequest.getPin())) + " " + CommonConstant.checkNumber(dpdcBillPayRequest.getNotificationNumber());
            ApiManagerRequest apiManagerRequest = makeProcessedRequest(applicationProperties.getEncryptionEnabled() ?
                    iSecurityService.encrypt(formattedText, "", "") : formattedText, ContextUrl.API_MANAGER_CONTEXT.getUrl(), accountNo);

            CommonResponse response = getTransactionFormattedResponse(apiManagerGateway, apiManagerRequest, locale, true);
            return response;

        } else {
            throw new DecodePinException(CommonConstant.DECODE_EXCEPTION_MESSAGE);
        }


    }
}
