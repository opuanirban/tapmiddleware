package com.mfs.agent_middleware.service;

import java.util.Locale;

public interface MessageRetrieverService {
    String getMessage(Locale locale,String keyName);
}
