package com.mfs.agent_middleware.service;

import java.math.BigDecimal;

import com.mfs.agent_middleware.dao.RedisDao;
import com.mfs.agent_middleware.enumeration.TransactionEvents;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class TransactionProcessServiceImpl implements TransactionProcessService {


    public static final String DATA_SEPARATOR = "#";

    @Value("${process.timeout.in.mills}")
    private Integer timeout;

    @Value("${msisdn.blacklist.timeout.in.seconds}")
    private Integer blackListTimeout;

    @Autowired
    RedisDao redisDao;

    @Override
    public String storeProcess(String userNo, BigDecimal amount, String event) {
        String key = userNo + DATA_SEPARATOR + amount.toString() + DATA_SEPARATOR + event;
        redisDao.setValueWithTimeout(key, "", timeout / 1000);
        return redisDao.getValue(key);
    }

    @Override
    public String storeProcess(String userNo, BigDecimal amount,String referenceNo, String event) {
        String key = userNo + DATA_SEPARATOR + amount.toString() + DATA_SEPARATOR +referenceNo + DATA_SEPARATOR + event;
        redisDao.setValueWithTimeout(key, "", timeout / 1000);
        return redisDao.getValue(key);
    }

    @Override
    public Boolean findProcess(String userNo, BigDecimal amount, String event) {
        String key = userNo + DATA_SEPARATOR + amount.toString() + DATA_SEPARATOR + event;
        if (redisDao.getValue(key) != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public Boolean findProcess(String userNo, BigDecimal amount,String referenceNo, String event) {
        String key = userNo + DATA_SEPARATOR + amount.toString() +DATA_SEPARATOR + referenceNo + DATA_SEPARATOR + event;
        if (redisDao.getValue(key) != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public Integer findOtpCount(String userNo) {
        String key = userNo + DATA_SEPARATOR + TransactionEvents.OTP_EVENT;
        if (redisDao.getValue(key) != null) {
            return Integer.parseInt(redisDao.getValue(key));
        } else {
            return 0;
        }
    }

    @Override
    public void addToBlackList(String userNo) {
        String key = userNo + DATA_SEPARATOR + TransactionEvents.BLACKLIST;
        redisDao.setValueWithTimeout(key, "", blackListTimeout);
    }

    @Override
    public Boolean findInBlackList(String userNo) {
        String key = userNo + DATA_SEPARATOR + TransactionEvents.BLACKLIST;
        if (redisDao.getValue(key) != null) {
            return true;
        } else {
            return false;
        }
    }

}
