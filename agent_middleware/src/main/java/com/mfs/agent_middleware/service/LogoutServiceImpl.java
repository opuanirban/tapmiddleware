package com.mfs.agent_middleware.service;

import com.mfs.agent_middleware.config.ApplicationProperties;
import com.mfs.agent_middleware.dao.RedisDao;
import com.mfs.agent_middleware.dto.CommonResponse;
import com.mfs.agent_middleware.exception.ApiRequestException;
import com.mfs.agent_middleware.util.CommonConstant;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LogoutServiceImpl implements LogoutService {

    @Autowired
    RedisDao redisDao;

    @Autowired
    ApplicationProperties applicationProperties;


    @Override
    public CommonResponse addTokenToblacklist(String jwt) {

        String blackListedJwt = CommonConstant.LOGOUT_JWT + jwt;
        try {

            redisDao.setValueWithTimeout(blackListedJwt,"1",applicationProperties.getJwtExpireTime());
            System.out.println(redisDao.getValue(blackListedJwt));
            CommonResponse commonResponse= new CommonResponse();
            commonResponse.setMessage("Successfully Logged out");
            return commonResponse;

        }
        catch (Exception e){
            e.printStackTrace();
            throw new ApiRequestException("Logout Unsuccessful!");
        }
    }
}
