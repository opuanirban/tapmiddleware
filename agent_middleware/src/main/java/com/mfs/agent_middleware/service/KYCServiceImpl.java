package com.mfs.agent_middleware.service;

import java.nio.charset.CharsetEncoder;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Locale;

import com.mfs.agent_middleware.dto.KYCRegistrationResponse;
import com.mfs.agent_middleware.dto.KYCStatusResponse;
import com.mfs.agent_middleware.dto.NIDUploadApiResponse;
import com.mfs.agent_middleware.dto.NIDVerificationResponse;
import com.mfs.agent_middleware.exception.ApiRequestException;
import com.mfs.agent_middleware.exception.GigatechException;
import com.mfs.agent_middleware.gateway.KYCGateway;
import com.mfs.agent_middleware.util.CommonConstant;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class KYCServiceImpl implements KYCService {

    @Autowired
    KYCGateway kycGateway;

    @Override
    public NIDUploadApiResponse uploadNID(MultipartFile id_front, MultipartFile id_back,String msisdn, Boolean isNewRegistration, Locale locale) {
        return kycGateway.nidUpload(id_front, id_back,msisdn,isNewRegistration,locale);
    }

    @Override
    public KYCRegistrationResponse kycRegister(String nid_no, String dob, String applicant_name_ben, String applicant_name_eng, String father_name, String mother_name, String spouse_name, String pres_address, String perm_address, String id_front_name, String id_back_name, String gender, String profession, String nominee, String nominee_relation, String mobile_number, MultipartFile applicant_photo, Locale locale){
        // Check existing request status
        NIDVerificationResponse nidVerificationResponse = verifyNidStatus(mobile_number, locale);
        if(nidVerificationResponse.getStatus().equalsIgnoreCase(CommonConstant.SUCCESS)) {
            if(nidVerificationResponse.getData().getStatus().equalsIgnoreCase(CommonConstant.EC_REQUESTED) || nidVerificationResponse.getData().getStatus().equalsIgnoreCase(CommonConstant.PENDING)) {
                throw new GigatechException("Your previous KYC verification request is in progress, please wait for verification to complete");    
            } else if(nidVerificationResponse.getData().getStatus().equalsIgnoreCase(CommonConstant.PASSED)) {
                throw new GigatechException("Your previous KYC verification request is already verified, please login again to access all services. Thank You"); 
            }
        }
        checkEkycRequestValidation(nid_no, dob, applicant_name_eng, applicant_photo);
        return kycGateway.customerRegistration(nid_no, dob, applicant_name_ben, applicant_name_eng, father_name, mother_name, spouse_name, pres_address, perm_address, id_front_name, id_back_name, gender, profession, nominee, nominee_relation, mobile_number, applicant_photo,locale);  
    }

    @Override
    public NIDVerificationResponse verifyNidStatus(String mobile_number,Locale locale) {
        return kycGateway.nidVerifyStatus(mobile_number,locale);
    }

    @Override
    public String kycStatusCheck(String user_numeber, Locale locale) {
        return kycGateway.getUserRegistrationStatus(user_numeber,locale);
    }

    private void checkEkycRequestValidation(String nidNumber, String dob, String applicationNameEng, MultipartFile applicatantPhoto){
        if(nidNumber == null || nidNumber.isEmpty()) {
            throw new ApiRequestException("User NID number not present");
        } else {
            int nidLength = nidNumber.length();
            if(!(nidLength == 10 || nidLength == 13 || nidLength == 17)) {
                throw new ApiRequestException("Invalid NID Number");
            }
        }

        if(dob == null || dob.isEmpty()) {
            throw new ApiRequestException("User Date of Birth can not be empty");
        } else {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
	        dateFormat.setLenient(false);
            try {
                dateFormat.parse(dob);
            } catch (Exception e) {
                throw new ApiRequestException("User Date of Birth is not in currect format");
            }
        }

        CharsetEncoder asciiEncoder = StandardCharsets.US_ASCII.newEncoder();
        CharsetEncoder isoEncoder = StandardCharsets.ISO_8859_1.newEncoder();
        if(applicationNameEng == null || applicationNameEng.isEmpty()){
            throw new ApiRequestException("User English Name not present");
        }else if(!(asciiEncoder.canEncode(applicationNameEng) || isoEncoder.canEncode(applicationNameEng))) {
            throw new ApiRequestException("User English Name is not in current format");
        }
    }
}
