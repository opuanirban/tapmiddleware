package com.mfs.agent_middleware.service;

import com.mfs.agent_middleware.dto.CommonResponse;

import org.springframework.web.multipart.MultipartFile;

public interface ErrorMessageUploader {
    public CommonResponse uploadFile(MultipartFile file);
}
