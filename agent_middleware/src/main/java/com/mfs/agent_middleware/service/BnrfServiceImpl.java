package com.mfs.agent_middleware.service;

import java.util.Base64;
import java.util.Locale;

import com.mfs.agent_middleware.dto.ApiManagerRequest;
import com.mfs.agent_middleware.dto.BnrfParam;
import com.mfs.agent_middleware.dto.CommonResponse;
import com.mfs.agent_middleware.enumeration.ContextUrl;
import com.mfs.agent_middleware.gateway.ApiManagerGateway;
import com.mfs.agent_middleware.service.security.ISecurityService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BnrfServiceImpl extends ProcessRequestService implements BnrfService{

    @Autowired
    ApiManagerGateway apiManagerGateway;

    @Autowired
    ISecurityService iSecurityService;

    @Override
    public CommonResponse bnrfRequest(BnrfParam bnrfParam,String accountNo,Locale locale) {

        String formattedText = "TRUSTMM BNRF " + bnrfParam.getRequirementType() + " " + new String(Base64.getDecoder().decode(bnrfParam.getPin()))+ " " +bnrfParam.getNotificationNumber();
        ApiManagerRequest apiManagerRequest = makeProcessedRequest(applicationProperties.getEncryptionEnabled()?
                iSecurityService.encrypt(formattedText,"",""):formattedText, ContextUrl.API_MANAGER_CONTEXT.getUrl(), accountNo);

        return getTransactionFormattedResponse(apiManagerGateway, apiManagerRequest, locale, true);
    }
}
