package com.mfs.agent_middleware.service;

import java.util.Locale;

import com.mfs.agent_middleware.dto.CommonResponse;
import com.mfs.agent_middleware.dto.DescoPostPaidRequest;
import com.mfs.agent_middleware.dto.DescoPrepaidRequest;

public interface DescoService {
    public CommonResponse payDescoBill(DescoPrepaidRequest prepaidRequest, String accountNo, Locale locale);

    public CommonResponse payDescoBill(DescoPostPaidRequest postPaidRequest,String accountNo,Locale locale);
}
