package com.mfs.agent_middleware.service;

import java.util.Locale;

import com.mfs.agent_middleware.dto.CommonResponse;
import com.mfs.agent_middleware.dto.NIDFeePayment;
import com.mfs.agent_middleware.dto.NidServiceListParams;
import com.mfs.agent_middleware.dto.PassportFeePayment;

public interface PassportPaymentService {
    public CommonResponse payPassportFee(PassportFeePayment passportNidFeePayment, String account, Locale locale);

    public CommonResponse payNIDFee(NIDFeePayment nidFeePayment, String account, Locale locale);

    public NidServiceListParams[] getNidServices(Locale locale);
}
