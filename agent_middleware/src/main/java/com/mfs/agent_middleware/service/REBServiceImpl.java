package com.mfs.agent_middleware.service;

import java.util.Base64;
import java.util.Locale;
import java.util.regex.Pattern;

import com.mfs.agent_middleware.config.ApplicationProperties;
import com.mfs.agent_middleware.dto.ApiManagerRequest;
import com.mfs.agent_middleware.dto.CommonResponse;
import com.mfs.agent_middleware.dto.REBPrepaidRequest;
import com.mfs.agent_middleware.enumeration.ContextUrl;
import com.mfs.agent_middleware.enumeration.TransactionEvents;
import com.mfs.agent_middleware.exception.DecodePinException;
import com.mfs.agent_middleware.exception.TransactionProcessException;
import com.mfs.agent_middleware.gateway.ApiManagerGateway;
import com.mfs.agent_middleware.service.security.ISecurityService;
import com.mfs.agent_middleware.util.CommonConstant;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class REBServiceImpl extends ProcessRequestService implements REBService {
    @Autowired
    ApiManagerGateway apiManagerGateway;

    @Autowired
    ISecurityService iSecurityService;

    @Autowired
    TransactionProcessService transactionProcessService;
    
    @Autowired
    ApplicationProperties appProperties;
    
    @Override
    public CommonResponse payREBPrepaidBill(REBPrepaidRequest prepaidRequest, String accountNo, Locale locale) {
        if (appProperties.getCheckEnabled()) {
            if (transactionProcessService.findProcess(accountNo, prepaidRequest.getBillAmount(),prepaidRequest.getMeterNumber(), TransactionEvents.REB_PREPAID.toString())) {
                log.error("Same transaction within 10 minutes");
                throw new TransactionProcessException("User not allowed to do the same transaction within " +
                        appProperties.getDuplicateReqTimeOutInMills() / 60000 + " minutes of last transaction");
            } else {
                return doPayREBBill(prepaidRequest, accountNo, locale);
            }
        } else {
            return doPayREBBill(prepaidRequest, accountNo, locale);
        }
    }

    private CommonResponse doPayREBBill(REBPrepaidRequest prepaidRequest, String accountNo, Locale locale) {
        if (Pattern.matches("......==", prepaidRequest.getPin())) {
            String formattedText = "TRUSTMM REBPR " + prepaidRequest.getMeterNumber() + " " + prepaidRequest.getTransactionId() + " " + prepaidRequest.getBillAmount() + " " + new String(Base64.getDecoder().decode(prepaidRequest.getPin())) + " " + CommonConstant.checkNumber(prepaidRequest.getNotificationNumber());
            ApiManagerRequest apiManagerRequest = makeProcessedRequest(appProperties.getEncryptionEnabled() ?
                    iSecurityService.encrypt(formattedText, "", "") : formattedText, ContextUrl.API_MANAGER_CONTEXT.getUrl(), accountNo);

            CommonResponse response = getTransactionFormattedResponse(apiManagerGateway, apiManagerRequest, locale, true);

            try {
                transactionProcessService.storeProcess(accountNo, prepaidRequest.getBillAmount(),prepaidRequest.getMeterNumber(), TransactionEvents.REB_PREPAID.toString());
            } catch (Exception e) {
                log.error(e.getMessage());
            }
            return response;

        } else {
            throw new DecodePinException(CommonConstant.DECODE_EXCEPTION_MESSAGE);
        }
    }
    
}
