package com.mfs.agent_middleware.service;

import java.util.Locale;

import com.mfs.agent_middleware.dto.FeatureConfigList;

public interface FetchTransactionTypeService {
    public FeatureConfigList fetchTypes(Locale locale);
}
