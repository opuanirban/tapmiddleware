package com.mfs.agent_middleware.exception;

public class UserRegistrationRequestException extends RuntimeException {

    public UserRegistrationRequestException(String message) {
        super(message);
    }

    public UserRegistrationRequestException(String message, Throwable cause) {
        super(message, cause);
    }
}
