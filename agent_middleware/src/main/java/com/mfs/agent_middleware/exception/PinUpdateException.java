package com.mfs.agent_middleware.exception;

public class PinUpdateException extends RuntimeException {

    public PinUpdateException(String message) {
        super(message);
    }

    public PinUpdateException(String message, Throwable cause) {
        super(message, cause);
    }
}
