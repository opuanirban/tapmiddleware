package com.mfs.agent_middleware.exception;

public class OTPValidationRequestException extends RuntimeException {

    public OTPValidationRequestException(String message) {
        super(message);
    }

    public OTPValidationRequestException(String message, Throwable cause) {
        super(message, cause);
    }
}
