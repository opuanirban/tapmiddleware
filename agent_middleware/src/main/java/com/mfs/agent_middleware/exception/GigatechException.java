package com.mfs.agent_middleware.exception;

public class GigatechException extends RuntimeException {

    public GigatechException(String message) {
        super(message);
    }

    public GigatechException(String message, Throwable cause) {
        super(message, cause);
    }
}
