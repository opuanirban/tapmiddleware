package com.mfs.agent_middleware.exception;


import lombok.Data;

@Data
public class CustomException {
    private final String code;
    private final String message;
}
