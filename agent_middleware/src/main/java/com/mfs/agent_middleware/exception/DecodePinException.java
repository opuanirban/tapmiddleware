package com.mfs.agent_middleware.exception;

public class DecodePinException extends RuntimeException {
    public DecodePinException(String message) {
        super(message);
    }

    public DecodePinException(String message, Throwable cause) {
        super(message, cause);
    }
}
