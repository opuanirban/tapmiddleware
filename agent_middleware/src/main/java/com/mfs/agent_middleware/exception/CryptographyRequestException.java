package com.mfs.agent_middleware.exception;

public class CryptographyRequestException extends RuntimeException {

    public CryptographyRequestException(String message) {
        super(message);
    }

    public CryptographyRequestException(String message, Throwable cause) {
        super(message, cause);
    }
}
