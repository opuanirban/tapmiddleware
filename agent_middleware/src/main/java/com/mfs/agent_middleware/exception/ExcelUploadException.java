package com.mfs.agent_middleware.exception;

public class ExcelUploadException extends RuntimeException {

    public ExcelUploadException(String message) {
        super(message);
    }

    public ExcelUploadException(String message, Throwable cause) {
        super(message, cause);
    }
}
