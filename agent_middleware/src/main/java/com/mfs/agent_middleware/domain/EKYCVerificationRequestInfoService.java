package com.mfs.agent_middleware.domain;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class EKYCVerificationRequestInfoService {
    @Autowired
    EKYCVerificationRequestInfoRepository ekycVerificationRequestInfoRepository;
    
    @Async("threadPoolTaskExecutor")
    public void saveEkycRequestInfo(EKYCVerificationRequestInfo ekycVerificationRequestInfo) {
        log.debug("async ekyc verification request save: {}", ekycVerificationRequestInfo);
        try {
            ekycVerificationRequestInfoRepository.save(ekycVerificationRequestInfo);
        } catch (Exception e) {
            log.error("ekyc request data save error in mongoDB");
        }
    }
}
