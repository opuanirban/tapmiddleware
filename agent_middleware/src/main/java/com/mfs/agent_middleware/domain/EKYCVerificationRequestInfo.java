package com.mfs.agent_middleware.domain;
import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Data
@Document(collection = "Ekyc-Verification-Request-Agent")
public class EKYCVerificationRequestInfo {
    @Id
    private String id;

    private String nidNo;
    private String dob;
    private String applicantNameBn;
    private String applicantNameEn;
    private String fatherName;
    private String motherName;
    private String spouseName;
    private String presAddress;
    private String permAddress;
    private String idFrontName;
    private String idBackName;
    private String gender;
    private String profession;
    private String nominee;
    private String nomineeRelation;
    private String mobileNumber;
    private Date requestDate;
}
