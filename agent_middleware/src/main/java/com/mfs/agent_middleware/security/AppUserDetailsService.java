package com.mfs.agent_middleware.security;

import java.util.ArrayList;

import com.mfs.agent_middleware.gateway.ApiManagerGateway;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class AppUserDetailsService implements UserDetailsService {

    @Autowired
    ApiManagerGateway apiManagerGateway;


    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        String pin = apiManagerGateway.userRefresh(s);
        return new User(s, pin, new ArrayList<>());
    }

}