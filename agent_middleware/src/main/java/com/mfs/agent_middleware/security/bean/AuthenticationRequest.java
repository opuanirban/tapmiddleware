package com.mfs.agent_middleware.security.bean;

import lombok.Data;

@Data
public class AuthenticationRequest {
    private String msisdn;
    private String pin;
}