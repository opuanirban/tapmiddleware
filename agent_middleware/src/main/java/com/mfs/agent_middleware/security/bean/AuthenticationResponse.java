package com.mfs.agent_middleware.security.bean;

import com.mfs.agent_middleware.dto.UserLoginInfo;

import lombok.Data;

@Data
public class AuthenticationResponse {

    private String jwt;
    private Integer timeout;
    private UserLoginInfo userLoginInfo;

}
