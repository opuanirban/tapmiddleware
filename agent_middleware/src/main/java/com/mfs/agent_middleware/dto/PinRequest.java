package com.mfs.agent_middleware.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class PinRequest {
    @NotNull
    private String pin;
}
