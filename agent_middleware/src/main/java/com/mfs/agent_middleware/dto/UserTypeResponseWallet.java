package com.mfs.agent_middleware.dto;

import lombok.Data;

@Data
public class UserTypeResponseWallet {
    private String userType;
    private String userName;
}
