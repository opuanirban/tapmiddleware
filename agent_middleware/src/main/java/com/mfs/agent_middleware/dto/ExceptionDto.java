package com.mfs.agent_middleware.dto;

import lombok.Data;

@Data
public class ExceptionDto {
    private String error_code;
    private String error_description;
}
