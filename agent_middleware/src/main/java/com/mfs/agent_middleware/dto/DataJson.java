package com.mfs.agent_middleware.dto;

import lombok.Data;

@Data
public class DataJson {
    private String Id;
    private String Code;
    private String Description;
    private String IconSource;
    private String TargetType;
    private String LabelView;
    private String DefaultNoOfMonth;
    private String TermsAndConditions;
}
