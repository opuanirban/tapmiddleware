package com.mfs.agent_middleware.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
public class DescoPostPaidRequest {
    @NotNull
    @NotEmpty
    private String billNumber;
    @NotNull
    @NotEmpty
    @Pattern(regexp="[0-9]+")
    private String notificationNumber;
    @NotNull
    @NotEmpty
    private String pin;


}
