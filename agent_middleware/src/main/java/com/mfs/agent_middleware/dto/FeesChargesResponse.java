package com.mfs.agent_middleware.dto;

import lombok.Data;

@Data
public class FeesChargesResponse {
    private String ChargeAmount;
}
