package com.mfs.agent_middleware.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class NescoBillInfo {
    @NotNull
    @NotEmpty
    private String accountNo;
    private String billMonth;
    private String billYear;
}
