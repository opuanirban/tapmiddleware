package com.mfs.agent_middleware.dto;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.math.BigDecimal;

import lombok.Data;

@Data
public class CashInRequest {
    @NotNull
    @NotEmpty
    @Pattern(regexp="[0-9]+")
    private String recipientNumber;
    @NotNull
    @DecimalMin(value = "0.0", inclusive = false)
    private BigDecimal amount;
    @NotNull
    @NotEmpty
    private String pin;
}
