package com.mfs.agent_middleware.dto;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import com.mfs.agent_middleware.enumeration.ConnectionTypeEnum;

import lombok.Data;

@Data
public class TopUpRequest {
    @NotNull
    @NotEmpty
    @Pattern(regexp="[0-9]+")
    private String recipientNumber;
    @NotNull
//    @Pattern(regexp = "[\\s]*[0-9]*[1-9]+",message="msg")
    @Min(value = 1)
    private int amount;
    @NotNull
    private ConnectionTypeEnum connectionType;
    @NotNull
    @NotEmpty
    private String operator;
    @NotNull
    @NotEmpty
    private String pin;

}
