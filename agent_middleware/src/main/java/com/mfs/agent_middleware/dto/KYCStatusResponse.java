package com.mfs.agent_middleware.dto;

import lombok.Data;

@Data
public class KYCStatusResponse {
    Boolean ekyc_status;

}
