package com.mfs.agent_middleware.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class TopUpRequest2 {
    @JsonProperty("OperatorId")
    private int operatorId;
    @JsonProperty("RecipientMsisdn")
    private String recipientMsisdn;
    @JsonProperty("Amount")
    private int amount;
    @JsonProperty("ConnectionType")
    private String connectionType;
    @JsonProperty("FromAccount")
    private String fromAccount;
    @JsonProperty("Pin")
    private String pin;
}
