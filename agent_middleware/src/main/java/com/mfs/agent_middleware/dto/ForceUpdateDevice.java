package com.mfs.agent_middleware.dto;

import lombok.Data;

@Data
public class ForceUpdateDevice {
    private String forceUpdateVersion;

    private String blacklistVersion;

    private String message;

    private String link;
}
