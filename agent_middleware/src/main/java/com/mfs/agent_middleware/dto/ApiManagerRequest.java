package com.mfs.agent_middleware.dto;

import lombok.Data;

@Data
public class ApiManagerRequest {
    private Integer msgId;
    private String userNumber;
    private String smsText;
    private Integer telcoId;
    private String shortCode;
    private String encKey;
    private String context;
}
