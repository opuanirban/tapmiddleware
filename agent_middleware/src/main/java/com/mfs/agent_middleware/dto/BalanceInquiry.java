package com.mfs.agent_middleware.dto;

import lombok.Data;

@Data
public class BalanceInquiry {
    private String currentBalance;
}
