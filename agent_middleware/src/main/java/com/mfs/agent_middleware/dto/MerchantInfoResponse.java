package com.mfs.agent_middleware.dto;

import lombok.Data;

@Data
public class MerchantInfoResponse {

    private long AccountId;

    private long AccountNumber;

    private String AccountName;

    private String AccountType;
}
