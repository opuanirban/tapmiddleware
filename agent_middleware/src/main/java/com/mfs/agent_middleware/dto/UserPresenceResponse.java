package com.mfs.agent_middleware.dto;

import lombok.Data;

@Data
public class UserPresenceResponse {
    private String userStatus;
    private String verificationStatus;
}
