package com.mfs.agent_middleware.dto;

import lombok.Data;

@Data
public class ForceUpdateResponse {
    private String forceUpdateVersion;

    private String blacklistVersion;

    private String message;

    private String link;

}
