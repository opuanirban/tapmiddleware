package com.mfs.agent_middleware.dto;

import java.math.BigDecimal;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import lombok.Data;

@Data
public class NescoPrepaidRequest {
    private String customerNumber;
    private String meterNumber;
    @NotNull
    @NotEmpty
    private String transactionId;
    @NotNull
    @NotEmpty
    @Pattern(regexp="[0-9]+")
    private String notificationNumber;
    @NotNull
    @DecimalMin(value = "0.0", inclusive = false)
    private BigDecimal billAmount;
    @NotNull
    @NotEmpty
    private String pin;
}
