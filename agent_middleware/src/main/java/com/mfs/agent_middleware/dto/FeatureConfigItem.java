package com.mfs.agent_middleware.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class FeatureConfigItem {
    private String featureName;
    private String code;
    private BigDecimal minAmount;
    private BigDecimal maxAmount;
}
