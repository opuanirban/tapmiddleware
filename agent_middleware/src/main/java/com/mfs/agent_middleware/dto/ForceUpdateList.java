package com.mfs.agent_middleware.dto;

import lombok.Data;

@Data
public class ForceUpdateList {
    private ForceUpdateDevice android;
    private ForceUpdateDevice iOS;
    private ForceUpdateDevice huawei;
}
