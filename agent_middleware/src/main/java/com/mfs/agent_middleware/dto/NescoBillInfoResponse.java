package com.mfs.agent_middleware.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class NescoBillInfoResponse extends BillInfoResponse {
    private String customerName;
    private String dueDate;
    private String transactionId;
}
