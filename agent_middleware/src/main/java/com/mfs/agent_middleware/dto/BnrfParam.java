package com.mfs.agent_middleware.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class BnrfParam {
    @NotNull
    private Integer requirementType;
    @NotEmpty
    @NotNull
    private String pin;
    @NotEmpty
    @NotNull
    private String notificationNumber;
}
