package com.mfs.agent_middleware.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class FeatureHidePermissionRequest {
    @NotNull
    @NotEmpty
    private String hiddenFeatureVersion;

    @NotNull
    @NotEmpty
    private boolean featureIsHidden;

    @NotNull
    @NotEmpty
    private String requestAuthKey;

}
