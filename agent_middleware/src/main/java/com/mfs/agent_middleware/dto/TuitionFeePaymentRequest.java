package com.mfs.agent_middleware.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class TuitionFeePaymentRequest {

    @NotNull
    @NotEmpty
    private String regNo;
    @NotNull
    @NotEmpty
    private String numberOfMonth;
    @NotNull
    @NotEmpty
    private String shortName;
    @NotNull
    @NotEmpty
    private String Pin;
    @NotNull
    @NotEmpty
    private String institutionCode;
}
