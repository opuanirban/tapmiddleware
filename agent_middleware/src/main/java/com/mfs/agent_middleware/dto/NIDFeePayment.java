package com.mfs.agent_middleware.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
public class NIDFeePayment {
    @NotNull
    @NotEmpty
    @Pattern(regexp="[0-9]+")
    private String nidNo;
    @NotNull
    @NotEmpty
    private String correctionType;
    @NotNull
    @NotEmpty
    private String serviceType;
    @NotNull
    @NotEmpty
    private String pin;
    @NotNull
    @NotEmpty
    @Pattern(regexp="[0-9]+")
    private String notificationNumber;
}
