package com.mfs.agent_middleware.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
public class DPDCBillPayRequest extends DPDCBillInfo{
    @NotEmpty
    @NotNull
    private String pin;
    @NotEmpty
    @NotNull
    @Pattern(regexp="[0-9]+")
    private String notificationNumber;
}
