package com.mfs.agent_middleware.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
public class GenerateOTPRequest {
    @NotNull
    @NotEmpty
    private String serviceName;
    @NotNull
    @NotEmpty
    @Pattern(regexp="[0-9]+")
    private String receiverInfo;
    @NotNull
    @NotEmpty
    private String deviceType;

}
