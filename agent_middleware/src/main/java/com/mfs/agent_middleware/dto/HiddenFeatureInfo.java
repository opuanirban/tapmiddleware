package com.mfs.agent_middleware.dto;

import lombok.Data;

@Data
public class HiddenFeatureInfo {
    private String hiddenFeatureVersion;
    private boolean hiddenFeatureStatus;
}
