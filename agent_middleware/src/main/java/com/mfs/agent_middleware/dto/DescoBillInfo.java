package com.mfs.agent_middleware.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class DescoBillInfo {

    @NotNull
    private String billNo;
}
