package com.mfs.agent_middleware.dto;

import lombok.Data;

@Data
public class DonationMerchantListRequest {
    private String donationMerchantList;
}
