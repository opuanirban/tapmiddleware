package com.mfs.agent_middleware.dto;

import lombok.Data;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.math.BigDecimal;

@Data
public class DescoPrepaidRequest {
    @NotNull
    @NotEmpty
    private String meterNumber;
    @NotNull
    @NotEmpty
    @Pattern(regexp="[0-9]+")
    private String notificationNumber;
    @NotNull
    @DecimalMin(value = "0.0", inclusive = false)
    private BigDecimal billAmount;
    @NotNull
    @NotEmpty
    private String pin;

}
