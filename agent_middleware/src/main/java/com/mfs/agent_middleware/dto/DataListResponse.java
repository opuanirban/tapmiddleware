package com.mfs.agent_middleware.dto;

import lombok.Data;

@Data
public class DataListResponse {
    private Integer categoryId;
    private String categoryTitle;
    private String categoryIconSource;
    private DataJson[] itemList;
}
