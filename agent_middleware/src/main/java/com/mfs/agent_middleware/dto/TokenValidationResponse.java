package com.mfs.agent_middleware.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class TokenValidationResponse {
    private Boolean isValidated;
}
