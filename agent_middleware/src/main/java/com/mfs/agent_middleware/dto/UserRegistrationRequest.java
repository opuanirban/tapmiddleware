package com.mfs.agent_middleware.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import com.mfs.agent_middleware.enumeration.GenderEnum;
import com.mfs.agent_middleware.enumeration.MobileOperatorEnum;
import com.mfs.agent_middleware.enumeration.UserGroupEnum;

import lombok.Data;

@Data
public class UserRegistrationRequest {
    @NotNull
    @NotEmpty
    @Pattern(regexp="[0-9]+")
    private String mobileNumber;
    @NotNull
    @NotEmpty
    private String pin;
//    @NotNull
//    @NotEmpty
    private String nid;
    @NotNull
    @NotEmpty
    private String firstName;
//    @NotNull
//    @NotEmpty
    private String lastName;
    @NotNull
    private GenderEnum gender;
    @NotNull
    private UserGroupEnum accountType;
//    @NotNull
//    @NotEmpty
    private String dob;
    @NotNull
    @NotEmpty
    private String occupation;
    @NotNull
    private MobileOperatorEnum mno;

}
