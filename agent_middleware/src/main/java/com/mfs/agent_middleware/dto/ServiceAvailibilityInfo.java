package com.mfs.agent_middleware.dto;

import lombok.Data;

@Data
public class ServiceAvailibilityInfo {
    private Boolean isServiceAvailable;
    private String notificationMessageBn;
    private String notificationMessageEn;
}
