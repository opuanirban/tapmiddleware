package com.mfs.agent_middleware.dto;

import lombok.Data;

@Data
public class UserStatusInfo {
    private boolean userStatus;
}
