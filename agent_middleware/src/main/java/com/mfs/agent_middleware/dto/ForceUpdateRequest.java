package com.mfs.agent_middleware.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class ForceUpdateRequest {
    @NotNull
    @NotEmpty
    private String platform;
    @NotNull
    @NotEmpty
    private String forceUpdateVersion;

    private String blacklistVersion;

    private String requestAuthKey;

    private String message;

    private String link;
}
