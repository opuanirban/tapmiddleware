package com.mfs.agent_middleware.dto;

import lombok.Data;

@Data
public class GenerateOTPResponse extends BaseResponse {
    private String otpReferenceId;
}
