package com.mfs.agent_middleware.dto;

import lombok.Data;

@Data
public class TransactionTypeParams {
    private String TransactionTypeCode;
    private String TransactionType;
    private String UsedKeyword;
}
