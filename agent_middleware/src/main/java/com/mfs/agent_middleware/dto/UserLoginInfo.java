package com.mfs.agent_middleware.dto;

import lombok.Data;

@Data
public class UserLoginInfo {
    private String userName;
    private String balance;
    private String userStatus;
}
