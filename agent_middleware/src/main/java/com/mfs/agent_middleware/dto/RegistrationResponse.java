package com.mfs.agent_middleware.dto;

import lombok.Data;

@Data
public class RegistrationResponse {
    private String message;
    private String ekyc_status;
    private String ekyc_status_message;
}
