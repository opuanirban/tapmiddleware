package com.mfs.agent_middleware.dto;

import lombok.Data;

@Data
public class ServiceConfigResponse {
    private ServerConfig serverConfig;
    private ClientConfigs clientConfig;

}
